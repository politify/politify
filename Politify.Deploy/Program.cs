﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Politify.Code;
using Politify.Code.BusinessLayer;
using Politify.Models;

namespace Politify.Deploy
{
    public class Program
    {
        static string filePathConfigSetting = "dataDir";

        static void Main(string[] args)
        {
            PolitifyDbContext db = new PolitifyDbContext();

            var dataDir = ConfigurationManager.AppSettings[filePathConfigSetting];

            if (String.IsNullOrEmpty(dataDir))
            {
                throw new ArgumentException("Configuration setting 'dataDir' not set.");
            }

            Console.WriteLine("Writing to db '{0}'", db.Database.Connection.Database);
                
            db.Database.CommandTimeout = 72000;
            db.Database.Log = (s) => Console.WriteLine(s);

            foreach (string dir in Directory.GetFiles(dataDir))
            {
                db.Database.ExecuteSqlCommand(File.ReadAllText(dir));
                Console.WriteLine("done sqling file '{0}'", dir);
            }
        }
    }
}
