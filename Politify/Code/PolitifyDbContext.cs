﻿using Microsoft.AspNet.Identity.EntityFramework;
using Politify.Code.BusinessLayer;
using Politify.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Politify.Code
{
    public class PolitifyDbContext : DbContext
    {
        public DbSet<Organisation> Organisations { get; set; }

        public DbSet<Discussion> Discussions { get; set; }
        
        public DbSet<SignatureCount> SignatureCounts { get; set; }

        public DbSet<Postcode> Postcodes { get; set; }

        public DbSet<Country> Countries { get; set; }
        
        public DbSet<Vote> Votes { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Category> Categories { get; set; }
        
        public DbSet<Section> Sections { get; set; }

        public DbSet<SectionEdit> SectionEdits { get; set; }

        public DbSet<SectionEvidence> SectionEvidences { get; set; }

        public DbSet<SectionExplanation> SectionExplanations { get; set; }

        public DbSet<SectionVote> SectionVotes { get; set; }

        public DbSet<SectionComment> SectionComments { get; set; }

        public DbSet<Staff> Staff { get; set; }

        public DbSet<Party> Parties { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public DbSet<OrganisationVM> OrganisationVMs { get; set; }

        public DbSet<DiscussionVM> DiscussionVMs { get; set; }        

        public DbSet<OrganisationCategoryVM> OrganisationCategoryVMs { get; set; }

        public DbSet<SectionVM> SectionVMs { get; set; }

        public DbSet<SectionEditVM> SectionEditVMs { get; set; }

        public DbSet<SectionExplanationVM> SectionExplanationVMs { get; set; }

        public DbSet<SectionEvidenceVM> SectionEvidenceVMs { get; set; }

        public DbSet<SectionCommentVM> SectionCommentVMs { get; set; }

        public PolitifyDbContext()
            : base("PolitifyConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUserLogin>().HasKey(l => l.UserId);
            modelBuilder.Entity<IdentityRole>().HasKey(r => r.Id);
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
            
            modelBuilder.Entity<Organisation>()
                .HasRequired<Country>(g => g.Country)
                .WithMany(o => o.Organisations)
                .Map(x => x.MapKey("CountryID"));

            modelBuilder.Entity<Country>().HasMany<Organisation>(c => c.Organisations);

            modelBuilder.Entity<Section>()
            .HasOptional(entity => entity.Parent)
                .WithMany(parent => parent.Children);

            //modelBuilder.Entity<Section>()
            //    .HasOptional(s => s.Discussion);
            //
            //modelBuilder.Entity<Discussion>()
            //    .HasRequired(d => d.Section)
            //    .WithRequiredPrincipal();

            modelBuilder.Entity<Organisation>()
            .HasMany<Organisation>(entity => entity.Parents)
                .WithMany(parent => parent.Children);

            modelBuilder.Entity<Staff>().ToTable("dbo.Staff");

            modelBuilder.Entity<OrganisationVM>()
                .HasMany<OrganisationCategoryVM>(o => o.Categories)
                .WithRequired(oc => oc.Organisation);

            modelBuilder.Entity<OrganisationVM>()
                .HasMany<OrganisationVM>(o => o.Children)
                .WithMany(o => o.Parents).Map( x => {
                    x.MapLeftKey("Organisation_OrganisationId");
                    x.MapRightKey("Organisation_OrganisationId1");
                    x.ToTable("OrganisationOrganisations");
                });
                
        }

        public static PolitifyDbContext Create()
        {
            return new PolitifyDbContext();
        }
    }
}