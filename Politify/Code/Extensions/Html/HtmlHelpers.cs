﻿using System.Web;
using System.Web.Mvc;
using Politify.Code.Enums;

namespace Politify.Code.Extensions.Html
{
  public static class HtmlHelpers
  {
    public static MvcHtmlString FileDownloadLink(this HtmlHelper helper, string filePath, string text, FileType fileType)
    {
      return new MvcHtmlString(string.Format("<a href=\"{0}\" class=\"fileDownload fileDownload{2}\">{1}</a>", (object) VirtualPathUtility.ToAbsolute(filePath), (object) text, (object) fileType));
    }
  }
}
