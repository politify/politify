﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Politify.Code.Extensions
{
    public static class StringExtensions
    {
        public static String Middle(this String str, string firstSnippet, string secondSnippet)
        {
            if (str == null) return null;

            int indexOfFirstSnippet = str.IndexOf(firstSnippet);
            if (indexOfFirstSnippet == -1) return null;

            string tmp = str.Substring(indexOfFirstSnippet + firstSnippet.Length);

            int indexOfSecondSnippet = tmp.IndexOf(secondSnippet);

            if (indexOfSecondSnippet == -1) indexOfSecondSnippet = str.Length - 1;

            return str.Middle(indexOfFirstSnippet + firstSnippet.Length, indexOfSecondSnippet);
        }

        public static String Middle(this String str, int startIndex, int endIndex)
        {
            if (startIndex < 0 || endIndex > str.Length - 1 || endIndex == 0) return null;
            return str.Substring(startIndex, endIndex + 1 - startIndex);
        }
    }
}