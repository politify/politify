﻿using Politify.Code.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Politify.Models;
using System.Linq.Expressions;

namespace Politify.Code.LogicLayer
{
    public class GenericLogic : IDisposable
    {
        protected PolitifyDbContext dbContext;

        public GenericLogic()
        {
            dbContext = new PolitifyDbContext();
        }

        public GenericLogic(PolitifyDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public virtual void Add<O, I>(I inputModel)
            where O : class, IAddable, new()
            where I : class, IInputModel<O>
        {
            if ((object)inputModel == null)
                throw new ArgumentException("Cannot add a null entity.");
            O instance = Activator.CreateInstance<O>();
            instance.Add<I>(inputModel);
            this.dbContext.Set<O>().Add(instance);
            this.dbContext.SaveChanges();
        }

        public virtual void AddUser<O, I>(I inputModel)
            where O : class, IAddable, IUser, new()
            where I : class, IInputModel<O>
        {
            if ((object)inputModel == null)
                throw new ArgumentException("Cannot add a null entity.");
            O instance = Activator.CreateInstance<O>();
            instance.Add<I>(inputModel);
            instance.User = this.dbContext.Set<ApplicationUser>().SingleOrDefault(u => u.UserName == HttpContext.Current.User.Identity.Name);
            this.dbContext.Set<O>().Add(instance);
            this.dbContext.SaveChanges();
        }

        public virtual T Find<T>(object[] keyValues) where T : class
        {
            return this.dbContext.Set<T>().Find(keyValues);
        }

        public virtual T FindSingle<T>(Expression<Func<T, bool>> func, string includeObjects = null) where T : class
        {
            if (includeObjects == null) return this.dbContext.Set<T>().Single(func);
            return this.dbContext.Set<T>().Include(includeObjects).Single(func);
        }

        public virtual T FindSingle<T, TProp>(Expression<Func<T, bool>> func, Expression<Func<T, TProp>> funcInclude = null) where T : class
        {
            return this.dbContext.Set<T>().Include(funcInclude).Single(func);
        }

        public virtual T FindSingleOrDefault<T>(Expression<Func<T, bool>> func, string includeObjects = null) where T : class
        {
            if (includeObjects == null) return this.dbContext.Set<T>().SingleOrDefault(func);
            return this.dbContext.Set<T>().Include(includeObjects).SingleOrDefault(func);
        }

        public virtual T FindSingleOrDefault<T, TProp>(Expression<Func<T, bool>> func, Expression<Func<T, TProp>> funcInclude = null) where T : class
        {
            return this.dbContext.Set<T>().Include(funcInclude).SingleOrDefault(func);
        }

        public virtual void Edit<O, I>(I inputModel)
            where O : class, IEditable
            where I : IDataKey, IInputModel<O>
        {
            if ((object)inputModel == null)
                throw new ArgumentException("Cannot edit a null entity.");
            O entity = this.dbContext.Set<O>().Find(inputModel.GetKey());
            if ((object)entity == null)
                return;
            entity.Edit<I>(inputModel);
            this.dbContext.Entry<O>(entity).CurrentValues.SetValues((object)entity);
            this.dbContext.SaveChanges();
        }

        public virtual void Remove<T>(T obj) where T : class
        {
            this.dbContext.Set<T>().Remove(obj);
            this.dbContext.SaveChanges();
        }

        public virtual void Remove<T>(object[] keyValues) where T : class
        {
            this.Remove<T>(this.dbContext.Set<T>().Find(keyValues));
        }

        public virtual DbSet<T> List<T>() where T : class
        {
            return this.dbContext.Set<T>();
        }

        public virtual IList<T> List<T>(Expression<Func<T,bool>> func, string includeObjects = null) where T : class
        {
            return this.dbContext.Set<T>().Where(func).Include(includeObjects).ToList();
        }

        public void Dispose()
        {
            if (dbContext != null) this.dbContext.Dispose();
        }
    }
}