﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Politify.Code.LogicLayer
{
    public class UserLogic : GenericLogic
    {
        public bool ValidatePostcode(string postcode)
        {
            var postcodeTmp = postcode.Replace(" ", String.Empty).ToUpper();
            return dbContext.Postcodes.SingleOrDefault(p => p.PostCode == postcodeTmp) != null;
        }
    }
}