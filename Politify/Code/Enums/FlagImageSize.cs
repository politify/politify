﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Politify.Code.Enums
{
    public enum FlagImageSize
    {
        Size16 = 16,
        Size24 = 24,
        Size32 = 32,
        Size48 = 48
    }
}