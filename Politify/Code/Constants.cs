﻿using System.Reflection;
using System.Configuration;
using Politify.Code.Enums;

namespace Politify.Code
{
    /// <summary>
    /// Houses constants for the application. These can also be set at runtime by adding to the web.config.
    /// </summary>
    public static class Constants
    {
        static Constants()
        {
            foreach (var prop in typeof(Constants).GetFields())
            {
                if (ConfigurationManager.AppSettings[prop.Name] != null)
                {
                    var field = typeof(Constants).GetField(prop.Name);
                    //todo: Implement for different type constants, string supported only currently.
                    //var value = (field.FieldType.GetType())ConfigurationManager.AppSettings[prop.Name];
                    field.SetValue(null, (string)ConfigurationManager.AppSettings[prop.Name]);
                }
            }
        }

        public const string ReturnUrl = "returnUrl";
        public const string LawPdfFilePath = "~/Content/Laws/";
        public static string BaseFlagFilePath = "~/Content/Images/Flags/";
        public static string FlagFilePath16() { return BaseFlagFilePath + "16/"; }
        public static string FlagFilePath24() { return BaseFlagFilePath + "24/"; }
        public static string FlagFilePath32() { return BaseFlagFilePath + "32/"; }
        public static string FlagFilePath48() { return BaseFlagFilePath + "48/"; }
        public static string GetFlagFilePath(string countryName, FlagImageSize size = FlagImageSize.Size16)
        {
            return string.Format("{0}{1}/{2}.png", BaseFlagFilePath, (int)size, countryName);
        }

        //Foreign keys
        public const string GovernmentCountryForeignKeyName = "CountryID";
    }
}
