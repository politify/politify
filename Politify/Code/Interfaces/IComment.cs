﻿using Politify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Politify.Code.Interfaces
{
    public interface IComment : IUser
    {
        int CommentID { get; set; }
        string Text { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        bool Edited { get; set; }
        DateTimeOffset? LastEdit { get; set; }
        string LastText { get; set; }
        string OriginalText { get; set; }
    }

    public interface ICommentInputModel<out T> : IInputModel<T>, IDataKey
    {
        int CommentID { get; set; }
        string Text { get; set; }
    }
}