﻿using Politify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Politify.Code.Interfaces
{
    public interface IVote : IUser
    {
        int VoteID { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        bool IsPositive { get; set; }
    }

    public interface IVoteInputModel<out T> : IInputModel<T>
    {
        int VoteID { get; set; }

        bool IsPositive { get; set; }
    }
}
