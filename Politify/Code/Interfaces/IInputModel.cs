﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Politify.Code.Interfaces
{
    public interface IInputModel<out T>
    {
        T ModelType { get; }
    }
}
