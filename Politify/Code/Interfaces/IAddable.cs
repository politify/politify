﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Politify.Code.Interfaces
{
    public interface IAddable
    {
        void Add<T>(T inputModel);
    }
}
