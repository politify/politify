﻿using Politify.Code;
using Politify.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using Microsoft.AspNet.Identity;

namespace Politify.Controllers.OData
{
    public class SectionCommentVMController : GenericApiController
    {
        // GET: odata/SectionCommentApi
        [EnableQuery]
        public IQueryable<SectionCommentVM> Get()
        {
            return db.SectionCommentVMs;
        }

        // GET: odata/SectionCommentApi(5)
        [EnableQuery]
        public SingleResult<SectionCommentVM> Get([FromODataUri] int key)
        {
            return SingleResult.Create(db.SectionCommentVMs.Where(sectionComment => sectionComment.CommentID == key));
        }
    }
}
