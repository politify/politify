﻿using Politify.Models;
using System.Linq;
using System.Web.Http;
using System.Web.OData.Query;
using System.Web.OData;

namespace Politify.Controllers.OData
{
    public class SectionVMController : GenericApiController
    {
        // GET: odata/SectionsApi
        [EnableQuery()]
        public IQueryable<SectionVM> Get()
        {
            return db.SectionVMs;
        }

        // GET: odata/SectionsApi(5)
        [EnableQuery(MaxExpansionDepth= 5, AllowedQueryOptions = AllowedQueryOptions.All)]
        public SingleResult<SectionVM> Get([FromODataUri] int key)
        {
            return SingleResult.Create(db.SectionVMs.Where(section => section.SectionID == key));
        }
        
        //// POST: odata/SectionsApi
        //[Authorize]
        //public async Task<IHttpActionResult> Post(Section section)
        //{
        //    var userId = User.Identity.GetUserId();
        //    if (String.IsNullOrEmpty(userId)) return NotFound();
        //
        //    section.User = await db.Set<ApplicationUser>().SingleOrDefaultAsync(u => u.Id == userId);
        //    section.IsUserCreated = true;
        //
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //
        //    db.Sections.Add(section);
        //    await db.SaveChangesAsync();
        //
        //    return Created(section);
        //}
        //
        //// PATCH: odata/SectionsApi(5)
        //[AcceptVerbs("PATCH", "MERGE")]
        //[Authorize]
        //public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<Section> patch)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //
        //    Section section = await db.Sections.FindAsync(key);
        //    if (section == null)
        //    {
        //        return NotFound();
        //    }
        //    else if(!section.IsUserCreated){
        //        return StatusCode(HttpStatusCode.Forbidden);
        //    }
        //    else if (section.User.Id != User.Identity.GetUserId())
        //        return StatusCode(HttpStatusCode.Forbidden);
        //
        //    patch.TrySetPropertyValue("IsUserCreated", true);
        //    patch.Patch(section);
        //
        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SectionExists(key))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //
        //    return Updated(section);
        //}
        //
        //[Authorize]
        //// DELETE: odata/SectionsApi(5)
        //public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        //{
        //    Section section = await db.Sections.FindAsync(key);
        //    if (section == null)
        //    {
        //        return NotFound();
        //    }
        //
        //    if (!section.IsUserCreated || section.User.Id != User.Identity.GetUserId()) return StatusCode(HttpStatusCode.Forbidden);
        //
        //    db.Sections.Remove(section);
        //    await db.SaveChangesAsync();
        //
        //    return StatusCode(HttpStatusCode.NoContent);
        //}
                
        // GET: odata/SectionsApi(5)/Parent
    }
}
