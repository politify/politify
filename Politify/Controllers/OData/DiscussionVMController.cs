﻿using Politify.Models;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace Politify.Controllers.OData
{
    public class DiscussionVMController : GenericApiController
    {
        // GET: odata/DiscussionVM
        [EnableQuery]
        public IQueryable<DiscussionVM> GetDiscussionVM()
        {
            return db.DiscussionVMs;
        }

        // GET: odata/DiscussionVM(5)
        [EnableQuery(MaxExpansionDepth = 7)]
        public SingleResult<DiscussionVM> GetDiscussionVM([FromODataUri] int key)
        {
            return SingleResult.Create(db.DiscussionVMs.Where(discussionVM => discussionVM.DiscussionId == key));
        }
    }
}
