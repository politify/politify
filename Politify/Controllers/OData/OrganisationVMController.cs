﻿using Politify.Models;
using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Query;
using System.Web.OData.Routing;

namespace Politify.Controllers.OData
{
    public class OrganisationVMController : GenericApiController
    {
        // GET: odata/OrganisationVM
        [EnableQuery]
        public IQueryable<OrganisationVM> Get()
        {
            return db.OrganisationVMs;
        }

        // GET: odata/OrganisationVM(5)
        [EnableQuery(MaxExpansionDepth = 5, AllowedQueryOptions = AllowedQueryOptions.All)]
        public SingleResult<OrganisationVM> Get([FromODataUri] int key)
        {
            return SingleResult.Create(db.OrganisationVMs.Where(organisationVM => organisationVM.OrganisationId == key));
        }

        [HttpGet]
        [ODataRoute("UserLocationOrgs(userId={userId})")]
        [EnableQuery(MaxExpansionDepth = 5)]
        public IHttpActionResult UserLocationOrgs([FromODataUri] string userId)//todo: remove hardcoded government Id
        {
            if (String.IsNullOrEmpty(userId))
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (CurrentUser != null && CurrentUser.Postcode != null)
                        return Ok(db.OrganisationVMs.Where(o => o.OnsCode != null && (o.OnsCode == CurrentUser.Postcode.Osward || o.OnsCode == CurrentUser.Postcode.Oscty || o.OnsCode == CurrentUser.Postcode.Oslaua
                            || o.OnsCode == CurrentUser.Postcode.Parish || o.OnsCode == CurrentUser.Postcode.Pcon) || o.OrganisationId == 2));
                }
            }
            else
            {
                ApplicationUser user = db.ApplicationUsers.Include("Postcode").Where(u => u.Id == userId).Single();

                if (user != null && user.Postcode != null)
                    return Ok(db.OrganisationVMs.Where(o => o.OnsCode != null && ( o.OnsCode == user.Postcode.Osward || o.OnsCode == user.Postcode.Oscty || o.OnsCode == user.Postcode.Oslaua ||
                        o.OnsCode == user.Postcode.Parish || o.OnsCode == user.Postcode.Pcon) || o.OrganisationId == 2));
            }

            return NotFound();
        }

        [HttpGet]
        [ODataRoute("UserWorkLocationOrgs(userId={userId})")]
        [EnableQuery(MaxExpansionDepth = 5)]
        public IHttpActionResult UserWorkLocationOrgs([FromODataUri] string userId)
        {
            if (String.IsNullOrEmpty(userId))
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (CurrentUser != null && CurrentUser.WorkPostcode != null)
                    {
                        return Ok(db.OrganisationVMs.Where(o => o.OnsCode != null && (o.OnsCode == CurrentUser.WorkPostcode.Osward || o.OnsCode == CurrentUser.WorkPostcode.Oscty ||
                            o.OnsCode == CurrentUser.WorkPostcode.Oslaua || o.OnsCode == CurrentUser.WorkPostcode.Parish || o.OnsCode == CurrentUser.WorkPostcode.Pcon) || o.OrganisationId == 2));
                    }
                }
            }
            else
            {
                ApplicationUser user = db.ApplicationUsers.Include("WorkPostcode").Where(u => u.Id == userId).Single();

                if (user != null && user.WorkPostcode != null)
                    return Ok(db.OrganisationVMs.Where(o =>  o.OnsCode != null && (o.OnsCode == user.WorkPostcode.Osward || o.OnsCode == user.WorkPostcode.Oscty 
                        || o.OnsCode == user.WorkPostcode.Oslaua || o.OnsCode == user.WorkPostcode.Parish || o.OnsCode == user.WorkPostcode.Pcon ) || o.OrganisationId == 2));
            }

            return NotFound();
        }
    }
}
