﻿using Politify.Models;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace Politify.Controllers.OData
{
    public class CountryVMController : GenericApiController
    {
        // GET: odata/Country
        [EnableQuery]
        public IQueryable<CountryVM> Get()
        {
            var a = db.Countries.ToList().Select<Country, CountryVM>(c => new CountryVM(c)).AsQueryable();
            return a;
        }

        // GET: odata/Country(5)
        [EnableQuery]
        public SingleResult<CountryVM> Get([FromODataUri] int key)
        {
            return SingleResult.Create(db.Countries.Select<Country, CountryVM>(c => new CountryVM(c)).Where(c => c.CountryID == key));
        }
    }
}
