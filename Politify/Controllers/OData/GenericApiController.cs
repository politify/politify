﻿using System.Data.SqlClient;
using Politify.Code;
using Politify.Models;
using System.Web.OData;
using System.Web.Mvc;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Linq;

namespace Politify.Controllers.OData
{
    public abstract class GenericApiController : ODataController
    {
        protected PolitifyDbContext db = new PolitifyDbContext();

        protected ApplicationUser CurrentUser
        {
            get
            {
                try
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        string userId = User.Identity.GetUserId();
                        return db.ApplicationUsers.AsNoTracking().Single(u => u.Id == userId);
                    }

                    return null;
                }
                catch
                {
                    return null;
                }
            }
        }

        protected bool UserCanAddToOrg(int orgId)
        {
            if (CurrentUser == null) return false;

            return db.Database
                .SqlQuery<bool>("select dbo.UserCanAddToOrganisation(@user_id, @OrganisationId)",
                new SqlParameter("@user_id", CurrentUser.Id),
                new SqlParameter("@OrganisationId", orgId)).First();
        }

        protected bool UserCanAddToOrgcat(int orgCatId)
        {
            if (CurrentUser == null) return false;

            return db.Database
                .SqlQuery<bool>("select dbo.UserCanAddToOrgCat(@user_id, @OrganisationId)",
                new SqlParameter("@user_id", CurrentUser.Id),
                new SqlParameter("@OrganisationId", orgCatId)).First();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}