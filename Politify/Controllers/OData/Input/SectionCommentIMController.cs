﻿using Politify.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;


namespace Politify.Controllers.OData
{
    [Authorize]
    public class SectionCommentIMController : GenericApiController
    {
        // POST: api/SectionCommentVMs
        [ResponseType(typeof(SectionCommentVM))]
        public async Task<IHttpActionResult> PostSectionCommentIM(SectionCommentIM sectionCommentIM)
        {
            if (sectionCommentIM.OrganisationCategoryId == 0 || !UserCanAddToOrgcat(sectionCommentIM.OrganisationCategoryId))
            {
                return new UnauthorizedResult(null, this);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SectionComment sectionComment = new SectionComment(sectionCommentIM);
            try
            {
                db.SectionComments.Add(sectionComment);
                db.Entry<ApplicationUser>(sectionComment.User).State = EntityState.Unchanged;
                db.Entry<Section>(sectionComment.Section).State = EntityState.Unchanged;
                await db.SaveChangesAsync();
            }
            catch(Exception e)
            {
                return new ExceptionResult(e, this);
            }

            var sectionCommentVM = new SectionCommentVM(sectionComment);

            return Ok(sectionCommentVM);

            //todo: sort the route to prevent .net throwing a 501.
            //return CreatedAtRoute(String.Format("odata/SectionCommentVM({0})", sectionComment.CommentID), 
            //    new { id = sectionComment.CommentID }, new SectionCommentVM(sectionComment));
        }

        // PUT: api/SectionCommentVMs/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutSectionCommentIM(int id, SectionComment sectionComment)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //
        //    if (id != sectionComment.CommentID)
        //    {
        //        return BadRequest();
        //    }
        //
        //    db.Entry(sectionComment).State = EntityState.Modified;
        //
        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SectionCommentVMExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //
        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        //
        //// DELETE: api/SectionCommentVMs/5
        //[ResponseType(typeof(SectionCommentVM))]
        //public async Task<IHttpActionResult> DeleteSectionCommentIM(int id)
        //{
        //    SectionCommentVM sectionCommentVM = await db.SectionCommentVMs.FindAsync(id);
        //    if (sectionCommentVM == null)
        //    {
        //        return NotFound();
        //    }
        //
        //    db.SectionCommentVMs.Remove(sectionCommentVM);
        //    await db.SaveChangesAsync();
        //
        //    return Ok(sectionCommentVM);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SectionCommentVMExists(int id)
        {
            return db.SectionCommentVMs.Count(e => e.CommentID == id) > 0;
        }
    }
}