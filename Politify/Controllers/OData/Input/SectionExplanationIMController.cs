﻿using Politify.Code;
using Politify.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using Microsoft.AspNet.Identity;
using System.Web.Http.Description;
using System.Web.Http.Results;


namespace Politify.Controllers.OData
{
    [Authorize]
    public class SectionExplanationIMController : GenericApiController
    {
        // POST: api/SectionExplanationVMs
        [ResponseType(typeof(SectionExplanationVM))]
        public async Task<IHttpActionResult> PostSectionExplanationIM(SectionExplanationIM sectionExplanationIM)
        {
            if (sectionExplanationIM.OrganisationCategoryId == 0 || !UserCanAddToOrgcat(sectionExplanationIM.OrganisationCategoryId))
            {
                return new UnauthorizedResult(null, this);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SectionExplanation sectionExplanation = new SectionExplanation(sectionExplanationIM);
            try
            {
                db.SectionExplanations.Add(sectionExplanation);
                db.Entry<ApplicationUser>(sectionExplanation.User).State = EntityState.Unchanged;
                db.Entry<Section>(sectionExplanation.Section).State = EntityState.Unchanged;
                await db.SaveChangesAsync();
            }
            catch(Exception e)
            {
                return new ExceptionResult(e, this);
            }

            return Ok(new SectionExplanationVM(sectionExplanation));

            //todo: sort the route to prevent .net throwing a 501.
            //return CreatedAtRoute(String.Format("odata/SectionExplanationVM({0})", sectionExplanation.SectionExplanationId), 
            //    new { id = sectionExplanation.SectionExplanationId }, new SectionExplanationVM(sectionExplanation));
        }

        // PUT: api/SectionExplanationVMs/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutSectionExplanationIM(int id, SectionExplanation sectionExplanation)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //
        //    if (id != sectionExplanation.ExplanationID)
        //    {
        //        return BadRequest();
        //    }
        //
        //    db.Entry(sectionExplanation).State = EntityState.Modified;
        //
        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SectionExplanationVMExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //
        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        //
        //// DELETE: api/SectionExplanationVMs/5
        //[ResponseType(typeof(SectionExplanationVM))]
        //public async Task<IHttpActionResult> DeleteSectionExplanationIM(int id)
        //{
        //    SectionExplanationVM sectionExplanationVM = await db.SectionExplanationVMs.FindAsync(id);
        //    if (sectionExplanationVM == null)
        //    {
        //        return NotFound();
        //    }
        //
        //    db.SectionExplanationVMs.Remove(sectionExplanationVM);
        //    await db.SaveChangesAsync();
        //
        //    return Ok(sectionExplanationVM);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SectionExplanationVMExists(int id)
        {
            return db.SectionExplanationVMs.Count(e => e.SectionExplanationId == id) > 0;
        }
    }
}