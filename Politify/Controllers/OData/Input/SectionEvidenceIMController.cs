﻿using Politify.Code;
using Politify.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using Microsoft.AspNet.Identity;
using System.Web.Http.Description;
using System.Web.Http.Results;


namespace Politify.Controllers.OData
{
    [Authorize]
    public class SectionEvidenceIMController : GenericApiController
    {
        // POST: api/SectionEvidenceVMs
        [ResponseType(typeof(SectionEvidenceVM))]
        public async Task<IHttpActionResult> PostSectionEvidenceIM(SectionEvidenceIM sectionEvidenceIM)
        {
            if (sectionEvidenceIM.OrganisationCategoryId == 0 || !UserCanAddToOrgcat(sectionEvidenceIM.OrganisationCategoryId))
            {
                return new UnauthorizedResult(null, this);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SectionEvidence sectionEvidence = new SectionEvidence(sectionEvidenceIM);
            try
            {
                db.SectionEvidences.Add(sectionEvidence);
                db.Entry<ApplicationUser>(sectionEvidence.User).State = EntityState.Unchanged;
                db.Entry<Section>(sectionEvidence.Section).State = EntityState.Unchanged;
                await db.SaveChangesAsync();
            }
            catch(Exception e)
            {
                return new ExceptionResult(e, this);
            }

            return Ok(sectionEvidence);
            //todo: sort the route to prevent .net throwing a 501.
            //return CreatedAtRoute(String.Format("odata/SectionEvidenceVM({0})", sectionEvidence.SectionEvidenceId),
            //    new { id = sectionEvidence.SectionEvidenceId }, new SectionEvidenceVM(sectionEvidence));
        }

        // PUT: api/SectionEvidenceVMs/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutSectionEvidenceIM(int id, SectionEvidence sectionEvidence)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //
        //    if (id != sectionEvidence.EvidenceID)
        //    {
        //        return BadRequest();
        //    }
        //
        //    db.Entry(sectionEvidence).State = EntityState.Modified;
        //
        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SectionEvidenceVMExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //
        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        //
        //// DELETE: api/SectionEvidenceVMs/5
        //[ResponseType(typeof(SectionEvidenceVM))]
        //public async Task<IHttpActionResult> DeleteSectionEvidenceIM(int id)
        //{
        //    SectionEvidenceVM sectionEvidenceVM = await db.SectionEvidenceVMs.FindAsync(id);
        //    if (sectionEvidenceVM == null)
        //    {
        //        return NotFound();
        //    }
        //
        //    db.SectionEvidenceVMs.Remove(sectionEvidenceVM);
        //    await db.SaveChangesAsync();
        //
        //    return Ok(sectionEvidenceVM);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SectionEvidenceVMExists(int id)
        {
            return db.SectionEvidenceVMs.Count(e => e.SectionEvidenceId == id) > 0;
        }
    }
}