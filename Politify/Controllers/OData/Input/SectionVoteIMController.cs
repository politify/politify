﻿using Politify.Code;
using Politify.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using Microsoft.AspNet.Identity;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Politify.Models.JsonViewModels;


namespace Politify.Controllers.OData
{
    [Authorize]
    public class SectionVoteIMController : GenericApiController
    {
        // POST: api/SectionVoteVMs
        [ResponseType(typeof(SectionVoteViewModel))]
        public async Task<IHttpActionResult> PostSectionVoteIM(SectionVoteIM sectionVoteIM)
        {
            if (sectionVoteIM.OrganisationCategoryId == 0 || !UserCanAddToOrgcat(sectionVoteIM.OrganisationCategoryId))
            {
                return new UnauthorizedResult(null, this);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SectionVote sectionVote = new SectionVote(sectionVoteIM);
            try
            {
                SectionVote oldVote = null;

                if (sectionVote.Section != null)
                {
                    oldVote = db.SectionVotes.SingleOrDefault(s => s.User.Id == sectionVoteIM.User_ID && s.Section.SectionID == sectionVoteIM.SectionID);
                    
                    db.Entry<Section>(sectionVote.Section).State = EntityState.Unchanged;
                }
                else if (sectionVote.SectionEdit != null)
                {
                    oldVote = db.SectionVotes.SingleOrDefault(s => s.User.Id == sectionVoteIM.User_ID && s.SectionEdit.SectionEditId == sectionVoteIM.SectionEditID);

                    db.Entry<SectionEdit>(sectionVote.SectionEdit).State = EntityState.Unchanged;
                }
                else if (sectionVote.SectionEvidence != null)
                {
                    oldVote = db.SectionVotes.SingleOrDefault(s => s.User.Id == sectionVoteIM.User_ID && s.SectionEvidence.SectionEvidenceId == sectionVoteIM.SectionEvidenceID);

                    db.Entry<SectionEvidence>(sectionVote.SectionEvidence).State = EntityState.Unchanged;
                }
                else if (sectionVote.SectionExplanation != null)
                {
                    oldVote = db.SectionVotes.SingleOrDefault(s => s.User.Id == sectionVoteIM.User_ID && s.SectionExplanation.SectionExplanationId == sectionVoteIM.SectionExplanationID);

                    db.Entry<SectionExplanation>(sectionVote.SectionExplanation).State = EntityState.Unchanged;
                }
                else if (sectionVote.SectionComment != null)
                {
                    oldVote = db.SectionVotes.SingleOrDefault(s => s.User.Id == sectionVoteIM.User_ID && s.SectionComment.CommentID == sectionVoteIM.SectionCommentID);

                    db.Entry<SectionComment>(sectionVote.SectionComment).State = EntityState.Unchanged;
                }
                else
                {
                    return BadRequest();
                }
                
                db.SectionVotes.Add(sectionVote);
                db.Entry<ApplicationUser>(sectionVote.User).State = EntityState.Unchanged;

                bool wasOldVote = oldVote != null;
                bool oldVoteWasPositive = false;

                if (wasOldVote)
                {
                    oldVoteWasPositive = oldVote.IsPositive;                    
                    db.SectionVotes.Remove(oldVote);
                }

                await db.SaveChangesAsync();

                return Json<SectionVoteViewModel>(new SectionVoteViewModel(){
                    sectionVote = new SectionVoteVM(sectionVote),
                    wasExistingVote = wasOldVote,
                    existingVoteWasPositive = oldVoteWasPositive
                });
            }
            catch(Exception e)
            {
                return new ExceptionResult(e, this);
            }


            //todo: sort the route to prevent .net throwing a 501.
            return Ok(sectionVote);
        }

        // PUT: api/SectionVoteVMs/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutSectionVoteIM(int id, SectionVote sectionVote)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //
        //    if (id != sectionVote.VoteID)
        //    {
        //        return BadRequest();
        //    }
        //
        //    db.Entry(sectionVote).State = EntityState.Modified;
        //
        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SectionVoteVMExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //
        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        //
        //// DELETE: api/SectionVoteVMs/5
        //[ResponseType(typeof(SectionVoteVM))]
        //public async Task<IHttpActionResult> DeleteSectionVoteIM(int id)
        //{
        //    SectionVoteVM sectionVoteVM = await db.SectionVoteVMs.FindAsync(id);
        //    if (sectionVoteVM == null)
        //    {
        //        return NotFound();
        //    }
        //
        //    db.SectionVoteVMs.Remove(sectionVoteVM);
        //    await db.SaveChangesAsync();
        //
        //    return Ok(sectionVoteVM);
        //}
    }
}