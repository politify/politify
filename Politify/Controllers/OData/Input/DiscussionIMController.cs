﻿using System.Net;
using Politify.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;


namespace Politify.Controllers.OData
{
    [Authorize]
    public class DiscussionIMController : GenericApiController
    {
        // POST: api/DiscussionVMs
        [ResponseType(typeof(DiscussionVM))]
        public async Task<IHttpActionResult> PostDiscussionIM(DiscussionIM DiscussionIM)
        {
            if (!UserCanAddToOrgcat(DiscussionIM.OrganisationCategoryId)) // user can add
            {
                return new UnauthorizedResult(null, this);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Discussion Discussion = new Discussion(DiscussionIM);
            try
            {
                db.Discussions.Add(Discussion);
                db.Entry<ApplicationUser>(Discussion.User).State = EntityState.Unchanged;
                db.Entry<ApplicationUser>(Discussion.Section.User).State = EntityState.Unchanged;
                db.Entry<OrganisationCategory>(Discussion.OrgCat).State = EntityState.Unchanged;
                await db.SaveChangesAsync();
            }
            catch(Exception e)
            {
                return new ExceptionResult(e, this);
            }

            var DiscussionVM = new DiscussionVM(Discussion);

            return Ok(DiscussionVM);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}