﻿using Politify.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;


namespace Politify.Controllers.OData
{
    [Authorize]
    public class SectionEditIMController : GenericApiController
    {
        // POST: api/SectionEditVMs
        [ResponseType(typeof(SectionEditVM))]
        public async Task<IHttpActionResult> PostSectionEditIM(SectionEditIM sectionEditIM)
        {
            if (sectionEditIM.OrganisationCategoryId == 0 || !UserCanAddToOrgcat(sectionEditIM.OrganisationCategoryId))
            {
                return new UnauthorizedResult(null, this);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SectionEdit sectionEdit = new SectionEdit(sectionEditIM);
            try
            {
                db.SectionEdits.Add(sectionEdit);
                db.Entry<ApplicationUser>(sectionEdit.User).State = EntityState.Unchanged;
                db.Entry<Section>(sectionEdit.Section).State = EntityState.Unchanged;
                await db.SaveChangesAsync();
            }
            catch(Exception e)
            {
                return new ExceptionResult(e, this);
            }

            return Ok(sectionEdit);

            //todo: sort the route to prevent .net throwing a 501.
            //return CreatedAtRoute(String.Format("odata/SectionEditVM({0})", sectionEdit.SectionEditId), 
            //    new { id = sectionEdit.SectionEditId }, new SectionEditVM(sectionEdit));
        }

        // PUT: api/SectionEditVMs/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutSectionEditIM(int id, SectionEdit sectionEdit)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //
        //    if (id != sectionEdit.EditID)
        //    {
        //        return BadRequest();
        //    }
        //
        //    db.Entry(sectionEdit).State = EntityState.Modified;
        //
        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SectionEditVMExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //
        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        //
        //// DELETE: api/SectionEditVMs/5
        //[ResponseType(typeof(SectionEditVM))]
        //public async Task<IHttpActionResult> DeleteSectionEditIM(int id)
        //{
        //    SectionEditVM sectionEditVM = await db.SectionEditVMs.FindAsync(id);
        //    if (sectionEditVM == null)
        //    {
        //        return NotFound();
        //    }
        //
        //    db.SectionEditVMs.Remove(sectionEditVM);
        //    await db.SaveChangesAsync();
        //
        //    return Ok(sectionEditVM);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SectionEditVMExists(int id)
        {
            return db.SectionEditVMs.Count(e => e.SectionEditId == id) > 0;
        }
    }
}