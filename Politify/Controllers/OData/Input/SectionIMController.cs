﻿using Politify.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;


namespace Politify.Controllers.OData
{
    [Authorize]
    public class SectionIMController : GenericApiController
    {
        // POST: api/SectionVMs
        [ResponseType(typeof(SectionVM))]
        public async Task<IHttpActionResult> PostSectionIM(SectionIM sectionIM)
        {
            if (sectionIM.OrganisationCategoryId == 0 || !UserCanAddToOrgcat(sectionIM.OrganisationCategoryId))
            {
                return new UnauthorizedResult(null, this);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Section section = new Section(sectionIM);
            
            try
            {
                db.Sections.Add(section);
                db.Entry<ApplicationUser>(section.User).State = EntityState.Unchanged;
                db.Entry<Section>(section.Parent).State = EntityState.Unchanged;
                await db.SaveChangesAsync();
            }
            catch(Exception e)
            {
                return new ExceptionResult(e, this);
            }

            var sectionVM = new SectionVM(section);

            return Ok(sectionVM);

            //todo: sort the route to prevent .net throwing a 501.
            //return CreatedAtRoute(String.Format("odata/SectionVM({0})", section.CommentID), 
            //    new { id = section.CommentID }, new SectionVM(section));
        }

        // PUT: api/SectionCommentVMs/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutSectionCommentIM(int id, SectionComment sectionComment)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //
        //    if (id != sectionComment.CommentID)
        //    {
        //        return BadRequest();
        //    }
        //
        //    db.Entry(sectionComment).State = EntityState.Modified;
        //
        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!SectionCommentVMExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //
        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        //
        //// DELETE: api/SectionCommentVMs/5
        //[ResponseType(typeof(SectionCommentVM))]
        //public async Task<IHttpActionResult> DeleteSectionCommentIM(int id)
        //{
        //    SectionCommentVM sectionCommentVM = await db.SectionCommentVMs.FindAsync(id);
        //    if (sectionCommentVM == null)
        //    {
        //        return NotFound();
        //    }
        //
        //    db.SectionCommentVMs.Remove(sectionCommentVM);
        //    await db.SaveChangesAsync();
        //
        //    return Ok(sectionCommentVM);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SectionCommentVMExists(int id)
        {
            return db.SectionCommentVMs.Count(e => e.CommentID == id) > 0;
        }
    }
}