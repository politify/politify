﻿using Politify.Models;
using System.Linq;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;

namespace Politify.Controllers.OData
{
    public class IdentityController : GenericApiController
    {
        //// GET: odata/Identity
        //[EnableQuery]
        //public IQueryable<ApplicationUser> GetIdentity()
        //{
        //    return db.ApplicationUsers;
        //}

        // GET: odata/Identity(5)
        [EnableQuery]
        public SingleResult<ApplicationUser> GetApplicationUser([FromODataUri] string key)
        {
            return SingleResult.Create(db.ApplicationUsers.Where(applicationUser => applicationUser.Id == key));
        }
        
        [HttpGet]
        [ODataRoute("CanUserAddToOrg(orgId={orgId})")]
        public bool CanUserAddToOrg([FromODataUri] int orgId)
        {
            if (!User.Identity.IsAuthenticated) return false;
        
            return db.Database
                .SqlQuery<bool>("select dbo.UserCanAddToOrganisation(@user_id, @OrganisationId)",
                new SqlParameter("@user_id", User.Identity.GetUserId()),
                new SqlParameter("@OrganisationId", orgId)).First();
        }
        
        [HttpGet]
        [ODataRoute("CanUserAddToOrgCat(orgCatId={orgCatId})")]
        public bool CanUserAddToOrgCat([FromODataUri] int orgCatId)
        {
            if (!User.Identity.IsAuthenticated) return false;
        
            return db.Database
                .SqlQuery<bool>("select dbo.UserCanAddToOrgCat(@user_id, @OrganisationId)",
                new SqlParameter("@user_id", User.Identity.GetUserId()),
                new SqlParameter("@OrganisationId", orgCatId)).First();
        }
    }
}
