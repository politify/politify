﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using Politify.Code;
using Politify.Models;

namespace Politify.Controllers
{
    public class SiteMapController : Controller
    {
        static XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";

        public ContentResult Index()
        {
            return GetXmlSitemapIndex("/SiteMap/Static", "/SiteMap/Organisations");
        }
        
        public ContentResult Static()
        {
            return GetXmlSitemap(
                "/",
                "/Home/About",
                "/Home/Contact",
                "/Home/WhatCanYouDoOnEnvocal",
                "/Account/Register",
                "/Account/Login",
                "/Organisation",
                "/Discussion");
        }

        public ContentResult Organisations()
        {
            PolitifyDbContext db = new PolitifyDbContext();

            return GetXmlSitemap(
                db.Organisations.Select(o => "/Organisation/Details/" + o.OrganisationId.ToString()).ToArray());
        }

        private ContentResult GetXmlSitemapIndex(params string[] elements)
        {
            XElement root = new XElement(xmlns + "sitemapindex");

            root.Add(elements.Select(e => GetSiteMapXElement(e)));

            return GetXmlContent(root);
        }

        private ContentResult GetXmlSitemap(params string[] elements)
        {
            XElement root = new XElement(xmlns + "urlset");

            root.Add(elements.Select(e => GetUrlXElement(e)));

            return GetXmlContent(root);
        }

        private ContentResult GetXmlContent(XElement root){

            using (MemoryStream ms = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(ms, Encoding.UTF8))
                {
                    new XDocument(new XDeclaration("1.0", "UTF-8", null), root).Save(writer);
                }

                return Content(Encoding.UTF8.GetString(ms.ToArray()), "text/xml", Encoding.UTF8);
            }
        }
        
        private XElement GetSiteMapXElement(string path)
        {
            string currUrl = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority);

            return new XElement(xmlns + "sitemap",
                    new XElement(xmlns + "loc", currUrl + path));
        }

        private XElement GetUrlXElement(string path)
        {
            string currUrl = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority);

            return new XElement(xmlns + "url",
                    new XElement(xmlns + "loc", currUrl + path),
                    new XElement(xmlns + "changefreq", "daily"));
        }
    }
}