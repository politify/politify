﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Politify.Code;
using Politify.Models;

namespace Politify.Controllers
{
    public class DiscussionController : Controller
    {
        private PolitifyDbContext db = new PolitifyDbContext();

        // GET: Discussion
        public ActionResult Index()
        {
            return View();        
        }

        // GET: Discussion/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DiscussionVM discussion = await db.DiscussionVMs.FindAsync(id);
            if (discussion == null)
            {
                return HttpNotFound();
            }
            return View(discussion);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
