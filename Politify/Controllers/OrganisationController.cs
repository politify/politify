﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Politify.Code;
using Politify.Models;


namespace Politify.Controllers
{
    public class OrganisationController : Controller
    {
        private PolitifyDbContext db = new PolitifyDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisationVM org = await db.OrganisationVMs.FindAsync(id);
            if (org == null)
            {
                return HttpNotFound();
            }
            return View(org);
        }

        protected override void Dispose(bool disposing)
        {
            int num = disposing ? 1 : 0;
            base.Dispose(disposing);
        }
    }
}
