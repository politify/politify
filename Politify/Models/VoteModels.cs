﻿using Politify.Code.Enums;
using Politify.Code.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Politify.Models
{
    public class Vote : IVote
    {
        [Key]
        public int VoteID { get; set; }

        [Required]
        public DateTimeOffset CreatedDate { get; set; }

        [Required]
        public bool IsPositive { get; set; }

        [Required]
        public virtual ApplicationUser User { get; set; }

        public virtual Organisation Organisation { get; set; }

        public virtual Staff Staff { get; set; }

        public virtual SignatureCount SignatureCount { get; set; }

        public virtual Party Party { get; set; }
    }
}