﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Politify.Models
{
    public class Staff
    {
        [Key]
        public int StaffId { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public string JobTitle { get; set; }

        public string Bio { get; set; }

        public string ProfilePicture { get; set; }

        public virtual ApplicationUser User { get; set; }

        public string EmailAddress { get; set; }

        public string Twitter { get; set; }

        public string Website { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        [Required]
        [ForeignKey("Organisation_OrganisationId")]
        public virtual Organisation Organisation { get; set; }

        public int Organisation_OrganisationId { get; set; }
    }

    public class StaffApiViewModel
    {
        [Key]
        public int StaffId { get; set; }
        public String Name { get; set; }
        public string JobTitle { get; set; }
        public string ProfilePicture { get; set; }
    }
}