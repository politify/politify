﻿using Politify.Code.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Politify.Models.Comments
{
    public abstract class Comment : IComment
    {
        [Key]
        public int CommentID { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        [Required]
        public virtual ApplicationUser User { get; set; }

        public bool Edited { get; set; }

        public DateTimeOffset? LastEdit { get; set; }

        public string LastText { get; set; }

        public string OriginalText { get; set; }

        public void Add(ICommentInputModel<IComment> inputModel)
        {
            this.CreatedDate = DateTime.Now;
            this.Text = inputModel.Text;
        }

        public void Edit(ICommentInputModel<IComment> inputModel)
        {
            if (inputModel.GetKey() != this.GetKey())
                throw new ApplicationException("Incorrect input model has been passed in.");
            if (!this.Edited)
            {
                this.OriginalText = this.Text;
                this.Edited = true;
            }
            this.LastText = this.Text;
            this.Text = inputModel.Text;
            this.LastEdit = new DateTime?(DateTime.Now);
        }

        public object[] GetKey()
        {
            return new object[1] { (object)this.CommentID };
        }
    }

    public abstract class CommentInputModel<T> : ICommentInputModel<T>, IDataKey where T: IComment, new()
    {
        public int CommentID { get; set; }
        
        public string Text { get; set; }

        public T ModelType
        {
            get
            {
                return new T();
            }
        }

        public object[] GetKey()
        {
            return new object[1] { (object)this.CommentID };
        }
    }

    public class CommentViewModel
    {
        public int CommentID { get; set; }

        public string Text { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public virtual ApplicationUser User { get; set; }

        public bool Edited { get; set; }

        public CommentViewModel(IComment comment)
        {
            this.CommentID = comment.CommentID;
            this.Text = comment.Text;
            this.CreatedDate = comment.CreatedDate;
            this.User = comment.User;
            this.Edited = comment.Edited;
        }
    }
}