﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Politify.Models
{
    public class Discussion
    {
        public Discussion() { }

        public Discussion(DiscussionIM discussionIM)
        {
            if (discussionIM == null) return;

            var user = new ApplicationUser() { Id = discussionIM.User_Id };

            DateCreated = discussionIM.DateCreated;
            Section = new Section()
            {
                Text = discussionIM.Text,        
                Html = discussionIM.Html,
                OrderQualifier = discussionIM.OrderQualifier,
                User = user
            };
            User = user;
            OrgCat = new OrganisationCategory(){
                OrganisationCategoryId = discussionIM.OrganisationCategoryId       
            };
        }

        [Key]
        public int DiscussionId { get; set; }

        [Required]
        public Section Section { get; set; }

        [Required]
        public ApplicationUser User { get; set; }

        [Required]
        public DateTimeOffset DateCreated { get; set; }

        [Required]
        public virtual OrganisationCategory OrgCat { get; set; }
    }

    public class DiscussionVM
    {
        public DiscussionVM() { }

        public DiscussionVM(Discussion Discussion)
        {
            if (Discussion == null) return;

            User = Discussion.User;
            DateCreated = Discussion.DateCreated;
            if (Discussion.OrgCat != null)
            {
                OrgCat = new OrganisationCategoryVM(Discussion.OrgCat);
                OrgCat_OrganisationCategoryId = Discussion.OrgCat.OrganisationCategoryId;
            }

            DiscussionId = Discussion.DiscussionId;

            if (Discussion.Section != null)
            {
                Text = Discussion.Section.Text;
                Html = Discussion.Section.Html;
                Section_SectionID = Discussion.Section.SectionID;
            }
        }
        [Key]
        public int DiscussionId { get; set; }

        [ForeignKey("Section_SectionID")]
        public virtual SectionVM Section { get; set; }
        public int Section_SectionID { get; set; }

        [ForeignKey("User_Id")]
        public virtual ApplicationUser User { get; set; }
        public string User_Id { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        [ForeignKey("OrgCat_OrganisationCategoryId")]
        public virtual OrganisationCategoryVM OrgCat { get; set; }
        public int OrgCat_OrganisationCategoryId { get; set; }

        public string Text { get; set; }

        public string Html { get; set; }

        public bool IsUserCreated { get; set; }

        [ForeignKey("Organisation_OrganisationId")]
        public virtual OrganisationVM Organisation { get; set; }
        public int? Organisation_OrganisationId { get; set; }

        public double? Positives { get; set; }

        public double? Negatives { get; set; }

        public double? Score { get; set; }

        public double? CommentCount { get; set; }
    }

    public class DiscussionIM
    {
        [Key]
        public int DiscussionId { get; set; }

        public string Text { get; set; }
        
        public string Html { get; set; }
        
        public string OrderQualifier { get; set; }

        public string User_Id { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public int OrganisationCategoryId { get; set; }
    }
}