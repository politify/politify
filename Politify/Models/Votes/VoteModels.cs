﻿using Politify.Code.Enums;
using Politify.Code.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Politify.Models.Votes
{
    public abstract class Vote : IVote
    {
        [Key]
        public int VoteID { get; set; }

        [Required]
        public DateTimeOffset CreatedDate { get; set; }

        [Required]
        public bool IsPositive { get; set; }

        [Required]
        public virtual ApplicationUser User { get; set; }

        public virtual void Add(IVoteInputModel<IVote> inputModel)
        {
            this.CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
            this.IsPositive = inputModel.IsPositive;
        }
        
        public object[] GetKey()
        {
            return new object[1] { (object)this.VoteID };
        }
    }

    public abstract class VoteInputModel<T> : IVoteInputModel<T>, IDataKey where T: IVote, new()
    {
        public int VoteID { get; set; }

        public bool IsPositive { get; set; }

        public T ModelType
        {
            get
            {
                return new T();
            }
        }

        public object[] GetKey()
        {
            return new object[1] { (object)this.VoteID };
        }
    }

    public abstract class VoteViewModel
    {
        public int VoteID { get; set; }

        public bool? IsPositive { get; set; }

        public ApplicationUser User { get; set; }

        public VoteViewModel(IVote vote)
        {
            if (vote == null) return;

            this.VoteID = vote.VoteID;
            this.IsPositive = new bool?(vote.IsPositive);
            this.User = vote.User;
        }
    }
}