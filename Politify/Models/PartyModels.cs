﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Politify.Models
{
    public class Party
    {
        [Key]
        public int PartyID { get; set; }

        [Required]
        public string Name { get; set; }

        public DateTimeOffset DateCreated { get; set; }
    }

    public class PartyApiViewModel
    {
        [Key]
        public int PartyID { get; set; }
        public string Name { get; set; }
    }
}
