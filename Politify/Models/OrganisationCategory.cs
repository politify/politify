﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Politify.Models
{
    public class OrganisationCategory
    {
        [Key]
        public int OrganisationCategoryId { get; set; }

        [Required]
        public virtual Organisation Organisation { get; set; }

        [Required]
        public virtual Category Category { get; set; }

        public virtual ICollection<Discussion> Discussions { get; set; }
    }

    public class OrganisationCategoryVM
    {
        public OrganisationCategoryVM() { }

        public OrganisationCategoryVM(OrganisationCategory organisationCategory)
        {
        }

        [Key]
        public int OrganisationCategoryId { get; set; }

        public int CategoryId { get; set; }
        public int Order { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("Organisation_OrganisationId")]
        public virtual OrganisationVM Organisation { get; set; }

        public int Organisation_OrganisationId { get; set; }

        [ForeignKey("OrgCat_OrganisationCategoryId")]
        public virtual ICollection<DiscussionVM> Discussions { get; set; }
    }
}