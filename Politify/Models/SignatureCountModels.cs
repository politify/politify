﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Politify.Models
{
    public class SignatureCount
    {
        [Key]
        public int SignatureCountId { get; set; }

        [Required]
        public int Count { get; set; }

        [Required]
        public string Action { get; set; }

        [Required]
        [ForeignKey("Organisation_OrganisationId")]
        public virtual Organisation Organisation { get; set; }

        public int Organisation_OrganisationId { get; set; }
    }

    public class SignatureCountApiViewModel
    {
        [Key]
        public int SignatureCountId { get; set; }
        public int Count { get; set; }
        public string Action { get; set; }
    }
}