﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Politify.Models
{    
    public class Country
    {
        [Key]
        public int CountryID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(5)]
        public string NameShort { get; set; }

        [StringLength(50)]
        public string Language { get; set; }

        [StringLength(50)]
        public string ImageFileName { get; set; }
        
        public virtual ICollection<Organisation> Organisations { get; set; }
    }
    
    //[NotMapped]
    public class CountryVM
    {
        [Key]
        public int CountryID { get; set; }
        public string Name { get; set; }
        public string NameShort { get; set; }
        public string Language { get; set; }
        public string ImageFileName { get; set; }

        [ForeignKey("Country_CountryID")]
        public virtual ICollection<OrganisationVM> Organisations { get; set; }

        public CountryVM()
        {

        }

        public CountryVM(Country country)
        {
            if (country == null) return;

            this.CountryID = country.CountryID;
            this.Name = country.Name;
            this.NameShort = country.NameShort;
            this.Language = country.Language;
            this.ImageFileName = country.ImageFileName;
        }
    }
}