﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Politify.Models.Votes;
using System.Collections;
using System.Collections.Generic;

namespace Politify.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        public virtual Country Country { get; set; }

        [Required]
        [StringLength(30)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(30)]
        public string Surname { get; set; }

        [Required]
        public override string Email
        {
            get
            {
                return base.Email;
            }
            set
            {
                base.Email = value;
            }
        }

        public virtual Postcode Postcode { get; set; }

        public virtual Postcode WorkPostcode { get; set; }

        //[ForeignKey("User_Id")]
        //public virtual ICollection<SectionVote> SectionVotes { get; set; }
        //
        //public virtual ICollection<SectionCommentVM> SectionComments { get; set; }
        //
        //public virtual ICollection<SectionEditVM> SectionEdits { get; set; }
        //
        //public virtual ICollection<SectionExplanationVM> SectionExplanations { get; set; }
        //
        //public virtual ICollection<SectionEvidenceVM> SectionEvidences { get; set; }

        public virtual ICollection<Vote> Votes { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    [ComplexType]
    public class ProfileViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public CountryVM Country { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public virtual ApplicationUser User { get; set; }

        public ProfileViewModel(ApplicationUser user)
        {
            if (user == null) return;

            this.User = user;
            this.Id = user.Id;
            this.UserName = user.UserName;
            this.Country = null;
            this.FirstName = user.FirstName;
            this.Surname = user.Surname;
        }
    }
}