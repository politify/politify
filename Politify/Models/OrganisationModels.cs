﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Politify.Models
{
    public class Organisation
    {
        [Key]
        public int OrganisationId { get; set; }

        [Required]
        [MaxLength(400)]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public virtual Country Country { get; set; }

        [MaxLength(100)]
        public string ProfilePicture { get; set; }
             
        [Required]
        public OrganisationType OrganisationType { get; set; }

        public virtual ICollection<OrganisationCategory> OrgCats { get; set; }

        public virtual ICollection<Organisation> Children { get; set; }

        public virtual ICollection<Organisation> Parents { get; set; }

        public virtual IQueryable<Staff> Staff { get; set; }

        public virtual IQueryable<SignatureCount> SignatureCounts { get; set; }

        public String EmailAddress { get; set; }

        [StringLength(9)]
        public String OnsCode { get; set; }

        public virtual IQueryable<Vote> Votes { get; set; }

        public virtual IQueryable<Comment> Comments { get; set; }
    }

    public class OrganisationVM
    {
        [Key]
        public int OrganisationId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [ForeignKey("Country_CountryID")]
        public virtual CountryVM Country { get; set; }
        public int? Country_CountryID { get; set; }

        public string ProfilePicture { get; set; }

        public OrganisationType OrganisationType { get; set; }

        [ForeignKey("Organisation_OrganisationId")]
        public virtual ICollection<OrganisationCategoryVM> Categories { get; set; }

        public virtual ICollection<OrganisationVM> Children { get; set; }
        
        public virtual ICollection<OrganisationVM> Parents { get; set; }

        [ForeignKey("Organisation_OrganisationId")]
        public virtual ICollection<Staff> Staff { get; set; }

        [ForeignKey("Organisation_OrganisationId")]
        public virtual ICollection<SignatureCount> SignatureCounts { get; set; }

        public String EmailAddress { get; set; }

        public String OnsCode { get; set; }

        public double? Positives { get; set; }

        public double? Negatives { get; set; }

        public double? CommentCount { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }

    public class OrganisationIM
    {   
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual Country Country { get; set; }

        public string ProfilePicture { get; set; }

        public OrganisationType OrganisationType { get; set; }

        public string EmailAddress { get; set; }

        //needs parent
    }

    //If you change this objects namespace you must change javascript const 'envocal.consts.orgTypeCSharpNameSpace' so odata can find the OrganisationType object
    public enum OrganisationType
    {
        Unknown = 0,
        Ward = 1,
        District = 2,
        County = 3,
        UnitaryAuthority = 4,
        Constituency = 5,
        Government = 6,
        LondonBorough = 7,
        MetropolitanDistrict = 8,
        Envocal = 9
    }
}
