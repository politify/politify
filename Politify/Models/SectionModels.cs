﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Politify.Models
{
    public class Section
    {
        public Section(){        }

        public Section(SectionIM sectionIM)
        {
            if (sectionIM == null) return;

            this.Text = sectionIM.Text;
            this.Html = sectionIM.Html;
            this.OrderQualifier = sectionIM.OrderQualifier;
            //this.Organisation = sectionIM.Organisation;
            this.DateCreated = sectionIM.DateCreated;
            this.User = new ApplicationUser() { Id = sectionIM.User_ID };

            if (sectionIM.ParentId != null) Parent = new Section() { SectionID = sectionIM.ParentId?? 0 };
        }

        [Key]
        public int SectionID { get; set; }
        
        public string Text { get; set; }

        public string Html { get; set; }

        public string OrderQualifier { get; set; }

        public virtual Organisation Organisation { get; set; }

        public virtual Section Parent { get; set; }

        public virtual ICollection<Section> Children { get; set; }

        public virtual ICollection<SectionEdit> SectionEdits { get; set; }

        public virtual ICollection<SectionExplanation> SectionExplanations { get; set; }

        public virtual ICollection<SectionEvidence> SectionEvidences { get; set; }

        public virtual bool IsUserCreated { get; set; }
                
        public virtual ApplicationUser User { get; set; }

        public DateTimeOffset DateCreated { get; set; }
        
        public virtual ICollection<SectionVote> Votes { get; set; }

        public virtual ICollection<SectionComment> Comments { get; set; }

        //public virtual Discussion Discussion { get; set; }
    }

    public class SectionVM
    {
        public SectionVM() { }

        public SectionVM(Section section)
        {
            SectionID = section.SectionID;
            Text = section.Text;
            Html = section.Html;
            OrderQualifier = section.OrderQualifier;
        }

        [Key]
        public int SectionID { get; set; }

        public string Text { get; set; }

        public string Html { get; set; }

        public string OrderQualifier { get; set; }

        [ForeignKey("Organisation_OrganisationId")]
        public virtual OrganisationVM Organisation { get; set; }
        public int? Organisation_OrganisationId { get; set; }

        [ForeignKey("Parent_SectionID")]
        public virtual SectionVM Parent { get; set; }
        public int? Parent_SectionID { get; set; }

        [ForeignKey("Parent_SectionID")]
        public virtual ICollection<SectionVM> Children { get; set; }

        public virtual ICollection<SectionEditVM> SectionEdits { get; set; }

        public virtual ICollection<SectionExplanationVM> SectionExplanations { get; set; }

        public virtual ICollection<SectionEvidenceVM> SectionEvidences { get; set; }

        public virtual bool IsUserCreated { get; set; }

        [ForeignKey("User_ID")]
        public virtual ApplicationUser User { get; set; }
        public string User_ID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public double? Positives { get; set; }

        public double? Negatives { get; set; }

        public double? CommentCount { get; set; }

        public virtual ICollection<SectionCommentVM> Comments { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        //[ForeignKey("Parent_SectionID")]
        //public virtual DiscussionVM Discussion { get; set; }
        //public string Discussion_DiscussionId { get; set; }
    }

    public class SectionIM
    {
        [Key]
        public int SectionId { get; set; }

        public string Text { get; set; }

        public string Html { get; set; }

        public string OrderQualifier { get; set; }

        public string User_ID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public int? ParentId { get; set; }

        public int OrganisationCategoryId { get; set; }
    }

    public class SectionEdit
    {
        public SectionEdit() { }

        public SectionEdit(SectionEditIM sectionEditIM)
        {
            this.Text = sectionEditIM.Text;
            this.DateCreated = sectionEditIM.DateCreated;
            this.User = new ApplicationUser(){ Id = sectionEditIM.User_ID };
            this.Section = new Section() { SectionID = sectionEditIM.SectionID };
        }

        [Key]
        public int SectionEditId { get; set; }

        public virtual Section Section { get; set; }

        public string Text { get; set; }

        public virtual ApplicationUser User { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public virtual ICollection<SectionVote> Votes { get; set; }

        public virtual ICollection<SectionComment> Comments { get; set; }
    }
    
    public class SectionEditVM
    {
        [Key]
        public int SectionEditId { get; set; }

        [ForeignKey("Section_SectionID")]
        public virtual SectionVM Section { get; set; }
        public int Section_SectionID { get; set; }

        public string Text { get; set; }

        [ForeignKey("User_ID")]
        public virtual ApplicationUser User { get; set; }
        public string User_ID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public double? Positives { get; set; }

        public double? Negatives { get; set; }

        public double? CommentCount { get; set; }

        [ForeignKey("SectionEdit_SectionEditId")]
        public virtual ICollection<SectionCommentVM> Comments { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        [ForeignKey("SectionEdit_SectionEditId")]
        public ICollection<SectionVote> UserVotes { get; set; }

        public SectionEditVM() { }

        public SectionEditVM(SectionEdit edit)
        {
            this.SectionEditId = edit.SectionEditId;
            this.Text = edit.Text;
            this.DateCreated = edit.DateCreated;
        }
    }

    public class SectionEditIM
    {
        [Key]
        public int SectionID { get; set; }

        public string Text { get; set; }

        public string User_ID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public int OrganisationCategoryId { get; set; }
    }

    public class SectionExplanation
    {
        public SectionExplanation() { }

        public SectionExplanation(SectionExplanationIM sectionExpIM)
        {
            this.Text = sectionExpIM.Text;
            this.DateCreated = sectionExpIM.DateCreated;
            this.User = new ApplicationUser(){ Id = sectionExpIM.User_ID };
            this.Section = new Section() { SectionID = sectionExpIM.SectionID };
        }

        [Key]
        public int SectionExplanationId { get; set; }

        public virtual Section Section { get; set; }

        public string Text { get; set; }

        public virtual ApplicationUser User { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public virtual ICollection<SectionVote> Votes { get; set; }

        public virtual ICollection<SectionComment> Comments { get; set; }
    }

    public class SectionExplanationVM
    {
        [Key]
        public int SectionExplanationId { get; set; }

        [ForeignKey("Section_SectionID")]
        public virtual SectionVM Section { get; set; }
        public int Section_SectionID { get; set; }

        public string Text { get; set; }

        [ForeignKey("User_ID")]
        public virtual ApplicationUser User { get; set; }
        public string User_ID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public double? Positives { get; set; }

        public double? Negatives { get; set; }

        public double? CommentCount { get; set; }

        [ForeignKey("SectionExplanation_SectionExplanationId")]
        public virtual ICollection<SectionCommentVM> Comments { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }
        
        public SectionExplanationVM() { }

        public SectionExplanationVM(SectionExplanation explanation)
        {
            this.SectionExplanationId = explanation.SectionExplanationId;
            this.Text = explanation.Text;
            this.DateCreated = explanation.DateCreated;
        }
    }

    public class SectionExplanationIM
    {
        [Key]
        public int SectionID { get; set; }

        public string Text { get; set; }

        public string User_ID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public int OrganisationCategoryId { get; set; }
    }

    public class SectionEvidence
    {
        public SectionEvidence() { }

        public SectionEvidence(SectionEvidenceIM sectionEvidenceIM)
        {
            this.Text = sectionEvidenceIM.Text;
            this.DateCreated = sectionEvidenceIM.DateCreated;
            this.User = new ApplicationUser(){ Id = sectionEvidenceIM.User_ID };
            this.Section = new Section() { SectionID = sectionEvidenceIM.SectionID };
        }

        [Key]
        public int SectionEvidenceId { get; set; }

        public virtual Section Section { get; set; }

        public string Text { get; set; }

        public string FileName { get; set; }

        public virtual ApplicationUser User { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public virtual ICollection<SectionVote> Votes { get; set; }

        public virtual ICollection<SectionComment> Comments { get; set; }
    }

    public class SectionEvidenceVM
    {
        [Key]
        public int SectionEvidenceId { get; set; }

        [ForeignKey("Section_SectionID")]
        public virtual SectionVM Section { get; set; }
        public int Section_SectionID { get; set; }

        public string Text { get; set; }

        public string FileName { get; set; }

        [ForeignKey("User_ID")]
        public virtual ApplicationUser User { get; set; }
        public string User_ID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public double? Positives { get; set; }

        public double? Negatives { get; set; }

        public double? CommentCount { get; set; }

        [ForeignKey("SectionEvidence_SectionExplanationId")]
        public virtual ICollection<SectionCommentVM> Comments { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public SectionEvidenceVM() { }

        public SectionEvidenceVM(SectionEvidence evidence)
        {
            this.SectionEvidenceId = evidence.SectionEvidenceId;
            this.Text = evidence.Text;
            this.DateCreated = evidence.DateCreated;
        }
    }

    public class SectionEvidenceIM
    {
        [Key]
        public int SectionID { get; set; }

        public string Text { get; set; }

        public string User_ID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public int OrganisationCategoryId { get; set; }
    }

    public class SectionComment
    {
        public SectionComment() { }

        public SectionComment(SectionCommentIM sectionCommentIM)
        {
            this.Text = sectionCommentIM.Text;
            this.CreatedDate = sectionCommentIM.CreatedDate;
            this.User = new ApplicationUser(){ Id = sectionCommentIM.User_ID };
            if (sectionCommentIM.SectionID.HasValue && sectionCommentIM.SectionID != 0)
            {
                int id = sectionCommentIM.SectionID ?? 0;
                this.Section = new Section() { SectionID = id };
            }
            if (sectionCommentIM.SectionEditID.HasValue && sectionCommentIM.SectionEditID != 0)
            {
                int id = sectionCommentIM.SectionEditID ?? 0;
                this.SectionEdit = new SectionEdit() { SectionEditId = id };
            }
            if (sectionCommentIM.SectionEvidenceID.HasValue && sectionCommentIM.SectionEvidenceID != 0)
            {
                int id = sectionCommentIM.SectionEvidenceID ?? 0;
                this.SectionEvidence = new SectionEvidence() { SectionEvidenceId = id };
            }
            if (sectionCommentIM.SectionExplanationID.HasValue && sectionCommentIM.SectionExplanationID != 0)
            {
                int id = sectionCommentIM.SectionExplanationID ?? 0;
                this.SectionExplanation = new SectionExplanation() { SectionExplanationId = id };
            }
        }

        [Key]
        public int CommentID { get; set; }

        [Required]
        public string Text { get; set; }

        public virtual Section Section { get; set; }

        public virtual SectionEdit SectionEdit { get; set; }

        public virtual SectionExplanation SectionExplanation { get; set; }

        public virtual SectionEvidence SectionEvidence { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        [Required]
        public virtual ApplicationUser User { get; set; }

        public bool Edited { get; set; }

        public DateTimeOffset? LastEdit { get; set; }

        public string LastText { get; set; }

        public string OriginalText { get; set; }

        public virtual ICollection<SectionVote> Votes { get; set; }

        public virtual SectionComment Parent { get; set; }

        public virtual ICollection<SectionComment> Comments { get; set; }
    }

    public class SectionCommentVM
    {
        [Key]
        public int CommentID { get; set; }

        public string Text { get; set; }

        [ForeignKey("Section_SectionID")]
        public virtual SectionVM Section { get; set; }
        public int? Section_SectionID { get; set; }

        [ForeignKey("SectionExplanation_SectionExplanationId")]
        public virtual SectionExplanationVM SectionExplanation { get; set; }
        public int? SectionExplanation_SectionExplanationId { get; set; }

        [ForeignKey("SectionEvidence_SectionEvidenceId")]
        public virtual SectionEvidenceVM SectionEvidence { get; set; }
        public int? SectionEvidence_SectionEvidenceId { get; set; }

        [ForeignKey("SectionEdit_SectionEditId")]
        public virtual SectionEditVM SectionEdit { get; set; }
        public int? SectionEdit_SectionEditId { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        [ForeignKey("User_ID")]
        public virtual ApplicationUser User { get; set; }
        public string User_ID { get; set; }

        public bool Edited { get; set; }

        public DateTimeOffset? LastEdit { get; set; }

        public string LastText { get; set; }

        public string OriginalText { get; set; }

        public double? Positives { get; set; }

        public double? Negatives { get; set; }

        [ForeignKey("Parent_CommentID")]
        public virtual SectionCommentVM Parent { get; set; }
        public int? Parent_CommentID { get; set; }

        [ForeignKey("Parent_CommentID")]
        public virtual ICollection<SectionCommentVM> Comments { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public SectionCommentVM() { }

        public SectionCommentVM(SectionComment comment) {
            this.CommentID = comment.CommentID;
            this.Text = comment.Text;
            this.CreatedDate = comment.CreatedDate;
            this.Edited = comment.Edited;
            this.LastEdit = comment.LastEdit;
            this.OriginalText = comment.OriginalText;
        }
    }

    public class SectionCommentIM
    {
        [Key]
        public int? SectionID { get; set; }

        public int? SectionEditID { get; set; }

        public int? SectionCommentID { get; set; }

        public int? SectionExplanationID { get; set; }

        public int? SectionEvidenceID { get; set; }

        public string Text { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public string User_ID { get; set; }

        public int OrganisationCategoryId { get; set; }
    }

    public class SectionVote
    {
        public SectionVote() { }

        public SectionVote(SectionVoteIM sectionVoteIM)
        {
            this.IsPositive = sectionVoteIM.IsPositive;
            this.DateCreated = sectionVoteIM.CreatedDate;
            this.User = new ApplicationUser(){ Id = sectionVoteIM.User_ID };
            if (sectionVoteIM.SectionID != 0)
            {
                int id = sectionVoteIM.SectionID ?? 0;
                this.Section = new Section() { SectionID = id };
            }
            if (sectionVoteIM.SectionEditID != 0)
            {
                int id = sectionVoteIM.SectionEditID ?? 0;
                this.SectionEdit = new SectionEdit() { SectionEditId = id };
            }
            if (sectionVoteIM.SectionEvidenceID != 0)
            {
                int id = sectionVoteIM.SectionEvidenceID ?? 0;
                this.SectionEvidence = new SectionEvidence() { SectionEvidenceId = id };
            }
            if (sectionVoteIM.SectionExplanationID != 0)
            {
                int id = sectionVoteIM.SectionExplanationID ?? 0;
                this.SectionExplanation = new SectionExplanation() { SectionExplanationId = id };
            }
            if (sectionVoteIM.SectionCommentID != 0)
            {
                int id = sectionVoteIM.SectionCommentID ?? 0;
                this.SectionComment = new SectionComment() { CommentID = id };
            }
        }

        [Key]
        public int VoteID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        [Required]
        public bool IsPositive { get; set; }

        [Required]
        [ForeignKey("User_Id")]
        public virtual ApplicationUser User { get; set; }
        public string User_Id { get; set; }

        public Section Section { get; set; }

        [ForeignKey("SectionEdit_SectionEditId")]
        public SectionEdit SectionEdit { get; set; }
        public int? SectionEdit_SectionEditId { get; set; }

        public SectionComment SectionComment { get; set; }

        public SectionExplanation SectionExplanation { get; set; }

        public SectionEvidence SectionEvidence { get; set; }
    }

    public class SectionVoteVM
    {
        public SectionVoteVM(SectionVote sectionVote)
        {
            if (sectionVote == null) return;

            this.VoteID = sectionVote.VoteID;
            this.DateCreated = sectionVote.DateCreated;
            this.IsPositive = sectionVote.IsPositive;
            this.User = sectionVote.User;
        }
        public int VoteID { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public bool IsPositive { get; set; }

        public virtual ApplicationUser User { get; set; }
    }

    [Serializable]
    public class SectionVoteIM
    {
        [Key]
        public int? SectionID { get; set; }

        public int? SectionEditID { get; set; }

        public int? SectionCommentID { get; set; }

        public int? SectionExplanationID { get; set; }

        public int? SectionEvidenceID { get; set; }

        public bool IsPositive { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public string User_ID { get; set; }

        public int OrganisationCategoryId { get; set; }
    }
}