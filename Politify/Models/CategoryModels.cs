﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Politify.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }
        public int Order { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public int OrganisationType { get; set; }

        public virtual IQueryable<Discussion> Discussions { get; set; }

        public virtual ICollection<OrganisationCategory> OrgCat { get; set; }
    }
}