﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Politify.Models
{
    public class Postcode
    {
        [Key]
        [MaxLength(15)]
        public string PostCode { get; set; }

        public Organisation Ward { get; set; }

        [MaxLength(9)]
        public string Osward { get; set; }

        [MaxLength(9)]
        public string Oscty { get; set; }

        [MaxLength(9)]
        public string Oslaua { get; set; }

        [MaxLength(9)]
        public string Parish { get; set; }

        [MaxLength(9)]
        public string Pcon { get; set; }

        [MaxLength(9)]
        public string Eer { get; set; }

        [MaxLength(9)]
        public string Ctry { get; set; }
    }
}