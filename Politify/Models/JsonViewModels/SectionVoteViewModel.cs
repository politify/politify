﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Politify.Models.JsonViewModels
{
    public class SectionVoteViewModel
    {
        public bool wasExistingVote { get; set; }

        public bool existingVoteWasPositive { get; set; }

        public SectionVoteVM sectionVote { get; set; }

        #region Vote
        
        //public int CommentID { get; set; }
        //
        //public string Text { get; set; }
        //
        //public DateTimeOffset CreatedDate { get; set; }
        //
        //public virtual ApplicationUser User { get; set; }
        //
        //public bool Edited { get; set; }
        //
        //public DateTimeOffset? LastEdit { get; set; }
        //
        //public string LastText { get; set; }
        //
        //public string OriginalText { get; set; }
        //
        //public virtual Comment Parent { get; set; }
        //
        //public virtual IQueryable<Comment> Comments { get; set; }
        //
        //public virtual Organisation Organisation { get; set; }
        //
        //public virtual Staff Staff { get; set; }
        //
        //public virtual SignatureCount SignatureCount { get; set; }
        //
        //public virtual Party Party { get; set; }

        #endregion
    }
}