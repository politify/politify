﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Politify.Startup))]
namespace Politify
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
