namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SectionVotesCommentsDateCreated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sections", "DateCreated", c => c.DateTimeOffset(nullable: false, precision: 7));
            AddColumn("dbo.SectionVotes", "Section_SectionID", c => c.Int());
            CreateIndex("dbo.SectionVotes", "Section_SectionID");
            AddForeignKey("dbo.SectionVotes", "Section_SectionID", "dbo.Sections", "SectionID");
            DropIndex("dbo.Sections", "IX_Category_CategoryId");
            AlterColumn("dbo.Sections", "Category_CategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Sections", "Category_CategoryId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SectionVotes", "Section_SectionID", "dbo.Sections");
            DropIndex("dbo.SectionVotes", new[] { "Section_SectionID" });
            DropColumn("dbo.SectionVotes", "Section_SectionID");
            DropColumn("dbo.Sections", "DateCreated");
            DropIndex("dbo.Sections", "IX_Category_CategoryId");
            AlterColumn("dbo.Sections", "Category_CategoryId", c => c.Int());
            CreateIndex("dbo.Sections", "Category_CategoryId");
        }
    }
}
