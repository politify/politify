namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganisationVmUpdate131 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.OrganisationVMs");
            DropForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations");
            DropIndex("dbo.Votes", new[] { "Organisation_OrganisationId" });
            Sql(@"
CREATE VIEW [dbo].[DiscussionVMs]
AS
SELECT        dbo.Discussions.DiscussionId, dbo.Discussions.DateCreated, dbo.Discussions.Category_CategoryId, dbo.Discussions.Section_SectionID, dbo.Discussions.User_Id,
                          dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.IsUserCreated, dbo.Sections.Organisation_OrganisationId
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions ON dbo.Sections.SectionID = dbo.Discussions.Section_SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON dbo.Discussions.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON dbo.Discussions.Section_SectionID = c.SectionID

");
            AlterColumn("dbo.Votes", "Organisation_OrganisationId", c => c.Int());
            CreateIndex("dbo.Votes", "Organisation_OrganisationId");
            AddForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.DiscussionVMs", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.DiscussionVMs", "Section_SectionID", "dbo.Sections");
            DropForeignKey("dbo.DiscussionVMs", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.DiscussionVMs", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Votes", new[] { "Organisation_OrganisationId" });
            DropIndex("dbo.DiscussionVMs", new[] { "Organisation_OrganisationId" });
            DropIndex("dbo.DiscussionVMs", new[] { "Category_CategoryId" });
            DropIndex("dbo.DiscussionVMs", new[] { "User_Id" });
            DropIndex("dbo.DiscussionVMs", new[] { "Section_SectionID" });
            AlterColumn("dbo.Votes", "Organisation_OrganisationId", c => c.Int(nullable: false));
            Sql("Drop View dbo.DiscussionVMs");
            CreateIndex("dbo.Votes", "Organisation_OrganisationId");
            AddForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId", cascadeDelete: true);
            AddForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.OrganisationVMs", "OrganisationId", cascadeDelete: true);
        }
    }
}
