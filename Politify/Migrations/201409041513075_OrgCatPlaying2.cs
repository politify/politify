namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrgCatPlaying2 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER VIEW [dbo].[OrganisationCategoryVMs]
AS
SELECT        dbo.Categories.CategoryId, dbo.Categories.[Order], dbo.Categories.Title, dbo.Categories.Description, dbo.OrganisationCategories.Organisation_OrganisationId, 
                         dbo.OrganisationCategories.OrganisationCategoryId
FROM            dbo.Categories INNER JOIN
                         dbo.OrganisationCategories ON dbo.Categories.CategoryId = dbo.OrganisationCategories.Category_CategoryId

");
            Sql(@"
ALTER VIEW [dbo].[DiscussionVMs]
AS
SELECT        dbo.Discussions.DiscussionId, dbo.Discussions.DateCreated, dbo.Discussions.Section_SectionID, dbo.Discussions.User_Id, dbo.Sections.Text, dbo.Sections.Html, 
                         dbo.Sections.IsUserCreated, dbo.Sections.Organisation_OrganisationId, v.Positives, v.Negatives, c.CommentCount, 
                         dbo.Discussions.OrgCat_OrganisationCategoryId
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions ON dbo.Sections.SectionID = dbo.Discussions.Section_SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON dbo.Discussions.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON dbo.Discussions.Section_SectionID = c.SectionID

");

        }
        
        public override void Down()
        {
            Sql(@"
ALTER VIEW [dbo].[OrganisationCategoryVMs]
AS
SELECT        dbo.Categories.CategoryId, dbo.Categories.[Order], dbo.Categories.Title, dbo.Categories.Description, dbo.OrganisationCategories.Organisation_OrganisationId
FROM            dbo.Categories INNER JOIN
                         dbo.OrganisationCategories ON dbo.Categories.CategoryId = dbo.OrganisationCategories.Category_CategoryId

");
            Sql(@"
ALTER VIEW [dbo].[DiscussionVMs]
AS
SELECT        dbo.Discussions.DiscussionId, dbo.Discussions.DateCreated, dbo.Discussions.Category_CategoryId, dbo.Discussions.Section_SectionID, dbo.Discussions.User_Id,
                          dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.IsUserCreated, dbo.Sections.Organisation_OrganisationId
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions ON dbo.Sections.SectionID = dbo.Discussions.Section_SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON dbo.Discussions.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON dbo.Discussions.Section_SectionID = c.SectionID

");
        }
    }
}
