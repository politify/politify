namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DiscussionRemove : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Discussions", "Category_CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Discussions", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Comments", "Discussion_DiscussionId", "dbo.Discussions");
            DropForeignKey("dbo.Sections", "Discussion_DiscussionId", "dbo.Discussions");
            DropForeignKey("dbo.Votes", "Discussion_DiscussionId", "dbo.Discussions");
            DropIndex("dbo.Comments", new[] { "Discussion_DiscussionId" });
            DropIndex("dbo.Discussions", new[] { "Category_CategoryId" });
            DropIndex("dbo.Discussions", new[] { "User_Id" });
            DropIndex("dbo.Sections", new[] { "Discussion_DiscussionId" });
            DropIndex("dbo.Votes", new[] { "Discussion_DiscussionId" });
            AddColumn("dbo.Sections", "Category_CategoryId", c => c.Int());
            CreateIndex("dbo.Sections", "Category_CategoryId");
            AddForeignKey("dbo.Sections", "Category_CategoryId", "dbo.Categories", "CategoryId");
            DropColumn("dbo.Comments", "Discussion_DiscussionId");
            DropColumn("dbo.Sections", "Discussion_DiscussionId");
            DropColumn("dbo.Votes", "Discussion_DiscussionId");
            DropTable("dbo.Discussions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Discussions",
                c => new
                    {
                        DiscussionId = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTimeOffset(nullable: false, precision: 7),
                        Category_CategoryId = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.DiscussionId);
            
            AddColumn("dbo.Votes", "Discussion_DiscussionId", c => c.Int());
            AddColumn("dbo.Sections", "Discussion_DiscussionId", c => c.Int());
            AddColumn("dbo.Comments", "Discussion_DiscussionId", c => c.Int());
            DropForeignKey("dbo.Sections", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Sections", new[] { "Category_CategoryId" });
            DropColumn("dbo.Sections", "Category_CategoryId");
            CreateIndex("dbo.Votes", "Discussion_DiscussionId");
            CreateIndex("dbo.Sections", "Discussion_DiscussionId");
            CreateIndex("dbo.Discussions", "User_Id");
            CreateIndex("dbo.Discussions", "Category_CategoryId");
            CreateIndex("dbo.Comments", "Discussion_DiscussionId");
            AddForeignKey("dbo.Votes", "Discussion_DiscussionId", "dbo.Discussions", "DiscussionId");
            AddForeignKey("dbo.Sections", "Discussion_DiscussionId", "dbo.Discussions", "DiscussionId");
            AddForeignKey("dbo.Comments", "Discussion_DiscussionId", "dbo.Discussions", "DiscussionId");
            AddForeignKey("dbo.Discussions", "User_Id", "dbo.ApplicationUsers", "Id");
            AddForeignKey("dbo.Discussions", "Category_CategoryId", "dbo.Categories", "CategoryId");
        }
    }
}
