namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FunctionUserCanAddToOrgCat : DbMigration
    {
        public override void Up()
        {
            Sql(@"DROP FUNCTION [dbo].[UserOrganisationCanCrud]");

            Sql(@"
CREATE FUNCTION [dbo].[UserCanAddToOrganisation]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrganisationId int
)
RETURNS bit
AS
BEGIN
	DECLARE @canCrud bit
	declare @wards table(wardId int)

	insert into @wards 
	select Ward_OrganisationId--@postcode = Postcode_PostCode, @workPostcode = WorkPostcode_PostCode
	 from dbo.ApplicationUsers u
	 join dbo.Postcodes p
	 on u.Postcode_PostCode = p.PostCode or u.WorkPostcode_PostCode = p.Postcode
	 where Id = @user_id

	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   organisationid in (select wardid from @wards) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    q
			ON      m.organisationid = q.Parent_OrganisationId		
					)
	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END
");


            Sql(@"
CREATE FUNCTION [dbo].[UserCanAddToOrgCat]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrgCatId int
)
RETURNS bit
AS
BEGIN
	declare @OrganisationId int
	select @OrganisationId = Organisation_OrganisationId from [dbo].OrganisationCategories where OrganisationCategoryId = @OrgCatId

	DECLARE @canCrud bit
	declare @wards table(wardId int)

	insert into @wards 
	select Ward_OrganisationId--@postcode = Postcode_PostCode, @workPostcode = WorkPostcode_PostCode
	 from dbo.ApplicationUsers u
	 join dbo.Postcodes p
	 on u.Postcode_PostCode = p.PostCode or u.WorkPostcode_PostCode = p.Postcode
	 where Id = @user_id

	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   organisationid in (select wardid from @wards) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    q
			ON      m.organisationid = q.Parent_OrganisationId		
					)
	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END
");
        }
        
        public override void Down()
        {
        }
    }
}
