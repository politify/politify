namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserToSection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sections", "IsUserCreated", c => c.Boolean(nullable: false));
            AddColumn("dbo.Sections", "User_Id", c => c.String(maxLength: 128, nullable: true));
            CreateIndex("dbo.Sections", "User_Id");
            AddForeignKey("dbo.Sections", "User_Id", "dbo.ApplicationUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sections", "User_Id", "dbo.ApplicationUsers");
            DropIndex("dbo.Sections", new[] { "User_Id" });
            DropColumn("dbo.Sections", "User_Id");
            DropColumn("dbo.Sections", "IsUserCreated");
        }
    }
}
