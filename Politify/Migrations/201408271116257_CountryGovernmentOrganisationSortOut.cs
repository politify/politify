namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountryGovernmentOrganisationSortOut : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Organisations", "Country_CountryID", c => c.Int());
            CreateIndex("dbo.Organisations", "Country_CountryID");
            AddForeignKey("dbo.Organisations", "Country_CountryID", "dbo.Countries", "CountryID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Organisations", "Country_CountryID", "dbo.Countries");
            DropIndex("dbo.Organisations", new[] { "Country_CountryID" });
            DropColumn("dbo.Organisations", "Country_CountryID");
        }
    }
}
