namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedOldViewModels : DbMigration
    {
        public override void Up()
        {
            Sql("drop view dbo.PoliticianViewModel");
            Sql("drop view dbo.LawViewModel");
            Sql("drop view dbo.PartyViewModel");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PartyViewModel",
                c => new
                    {
                        PartyID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.PartyID);
            
            CreateTable(
                "dbo.LawViewModel",
                c => new
                    {
                        LawID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        YearPassed = c.Int(nullable: false),
                        YearPublicationNumber = c.Int(nullable: false),
                        Positives = c.Double(),
                        Negatives = c.Double(),
                        Score = c.Double(),
                    })
                .PrimaryKey(t => t.LawID);
            
            CreateTable(
                "dbo.PoliticianViewModel",
                c => new
                    {
                        PoliticianID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Positives = c.Double(),
                        Negatives = c.Double(),
                        PartyID = c.Int(nullable: false),
                        PartyName = c.String(),
                        ImageFilePath = c.String(),
                        Constituency = c.String(),
                        Score = c.Double(),
                    })
                .PrimaryKey(t => t.PoliticianID);
            
        }
    }
}
