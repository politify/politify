namespace Politify.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.IO;
    using Politify.Code;
    
    public partial class OrganisationVMManyToMany : DbMigration
    {
        public override void Up()
        {
            var dataDir = AppDomain.CurrentDomain.GetData("DataDirectory");
            string standingDataDir = (dataDir == null ? "C:/inetpub/wwwroot/UAT/App_Data" : dataDir.ToString());

            standingDataDir += "/SQL/Standing Data/";

            PolitifyDbContext db = new PolitifyDbContext();

            var oldTimeout = db.Database.CommandTimeout;
            db.Database.CommandTimeout = 72000;

            List<string> files = new List<string>() { "dbo.OrganisationOrganisations.Table.sql" };


            foreach (string dir in files)
            {
                db.Database.ExecuteSqlCommand(File.ReadAllText(standingDataDir + dir));
            }

            db.Database.CommandTimeout = oldTimeout;

            Sql(@"
    ALTER FUNCTION [dbo].[UserCanAddToOrganisation]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrganisationId int
)
RETURNS bit
AS
BEGIN
	DECLARE @canCrud bit
	declare @wards table(wardId int)

	insert into @wards 
	select Ward_OrganisationId--@postcode = Postcode_PostCode, @workPostcode = WorkPostcode_PostCode
	 from dbo.ApplicationUsers u
	 join dbo.Postcodes p
	 on u.Postcode_PostCode = p.PostCode or u.WorkPostcode_PostCode = p.Postcode
	 where Id = @user_id

	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   organisationid in (select wardid from @wards) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    [dbo].[OrganisationOrganisations] oo 
				on m.OrganisationId = oo.Organisation_OrganisationId
			JOIN	q
				ON q.OrganisationId = oo.Organisation_OrganisationId1)

	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END

");

            Sql(@"
ALTER FUNCTION [dbo].[UserCanAddToOrgCat]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrgCatId int
)
RETURNS bit
AS
BEGIN
	declare @OrganisationId int
	select @OrganisationId = Organisation_OrganisationId from [dbo].OrganisationCategories where OrganisationCategoryId = @OrgCatId

	DECLARE @canCrud bit
	declare @wards table(wardId int)

	insert into @wards 
	select Ward_OrganisationId--@postcode = Postcode_PostCode, @workPostcode = WorkPostcode_PostCode
	 from dbo.ApplicationUsers u
	 join dbo.Postcodes p
	 on u.Postcode_PostCode = p.PostCode or u.WorkPostcode_PostCode = p.Postcode
	 where Id = @user_id
	 	
	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   organisationid in (select wardid from @wards) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    [dbo].[OrganisationOrganisations] oo 
				on m.OrganisationId = oo.Organisation_OrganisationId
			JOIN	q
				ON q.OrganisationId = oo.Organisation_OrganisationId1)	

	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END

");
        }
        
        public override void Down()
        {
            Sql(@"Delete from [dbo].[OrganisationOrganisations]");

            Sql(@"
ALTER FUNCTION [dbo].[UserCanAddToOrganisation]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrganisationId int
)
RETURNS bit
AS
BEGIN
	DECLARE @canCrud bit
	declare @wards table(wardId int)

	insert into @wards 
	select Ward_OrganisationId--@postcode = Postcode_PostCode, @workPostcode = WorkPostcode_PostCode
	 from dbo.ApplicationUsers u
	 join dbo.Postcodes p
	 on u.Postcode_PostCode = p.PostCode or u.WorkPostcode_PostCode = p.Postcode
	 where Id = @user_id

	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   organisationid in (select wardid from @wards) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    q
			ON      m.organisationid = q.Parent_OrganisationId		
					)
	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END

");

            Sql(@"
ALTER FUNCTION [dbo].[UserCanAddToOrgCat]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrgCatId int
)
RETURNS bit
AS
BEGIN
	declare @OrganisationId int
	select @OrganisationId = Organisation_OrganisationId from [dbo].OrganisationCategories where OrganisationCategoryId = @OrgCatId

	DECLARE @canCrud bit
	declare @wards table(wardId int)

	insert into @wards 
	select Ward_OrganisationId--@postcode = Postcode_PostCode, @workPostcode = WorkPostcode_PostCode
	 from dbo.ApplicationUsers u
	 join dbo.Postcodes p
	 on u.Postcode_PostCode = p.PostCode or u.WorkPostcode_PostCode = p.Postcode
	 where Id = @user_id

	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   organisationid in (select wardid from @wards) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    q
			ON      m.organisationid = q.Parent_OrganisationId		
					)
	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END

");
        }
    }
}
