namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganisationMultipleParents : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Organisations", "Parent_OrganisationId", "dbo.Organisations");
            DropIndex("dbo.Organisations", new[] { "Parent_OrganisationId" });
            CreateTable(
                "dbo.OrganisationOrganisations",
                c => new
                    {
                        Organisation_OrganisationId = c.Int(nullable: false),
                        Organisation_OrganisationId1 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organisation_OrganisationId, t.Organisation_OrganisationId1 })
                .ForeignKey("dbo.Organisations", t => t.Organisation_OrganisationId)
                .ForeignKey("dbo.Organisations", t => t.Organisation_OrganisationId1)
                .Index(t => t.Organisation_OrganisationId)
                .Index(t => t.Organisation_OrganisationId1);
            
            DropColumn("dbo.Organisations", "Parent_OrganisationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Organisations", "Parent_OrganisationId", c => c.Int());
            DropForeignKey("dbo.OrganisationOrganisations", "Organisation_OrganisationId1", "dbo.Organisations");
            DropForeignKey("dbo.OrganisationOrganisations", "Organisation_OrganisationId", "dbo.Organisations");
            DropIndex("dbo.OrganisationOrganisations", new[] { "Organisation_OrganisationId1" });
            DropIndex("dbo.OrganisationOrganisations", new[] { "Organisation_OrganisationId" });
            DropTable("dbo.OrganisationOrganisations");
            CreateIndex("dbo.Organisations", "Parent_OrganisationId");
            AddForeignKey("dbo.Organisations", "Parent_OrganisationId", "dbo.Organisations", "OrganisationId");
        }
    }
}
