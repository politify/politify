namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserCanAddFunctionsChange : DbMigration
    {
        public override void Up()
        {
            Sql(
                @"
ALTER FUNCTION [dbo].[UserCanAddToOrganisation]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrganisationId int
)
RETURNS bit
AS
BEGIN
	DECLARE @canCrud bit
	declare @orgOns table(onscd varchar(9))

	insert into @orgOns 
	SELECT distinct value
	  FROM ( select * from [Politify].[dbo].[Postcodes] p
		  join ApplicationUsers u
		  on p.PostCode = u.Postcode_PostCode or p.PostCode = u.WorkPostcode_PostCode
		  where u.Id = @user_id
		 ) p
	  unpivot
	(
	  value
	  for col in (osward, oscty, oslaua, Pcon)
	) un

	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   onscode in (select onscd from @orgOns) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    [dbo].[OrganisationOrganisations] oo 
				on m.OrganisationId = oo.Organisation_OrganisationId
			JOIN	q
				ON q.OrganisationId = oo.Organisation_OrganisationId1)

	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END

");
            Sql(@"
ALTER FUNCTION [dbo].[UserCanAddToOrgCat]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrgCatId int
)
RETURNS bit
AS
BEGIN
	declare @OrganisationId int
	select @OrganisationId = Organisation_OrganisationId from [dbo].OrganisationCategories where OrganisationCategoryId = @OrgCatId

	DECLARE @canCrud bit
	declare @orgOns table(onscd varchar(9))

	
	insert into @orgOns 
	SELECT distinct value
	  FROM ( select * from [Politify].[dbo].[Postcodes] p
		  join ApplicationUsers u
		  on p.PostCode = u.Postcode_PostCode or p.PostCode = u.WorkPostcode_PostCode
		  where u.Id = @user_id
		 ) p
	  unpivot
	(
	  value
	  for col in (osward, oscty, oslaua, Pcon)
	) un
	 	
	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   onscode in (select onscd from @orgOns) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    [dbo].[OrganisationOrganisations] oo 
				on m.OrganisationId = oo.Organisation_OrganisationId
			JOIN	q
				ON q.OrganisationId = oo.Organisation_OrganisationId1)	

	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END
");
        }
        
        public override void Down()
        {
            Sql(@"
ALTER FUNCTION [dbo].[UserCanAddToOrganisation]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrganisationId int
)
RETURNS bit
AS
BEGIN
	DECLARE @canCrud bit
	declare @wards table(wardId int)

	insert into @wards 
	select Ward_OrganisationId--@postcode = Postcode_PostCode, @workPostcode = WorkPostcode_PostCode
	 from dbo.ApplicationUsers u
	 join dbo.Postcodes p
	 on u.Postcode_PostCode = p.PostCode or u.WorkPostcode_PostCode = p.Postcode
	 where Id = @user_id

	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   organisationid in (select wardid from @wards) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    [dbo].[OrganisationOrganisations] oo 
				on m.OrganisationId = oo.Organisation_OrganisationId
			JOIN	q
				ON q.OrganisationId = oo.Organisation_OrganisationId1)

	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END
");

            Sql(@"
ALTER FUNCTION [dbo].[UserCanAddToOrgCat]
(
	-- Add the parameters for the function here
	@user_id nvarchar(128), 
	@OrgCatId int
)
RETURNS bit
AS
BEGIN
	declare @OrganisationId int
	select @OrganisationId = Organisation_OrganisationId from [dbo].OrganisationCategories where OrganisationCategoryId = @OrgCatId

	DECLARE @canCrud bit
	declare @wards table(wardId int)

	insert into @wards 
	select Ward_OrganisationId--@postcode = Postcode_PostCode, @workPostcode = WorkPostcode_PostCode
	 from dbo.ApplicationUsers u
	 join dbo.Postcodes p
	 on u.Postcode_PostCode = p.PostCode or u.WorkPostcode_PostCode = p.Postcode
	 where Id = @user_id
	 	
	;WITH q AS 
			(
			SELECT *
			FROM    [dbo].[Organisations]
			WHERE   organisationid in (select wardid from @wards) -- this condition defines the ultimate ancestors in your chain, change it as appropriate
			UNION all
			SELECT  m.*
			FROM    [dbo].[Organisations] m
			JOIN    [dbo].[OrganisationOrganisations] oo 
				on m.OrganisationId = oo.Organisation_OrganisationId
			JOIN	q
				ON q.OrganisationId = oo.Organisation_OrganisationId1)	

	SELECT @canCrud = Count(*)
	FROM    q where  q.OrganisationId = @OrganisationId

	RETURN @canCrud
END
");
        }
    }
}
