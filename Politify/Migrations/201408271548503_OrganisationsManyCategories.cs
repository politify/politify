namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganisationsManyCategories : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrganisationCategories",
                c => new
                    {
                        Organisation_OrganisationId = c.Int(nullable: false),
                        Category_CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organisation_OrganisationId, t.Category_CategoryId })
                .ForeignKey("dbo.Organisations", t => t.Organisation_OrganisationId, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_CategoryId, cascadeDelete: true)
                .Index(t => t.Organisation_OrganisationId)
                .Index(t => t.Category_CategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrganisationCategories", "Category_CategoryId", "dbo.Categories");
            DropForeignKey("dbo.OrganisationCategories", "Organisation_OrganisationId", "dbo.Organisations");
            DropIndex("dbo.OrganisationCategories", new[] { "Category_CategoryId" });
            DropIndex("dbo.OrganisationCategories", new[] { "Organisation_OrganisationId" });
            DropTable("dbo.OrganisationCategories");
        }
    }
}
