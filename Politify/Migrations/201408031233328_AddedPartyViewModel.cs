namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPartyViewModel : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE VIEW [dbo].[PartyViewModel]
AS
SELECT        PartyID, Name
FROM            dbo.Parties
");
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PartyViewModel");
        }
    }
}
