namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DiscussionVMScore : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER VIEW [dbo].[DiscussionVMs] AS 
SELECT        d.DiscussionId, d.DateCreated, d.Section_SectionID, d.User_Id, dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.IsUserCreated, 
                         dbo.Sections.Organisation_OrganisationId, v.Positives, v.Negatives, c.CommentCount, d.OrgCat_OrganisationCategoryId, (COALESCE(v.Positives, 0) - COALESCE(v.Negatives, 0)) as Score
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions AS d ON dbo.Sections.SectionID = d.Section_SectionID LEFT OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (Section_SectionID IS NOT NULL)
                               GROUP BY Section_SectionID) AS v ON d.Section_SectionID = v.SectionID LEFT OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (Section_SectionID IS NOT NULL)
                               GROUP BY Section_SectionID) AS c ON d.Section_SectionID = c.SectionID
");
        }
        
        public override void Down()
        {            
            Sql(@"
ALTER VIEW [dbo].[DiscussionVMs] AS 
SELECT        d.DiscussionId, d.DateCreated, d.Section_SectionID, d.User_Id, dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.IsUserCreated, 
                         dbo.Sections.Organisation_OrganisationId, v.Positives, v.Negatives, c.CommentCount, d.OrgCat_OrganisationCategoryId
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions AS d ON dbo.Sections.SectionID = d.Section_SectionID LEFT OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (Section_SectionID IS NOT NULL)
                               GROUP BY Section_SectionID) AS v ON d.Section_SectionID = v.SectionID LEFT OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (Section_SectionID IS NOT NULL)
                               GROUP BY Section_SectionID) AS c ON d.Section_SectionID = c.SectionID
");
        }
    }
}
