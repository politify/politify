namespace Politify.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Text;
    using Politify.Code;
    
    public partial class OrgWardsUACountyEER : DbMigration
    {
        public override void Up()
        {
            InsertStandingData("dbo.Organisations.Table.Wards.sql", 
                "INSERT INTO [dbo].[Organisations]([OnsCode],[Name],[OrganisationType],[CountryID])VALUES");

            InsertStandingData("dbo.Organisations.Table.Counties.sql",
                "INSERT INTO [dbo].[Organisations]([OnsCode],[Name],[OrganisationType],[CountryID])VALUES");

            InsertStandingData("dbo.Organisations.Table.LAUA.sql",
                "INSERT INTO [dbo].[Organisations]([OnsCode],[Name],[OrganisationType],[CountryID])VALUES");
        }
        
        public override void Down()
        {
        }

        private void InsertStandingData(string filename, string insertQuery)
        {
            var dataDir = AppDomain.CurrentDomain.GetData("DataDirectory");
            string standingDataDir = (dataDir == null ? "C:/inetpub/wwwroot/UAT/App_Data" : dataDir.ToString());

            standingDataDir += "/SQL/Standing Data/";

            PolitifyDbContext db = new PolitifyDbContext();

            var oldTimeout = db.Database.CommandTimeout;
            db.Database.CommandTimeout = 72000;

            List<string> files = new List<string>() { 
                filename
            };

            int pageSize = 100;
            List<string> queryLines = new List<string>();

            foreach (string dir in files)
            {
                int currLine = 0;

                var fileLines = File.ReadLines(standingDataDir + dir, Encoding.UTF8);

                foreach (var line in fileLines)
                {
                    currLine += 1;

                    queryLines.Add(line);

                    if (currLine % 100 == 0)
                    {
                        db.Database.ExecuteSqlCommand(insertQuery + String.Join(",", queryLines));

                        queryLines.Clear();
                    }
                }

                if (queryLines.Count > 0)
                {
                    db.Database.ExecuteSqlCommand(insertQuery + String.Join(",", queryLines));

                    queryLines.Clear();
                }
            }

            db.Database.CommandTimeout = oldTimeout;
        }
    }
}
