namespace Politify.Migrations
{
    using System;
    using System.Linq;
    using System.Data.Entity.Migrations;
    using Politify.Code;
    using Politify.Models;
    using System.Data.Entity;
    
    public partial class envocalEntity : DbMigration
    {
        public override void Up()
        {
            PolitifyDbContext db = new PolitifyDbContext();

            var envocal = db.Organisations.SingleOrDefault(o => o.OrganisationId == 10000);

            if (envocal == null)
            {
                Sql(@"SET IDENTITY_INSERT [Politify].[dbo].[Organisations] ON");

                Sql(@"
                    
INSERT INTO [Politify].[dbo].[Organisations]
           ([OrganisationId]
		   ,[Name]
           ,[Description]
           ,[ProfilePicture]
           ,[OrganisationType]
           ,[EmailAddress]
           ,[CountryID]
           ,[Parent_OrganisationId]
           ,[OnsCode])
     VALUES
           (10000,
		   'Envocal'
           ,'Envocal the organisation. We want to set a precedent here by allowing you to directly dictate what we are.'
           ,null
           ,9
           ,'envocal@envocal.com'
           ,417
           ,null
           ,null)");

                Sql(@"SET IDENTITY_INSERT [Politify].[dbo].[Organisations] OFF");
            }

        }
        
        public override void Down()
        {            
            PolitifyDbContext db = new PolitifyDbContext();

            var envocal = db.Organisations.SingleOrDefault(o => o.OrganisationId == 10000);

            if (envocal != null)
            {
                var org = db.Organisations.Remove(envocal);
                db.SaveChanges();
            }
        }
    }
}
