namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserPostcodes : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Postcodes");
            AddColumn("dbo.ApplicationUsers", "Postcode_PostCode", c => c.String(maxLength: 15));
            AddColumn("dbo.ApplicationUsers", "WorkPostcode_PostCode", c => c.String(maxLength: 15));
            AlterColumn("dbo.Postcodes", "PostCode", c => c.String(nullable: false, maxLength: 15));
            AddPrimaryKey("dbo.Postcodes", "PostCode");
            CreateIndex("dbo.ApplicationUsers", "Postcode_PostCode");
            CreateIndex("dbo.ApplicationUsers", "WorkPostcode_PostCode");
            AddForeignKey("dbo.ApplicationUsers", "Postcode_PostCode", "dbo.Postcodes", "PostCode");
            AddForeignKey("dbo.ApplicationUsers", "WorkPostcode_PostCode", "dbo.Postcodes", "PostCode");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ApplicationUsers", "WorkPostcode_PostCode", "dbo.Postcodes");
            DropForeignKey("dbo.ApplicationUsers", "Postcode_PostCode", "dbo.Postcodes");
            DropIndex("dbo.ApplicationUsers", new[] { "WorkPostcode_PostCode" });
            DropIndex("dbo.ApplicationUsers", new[] { "Postcode_PostCode" });
            DropPrimaryKey("dbo.Postcodes");
            AlterColumn("dbo.Postcodes", "PostCode", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.ApplicationUsers", "WorkPostcode_PostCode");
            DropColumn("dbo.ApplicationUsers", "Postcode_PostCode");
            AddPrimaryKey("dbo.Postcodes", "PostCode");
        }
    }
}
