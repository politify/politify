namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostcodeNullableWard : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Postcodes", "Ward_OrganisationId", "dbo.Organisations");
            DropIndex("dbo.Postcodes", new[] { "Ward_OrganisationId" });
            AlterColumn("dbo.Postcodes", "Ward_OrganisationId", c => c.Int());
            CreateIndex("dbo.Postcodes", "Ward_OrganisationId");
            AddForeignKey("dbo.Postcodes", "Ward_OrganisationId", "dbo.Organisations", "OrganisationId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Postcodes", "Ward_OrganisationId", "dbo.Organisations");
            DropIndex("dbo.Postcodes", new[] { "Ward_OrganisationId" });
            AlterColumn("dbo.Postcodes", "Ward_OrganisationId", c => c.Int(nullable: false));
            CreateIndex("dbo.Postcodes", "Ward_OrganisationId");
            AddForeignKey("dbo.Postcodes", "Ward_OrganisationId", "dbo.Organisations", "OrganisationId", cascadeDelete: true);
        }
    }
}
