namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrgCatPlaying : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrganisationCategories", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.OrganisationCategories", "Category_CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Discussions", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Discussions", new[] { "Category_CategoryId" });
            DropIndex("dbo.OrganisationCategories", new[] { "Organisation_OrganisationId" });
            DropIndex("dbo.OrganisationCategories", new[] { "Category_CategoryId" });
            DropTable("dbo.OrganisationCategories");
            CreateTable(
                "dbo.OrganisationCategories",
                c => new
                    {
                        OrganisationCategoryId = c.Int(nullable: false, identity: true),
                        Category_CategoryId = c.Int(nullable: false),
                        Organisation_OrganisationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrganisationCategoryId)
                .ForeignKey("dbo.Categories", t => t.Category_CategoryId, cascadeDelete: false)
                .ForeignKey("dbo.Organisations", t => t.Organisation_OrganisationId, cascadeDelete: false)
                .Index(t => t.Category_CategoryId)
                .Index(t => t.Organisation_OrganisationId);
            
            AddColumn("dbo.Discussions", "OrgCat_OrganisationCategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Discussions", "OrgCat_OrganisationCategoryId");
            AddForeignKey("dbo.Discussions", "OrgCat_OrganisationCategoryId", "dbo.OrganisationCategories", "OrganisationCategoryId", cascadeDelete: true);
            DropColumn("dbo.Discussions", "Category_CategoryId");
        }
        
        public override void Down()
        {            
            AddColumn("dbo.Discussions", "Category_CategoryId", c => c.Int(nullable: false));
            DropForeignKey("dbo.OrganisationCategories", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Discussions", "OrgCat_OrganisationCategoryId", "dbo.OrganisationCategories");
            DropIndex("dbo.Discussions", new[] { "OrgCat_OrganisationCategoryId" });
            DropColumn("dbo.Discussions", "OrgCat_OrganisationCategoryId");
            CreateTable(
                "dbo.OrganisationCategories",
                c => new
                    {
                        Organisation_OrganisationId = c.Int(nullable: false),
                        Category_CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organisation_OrganisationId, t.Category_CategoryId });
            DropTable("dbo.OrganisationCategories");
            CreateIndex("dbo.OrganisationCategories", "Category_CategoryId");
            CreateIndex("dbo.OrganisationCategories", "Organisation_OrganisationId");
            CreateIndex("dbo.DiscussionVMs", "Category_CategoryId");
            CreateIndex("dbo.Discussions", "Category_CategoryId");
            AddForeignKey("dbo.Discussions", "Category_CategoryId", "dbo.Categories", "CategoryId", cascadeDelete: true);
            AddForeignKey("dbo.OrganisationCategories", "Category_CategoryId", "dbo.Categories", "CategoryId", cascadeDelete: true);
            AddForeignKey("dbo.OrganisationCategories", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId", cascadeDelete: true);
        }
    }
}
