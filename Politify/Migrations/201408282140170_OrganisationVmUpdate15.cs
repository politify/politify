namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganisationVmUpdate15 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
Alter VIEW [dbo].[DiscussionVMs]
AS
SELECT        dbo.Discussions.DiscussionId, dbo.Discussions.DateCreated, dbo.Discussions.Category_CategoryId, dbo.Discussions.Section_SectionID, dbo.Discussions.User_Id,
                          dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.IsUserCreated, dbo.Sections.Organisation_OrganisationId, v.Positives, v.Negatives, c.CommentCount
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions ON dbo.Sections.SectionID = dbo.Discussions.Section_SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON dbo.Discussions.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON dbo.Discussions.Section_SectionID = c.SectionID
");
        }
        
        public override void Down()
        {
            Sql(@"
Alter VIEW [dbo].[DiscussionVMs]
AS
SELECT        dbo.Discussions.DiscussionId, dbo.Discussions.DateCreated, dbo.Discussions.Category_CategoryId, dbo.Discussions.Section_SectionID, dbo.Discussions.User_Id,
                          dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.IsUserCreated, dbo.Sections.Organisation_OrganisationId
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions ON dbo.Sections.SectionID = dbo.Discussions.Section_SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON dbo.Discussions.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON dbo.Discussions.Section_SectionID = c.SectionID
");
        }
    }
}
