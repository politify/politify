namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class ViewsCommentCountIntToDouble : DbMigration
    {
        public override void Up()
        {
            Sql(@"
Alter VIEW dbo.DiscussionVMs AS SELECT        d.DiscussionId, d.DateCreated, d.Section_SectionID, d.User_Id, dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.IsUserCreated, 
                         dbo.Sections.Organisation_OrganisationId, v.Positives, v.Negatives, c.CommentCount, d.OrgCat_OrganisationCategoryId
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions AS d ON dbo.Sections.SectionID = d.Section_SectionID LEFT OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (Section_SectionID IS NOT NULL)
                               GROUP BY Section_SectionID) AS v ON d.Section_SectionID = v.SectionID LEFT OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (Section_SectionID IS NOT NULL)
                               GROUP BY Section_SectionID) AS c ON d.Section_SectionID = c.SectionID

");

            Sql(@"
Alter VIEW dbo.SectionEditVMs AS  SELECT        edit.SectionEditId, edit.Text, edit.DateCreated, edit.Section_SectionID, edit.User_Id, v.Positives, v.Negatives, c.CommentCount, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionEdits AS edit INNER JOIN
                         dbo.ApplicationUsers ON edit.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionEdit_SectionEditId AS SectionEditID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (SectionEdit_SectionEditId IS NOT NULL)
                               GROUP BY SectionEdit_SectionEditId) AS v ON edit.SectionEditId = v.SectionEditID FULL OUTER JOIN
                             (SELECT        SectionEdit_SectionEditId AS SectionEditID, CONVERT(float, COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (SectionEdit_SectionEditId IS NOT NULL)
                               GROUP BY SectionEdit_SectionEditId) AS c ON edit.SectionEditId = c.SectionEditID

");

            Sql(@"
Alter VIEW dbo.SectionEvidenceVMs AS 
SELECT        ev.SectionEvidenceId, ev.Text, ev.FileName, ev.DateCreated, ev.Section_SectionID, ev.User_Id, v.Positives, v.Negatives, c.CommentCount, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.FirstName, dbo.ApplicationUsers.UserName
FROM            dbo.SectionEvidences AS ev INNER JOIN
                         dbo.ApplicationUsers ON ev.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionEvidence_SectionEvidenceId AS SectionEvID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, 
                                                         CONVERT(float, SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (SectionEvidence_SectionEvidenceId IS NOT NULL)
                               GROUP BY SectionEvidence_SectionEvidenceId) AS v ON ev.SectionEvidenceId = v.SectionEvID FULL OUTER JOIN
                             (SELECT        SectionEvidence_SectionEvidenceId AS SectionEvID, CONVERT(float, COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (SectionEvidence_SectionEvidenceId IS NOT NULL)
                               GROUP BY SectionEvidence_SectionEvidenceId) AS c ON ev.SectionEvidenceId = c.SectionEvID
");

            Sql(@"
Alter VIEW dbo.SectionExplanationVMs AS  SELECT        ex.SectionExplanationId, ex.Text, ex.DateCreated, ex.Section_SectionID, ex.User_Id, v.Positives, v.Negatives, c.CommentCount, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionExplanations AS ex INNER JOIN
                         dbo.ApplicationUsers ON ex.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionExplanation_SectionExplanationId AS SectionExID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, 
                                                         CONVERT(float, SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (SectionExplanation_SectionExplanationId IS NOT NULL)
                               GROUP BY SectionExplanation_SectionExplanationId) AS v ON ex.SectionExplanationId = v.SectionExID FULL OUTER JOIN
                             (SELECT        SectionExplanation_SectionExplanationId AS SectionExID, CONVERT(float,COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (SectionExplanation_SectionExplanationId IS NOT NULL)
                               GROUP BY SectionExplanation_SectionExplanationId) AS c ON ex.SectionExplanationId = c.SectionExID

");
        }

        public override void Down()
        {
            Sql(@"
    ALTER VIEW dbo.DiscussionVMs AS 
SELECT        d.DiscussionId, d.DateCreated, d.Section_SectionID, d.User_Id, dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.IsUserCreated, 
                         dbo.Sections.Organisation_OrganisationId, v.Positives, v.Negatives, c.CommentCount, d.OrgCat_OrganisationCategoryId
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions AS d ON dbo.Sections.SectionID = d.Section_SectionID LEFT OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (Section_SectionID IS NOT NULL)
                               GROUP BY Section_SectionID) AS v ON d.Section_SectionID = v.SectionID LEFT OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (Section_SectionID IS NOT NULL)
                               GROUP BY Section_SectionID) AS c ON d.Section_SectionID = c.SectionID
");
            Sql(@"
    ALTER VIEW dbo.SectionEditVMs AS SELECT        edit.SectionEditId, edit.Text, edit.DateCreated, edit.Section_SectionID, edit.User_Id, v.Positives, v.Negatives, c.CommentCount, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionEdits AS edit INNER JOIN
                         dbo.ApplicationUsers ON edit.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionEdit_SectionEditId AS SectionEditID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (SectionEdit_SectionEditId IS NOT NULL)
                               GROUP BY SectionEdit_SectionEditId) AS v ON edit.SectionEditId = v.SectionEditID FULL OUTER JOIN
                             (SELECT        SectionEdit_SectionEditId AS SectionEditID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (SectionEdit_SectionEditId IS NOT NULL)
                               GROUP BY SectionEdit_SectionEditId) AS c ON edit.SectionEditId = c.SectionEditID
");

            Sql(@"

Alter VIEW dbo.SectionEvidenceVMs AS 
SELECT        ev.SectionEvidenceId, ev.Text, ev.FileName, ev.DateCreated, ev.Section_SectionID, ev.User_Id, v.Positives, v.Negatives, c.CommentCount, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.FirstName, dbo.ApplicationUsers.UserName
FROM            dbo.SectionEvidences AS ev INNER JOIN
                         dbo.ApplicationUsers ON ev.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionEvidence_SectionEvidenceId AS SectionEvID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, 
                                                         CONVERT(float, SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (SectionEvidence_SectionEvidenceId IS NOT NULL)
                               GROUP BY SectionEvidence_SectionEvidenceId) AS v ON ev.SectionEvidenceId = v.SectionEvID FULL OUTER JOIN
                             (SELECT        SectionEvidence_SectionEvidenceId AS SectionEvID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (SectionEvidence_SectionEvidenceId IS NOT NULL)
                               GROUP BY SectionEvidence_SectionEvidenceId) AS c ON ev.SectionEvidenceId = c.SectionEvID

");

            Sql(@"
Alter VIEW dbo.SectionExplanationVMs AS SELECT        ex.SectionExplanationId, ex.Text, ex.DateCreated, ex.Section_SectionID, ex.User_Id, v.Positives, v.Negatives, c.CommentCount, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionExplanations AS ex INNER JOIN
                         dbo.ApplicationUsers ON ex.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionExplanation_SectionExplanationId AS SectionExID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, 
                                                         CONVERT(float, SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               WHERE        (SectionExplanation_SectionExplanationId IS NOT NULL)
                               GROUP BY SectionExplanation_SectionExplanationId) AS v ON ex.SectionExplanationId = v.SectionExID FULL OUTER JOIN
                             (SELECT        SectionExplanation_SectionExplanationId AS SectionExID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               WHERE        (SectionExplanation_SectionExplanationId IS NOT NULL)
                               GROUP BY SectionExplanation_SectionExplanationId) AS c ON ex.SectionExplanationId = c.SectionExID

");
        }
    }
}
