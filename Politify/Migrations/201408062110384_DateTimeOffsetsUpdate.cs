namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTimeOffsetsUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Businesses", "DateFormed", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.Governments", "DateFormed", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.GovernmentComments", "CreatedDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.GovernmentComments", "LastEdit", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.GovernmentVotes", "CreatedDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.LawComments", "CreatedDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.LawComments", "LastEdit", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.Laws", "DatePassed", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.Laws", "DateCreated", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.LawVotes", "CreatedDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.PoliticianComments", "CreatedDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.PoliticianComments", "LastEdit", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.Politicians", "DateCreated", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.PoliticianVotes", "CreatedDate", c => c.DateTimeOffset(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PoliticianVotes", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Politicians", "DateCreated", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PoliticianComments", "LastEdit", c => c.DateTime());
            AlterColumn("dbo.PoliticianComments", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.LawVotes", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Laws", "DateCreated", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Laws", "DatePassed", c => c.DateTime());
            AlterColumn("dbo.LawComments", "LastEdit", c => c.DateTime());
            AlterColumn("dbo.LawComments", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.GovernmentVotes", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.GovernmentComments", "LastEdit", c => c.DateTime());
            AlterColumn("dbo.GovernmentComments", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Governments", "DateFormed", c => c.DateTime());
            AlterColumn("dbo.Businesses", "DateFormed", c => c.DateTime());
        }
    }
}
