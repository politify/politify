// <auto-generated />
namespace Politify.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class ConstituencyCategory : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ConstituencyCategory));
        
        string IMigrationMetadata.Id
        {
            get { return "201501242039567_ConstituencyCategory"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
