namespace Politify.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Text;
    using Politify.Code;
    
    public partial class PostcodesData : DbMigration
    {
        public override void Up()
        {
            var dataDir = AppDomain.CurrentDomain.GetData("DataDirectory");
            string standingDataDir = (dataDir == null ? "C:/inetpub/wwwroot/UAT/App_Data" : dataDir.ToString());

            standingDataDir += "/SQL/Standing Data/New Postcodes/";

            PolitifyDbContext db = new PolitifyDbContext();

            var oldTimeout = db.Database.CommandTimeout;
            db.Database.CommandTimeout = 72000;

            List<string> files = new List<string>() { 
                "ONSPD_NOV_2013_UK.001.csv",
                "ONSPD_NOV_2013_UK.002.csv",
                "ONSPD_NOV_2013_UK.003.csv",
                "ONSPD_NOV_2013_UK.004.csv",
                "ONSPD_NOV_2013_UK.005.csv",
                "ONSPD_NOV_2013_UK.006.csv",
                "ONSPD_NOV_2013_UK.007.csv",
                "ONSPD_NOV_2013_UK.008.csv",
                "ONSPD_NOV_2013_UK.009.csv",
                "ONSPD_NOV_2013_UK.010.csv",
                "ONSPD_NOV_2013_UK.011.csv",
                "ONSPD_NOV_2013_UK.012.csv"
            };


            int pageSize = 100;
            List<string> queryLines = new List<string>();

            foreach (string dir in files)
            {
                int currLine = 0;

                var fileLines = File.ReadLines(standingDataDir + dir, Encoding.UTF8);

                foreach (var line in fileLines)
                {
                    currLine += 1;

                    queryLines.Add(line);

                    if (currLine % 100 == 0)
                    {
                        db.Database.ExecuteSqlCommand(@"INSERT INTO [dbo].[Postcodes]([PostCode],[Oscty],[Oslaua],[Osward],[Ctry],[Pcon],[Eer],[Parish])VALUES" 
                            + String.Join(",", queryLines));

                        queryLines.Clear();
                    }
                }

                if (queryLines.Count > 0)
                {
                    Sql(@"INSERT INTO [dbo].[Postcodes]([PostCode],[Oscty],[Oslaua],[Osward],[Ctry],[Pcon],[Eer],[Parish])VALUES" 
                        + String.Join(",", queryLines));

                    queryLines.Clear();
                }
            }

            db.Database.CommandTimeout = oldTimeout;
        }
        
        public override void Down()
        {
        }
    }
}
