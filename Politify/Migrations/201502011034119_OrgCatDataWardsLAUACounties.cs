namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrgCatDataWardsLAUACounties : DbMigration
    {
        public override void Up()
        {
            Sql(@"
declare @cats table(CategoryId int)

insert into @cats
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 1

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats c on 1 = 1
  where OrganisationType = 1
");

            Sql(@"
declare @cats table(CategoryId int)

insert into @cats
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 3

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats c on 1 = 1
  where OrganisationType = 3
");

            Sql(@"
declare @cats table(CategoryId int)

insert into @cats
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 4

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats c on 1 = 1
  where OrganisationType = 4
");
        }
        
        public override void Down()
        {
        }
    }
}
