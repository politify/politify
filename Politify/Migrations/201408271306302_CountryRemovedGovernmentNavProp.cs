namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountryRemovedGovernmentNavProp : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Organisations", "CountryID", "dbo.Countries");
            DropForeignKey("dbo.Organisations", "Country_CountryID", "dbo.Countries");
            DropIndex("dbo.Organisations", new[] { "Country_CountryID" });
            DropIndex("dbo.Organisations", new[] { "CountryID" });
            AlterColumn("dbo.Organisations", "CountryID", c => c.Int(nullable: false));
            DropColumn("dbo.Organisations", "Country_CountryID");
            CreateIndex("dbo.Organisations", "CountryID");
            AddForeignKey("dbo.Organisations", "CountryID", "dbo.Countries", "CountryID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Organisations", "CountryID", "dbo.Countries");
            DropIndex("dbo.Organisations", new[] { "CountryID" });
            AlterColumn("dbo.Organisations", "CountryID", c => c.Int());
            AddColumn("dbo.Organisations", "Country_CountryID", c => c.Int());
            AddColumn("dbo.Organisations", "CountryID", c => c.Int(nullable: false));
            CreateIndex("dbo.Organisations", "CountryID");
            CreateIndex("dbo.Organisations", "Country_CountryID");
            AddForeignKey("dbo.Organisations", "Country_CountryID", "dbo.Countries", "CountryID");
            AddForeignKey("dbo.Organisations", "CountryID", "dbo.Countries", "CountryID");
        }
    }
}
