namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SectionCommentDateTimeOffset : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SectionComments", "CreatedDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.SectionComments", "LastEdit", c => c.DateTimeOffset(precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SectionComments", "LastEdit", c => c.DateTime());
            AlterColumn("dbo.SectionComments", "CreatedDate", c => c.DateTime(nullable: false));
        }
    }
}
