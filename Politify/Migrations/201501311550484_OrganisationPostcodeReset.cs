namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganisationPostcodeReset : DbMigration
    {
        public override void Up()
        {
            Sql(@"
delete from organisationorganisations
");

            Sql(@"
  delete OrganisationCategories from organisationcategories oc join organisations o on oc.Organisation_OrganisationId = o.OrganisationId
	where o.OrganisationType not in (5, 9, 6)"
                );

            Sql(@"update applicationusers set [Postcode_PostCode] = null, [WorkPostcode_PostCode] = null");

            Sql(@"Delete from Postcodes");

            Sql(
                @"
  delete from organisations where OrganisationType not in (5, 9, 6)"
                );

        }
        
        public override void Down()
        {
        }
    }
}
