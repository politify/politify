// <auto-generated />
namespace Politify.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class StaffColumns : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(StaffColumns));
        
        string IMigrationMetadata.Id
        {
            get { return "201501241732563_StaffColumns"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
