namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSectionComments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SectionComments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        Edited = c.Boolean(nullable: false),
                        LastEdit = c.DateTime(),
                        LastText = c.String(),
                        OriginalText = c.String(),
                        User_Id = c.String(nullable: false, maxLength: 128),
                        SectionEdit_SectionEditId = c.Int(),
                        SectionEvidence_SectionExplanationId = c.Int(),
                        SectionExplanation_SectionExplanationId = c.Int(),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.SectionEdits", t => t.SectionEdit_SectionEditId)
                .ForeignKey("dbo.SectionEvidences", t => t.SectionEvidence_SectionExplanationId)
                .ForeignKey("dbo.SectionExplanations", t => t.SectionExplanation_SectionExplanationId)
                .Index(t => t.User_Id)
                .Index(t => t.SectionEdit_SectionEditId)
                .Index(t => t.SectionEvidence_SectionExplanationId)
                .Index(t => t.SectionExplanation_SectionExplanationId);            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SectionComments", "SectionExplanation_SectionExplanationId", "dbo.SectionExplanations");
            DropForeignKey("dbo.SectionComments", "SectionEvidence_SectionExplanationId", "dbo.SectionEvidences");
            DropForeignKey("dbo.SectionComments", "SectionEdit_SectionEditId", "dbo.SectionEdits");
            DropForeignKey("dbo.SectionComments", "User_Id", "dbo.ApplicationUsers");
            DropIndex("dbo.SectionComments", new[] { "SectionExplanation_SectionExplanationId" });
            DropIndex("dbo.SectionComments", new[] { "SectionEvidence_SectionExplanationId" });
            DropIndex("dbo.SectionComments", new[] { "SectionEdit_SectionEditId" });
            DropIndex("dbo.SectionComments", new[] { "User_Id" });
            DropTable("dbo.SectionComments");
        }
    }
}
