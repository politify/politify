namespace Politify.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.IO;
    using Politify.Code;
    
    public partial class StaffMPs : DbMigration
    {
        public override void Up()
        {
            var dataDir = AppDomain.CurrentDomain.GetData("DataDirectory");
            string standingDataDir = (dataDir == null ? "C:/inetpub/wwwroot/UAT/App_Data" : dataDir.ToString());

            standingDataDir += "/SQL/Standing Data/";

            PolitifyDbContext db = new PolitifyDbContext();

            var oldTimeout = db.Database.CommandTimeout;
            db.Database.CommandTimeout = 72000;

            List<string> files = new List<string>() { "dbo.Organisations.Table.Constituencies.sql", "dbo.Staff.Data.Politicians.sql" };


            foreach (string dir in files)
            {
                db.Database.ExecuteSqlCommand(File.ReadAllText(standingDataDir + dir));
            }

            db.Database.CommandTimeout = oldTimeout;
        }
        
        public override void Down()
        {
        }
    }
}
