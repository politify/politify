namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PoliticianViewModelViewNameUpdate : DbMigration
    {
        public override void Up()
        {
            //CAS: set the wrong name in dbContext compared to previous migrations sql command.
            //RenameTable(name: "dbo.PoliticianViewModels", newName: "PoliticianViewModel");
        }
        
        public override void Down()
        {
            //RenameTable(name: "dbo.PoliticianViewModel", newName: "PoliticianViewModels");
        }
    }
}
