namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StaffWebsite : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Staff", "Website", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Staff", "Website");
        }
    }
}
