namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostcodesCountryColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Postcodes", "Ctry", c => c.String(maxLength: 9));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Postcodes", "Ctry");
        }
    }
}
