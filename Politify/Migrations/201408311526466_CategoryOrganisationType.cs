namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryOrganisationType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "OrganisationType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Categories", "OrganisationType");
        }
    }
}
