namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostcodeONSPDColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Postcodes", "Osward", c => c.String(maxLength: 9));
            AddColumn("dbo.Postcodes", "Oscty", c => c.String(maxLength: 9));
            AddColumn("dbo.Postcodes", "Oslaua", c => c.String(maxLength: 9));
            AddColumn("dbo.Postcodes", "Parish", c => c.String(maxLength: 9));
            AddColumn("dbo.Postcodes", "Pcon", c => c.String(maxLength: 9));
            AddColumn("dbo.Postcodes", "Eer", c => c.String(maxLength: 9));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Postcodes", "Eer");
            DropColumn("dbo.Postcodes", "Pcon");
            DropColumn("dbo.Postcodes", "Parish");
            DropColumn("dbo.Postcodes", "Oslaua");
            DropColumn("dbo.Postcodes", "Oscty");
            DropColumn("dbo.Postcodes", "Osward");
        }
    }
}
