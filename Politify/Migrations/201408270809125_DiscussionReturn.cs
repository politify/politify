namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DiscussionReturn : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sections", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Sections", new[] { "Category_CategoryId" });
            CreateTable(
                "dbo.Discussions",
                c => new
                    {
                        DiscussionId = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTimeOffset(nullable: false, precision: 7),
                        Category_CategoryId = c.Int(nullable: false),
                        Section_SectionID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.DiscussionId)
                .ForeignKey("dbo.Categories", t => t.Category_CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Sections", t => t.Section_SectionID, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Category_CategoryId)
                .Index(t => t.Section_SectionID)
                .Index(t => t.User_Id);
            
            DropColumn("dbo.Sections", "Category_CategoryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sections", "Category_CategoryId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Discussions", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Discussions", "Section_SectionID", "dbo.Sections");
            DropForeignKey("dbo.Discussions", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Discussions", new[] { "User_Id" });
            DropIndex("dbo.Discussions", new[] { "Section_SectionID" });
            DropIndex("dbo.Discussions", new[] { "Category_CategoryId" });
            DropTable("dbo.Discussions");
            CreateIndex("dbo.Sections", "Category_CategoryId");
            AddForeignKey("dbo.Sections", "Category_CategoryId", "dbo.Categories", "CategoryId", cascadeDelete: true);
        }
    }
}
