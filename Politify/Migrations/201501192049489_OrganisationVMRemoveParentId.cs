namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganisationVMRemoveParentId : DbMigration
    {
        public override void Up()
        {
            Sql(@"
    Alter View [dbo].[OrganisationVMs] as

    SELECT        dbo.Organisations.OrganisationId, dbo.Organisations.Name, dbo.Organisations.Description, dbo.Organisations.ProfilePicture, dbo.Organisations.OrganisationType, 
                         dbo.Organisations.EmailAddress, dbo.Organisations.CountryID AS Country_CountryID, dbo.Organisations.OnsCode, v.Positives, v.Negatives, c.CommentCount
FROM            dbo.Organisations FULL OUTER JOIN
                             (SELECT        Organisation_OrganisationId AS OrganisationId, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.Votes
                               GROUP BY Organisation_OrganisationId) AS v ON dbo.Organisations.OrganisationId = v.OrganisationId FULL OUTER JOIN
                             (SELECT        Organisation_OrganisationId AS OrganisationId, COUNT(*) AS CommentCount
                               FROM            dbo.Comments
                               GROUP BY Organisation_OrganisationId) AS c ON dbo.Organisations.OrganisationId = c.OrganisationId
");
        }
        
        public override void Down()
        {

            Sql(@"
    Alter View [dbo].[OrganisationVMs] as

 SELECT        dbo.Organisations.OrganisationId, dbo.Organisations.Name, dbo.Organisations.Description, dbo.Organisations.ProfilePicture, dbo.Organisations.OrganisationType, 
                         dbo.Organisations.EmailAddress, dbo.Organisations.CountryID AS Country_CountryID, dbo.Organisations.OnsCode, v.Positives, v.Negatives, c.CommentCount, 
                         dbo.Organisations.Parent_OrganisationId
FROM            dbo.Organisations FULL OUTER JOIN
                             (SELECT        Organisation_OrganisationId AS OrganisationId, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.Votes
                               GROUP BY Organisation_OrganisationId) AS v ON dbo.Organisations.OrganisationId = v.OrganisationId FULL OUTER JOIN
                             (SELECT        Organisation_OrganisationId AS OrganisationId, COUNT(*) AS CommentCount
                               FROM            dbo.Comments
                               GROUP BY Organisation_OrganisationId) AS c ON dbo.Organisations.OrganisationId = c.OrganisationId
");
        }
    }
}
