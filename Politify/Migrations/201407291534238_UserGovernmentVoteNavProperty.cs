namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserGovernmentVoteNavProperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GovernmentVotes", "User_Id", "dbo.ApplicationUsers");
            AddForeignKey("dbo.GovernmentVotes", "User_Id", "dbo.ApplicationUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GovernmentVotes", "User_Id", "dbo.ApplicationUsers");
            AddForeignKey("dbo.GovernmentVotes", "User_Id", "dbo.ApplicationUsers", "Id", cascadeDelete: true);
        }
    }
}
