namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SectionOrganisation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sections", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Sections", new[] { "Category_CategoryId" });
            AddColumn("dbo.Categories", "Title", c => c.String(nullable: false));
            AddColumn("dbo.Categories", "Description", c => c.String());
            AddColumn("dbo.Sections", "Organisation_OrganisationId", c => c.Int());
            CreateIndex("dbo.Sections", "Category_CategoryId");
            CreateIndex("dbo.Sections", "Organisation_OrganisationId");
            AddForeignKey("dbo.Sections", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Sections", "Category_CategoryId", "dbo.Categories", "CategoryId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sections", "Category_CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Sections", "Organisation_OrganisationId", "dbo.Organisations");
            DropIndex("dbo.Sections", new[] { "Organisation_OrganisationId" });
            DropIndex("dbo.Sections", new[] { "Category_CategoryId" });
            DropColumn("dbo.Sections", "Organisation_OrganisationId");
            DropColumn("dbo.Categories", "Description");
            DropColumn("dbo.Categories", "Title");
            CreateIndex("dbo.Sections", "Category_CategoryId");
            AddForeignKey("dbo.Sections", "Category_CategoryId", "dbo.Categories", "CategoryId");
        }
    }
}
