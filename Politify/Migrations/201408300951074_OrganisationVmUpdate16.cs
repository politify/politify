namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganisationVmUpdate16 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrganisationCategories", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Organisations", "Parent_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Postcodes", "Ward_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Comments", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.SignatureCounts", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Staff", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Sections", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations");
            DropPrimaryKey("dbo.Organisations");
            AddColumn("dbo.SignatureCounts", "Organisation_OrganisationId", c => c.Int(nullable: false));
            AddColumn("dbo.Staff", "Organisation_OrganisationId", c => c.Int(nullable: false));
            AlterColumn("dbo.Organisations", "OrganisationId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Organisations", "OrganisationId");
            CreateIndex("dbo.Organisations", "OrganisationId");
            CreateIndex("dbo.SignatureCounts", "Organisation_OrganisationId");
            CreateIndex("dbo.Staff", "Organisation_OrganisationId");
            AddForeignKey("dbo.SignatureCounts", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId", cascadeDelete: true);
            AddForeignKey("dbo.Staff", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId", cascadeDelete: true);
            AddForeignKey("dbo.OrganisationCategories", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId", cascadeDelete: true);
            AddForeignKey("dbo.Organisations", "Parent_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Postcodes", "Ward_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Comments", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Sections", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Sections", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Comments", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Postcodes", "Ward_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Organisations", "Parent_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.OrganisationCategories", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Staff", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.SignatureCounts", "Organisation_OrganisationId", "dbo.Organisations");
            DropIndex("dbo.Staff", new[] { "Organisation_OrganisationId" });
            DropIndex("dbo.SignatureCounts", new[] { "Organisation_OrganisationId" });
            DropIndex("dbo.Organisations", new[] { "OrganisationId" });
            DropPrimaryKey("dbo.Organisations");
            DropColumn("dbo.Staff", "Organisation_OrganisationId");
            DropColumn("dbo.SignatureCounts", "Organisation_OrganisationId");
            AddPrimaryKey("dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Sections", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Staff", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId", cascadeDelete: true);
            AddForeignKey("dbo.SignatureCounts", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId", cascadeDelete: true);
            AddForeignKey("dbo.Comments", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Postcodes", "Ward_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Organisations", "Parent_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.OrganisationCategories", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId", cascadeDelete: true);
        }
    }
}
