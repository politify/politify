namespace Politify.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Politify.Code;
    using Politify.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PolitifyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Politify.Code.PolitifyDbContext";
            CommandTimeout = 72000;//20 hours
        }

        protected override void Seed(PolitifyDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var store = new RoleStore<IdentityRole>(context);
            var manager = new RoleManager<IdentityRole>(store);

            if (!context.Set<IdentityRole>().Any(r => r.Name == "Admin"))
            {
                var role = new IdentityRole { Name = "Admin" };
                manager.Create(role);
            }

            if (!context.Set<IdentityRole>().Any(r => r.Name == "Writer"))
            {
                var role = new IdentityRole { Name = "Writer" };
                manager.Create(role);
            }
            
            CreateUser(context,
                new ApplicationUser
                {
                    UserName = "este",
                    FirstName = "Edward",
                    Surname = "Stevens",
                    Country = context.Set<Country>().Single(c => c.Name == "UNITED KINGDOM"),
                    Email = "edant92@gmail.com"
                });
            
            CreateUser(context,
                new ApplicationUser
                {
                    UserName = "chris",
                    FirstName = "Chris",
                    Surname = "Stevens",
                    Country = context.Set<Country>().Single(c => c.Name == "UNITED KINGDOM"),
                    Email = "chrisstevens901@gmail.com"
                });

            //CreateUser(context,
            //    new ApplicationUser
            //    {
            //        UserName = "rich",
            //        FirstName = "Rich",
            //        Surname = "Cooper",
            //        Country = context.Set<Country>().Single(c => c.Name == "UNITED KINGDOM"),
            //        Email = "richardbcooper@me.com"
            //    }, "Writer");
        }

        void CreateUser(PolitifyDbContext context, ApplicationUser user, string role = "Admin")
        {
            if (!context.Set<ApplicationUser>().Any(u => u.UserName == user.UserName))
            {
                if (userStore == null) userStore = new UserStore<ApplicationUser>(context);
                if (userManager == null) userManager = new UserManager<ApplicationUser>(userStore);

                userManager.Create(user, "ChangeMe!");
                userManager.AddToRole(user.Id, role);
            }
        }

        UserStore<ApplicationUser> userStore;
        UserManager<ApplicationUser> userManager;
    }
}
