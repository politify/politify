namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganisationCategoryVmsReset : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER VIEW [dbo].[OrganisationCategoryVMs]
AS
SELECT        dbo.Categories.CategoryId, dbo.Categories.[Order], dbo.Categories.Title, dbo.Categories.Description, dbo.OrganisationCategories.Organisation_OrganisationId
FROM            dbo.Categories INNER JOIN
                         dbo.OrganisationCategories ON dbo.Categories.CategoryId = dbo.OrganisationCategories.Category_CategoryId

");
        }
        
        public override void Down()
        {
        }
    }
}
