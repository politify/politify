namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPartIdToPolitician : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER VIEW [dbo].[PoliticianViewModel]
AS
SELECT        p.PoliticianID, p.Name, v.Positives, v.Negatives, dbo.Parties.PartyID, dbo.Parties.Name AS PartyName, p.ImageFilePath, p.Constituency, v.Score
FROM            dbo.Politicians AS p INNER JOIN
                         dbo.Parties ON p.Party_PartyID = dbo.Parties.PartyID FULL OUTER JOIN
                             (SELECT        Politician_PoliticianID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN - 1 WHEN 1 THEN 1 ELSE 0 END)) AS Score
                               FROM            dbo.PoliticianVotes
                               GROUP BY Politician_PoliticianID) AS v ON p.PoliticianID = v.Politician_PoliticianID
");
        }
        
        public override void Down()
        {
            DropColumn("dbo.PoliticianViewModel", "PartyID");
        }
    }
}
