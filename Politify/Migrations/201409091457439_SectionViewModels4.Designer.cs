// <auto-generated />
namespace Politify.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class SectionViewModels4 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SectionViewModels4));
        
        string IMigrationMetadata.Id
        {
            get { return "201409091457439_SectionViewModels4"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
