namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SectionViewModels6 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER VIEW [dbo].[SectionCommentVMs]
AS
SELECT        c.CommentID, c.Text, c.CreatedDate, c.Edited, c.LastEdit, c.LastText, c.OriginalText, c.SectionEdit_SectionEditId, c.User_Id, c.SectionEvidence_SectionEvidenceId, 
                         c.SectionExplanation_SectionExplanationId, c.Parent_CommentID, c.Section_SectionID, v.Positives, v.Negatives, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionComments AS c INNER JOIN
                         dbo.ApplicationUsers ON c.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes AS SectionVotes_1
                               GROUP BY Section_SectionID) AS v ON c.Section_SectionID = v.SectionID

");
            Sql(@"
ALTER VIEW [dbo].[SectionEditVMs]
AS
SELECT        edit.SectionEditId, edit.Text, edit.DateCreated, edit.Section_SectionID, edit.User_Id, v.Positives, v.Negatives, c.CommentCount, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionEdits AS edit INNER JOIN
                         dbo.ApplicationUsers ON edit.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON edit.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON edit.Section_SectionID = c.SectionID

");
            Sql(@"
ALTER VIEW [dbo].[SectionEvidenceVMs]
AS
SELECT        ev.SectionEvidenceId, ev.Text, ev.FileName, ev.DateCreated, ev.Section_SectionID, ev.User_Id, v.Positives, v.Negatives, c.CommentCount, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.FirstName, dbo.ApplicationUsers.UserName
FROM            dbo.SectionEvidences AS ev INNER JOIN
                         dbo.ApplicationUsers ON ev.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON ev.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON ev.Section_SectionID = c.SectionID

");
            Sql(@"
ALTER VIEW [dbo].[SectionExplanationVMs]
AS
SELECT        ex.SectionExplanationId, ex.Text, ex.DateCreated, ex.Section_SectionID, ex.User_Id, v.Positives, v.Negatives, c.CommentCount, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionExplanations AS ex INNER JOIN
                         dbo.ApplicationUsers ON ex.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON ex.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON ex.Section_SectionID = c.SectionID

");
            Sql(@"
ALTER VIEW [dbo].[SectionVMs]
AS
SELECT        dbo.Sections.SectionID, dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.OrderQualifier, dbo.Sections.Parent_SectionID, dbo.Sections.IsUserCreated, 
                         dbo.Sections.User_Id, dbo.Sections.Organisation_OrganisationId, dbo.Sections.DateCreated, c.CommentCount, e.EditCount, ev.EvidenceCount, ex.ExplanationCount, 
                         v.Positives, v.Negatives, dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.FirstName, dbo.ApplicationUsers.UserName
FROM            dbo.Sections INNER JOIN
                         dbo.ApplicationUsers ON dbo.Sections.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON dbo.Sections.SectionID = c.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS EditCount
                               FROM            dbo.SectionEdits
                               GROUP BY Section_SectionID) AS e ON dbo.Sections.SectionID = e.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS EvidenceCount
                               FROM            dbo.SectionEvidences
                               GROUP BY Section_SectionID) AS ev ON dbo.Sections.SectionID = ev.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS ExplanationCount
                               FROM            dbo.SectionExplanations
                               GROUP BY Section_SectionID) AS ex ON dbo.Sections.SectionID = ex.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON dbo.Sections.SectionID = v.SectionID

");
        }
        
        public override void Down()
        {

            Sql(@"
ALTER VIEW [dbo].[SectionVMs]
AS
SELECT        dbo.Sections.SectionID, dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.OrderQualifier, dbo.Sections.Parent_SectionID, dbo.Sections.IsUserCreated, 
                         dbo.Sections.User_Id, dbo.Sections.Organisation_OrganisationId, dbo.Sections.DateCreated, c.CommentCount, e.EditCount, ev.EvidenceCount, ex.ExplanationCount, 
                         v.Positives, v.Negatives
FROM            dbo.Sections FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON dbo.Sections.SectionID = c.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS EditCount
                               FROM            dbo.SectionEdits
                               GROUP BY Section_SectionID) AS e ON dbo.Sections.SectionID = e.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS EvidenceCount
                               FROM            dbo.SectionEvidences
                               GROUP BY Section_SectionID) AS ev ON dbo.Sections.SectionID = ev.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS ExplanationCount
                               FROM            dbo.SectionExplanations
                               GROUP BY Section_SectionID) AS ex ON dbo.Sections.SectionID = ex.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON dbo.Sections.SectionID = v.SectionID

");
            Sql(@"
ALTER VIEW [dbo].[SectionExplanationVMs]
AS
SELECT        ex.SectionExplanationId, ex.Text, ex.DateCreated, ex.Section_SectionID, ex.User_Id, v.Positives, v.Negatives, c.CommentCount
FROM            dbo.SectionExplanations AS ex FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON ex.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON ex.Section_SectionID = c.SectionID

");
            Sql(@"
ALTER VIEW [dbo].[SectionEvidenceVMs]
AS
SELECT        ev.SectionEvidenceId, ev.Text, ev.FileName, ev.DateCreated, ev.Section_SectionID, ev.User_Id, v.Positives, v.Negatives, c.CommentCount
FROM            dbo.SectionEvidences AS ev FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON ev.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON ev.Section_SectionID = c.SectionID

");
            Sql(@"
ALTER VIEW [dbo].[SectionEditVMs]
AS
SELECT        edit.SectionEditId, edit.Text, edit.DateCreated, edit.Section_SectionID, edit.User_Id, v.Positives, v.Negatives, c.CommentCount
FROM            dbo.SectionEdits AS edit FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON edit.Section_SectionID = v.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON edit.Section_SectionID = c.SectionID

");
            Sql(@"
ALTER VIEW [dbo].[SectionCommentVMs]
AS
SELECT        c.CommentID, c.Text, c.CreatedDate, c.Edited, c.LastEdit, c.LastText, c.OriginalText, c.SectionEdit_SectionEditId, c.User_Id, c.SectionEvidence_SectionEvidenceId, 
                         c.SectionExplanation_SectionExplanationId, c.Parent_CommentID, c.Section_SectionID, v.Positives, v.Negatives
FROM            dbo.SectionComments AS c FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes AS SectionVotes_1
                               GROUP BY Section_SectionID) AS v ON c.Section_SectionID = v.SectionID

");

        }
    }
}
