namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SectionViewModels5 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER VIEW [dbo].[SectionVMs]
AS
SELECT        dbo.Sections.SectionID, dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.OrderQualifier, dbo.Sections.Parent_SectionID, dbo.Sections.IsUserCreated, 
                         dbo.Sections.User_Id, dbo.Sections.Organisation_OrganisationId, dbo.Sections.DateCreated, c.CommentCount, e.EditCount, ev.EvidenceCount, ex.ExplanationCount, 
                         v.Positives, v.Negatives
FROM            dbo.Sections FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON dbo.Sections.SectionID = c.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS EditCount
                               FROM            dbo.SectionEdits
                               GROUP BY Section_SectionID) AS e ON dbo.Sections.SectionID = e.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS EvidenceCount
                               FROM            dbo.SectionEvidences
                               GROUP BY Section_SectionID) AS ev ON dbo.Sections.SectionID = ev.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS ExplanationCount
                               FROM            dbo.SectionExplanations
                               GROUP BY Section_SectionID) AS ex ON dbo.Sections.SectionID = ex.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON dbo.Sections.SectionID = v.SectionID

");
        }
        
        public override void Down()
        {
            Sql(@"
ALTER VIEW [dbo].[SectionVMs]
AS
SELECT        dbo.Sections.SectionID, dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.OrderQualifier, dbo.Sections.Parent_SectionID, dbo.Sections.IsUserCreated, 
                         dbo.Sections.User_Id, dbo.Sections.Organisation_OrganisationId, dbo.Sections.DateCreated, c.CommentCount, e.EditCount, ev.EvidenceCount, ex.ExplanationCount, 
                         v.Positives, v.Negatives
FROM            dbo.Sections FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments
                               GROUP BY Section_SectionID) AS c ON dbo.Sections.SectionID = c.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS EditCount
                               FROM            dbo.SectionEdits
                               GROUP BY Section_SectionID) AS e ON dbo.Sections.SectionID = e.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS EvidenceCount
                               FROM            dbo.SectionEvidences
                               GROUP BY Section_SectionID) AS ev ON dbo.Sections.SectionID = ev.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS ExplanationCount
                               FROM            dbo.SectionExplanations
                               GROUP BY Section_SectionID) AS ex ON dbo.Sections.SectionID = ex.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes
                               GROUP BY Section_SectionID) AS v ON dbo.Sections.SectionID = v.SectionID

");
        }
    }
}
