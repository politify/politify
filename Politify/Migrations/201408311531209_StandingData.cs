namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.IO;

    public partial class StandingData : DbMigration
    {
        public override void Up()
        {
            string appDataFilepath = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();


            Sql("Delete from dbo.Categories");
            Sql(File.ReadAllText(appDataFilepath + "/SQL/Standing Data/dbo.Categories.Table.sql"));
            Sql("Delete from dbo.Countries");
            Sql(File.ReadAllText(appDataFilepath + "/SQL/Standing Data/dbo.Countries.Table.sql"));
            Sql("Delete from dbo.Organisations");
            Sql(File.ReadAllText(appDataFilepath + "/SQL/Standing Data/dbo.Organisations.Table.sql"));
            Sql("Delete from dbo.OrganisationCategories");
            Sql(File.ReadAllText(appDataFilepath + "/SQL/Standing Data/dbo.OrganisationCategories.Table.sql"));
        }
        
        public override void Down()
        {
        }
    }
}
