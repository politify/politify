namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SectionEvidenceIdName : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SectionComments", "SectionEvidence_SectionExplanationId", "dbo.SectionEvidences");
            DropForeignKey("dbo.SectionVotes", "SectionEvidence_SectionExplanationId", "dbo.SectionEvidences");
            RenameColumn(table: "dbo.SectionComments", name: "SectionEvidence_SectionExplanationId", newName: "SectionEvidence_SectionEvidenceId");
            RenameColumn(table: "dbo.SectionVotes", name: "SectionEvidence_SectionExplanationId", newName: "SectionEvidence_SectionEvidenceId");
            RenameIndex(table: "dbo.SectionComments", name: "IX_SectionEvidence_SectionExplanationId", newName: "IX_SectionEvidence_SectionEvidenceId");
            RenameIndex(table: "dbo.SectionVotes", name: "IX_SectionEvidence_SectionExplanationId", newName: "IX_SectionEvidence_SectionEvidenceId");
            DropPrimaryKey("dbo.SectionEvidences");
            RenameColumn("dbo.SectionEvidences", "SectionExplanationId", "SectionEvidenceId");
            //AlterColumn("dbo.SectionEvidences", "SectionExplanationId", c => c.Int(nullable: false, identity: false));
            //AddColumn("dbo.SectionEvidences", "SectionEvidenceId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.SectionEvidences", "SectionEvidenceId");
            AddForeignKey("dbo.SectionComments", "SectionEvidence_SectionEvidenceId", "dbo.SectionEvidences", "SectionEvidenceId");
            AddForeignKey("dbo.SectionVotes", "SectionEvidence_SectionEvidenceId", "dbo.SectionEvidences", "SectionEvidenceId");
            //DropColumn("dbo.SectionEvidences", "SectionExplanationId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SectionVotes", "SectionEvidence_SectionEvidenceId", "dbo.SectionEvidences");
            DropForeignKey("dbo.SectionComments", "SectionEvidence_SectionEvidenceId", "dbo.SectionEvidences");
            DropPrimaryKey("dbo.SectionEvidences");
            RenameColumn("dbo.SectionEvidences", "SectionEvidenceId", "SectionExplanationId");
            AddPrimaryKey("dbo.SectionEvidences", "SectionExplanationId");
            RenameIndex(table: "dbo.SectionVotes", name: "IX_SectionEvidence_SectionEvidenceId", newName: "IX_SectionEvidence_SectionExplanationId");
            RenameIndex(table: "dbo.SectionComments", name: "IX_SectionEvidence_SectionEvidenceId", newName: "IX_SectionEvidence_SectionExplanationId");
            RenameColumn(table: "dbo.SectionVotes", name: "SectionEvidence_SectionEvidenceId", newName: "SectionEvidence_SectionExplanationId");
            RenameColumn(table: "dbo.SectionComments", name: "SectionEvidence_SectionEvidenceId", newName: "SectionEvidence_SectionExplanationId");
            AddForeignKey("dbo.SectionVotes", "SectionEvidence_SectionExplanationId", "dbo.SectionEvidences", "SectionEvidenceId");
            AddForeignKey("dbo.SectionComments", "SectionEvidence_SectionExplanationId", "dbo.SectionEvidences", "SectionEvidenceId");
        }
    }
}
