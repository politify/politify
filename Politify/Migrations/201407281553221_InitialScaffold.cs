namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialScaffold : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Businesses",
                c => new
                    {
                        EntityID = c.Int(nullable: false, identity: true),
                        Profit = c.Double(),
                        Name = c.String(maxLength: 50),
                        Jurisdiction = c.String(maxLength: 50),
                        DateFormed = c.DateTime(),
                    })
                .PrimaryKey(t => t.EntityID);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        CountryID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        NameShort = c.String(nullable: false, maxLength: 5),
                        Language = c.String(maxLength: 50),
                        ImageFileName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.CountryID);
            
            CreateTable(
                "dbo.Governments",
                c => new
                    {
                        EntityID = c.Int(nullable: false, identity: true),
                        Credit = c.Double(),
                        Name = c.String(maxLength: 50),
                        Jurisdiction = c.String(maxLength: 50),
                        DateFormed = c.DateTime(),
                        CountryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityID)
                .ForeignKey("dbo.Countries", t => t.CountryID)
                .Index(t => t.CountryID);
            
            CreateTable(
                "dbo.GovernmentComments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        Edited = c.Boolean(nullable: false),
                        LastEdit = c.DateTime(),
                        LastText = c.String(),
                        OriginalText = c.String(),
                        Government_EntityID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Governments", t => t.Government_EntityID, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Government_EntityID)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.ApplicationUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false, maxLength: 30),
                        Surname = c.String(nullable: false, maxLength: 30),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                        Country_CountryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_CountryID, cascadeDelete: true)
                .Index(t => t.Country_CountryID);
            
            CreateTable(
                "dbo.IdentityUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.IdentityUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.IdentityUserRoles",
                c => new
                    {
                        RoleId = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ApplicationUser_Id = c.String(maxLength: 128),
                        IdentityRole_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.RoleId, t.UserId })
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .ForeignKey("dbo.IdentityRoles", t => t.IdentityRole_Id)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.IdentityRole_Id);
            
            CreateTable(
                "dbo.GovernmentVotes",
                c => new
                    {
                        VoteID = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        IsPositive = c.Boolean(nullable: false),
                        Government_EntityID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.VoteID)
                .ForeignKey("dbo.Governments", t => t.Government_EntityID, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Government_EntityID)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.LawComments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        Edited = c.Boolean(nullable: false),
                        LastEdit = c.DateTime(),
                        LastText = c.String(),
                        OriginalText = c.String(),
                        Law_LawID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Laws", t => t.Law_LawID, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Law_LawID)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Laws",
                c => new
                    {
                        LawID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Text = c.String(),
                        DocumentFilepath = c.String(),
                        DatePassed = c.DateTime(),
                        YearPassed = c.Int(nullable: false),
                        YearPublicationNumber = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        Government_EntityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LawID)
                .ForeignKey("dbo.Governments", t => t.Government_EntityID, cascadeDelete: true)
                .Index(t => t.Government_EntityID);
            
            CreateTable(
                "dbo.LawVotes",
                c => new
                    {
                        VoteID = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        IsPositive = c.Boolean(nullable: false),
                        Law_LawID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.VoteID)
                .ForeignKey("dbo.Laws", t => t.Law_LawID, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Law_LawID)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.PoliticianComments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        Edited = c.Boolean(nullable: false),
                        LastEdit = c.DateTime(),
                        LastText = c.String(),
                        OriginalText = c.String(),
                        Politician_PoliticianID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Politicians", t => t.Politician_PoliticianID, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Politician_PoliticianID)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Politicians",
                c => new
                    {
                        PoliticianID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Bio = c.String(),
                        ImageFilePath = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        Constituency = c.String(),
                        ConstituencyContact = c.String(),
                        ConstituencyEmail = c.String(),
                        ParliamentaryContact = c.String(),
                        WebsiteSocialMedia = c.String(),
                        Government_EntityID = c.Int(nullable: false),
                        Party_PartyID = c.Int(),
                    })
                .PrimaryKey(t => t.PoliticianID)
                .ForeignKey("dbo.Governments", t => t.Government_EntityID, cascadeDelete: true)
                .ForeignKey("dbo.Parties", t => t.Party_PartyID)
                .Index(t => t.Government_EntityID)
                .Index(t => t.Party_PartyID);
            
            CreateTable(
                "dbo.Parties",
                c => new
                    {
                        PartyID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PartyID);
            
            CreateTable(
                "dbo.PoliticianVotes",
                c => new
                    {
                        VoteID = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        IsPositive = c.Boolean(nullable: false),
                        Politician_PoliticianID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.VoteID)
                .ForeignKey("dbo.Politicians", t => t.Politician_PoliticianID, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Politician_PoliticianID)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.IdentityRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IdentityUserRoles", "IdentityRole_Id", "dbo.IdentityRoles");
            DropForeignKey("dbo.PoliticianComments", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.PoliticianComments", "Politician_PoliticianID", "dbo.Politicians");
            DropForeignKey("dbo.PoliticianVotes", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.PoliticianVotes", "Politician_PoliticianID", "dbo.Politicians");
            DropForeignKey("dbo.Politicians", "Party_PartyID", "dbo.Parties");
            DropForeignKey("dbo.Politicians", "Government_EntityID", "dbo.Governments");
            DropForeignKey("dbo.LawComments", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.LawComments", "Law_LawID", "dbo.Laws");
            DropForeignKey("dbo.LawVotes", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.LawVotes", "Law_LawID", "dbo.Laws");
            DropForeignKey("dbo.Laws", "Government_EntityID", "dbo.Governments");
            DropForeignKey("dbo.GovernmentVotes", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.GovernmentVotes", "Government_EntityID", "dbo.Governments");
            DropForeignKey("dbo.GovernmentComments", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.IdentityUserRoles", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.IdentityUserLogins", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.ApplicationUsers", "Country_CountryID", "dbo.Countries");
            DropForeignKey("dbo.IdentityUserClaims", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.GovernmentComments", "Government_EntityID", "dbo.Governments");
            DropForeignKey("dbo.Governments", "CountryID", "dbo.Countries");
            DropIndex("dbo.PoliticianVotes", new[] { "User_Id" });
            DropIndex("dbo.PoliticianVotes", new[] { "Politician_PoliticianID" });
            DropIndex("dbo.Politicians", new[] { "Party_PartyID" });
            DropIndex("dbo.Politicians", new[] { "Government_EntityID" });
            DropIndex("dbo.PoliticianComments", new[] { "User_Id" });
            DropIndex("dbo.PoliticianComments", new[] { "Politician_PoliticianID" });
            DropIndex("dbo.LawVotes", new[] { "User_Id" });
            DropIndex("dbo.LawVotes", new[] { "Law_LawID" });
            DropIndex("dbo.Laws", new[] { "Government_EntityID" });
            DropIndex("dbo.LawComments", new[] { "User_Id" });
            DropIndex("dbo.LawComments", new[] { "Law_LawID" });
            DropIndex("dbo.GovernmentVotes", new[] { "User_Id" });
            DropIndex("dbo.GovernmentVotes", new[] { "Government_EntityID" });
            DropIndex("dbo.IdentityUserRoles", new[] { "IdentityRole_Id" });
            DropIndex("dbo.IdentityUserRoles", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.IdentityUserLogins", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.IdentityUserClaims", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.ApplicationUsers", new[] { "Country_CountryID" });
            DropIndex("dbo.GovernmentComments", new[] { "User_Id" });
            DropIndex("dbo.GovernmentComments", new[] { "Government_EntityID" });
            DropIndex("dbo.Governments", new[] { "CountryID" });
            DropTable("dbo.IdentityRoles");
            DropTable("dbo.PoliticianVotes");
            DropTable("dbo.Parties");
            DropTable("dbo.Politicians");
            DropTable("dbo.PoliticianComments");
            DropTable("dbo.LawVotes");
            DropTable("dbo.Laws");
            DropTable("dbo.LawComments");
            DropTable("dbo.GovernmentVotes");
            DropTable("dbo.IdentityUserRoles");
            DropTable("dbo.IdentityUserLogins");
            DropTable("dbo.IdentityUserClaims");
            DropTable("dbo.ApplicationUsers");
            DropTable("dbo.GovernmentComments");
            DropTable("dbo.Governments");
            DropTable("dbo.Countries");
            DropTable("dbo.Businesses");
        }
    }
}
