namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LawViewModel : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE VIEW [dbo].[LawViewModel]
AS
SELECT        l.LawID, l.Title, l.YearPassed, l.YearPublicationNumber, v.Positives, v.Negatives, v.Score
FROM            dbo.Laws AS l FULL OUTER JOIN
                             (SELECT        Law_LawID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN - 1 WHEN 1 THEN 1 ELSE 0 END)) AS Score
                               FROM            dbo.LawVotes
                               GROUP BY Law_LawID) AS v ON l.LawID = v.Law_LawID
");
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LawViewModel");
        }
    }
}
