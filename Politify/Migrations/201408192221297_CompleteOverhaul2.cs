namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompleteOverhaul2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Staffs", newName: "Staff");
            DropForeignKey("dbo.Comments", "Government_EntityID", "dbo.Governments");
            DropForeignKey("dbo.GovernmentVotes", "Government_EntityID", "dbo.Governments");
            DropForeignKey("dbo.GovernmentVotes", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Comments", "Law_LawID", "dbo.Laws");
            DropForeignKey("dbo.Laws", "Government_EntityID", "dbo.Governments");
            DropForeignKey("dbo.Sections", "Law_LawID", "dbo.Laws");
            DropForeignKey("dbo.LawVotes", "Law_LawID", "dbo.Laws");
            DropForeignKey("dbo.LawVotes", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Comments", "Politician_PoliticianID", "dbo.Politicians");
            DropForeignKey("dbo.Politicians", "Government_EntityID", "dbo.Governments");
            DropForeignKey("dbo.Politicians", "Party_PartyID", "dbo.Parties");
            DropForeignKey("dbo.Votes", "Politician_PoliticianID", "dbo.Politicians");
            DropForeignKey("dbo.Organisations", "Country_CountryID", "dbo.Countries");
            DropIndex("dbo.Comments", new[] { "Government_EntityID" });
            DropIndex("dbo.Comments", new[] { "Law_LawID" });
            DropIndex("dbo.Comments", new[] { "Politician_PoliticianID" });
            DropIndex("dbo.Governments", new[] { "CountryID" });
            DropIndex("dbo.GovernmentVotes", new[] { "Government_EntityID" });
            DropIndex("dbo.GovernmentVotes", new[] { "User_Id" });
            DropIndex("dbo.Sections", new[] { "Law_LawID" });
            DropIndex("dbo.Laws", new[] { "Government_EntityID" });
            DropIndex("dbo.LawVotes", new[] { "Law_LawID" });
            DropIndex("dbo.LawVotes", new[] { "User_Id" });
            DropIndex("dbo.Votes", new[] { "Politician_PoliticianID" });
            DropIndex("dbo.Politicians", new[] { "Government_EntityID" });
            DropIndex("dbo.Politicians", new[] { "Party_PartyID" });
            RenameColumn(table: "dbo.Organisations", name: "Country_CountryID", newName: "CountryID");
            RenameIndex(table: "dbo.Organisations", name: "IX_Country_CountryID", newName: "IX_CountryID");
            AddColumn("dbo.Sections", "Discussion_DiscussionId", c => c.Int());
            CreateIndex("dbo.Sections", "Discussion_DiscussionId");
            AddForeignKey("dbo.Sections", "Discussion_DiscussionId", "dbo.Discussions", "DiscussionId");
            AddForeignKey("dbo.Organisations", "CountryID", "dbo.Countries", "CountryID");
            DropColumn("dbo.Comments", "Discriminator");
            DropForeignKey("dbo.Comments", "FK_dbo.GovernmentComments_dbo.ApplicationUsers_User_Id");
            DropForeignKey("dbo.Comments", "FK_dbo.GovernmentComments_dbo.Governments_Government_EntityID");
            DropColumn("dbo.Comments", "Government_EntityID");
            DropColumn("dbo.Comments", "Law_LawID");
            DropColumn("dbo.Comments", "Politician_PoliticianID");
            DropColumn("dbo.Sections", "Law_LawID");
            DropForeignKey("dbo.Votes", "FK_dbo.PoliticianVotes_dbo.ApplicationUsers_User_Id");
            DropForeignKey("dbo.Votes", "FK_dbo.PoliticianVotes_dbo.Politicians_Politician_PoliticianID");
            DropColumn("dbo.Votes", "Politician_PoliticianID");
            DropTable("dbo.Governments");
            DropTable("dbo.GovernmentVotes");
            DropTable("dbo.Laws");
            DropTable("dbo.LawVotes");
            DropTable("dbo.Politicians");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Politicians",
                c => new
                    {
                        PoliticianID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Bio = c.String(),
                        ImageFilePath = c.String(),
                        DateCreated = c.DateTimeOffset(nullable: false, precision: 7),
                        Constituency = c.String(),
                        ConstituencyContact = c.String(),
                        ConstituencyEmail = c.String(),
                        ParliamentaryContact = c.String(),
                        WebsiteSocialMedia = c.String(),
                        Government_EntityID = c.Int(nullable: false),
                        Party_PartyID = c.Int(),
                    })
                .PrimaryKey(t => t.PoliticianID);
            
            CreateTable(
                "dbo.LawVotes",
                c => new
                    {
                        VoteID = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        IsPositive = c.Boolean(nullable: false),
                        Law_LawID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.VoteID);
            
            CreateTable(
                "dbo.Laws",
                c => new
                    {
                        LawID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Text = c.String(),
                        DocumentFilepath = c.String(),
                        DatePassed = c.DateTimeOffset(precision: 7),
                        YearPassed = c.Int(nullable: false),
                        YearPublicationNumber = c.Int(nullable: false),
                        DateCreated = c.DateTimeOffset(nullable: false, precision: 7),
                        Government_EntityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LawID);
            
            CreateTable(
                "dbo.GovernmentVotes",
                c => new
                    {
                        VoteID = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        IsPositive = c.Boolean(nullable: false),
                        Government_EntityID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.VoteID);
            
            CreateTable(
                "dbo.Governments",
                c => new
                    {
                        EntityID = c.Int(nullable: false, identity: true),
                        Credit = c.Double(),
                        Name = c.String(maxLength: 50),
                        Jurisdiction = c.String(maxLength: 50),
                        DateFormed = c.DateTimeOffset(precision: 7),
                        CountryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityID);
            
            AddColumn("dbo.Votes", "Politician_PoliticianID", c => c.Int(nullable: false));
            AddColumn("dbo.Sections", "Law_LawID", c => c.Int());
            AddColumn("dbo.Comments", "Politician_PoliticianID", c => c.Int());
            AddColumn("dbo.Comments", "Law_LawID", c => c.Int());
            AddColumn("dbo.Comments", "Government_EntityID", c => c.Int());
            AddColumn("dbo.Comments", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            DropForeignKey("dbo.Organisations", "CountryID", "dbo.Countries");
            DropForeignKey("dbo.Sections", "Discussion_DiscussionId", "dbo.Discussions");
            DropIndex("dbo.Sections", new[] { "Discussion_DiscussionId" });
            DropColumn("dbo.Sections", "Discussion_DiscussionId");
            RenameIndex(table: "dbo.Organisations", name: "IX_CountryID", newName: "IX_Country_CountryID");
            RenameColumn(table: "dbo.Organisations", name: "CountryID", newName: "Country_CountryID");
            CreateIndex("dbo.Politicians", "Party_PartyID");
            CreateIndex("dbo.Politicians", "Government_EntityID");
            CreateIndex("dbo.Votes", "Politician_PoliticianID");
            CreateIndex("dbo.LawVotes", "User_Id");
            CreateIndex("dbo.LawVotes", "Law_LawID");
            CreateIndex("dbo.Laws", "Government_EntityID");
            CreateIndex("dbo.Sections", "Law_LawID");
            CreateIndex("dbo.GovernmentVotes", "User_Id");
            CreateIndex("dbo.GovernmentVotes", "Government_EntityID");
            CreateIndex("dbo.Governments", "CountryID");
            CreateIndex("dbo.Comments", "Politician_PoliticianID");
            CreateIndex("dbo.Comments", "Law_LawID");
            CreateIndex("dbo.Comments", "Government_EntityID");
            AddForeignKey("dbo.Organisations", "Country_CountryID", "dbo.Countries", "CountryID", cascadeDelete: true);
            AddForeignKey("dbo.Votes", "Politician_PoliticianID", "dbo.Politicians", "PoliticianID", cascadeDelete: true);
            AddForeignKey("dbo.Politicians", "Party_PartyID", "dbo.Parties", "PartyID");
            AddForeignKey("dbo.Politicians", "Government_EntityID", "dbo.Governments", "EntityID", cascadeDelete: true);
            AddForeignKey("dbo.Comments", "Politician_PoliticianID", "dbo.Politicians", "PoliticianID", cascadeDelete: true);
            AddForeignKey("dbo.LawVotes", "User_Id", "dbo.ApplicationUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LawVotes", "Law_LawID", "dbo.Laws", "LawID", cascadeDelete: true);
            AddForeignKey("dbo.Sections", "Law_LawID", "dbo.Laws", "LawID");
            AddForeignKey("dbo.Laws", "Government_EntityID", "dbo.Governments", "EntityID", cascadeDelete: true);
            AddForeignKey("dbo.Comments", "Law_LawID", "dbo.Laws", "LawID", cascadeDelete: true);
            AddForeignKey("dbo.GovernmentVotes", "User_Id", "dbo.ApplicationUsers", "Id");
            AddForeignKey("dbo.GovernmentVotes", "Government_EntityID", "dbo.Governments", "EntityID", cascadeDelete: true);
            AddForeignKey("dbo.Comments", "Government_EntityID", "dbo.Governments", "EntityID", cascadeDelete: true);
            RenameTable(name: "dbo.Staff", newName: "Staffs");
        }
    }
}
