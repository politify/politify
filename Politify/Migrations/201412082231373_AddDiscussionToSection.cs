namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDiscussionToSection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sections", "Discussion_DiscussionId", c => c.Int());
            CreateIndex("dbo.Sections", "Discussion_DiscussionId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Sections", new[] { "Discussion_DiscussionId" });
            DropColumn("dbo.Sections", "Discussion_DiscussionId");
        }
    }
}
