namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SectionViewModels7 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
    ALTER VIEW [dbo].[SectionCommentVMs] AS    SELECT        c.CommentID, c.Text, c.CreatedDate, c.Edited, c.LastEdit, c.LastText, c.OriginalText, c.SectionEdit_SectionEditId, c.User_Id, c.SectionEvidence_SectionEvidenceId, 
                         c.SectionExplanation_SectionExplanationId, c.Parent_CommentID, c.Section_SectionID, v.Positives, v.Negatives, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionComments AS c INNER JOIN
                         dbo.ApplicationUsers ON c.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionComment_CommentID AS SectionCommentID, 
											CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, 
											CONVERT(float, SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes where SectionComment_CommentID is not null
                               GROUP BY SectionComment_CommentID) AS v ON c.CommentID = v.SectionCommentID
");
        
            Sql(@"
ALTER VIEW [dbo].[SectionEditVMs] AS SELECT        edit.SectionEditId, edit.Text, edit.DateCreated, edit.Section_SectionID, edit.User_Id, v.Positives, v.Negatives, c.CommentCount, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionEdits AS edit INNER JOIN
                         dbo.ApplicationUsers ON edit.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionEdit_SectionEditId AS SectionEditID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes where SectionEdit_SectionEditId is not null
                               GROUP BY SectionEdit_SectionEditId) AS v ON edit.SectionEditId = v.SectionEditID FULL OUTER JOIN
                             (SELECT        SectionEdit_SectionEditId AS SectionEditID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments  where SectionEdit_SectionEditId is not null
                               GROUP BY SectionEdit_SectionEditId) AS c ON edit.SectionEditId = c.SectionEditID
");
            Sql(@"
ALTER VIEW [dbo].[SectionEvidenceVMs]
AS SELECT        ev.SectionEvidenceId, ev.Text, ev.FileName, ev.DateCreated, ev.Section_SectionID, ev.User_Id, v.Positives, v.Negatives, c.CommentCount, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.FirstName, dbo.ApplicationUsers.UserName
FROM            dbo.SectionEvidences AS ev INNER JOIN
                         dbo.ApplicationUsers ON ev.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionEvidence_SectionEvidenceId AS SectionEvID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes where SectionEvidence_SectionEvidenceId is not null
                               GROUP BY SectionEvidence_SectionEvidenceId) AS v ON ev.SectionEvidenceId = v.SectionEvID FULL OUTER JOIN
                             (SELECT        SectionEvidence_SectionEvidenceId AS SectionEvID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments where SectionEvidence_SectionEvidenceId is not null
                               GROUP BY SectionEvidence_SectionEvidenceId) AS c ON ev.SectionEvidenceId = c.SectionEvID
");

            Sql(@"
ALTER VIEW [dbo].[SectionExplanationVMs]
AS SELECT        ex.SectionExplanationId, ex.Text, ex.DateCreated, ex.Section_SectionID, ex.User_Id, v.Positives, v.Negatives, c.CommentCount, dbo.ApplicationUsers.FirstName, 
                         dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.UserName
FROM            dbo.SectionExplanations AS ex INNER JOIN
                         dbo.ApplicationUsers ON ex.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        SectionExplanation_SectionExplanationId AS SectionExID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes where SectionExplanation_SectionExplanationId is not null
                               GROUP BY SectionExplanation_SectionExplanationId) AS v ON ex.SectionExplanationId = v.SectionExID FULL OUTER JOIN
                             (SELECT        SectionExplanation_SectionExplanationId AS SectionExID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments where SectionExplanation_SectionExplanationId is not null
                               GROUP BY SectionExplanation_SectionExplanationId) AS c ON ex.SectionExplanationId = c.SectionExID
");

            Sql(@"
                ALTER VIEW [dbo].[SectionVMs]
AS
SELECT        dbo.Sections.SectionID, dbo.Sections.Text, dbo.Sections.Html, dbo.Sections.OrderQualifier, dbo.Sections.Parent_SectionID, dbo.Sections.IsUserCreated, 
                         dbo.Sections.User_Id, dbo.Sections.Organisation_OrganisationId, dbo.Sections.DateCreated, c.CommentCount, e.EditCount, ev.EvidenceCount, ex.ExplanationCount, 
                         v.Positives, v.Negatives, dbo.ApplicationUsers.Surname, dbo.ApplicationUsers.FirstName, dbo.ApplicationUsers.UserName
FROM            dbo.Sections LEFT OUTER JOIN
                         dbo.ApplicationUsers ON dbo.Sections.User_Id = dbo.ApplicationUsers.Id FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS CommentCount
                               FROM            dbo.SectionComments where Section_SectionID is not null
                               GROUP BY Section_SectionID) AS c ON dbo.Sections.SectionID = c.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS EditCount
                               FROM            dbo.SectionEdits
                               GROUP BY Section_SectionID) AS e ON dbo.Sections.SectionID = e.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS EvidenceCount
                               FROM            dbo.SectionEvidences
                               GROUP BY Section_SectionID) AS ev ON dbo.Sections.SectionID = ev.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, COUNT(*)) AS ExplanationCount
                               FROM            dbo.SectionExplanations
                               GROUP BY Section_SectionID) AS ex ON dbo.Sections.SectionID = ex.SectionID FULL OUTER JOIN
                             (SELECT        Section_SectionID AS SectionID, CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, CONVERT(float, 
                                                         SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes where Section_SectionID is not null
                               GROUP BY Section_SectionID) AS v ON dbo.Sections.SectionID = v.SectionID
");


            Sql(@"ALTER VIEW [dbo].[DiscussionVMs]
AS
SELECT        d.DiscussionId, d.DateCreated, d.Section_SectionID, d.User_Id, dbo.Sections.Text, dbo.Sections.Html, 
                         dbo.Sections.IsUserCreated, dbo.Sections.Organisation_OrganisationId, v.Positives, v.Negatives, c.CommentCount, 
                         d.OrgCat_OrganisationCategoryId
FROM            dbo.Sections INNER JOIN
                         dbo.Discussions d ON dbo.Sections.SectionID = d.Section_SectionID LEFT JOIN
                             (SELECT        Section_SectionID AS SectionID, 
										CONVERT(float, SUM(CASE IsPositive WHEN 1 THEN 1 ELSE 0 END)) AS Positives, 
										CONVERT(float, SUM(CASE IsPositive WHEN 0 THEN 1 ELSE 0 END)) AS Negatives
                               FROM            dbo.SectionVotes where Section_SectionID is not null
                               GROUP BY Section_SectionID) AS v ON d.Section_SectionID = v.SectionID LEFT JOIN
                             (SELECT        Section_SectionID AS SectionID, COUNT(*) AS CommentCount
                               FROM            dbo.SectionComments where Section_SectionID is not null
                               GROUP BY Section_SectionID) AS c ON d.Section_SectionID = c.SectionID
");
        }

        public override void Down()
        {
        }
    }
}
