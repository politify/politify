namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrgAddOnsCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Organisations", "OnsCode", c => c.String(maxLength: 9));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Organisations", "OnsCode");
        }
    }
}
