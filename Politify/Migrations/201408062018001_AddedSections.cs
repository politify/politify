namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSections : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sections",
                c => new
                    {
                        SectionID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        Html = c.String(),
                        OrderQualifier = c.String(),
                        Law_LawID = c.Int(),
                        Parent_SectionID = c.Int(),
                    })
                .PrimaryKey(t => t.SectionID)
                .ForeignKey("dbo.Laws", t => t.Law_LawID)
                .ForeignKey("dbo.Sections", t => t.Parent_SectionID)
                .Index(t => t.Law_LawID)
                .Index(t => t.Parent_SectionID);
            
            CreateTable(
                "dbo.SectionEdits",
                c => new
                    {
                        SectionEditId = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        DateCreated = c.DateTimeOffset(nullable: false, precision: 7),
                        Section_SectionID = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SectionEditId)
                .ForeignKey("dbo.Sections", t => t.Section_SectionID)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id)
                .Index(t => t.Section_SectionID)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.SectionVotes",
                c => new
                    {
                        VoteID = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTimeOffset(nullable: false, precision: 7),
                        IsPositive = c.Boolean(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                        SectionEdit_SectionEditId = c.Int(),
                        SectionEvidence_SectionExplanationId = c.Int(),
                        SectionExplanation_SectionExplanationId = c.Int(),
                    })
                .PrimaryKey(t => t.VoteID)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.SectionEdits", t => t.SectionEdit_SectionEditId)
                .ForeignKey("dbo.SectionEvidences", t => t.SectionEvidence_SectionExplanationId)
                .ForeignKey("dbo.SectionExplanations", t => t.SectionExplanation_SectionExplanationId)
                .Index(t => t.User_Id)
                .Index(t => t.SectionEdit_SectionEditId)
                .Index(t => t.SectionEvidence_SectionExplanationId)
                .Index(t => t.SectionExplanation_SectionExplanationId);
            
            CreateTable(
                "dbo.SectionEvidences",
                c => new
                    {
                        SectionExplanationId = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        FileName = c.String(),
                        DateCreated = c.DateTimeOffset(nullable: false, precision: 7),
                        Section_SectionID = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SectionExplanationId)
                .ForeignKey("dbo.Sections", t => t.Section_SectionID)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id)
                .Index(t => t.Section_SectionID)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.SectionExplanations",
                c => new
                    {
                        SectionExplanationId = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        DateCreated = c.DateTimeOffset(nullable: false, precision: 7),
                        Section_SectionID = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SectionExplanationId)
                .ForeignKey("dbo.Sections", t => t.Section_SectionID)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id)
                .Index(t => t.Section_SectionID)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SectionVotes", "SectionExplanation_SectionExplanationId", "dbo.SectionExplanations");
            DropForeignKey("dbo.SectionExplanations", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.SectionExplanations", "Section_SectionID", "dbo.Sections");
            DropForeignKey("dbo.SectionVotes", "SectionEvidence_SectionExplanationId", "dbo.SectionEvidences");
            DropForeignKey("dbo.SectionEvidences", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.SectionEvidences", "Section_SectionID", "dbo.Sections");
            DropForeignKey("dbo.SectionVotes", "SectionEdit_SectionEditId", "dbo.SectionEdits");
            DropForeignKey("dbo.SectionVotes", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.SectionEdits", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.SectionEdits", "Section_SectionID", "dbo.Sections");
            DropForeignKey("dbo.Sections", "Parent_SectionID", "dbo.Sections");
            DropForeignKey("dbo.Sections", "Law_LawID", "dbo.Laws");
            DropIndex("dbo.SectionExplanations", new[] { "User_Id" });
            DropIndex("dbo.SectionExplanations", new[] { "Section_SectionID" });
            DropIndex("dbo.SectionEvidences", new[] { "User_Id" });
            DropIndex("dbo.SectionEvidences", new[] { "Section_SectionID" });
            DropIndex("dbo.SectionVotes", new[] { "SectionExplanation_SectionExplanationId" });
            DropIndex("dbo.SectionVotes", new[] { "SectionEvidence_SectionExplanationId" });
            DropIndex("dbo.SectionVotes", new[] { "SectionEdit_SectionEditId" });
            DropIndex("dbo.SectionVotes", new[] { "User_Id" });
            DropIndex("dbo.SectionEdits", new[] { "User_Id" });
            DropIndex("dbo.SectionEdits", new[] { "Section_SectionID" });
            DropIndex("dbo.Sections", new[] { "Parent_SectionID" });
            DropIndex("dbo.Sections", new[] { "Law_LawID" });
            DropTable("dbo.SectionExplanations");
            DropTable("dbo.SectionEvidences");
            DropTable("dbo.SectionVotes");
            DropTable("dbo.SectionEdits");
            DropTable("dbo.Sections");
        }
    }
}
