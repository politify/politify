namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompleteOverhaul : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.GovernmentComments", newName: "Comments");
            RenameTable(name: "dbo.PoliticianVotes", newName: "Votes");
            DropIndex("dbo.Comments", new[] { "Government_EntityID" });
            DropIndex("dbo.LawComments", new[] { "Law_LawID" });
            DropIndex("dbo.LawComments", new[] { "User_Id" });
            DropIndex("dbo.PoliticianComments", new[] { "Politician_PoliticianID" });
            DropIndex("dbo.PoliticianComments", new[] { "User_Id" });
            RenameColumn(table: "dbo.SectionComments", name: "SectionComment_CommentID", newName: "Parent_CommentID");
            RenameIndex(table: "dbo.SectionComments", name: "IX_SectionComment_CommentID", newName: "IX_Parent_CommentID");
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Discussions",
                c => new
                    {
                        DiscussionId = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTimeOffset(nullable: false, precision: 7),
                        Category_CategoryId = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.DiscussionId)
                .ForeignKey("dbo.Categories", t => t.Category_CategoryId)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id)
                .Index(t => t.Category_CategoryId)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Organisations",
                c => new
                    {
                        OrganisationId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 400),
                        Description = c.String(),
                        ProfilePicture = c.String(maxLength: 100),
                        OrganisationType = c.Int(nullable: false),
                        EmailAddress = c.String(),
                        Country_CountryID = c.Int(nullable: false),
                        Parent_OrganisationId = c.Int(),
                    })
                .PrimaryKey(t => t.OrganisationId)
                .ForeignKey("dbo.Countries", t => t.Country_CountryID, cascadeDelete: true)
                .ForeignKey("dbo.Organisations", t => t.Parent_OrganisationId)
                .Index(t => t.Country_CountryID)
                .Index(t => t.Parent_OrganisationId);
            
            CreateTable(
                "dbo.SignatureCounts",
                c => new
                    {
                        SignatureCountId = c.Int(nullable: false, identity: true),
                        Count = c.Int(nullable: false),
                        Action = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.SignatureCountId);
            
            CreateTable(
                "dbo.Staffs",
                c => new
                    {
                        StaffId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        JobTitle = c.String(nullable: false),
                        ProfilePicture = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.StaffId)
                .ForeignKey("dbo.ApplicationUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Postcodes",
                c => new
                    {
                        PostCode = c.String(nullable: false, maxLength: 128),
                        Ward_OrganisationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PostCode)
                .ForeignKey("dbo.Organisations", t => t.Ward_OrganisationId, cascadeDelete: true)
                .Index(t => t.Ward_OrganisationId);
            
            AddColumn("dbo.Comments", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Comments", "Discussion_DiscussionId", c => c.Int());
            AddColumn("dbo.Comments", "Organisation_OrganisationId", c => c.Int());
            AddColumn("dbo.Comments", "Parent_CommentID", c => c.Int());
            AddColumn("dbo.Comments", "Party_PartyID", c => c.Int());
            AddColumn("dbo.Comments", "SignatureCount_SignatureCountId", c => c.Int());
            AddColumn("dbo.Comments", "Staff_StaffId", c => c.Int());
            AddColumn("dbo.Comments", "Law_LawID", c => c.Int());
            AddColumn("dbo.Comments", "Politician_PoliticianID", c => c.Int());
            AddColumn("dbo.Votes", "Discussion_DiscussionId", c => c.Int());
            AddColumn("dbo.Votes", "Organisation_OrganisationId", c => c.Int());
            AddColumn("dbo.Votes", "Party_PartyID", c => c.Int());
            AddColumn("dbo.Votes", "SignatureCount_SignatureCountId", c => c.Int());
            AddColumn("dbo.Votes", "Staff_StaffId", c => c.Int());
            AlterColumn("dbo.Comments", "Government_EntityID", c => c.Int());
            CreateIndex("dbo.Comments", "Government_EntityID");
            CreateIndex("dbo.Comments", "Discussion_DiscussionId");
            CreateIndex("dbo.Comments", "Organisation_OrganisationId");
            CreateIndex("dbo.Comments", "Parent_CommentID");
            CreateIndex("dbo.Comments", "Party_PartyID");
            CreateIndex("dbo.Comments", "SignatureCount_SignatureCountId");
            CreateIndex("dbo.Comments", "Staff_StaffId");
            CreateIndex("dbo.Comments", "Law_LawID");
            CreateIndex("dbo.Comments", "Politician_PoliticianID");
            CreateIndex("dbo.Votes", "Discussion_DiscussionId");
            CreateIndex("dbo.Votes", "Organisation_OrganisationId");
            CreateIndex("dbo.Votes", "Party_PartyID");
            CreateIndex("dbo.Votes", "SignatureCount_SignatureCountId");
            CreateIndex("dbo.Votes", "Staff_StaffId");
            AddForeignKey("dbo.Comments", "Discussion_DiscussionId", "dbo.Discussions", "DiscussionId");
            AddForeignKey("dbo.Comments", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Comments", "Parent_CommentID", "dbo.Comments", "CommentID");
            AddForeignKey("dbo.Comments", "Party_PartyID", "dbo.Parties", "PartyID");
            AddForeignKey("dbo.Comments", "SignatureCount_SignatureCountId", "dbo.SignatureCounts", "SignatureCountId");
            AddForeignKey("dbo.Comments", "Staff_StaffId", "dbo.Staffs", "StaffId");
            AddForeignKey("dbo.Votes", "Discussion_DiscussionId", "dbo.Discussions", "DiscussionId");
            AddForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations", "OrganisationId");
            AddForeignKey("dbo.Votes", "Party_PartyID", "dbo.Parties", "PartyID");
            AddForeignKey("dbo.Votes", "SignatureCount_SignatureCountId", "dbo.SignatureCounts", "SignatureCountId");
            AddForeignKey("dbo.Votes", "Staff_StaffId", "dbo.Staffs", "StaffId");
            DropTable("dbo.Businesses");
            DropTable("dbo.LawComments");
            DropTable("dbo.PoliticianComments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PoliticianComments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        Edited = c.Boolean(nullable: false),
                        LastEdit = c.DateTimeOffset(precision: 7),
                        LastText = c.String(),
                        OriginalText = c.String(),
                        Politician_PoliticianID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CommentID);
            
            CreateTable(
                "dbo.LawComments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        Edited = c.Boolean(nullable: false),
                        LastEdit = c.DateTimeOffset(precision: 7),
                        LastText = c.String(),
                        OriginalText = c.String(),
                        Law_LawID = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CommentID);
            
            CreateTable(
                "dbo.Businesses",
                c => new
                    {
                        EntityID = c.Int(nullable: false, identity: true),
                        Profit = c.Double(),
                        Name = c.String(maxLength: 50),
                        Jurisdiction = c.String(maxLength: 50),
                        DateFormed = c.DateTimeOffset(precision: 7),
                    })
                .PrimaryKey(t => t.EntityID);
            
            DropForeignKey("dbo.Votes", "Staff_StaffId", "dbo.Staffs");
            DropForeignKey("dbo.Votes", "SignatureCount_SignatureCountId", "dbo.SignatureCounts");
            DropForeignKey("dbo.Votes", "Party_PartyID", "dbo.Parties");
            DropForeignKey("dbo.Votes", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Votes", "Discussion_DiscussionId", "dbo.Discussions");
            DropForeignKey("dbo.Postcodes", "Ward_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Comments", "Staff_StaffId", "dbo.Staffs");
            DropForeignKey("dbo.Staffs", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Comments", "SignatureCount_SignatureCountId", "dbo.SignatureCounts");
            DropForeignKey("dbo.Comments", "Party_PartyID", "dbo.Parties");
            DropForeignKey("dbo.Comments", "Parent_CommentID", "dbo.Comments");
            DropForeignKey("dbo.Comments", "Organisation_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Organisations", "Parent_OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Organisations", "Country_CountryID", "dbo.Countries");
            DropForeignKey("dbo.Comments", "Discussion_DiscussionId", "dbo.Discussions");
            DropForeignKey("dbo.Discussions", "User_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Discussions", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Votes", new[] { "Staff_StaffId" });
            DropIndex("dbo.Votes", new[] { "SignatureCount_SignatureCountId" });
            DropIndex("dbo.Votes", new[] { "Party_PartyID" });
            DropIndex("dbo.Votes", new[] { "Organisation_OrganisationId" });
            DropIndex("dbo.Votes", new[] { "Discussion_DiscussionId" });
            DropIndex("dbo.Postcodes", new[] { "Ward_OrganisationId" });
            DropIndex("dbo.Staffs", new[] { "User_Id" });
            DropIndex("dbo.Organisations", new[] { "Parent_OrganisationId" });
            DropIndex("dbo.Organisations", new[] { "Country_CountryID" });
            DropIndex("dbo.Discussions", new[] { "User_Id" });
            DropIndex("dbo.Discussions", new[] { "Category_CategoryId" });
            DropIndex("dbo.Comments", new[] { "Politician_PoliticianID" });
            DropIndex("dbo.Comments", new[] { "Law_LawID" });
            DropIndex("dbo.Comments", new[] { "Staff_StaffId" });
            DropIndex("dbo.Comments", new[] { "SignatureCount_SignatureCountId" });
            DropIndex("dbo.Comments", new[] { "Party_PartyID" });
            DropIndex("dbo.Comments", new[] { "Parent_CommentID" });
            DropIndex("dbo.Comments", new[] { "Organisation_OrganisationId" });
            DropIndex("dbo.Comments", new[] { "Discussion_DiscussionId" });
            DropIndex("dbo.Comments", new[] { "Government_EntityID" });
            AlterColumn("dbo.Comments", "Government_EntityID", c => c.Int(nullable: false));
            DropColumn("dbo.Votes", "Staff_StaffId");
            DropColumn("dbo.Votes", "SignatureCount_SignatureCountId");
            DropColumn("dbo.Votes", "Party_PartyID");
            DropColumn("dbo.Votes", "Organisation_OrganisationId");
            DropColumn("dbo.Votes", "Discussion_DiscussionId");
            DropColumn("dbo.Comments", "Politician_PoliticianID");
            DropColumn("dbo.Comments", "Law_LawID");
            DropColumn("dbo.Comments", "Staff_StaffId");
            DropColumn("dbo.Comments", "SignatureCount_SignatureCountId");
            DropColumn("dbo.Comments", "Party_PartyID");
            DropColumn("dbo.Comments", "Parent_CommentID");
            DropColumn("dbo.Comments", "Organisation_OrganisationId");
            DropColumn("dbo.Comments", "Discussion_DiscussionId");
            DropColumn("dbo.Comments", "Discriminator");
            DropTable("dbo.Postcodes");
            DropTable("dbo.Staffs");
            DropTable("dbo.SignatureCounts");
            DropTable("dbo.Organisations");
            DropTable("dbo.Discussions");
            DropTable("dbo.Categories");
            RenameIndex(table: "dbo.SectionComments", name: "IX_Parent_CommentID", newName: "IX_SectionComment_CommentID");
            RenameColumn(table: "dbo.SectionComments", name: "Parent_CommentID", newName: "SectionComment_CommentID");
            CreateIndex("dbo.PoliticianComments", "User_Id");
            CreateIndex("dbo.PoliticianComments", "Politician_PoliticianID");
            CreateIndex("dbo.LawComments", "User_Id");
            CreateIndex("dbo.LawComments", "Law_LawID");
            CreateIndex("dbo.Comments", "Government_EntityID");
            RenameTable(name: "dbo.Votes", newName: "PoliticianVotes");
            RenameTable(name: "dbo.Comments", newName: "GovernmentComments");
        }
    }
}
