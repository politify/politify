namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SectionBetterNavProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SectionComments", "SectionComment_CommentID", c => c.Int());
            AddColumn("dbo.SectionComments", "Section_SectionID", c => c.Int());
            AddColumn("dbo.SectionVotes", "SectionComment_CommentID", c => c.Int());
            CreateIndex("dbo.SectionComments", "SectionComment_CommentID");
            CreateIndex("dbo.SectionComments", "Section_SectionID");
            CreateIndex("dbo.SectionVotes", "SectionComment_CommentID");
            AddForeignKey("dbo.SectionComments", "SectionComment_CommentID", "dbo.SectionComments", "CommentID");
            AddForeignKey("dbo.SectionComments", "Section_SectionID", "dbo.Sections", "SectionID");
            AddForeignKey("dbo.SectionVotes", "SectionComment_CommentID", "dbo.SectionComments", "CommentID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SectionVotes", "SectionComment_CommentID", "dbo.SectionComments");
            DropForeignKey("dbo.SectionComments", "Section_SectionID", "dbo.Sections");
            DropForeignKey("dbo.SectionComments", "SectionComment_CommentID", "dbo.SectionComments");
            DropIndex("dbo.SectionVotes", new[] { "SectionComment_CommentID" });
            DropIndex("dbo.SectionComments", new[] { "Section_SectionID" });
            DropIndex("dbo.SectionComments", new[] { "SectionComment_CommentID" });
            DropColumn("dbo.SectionVotes", "SectionComment_CommentID");
            DropColumn("dbo.SectionComments", "Section_SectionID");
            DropColumn("dbo.SectionComments", "SectionComment_CommentID");
        }
    }
}
