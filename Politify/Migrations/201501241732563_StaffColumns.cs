namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StaffColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Staff", "Bio", c => c.String());
            AddColumn("dbo.Staff", "EmailAddress", c => c.String());
            AddColumn("dbo.Staff", "Twitter", c => c.String());
            AddColumn("dbo.Staff", "DateCreated", c => c.DateTimeOffset(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Staff", "DateCreated");
            DropColumn("dbo.Staff", "Twitter");
            DropColumn("dbo.Staff", "EmailAddress");
            DropColumn("dbo.Staff", "Bio");
        }
    }
}
