namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTimeOffsetTest : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Parties", "DateCreated", c => c.DateTimeOffset(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Parties", "DateCreated", c => c.DateTime(nullable: false));
        }
    }
}
