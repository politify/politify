namespace Politify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganisationVmUpdate21 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE VIEW [dbo].[OrganisationCategoryVMs]
AS
SELECT        dbo.Categories.*, dbo.OrganisationCategories.Organisation_OrganisationId
FROM            dbo.Categories INNER JOIN
                         dbo.OrganisationCategories ON dbo.Categories.CategoryId = dbo.OrganisationCategories.Category_CategoryId

");
        }
        
        public override void Down()
        {
            Sql(@"
DROP VIEW [dbo].[OrganisationCategoryVMs]
AS
SELECT        dbo.Categories.*, dbo.OrganisationCategories.Organisation_OrganisationId
FROM            dbo.Categories INNER JOIN
                         dbo.OrganisationCategories ON dbo.Categories.CategoryId = dbo.OrganisationCategories.Category_CategoryId

");
        }
    }
}
