USE [Politify]
GO
SET IDENTITY_INSERT [dbo].[Politicians] ON 

INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (2, N'Mr Jim Hood MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25176.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Lanark and Hamilton East', N'
                        c/o Council Offices, South Vennel, Lanark, ML11 7JT<br>

                        
                            Tel: 01555 673177<br>
                        
                        
                            Fax: 01555 673188<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:davidsonh@parliament.uk">davidsonh@parliament.uk</a>
', N'davidsonh@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4585<br>
                        
                        
                            Fax: 020 7219 5872<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hoodj@parliament.uk">hoodj@parliament.uk</a>
                    ', N'jimhoodmp.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (8, N'Rt Hon Theresa May MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25452.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Maidenhead', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5206<br>
                        
                        
                            Fax: 020 7219 1145<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mayt@parliament.uk">mayt@parliament.uk</a>
', N'mayt@parliament.uk', N'
                        Maidenhead Conservative Association, 2 Castle End Farm, Ruscombe, RG10 9XQ<br>

                        
                            Tel: 0118 934 5433<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@maidenheadconservatives.com">office@maidenheadconservatives.com</a>
                    ', N'www.tmay.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (12, N'Fiona Mactaggart MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25187.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Slough', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3416<br>
                        
                        
                            Fax: 020 7219 0989<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mactaggartf@parliament.uk">mactaggartf@parliament.uk</a>
', N'mactaggartf@parliament.uk', N'
                        52 High Street, Chalvey, Slough, SL1 2SQ<br>

                        
                            Tel: 01753 518161<br>
                        
                        
                            Fax: 01753 550293<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:fiona.mactaggart@gmail.com">fiona.mactaggart@gmail.com</a>
                    ', N'www.fionamactaggart.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (14, N'Mark Reckless MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31596.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rochester and Strood', N'
                        Suite 6, 4A Castle View Mews, Castle Hill, Rochester, ME1 1LA<br>

                        
                            Tel: 01634 409917<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7135<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mark.reckless.mp@parliament.uk">mark.reckless.mp@parliament.uk</a>
                    ', N'www.markreckless.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (15, N'Rt Hon David Lidington MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25473.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Aylesbury', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3432<br>
                        
                        
                            Fax: 020 7219 2564<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david.lidington.mp@parliament.uk">david.lidington.mp@parliament.uk</a>
', N'david.lidington.mp@parliament.uk', N'
                        100 Walton Street, Aylesbury, HP21 7QP<br>

                        
                            Tel: 01296 482102<br>
                        
                        
                            Fax: 01296 398481<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@aylesburyconservatives.com">office@aylesburyconservatives.com</a>
                    ', N'www.davidlidington.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (16, N'Rt Hon Dominic Grieve QC MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25630.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Beaconsfield', N'
                        Disraeli House, 12 Aylesbury End, Beaconsfield, HP9 1LW<br>

                        
                            Tel: 01494 673745<br>
                        
                        
                            Fax: 01494 670428<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@beaconsfieldconservatives.co.uk">office@beaconsfieldconservatives.co.uk</a>
', N'office@beaconsfieldconservatives.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6220<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:dominic.grieve.mp@parliament.uk">dominic.grieve.mp@parliament.uk</a>
                    ', N'www.dominicgrieve.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (17, N'Rt Hon John Bercow MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25808.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Buckingham', N'
                        Speaker''s House, House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6346<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:john.bercow.mp@parliament.uk%20">john.bercow.mp@parliament.uk </a>
', N'john.bercow.mp@parliament.uk ', N'
                        Speaker’s Office, House of Commons, London, SW1A 0AA, (Please note: briefings and other representations from lobbying/interest groups should be sent to the Constituency Office)<br>

                        
                            Tel: 020 7219 5300<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:speakersoffice@parliament.uk%20">speakersoffice@parliament.uk </a>
                    ', N'www.johnbercow.co.uk', 1314, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (18, N'Rt Hon Cheryl Gillan MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25627.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chesham and Amersham', N'
                        7A Hill Avenue, Amersham, HP6 5BD<br>

                        
                            Tel: 01494 721577<br>
                        
                        
                            Fax: 01494 722107<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:shawmj@parliament.uk">shawmj@parliament.uk</a>
', N'shawmj@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4061<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:cheryl.gillan.mp@parliament.uk">cheryl.gillan.mp@parliament.uk</a>
                    ', N'www.cherylgillan.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (28, N'Norman Baker MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25802.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Lewes', N'
                        23 East Street, Lewes, BN7 2LJ<br>

                        
                            Tel: 01273 480281<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:normanbaker1997@gmail.com">normanbaker1997@gmail.com</a>
', N'normanbaker1997@gmail.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2864<br>
                        
                        
                            Fax: 020 7219 0445<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:bakern@parliament.uk">bakern@parliament.uk</a>
                    ', N'www.normanbaker.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (33, N'Bridget Phillipson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/79095.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Houghton and Sunderland South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7087<br>
                        
                        
                            Fax: 020 7219 2419<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bridget.phillipson.mp@parliament.uk">bridget.phillipson.mp@parliament.uk</a>
', N'bridget.phillipson.mp@parliament.uk', N'
                        1 and 1a Wylam Place, Mill Pit, Shiney Row, Houghton-le-Spring, DH4 4JT<br>

                        
                            Tel: 0191 385 7994<br>
                        
                        
                            Fax: 0191 385 5941<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.bridgetphillipson.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (35, N'Sir Bob Russell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25361.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Colchester', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5150<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:susan.hislop@parliament.uk">susan.hislop@parliament.uk</a>
', N'susan.hislop@parliament.uk', N'
                        Magdalen Hall, Wimpole Road, Colchester, CO1 2DE<br>

                        
                            Tel: 01206 506600<br>
                        
                        
                            Fax: 01206 506610<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@bobrussell.org.uk">info@bobrussell.org.uk</a>
                    ', N'www.bobrussell.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (36, N'Mrs Eleanor Laing MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25182.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Epping Forest', N'
                        Thatcher House, 4 Meadow Road, Loughton, IG10 4HX<br>

                        
                            Tel: 020 8508 6608<br>
                        
                        
                            Fax: 020 8508 8099<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:efca@binternet.com">efca@binternet.com</a>
', N'efca@binternet.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2086<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:eleanor.laing.mp@parliament.uk">eleanor.laing.mp@parliament.uk</a>
                    ', N'www.eleanorlaing.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (39, N'Mr John Whittingdale MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25239.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Maldon', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3557<br>
                        
                        
                            Fax: 020 7219 2522<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:john.whittingdale.mp@parliament.uk">john.whittingdale.mp@parliament.uk</a>
', N'john.whittingdale.mp@parliament.uk', N'
                        19 High Street, Maldon, Essex, CM9 5PE<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.johnwhittingdale.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (40, N'Rt Hon Sajid Javid MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84360.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bromsgrove', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7027<br>
                        
                        
                            Fax: 020 7219 0930<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sajid.javid.mp@parliament.uk">sajid.javid.mp@parliament.uk</a>
', N'sajid.javid.mp@parliament.uk', N'
                        Rear Office, 18 High Street, Bromsgrove, B61 8HQ<br>

                        
                            Tel: 01527 872135<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.sajidjavid.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (43, N'Rt Hon Sir Alan Haselhurst', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25174.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Saffron Walden', N'
                        The Old Armoury, Saffron Walden, CB10 1JN<br>

                        
                            Tel: 01799 506349<br>
                        
                        
                            Fax: 01799 506047<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@saffronwaldenconservatives.net">office@saffronwaldenconservatives.net</a>
', N'office@saffronwaldenconservatives.net', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5214<br>
                        
                        
                            Fax: 020 7219 5600<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:alan.haselhurst.mp@parliament.uk">alan.haselhurst.mp@parliament.uk</a>
                    ', N'www.siralanhaselhurst.net', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (44, N'Mr David Amess MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25775.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Southend West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3452<br>
                        
                        
                            Fax: 020 7219 2245<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:amessd@parliament.uk">amessd@parliament.uk</a>
', N'amessd@parliament.uk', N'
                        Iveagh Hall, 67 Leigh Road, Leigh-on-Sea, SS9 1JW<br>

                        
                            Tel: 01702 472391<br>
                        
                        
                            Fax: 01702 480677<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:swca@tory.org">swca@tory.org</a>
                    ', N'www.davidamess.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (46, N'Rt Hon Simon Burns MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25788.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chelmsford', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6811<br>
                        
                        
                            Fax: 020 7219 1035<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:burnss@parliament.uk">burnss@parliament.uk</a>
', N'burnss@parliament.uk', N'
                        Constituency address not publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.simonburnsmp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (47, N'Rt Hon George Howarth MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25504.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Knowsley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6902<br>
                        
                        
                            Fax: 020 7219 0495<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:george.howarth.mp@parliament.uk">george.howarth.mp@parliament.uk</a>
', N'george.howarth.mp@parliament.uk', N'
                        Lathom House, North Mersey Business Centre, Woodward Road, Kirkby, L33 7UY<br>

                        
                            Tel: 0151 546 9918<br>
                        
                        
                            Fax: 0151 546 9918<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.georgehowarth.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (53, N'Rt Hon David Willetts MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25241.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Havant', N'
                        c/o Havant Conservative Association, 19 South Street, Havant, PO9 1BU<br>

                        
                            Tel: 02392 499746<br>
                        
                        
                            Fax: 02392 498753<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david@davidwilletts.co.uk">david@davidwilletts.co.uk</a>
', N'david@davidwilletts.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4570<br>
                        
                        
                            Fax: 020 7219 2567<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:willettsd@parliament.uk">willettsd@parliament.uk</a>
                    ', N'www.davidwilletts.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (54, N'Mr Ivan Lewis MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25469.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bury South', N'
                        381 Bury New Road, Prestwich, Manchester, M25 1AW<br>

                        
                            Tel: 0161 773 5500<br>
                        
                        
                            Fax: 0161 773 7959<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ivanlewis@burysouth.fsnet.co.uk">ivanlewis@burysouth.fsnet.co.uk</a>
', N'ivanlewis@burysouth.fsnet.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2609<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:lewisi@parliament.uk">lewisi@parliament.uk</a>
                    ', N'@ivanlewis_mp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (55, N'Rt Hon Desmond Swayne MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25258.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'New Forest West', N'
                        4 Cliff Crescent, Marine Drive, Barton-on-Sea, New Milton, BH25 7EB<br>

                        
                            Tel: 01425 629844<br>
                        
                        
                            Fax: 01425 621898<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4886<br>
                        
                        
                            Fax: 020 7219 0901<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:swayned@parliament.uk">swayned@parliament.uk</a>
                    ', N'www.desmondswaynemp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (56, N'Rt Hon James Arbuthnot MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25712.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North East Hampshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4649<br>
                        
                        
                            Fax: 020 7219 3910<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:james.arbuthnot.mp@parliament.uk">james.arbuthnot.mp@parliament.uk</a>
', N'james.arbuthnot.mp@parliament.uk', N'
                        North East Hampshire Conservative Association, The Mount, Bounty Road, Basingstoke, RG21 3DD<br>

                        
                            Tel: 01256 322207<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@nehc.org.uk">office@nehc.org.uk</a>
                    ', N'www.jamesarbuthnot.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (57, N'Rt Hon Sir George Young MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25225.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North West Hampshire', N'
                        2 Church Close, Andover, SP10 1DP<br>

                        
                            Tel: 01264 401401<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sirgeorge@sirgeorgeyoung.org.uk">sirgeorge@sirgeorgeyoung.org.uk</a>
', N'sirgeorge@sirgeorgeyoung.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6665<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:george.young.mp@parliament.uk">george.young.mp@parliament.uk</a>
                    ', N'www.sirgeorgeyoung.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (59, N'Mr Mike Hancock', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25670.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Portsmouth South', N'
                        1A Albert Road, Southsea, PO5 2SE<br>

                        
                            Tel: 02392 861055<br>
                        
                        
                            Fax: 02392 830530<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:portsmouthldp@cix.co.uk">portsmouthldp@cix.co.uk</a>
', N'portsmouthldp@cix.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1102<br>
                        
                        
                            Fax: 020 7219 2496<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hancockm@parliament.uk">hancockm@parliament.uk</a>
                    ', N'www.mikehancock.co.uk', 1305, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (61, N'Rt Hon John Denham MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25582.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Southampton, Itchen', N'
                        20-22 Southampton Street, Southampton, SO15 2ED<br>

                        
                            Tel: 023 8033 9807<br>
                        
                        
                            Fax: 023 8033 9907<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:john@johndenham.org.uk">john@johndenham.org.uk</a>
', N'john@johndenham.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0067<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:denhamj@parliament.uk">denhamj@parliament.uk</a>
                    ', N'www.johndenham.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (62, N'Dr Alan Whitehead MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25238.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Southampton, Test', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5517<br>
                        
                        
                            Fax: 020 7219 0918<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:whiteheada@parliament.uk">whiteheada@parliament.uk</a>
', N'whiteheada@parliament.uk', N'
                        20-22 Southampton Street, Southampton, SO15 2ED<br>

                        
                            Tel: 02380 231942<br>
                        
                        
                            Fax: 02380 231943<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:alan@alan-whitehead.org.uk">alan@alan-whitehead.org.uk</a>
                    ', N'www.alan-whitehead.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (67, N'Mr Christopher Chope', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25799.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Christchurch', N'
                        18a Bargates, Christchurch, BH23 1QL<br>

                        
                            Tel: 01202 474949<br>
                        
                        
                            Fax: 01202 475548<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@christchurchconservatives.com">office@christchurchconservatives.com</a>
', N'office@christchurchconservatives.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5808<br>
                        
                        
                            Fax: 020 7219 6938<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:chopec@parliament.uk">chopec@parliament.uk</a>
                    ', N'www.christchurchconservatives.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (68, N'Rt Hon Peter Lilley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25184.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hitchin and Harpenden', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4577<br>
                        
                        
                            Fax: 020 7219 3840<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:lilleyp@parliament.uk">lilleyp@parliament.uk</a>
', N'lilleyp@parliament.uk', N'
                        Riverside House, 1 Place Farm, Wheathampstead, AL4 8SB<br>

                        
                            Tel: 01582 834344<br>
                        
                        
                            Fax: 01582 834884<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:feedback@peterlilley.co.uk;%20tory_herts@btconnect.com">feedback@peterlilley.co.uk; tory_herts@btconnect.com</a>
                    ', N'www.peterlilley.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (69, N'Oliver Heald QC MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25566.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North East Hertfordshire', N'
                        No constituency office publicised<br>

                        
                            Tel: 01763 247640<br>
                        
                        
                            Fax: 01763 247640<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:Oliver.heald.mp@parliament.uk">Oliver.heald.mp@parliament.uk</a>
                    ', N'www.oliverhealdmp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (76, N'Rt Hon Damian Green MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25546.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ashford', N'
                        c/o Hardy House, The Street, Bethersden, Ashford, TN26 3AG<br>

                        
                            Tel: 01233 820454<br>
                        
                        
                            Fax: 01233 820111<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3518<br>
                        
                        
                            Fax: 020 7219 0904<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:damian.green.mp@parliament.uk">damian.green.mp@parliament.uk</a>
                    ', N'www.damiangreenmp.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (77, N'Mr Julian Brazier MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25742.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Canterbury', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5178<br>
                        
                        
                            Fax: 020 7219 1685<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        PO Box 1116, Canterbury, CT1 9LQ<br>

                        
                            Tel: 01227 280277<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:canterbury@tory.org">canterbury@tory.org</a>
                    ', N'www.julianbrazier.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (87, N'Sir Roger Gale MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25648.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Thanet', N'
                        The Old Forge, 215a Canterbury Road, Birchington, CT7 9AH<br>

                        
                            Tel: 01843 848588<br>
                        
                        
                            Fax: 01843 844856<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:suzy@galemail.com">suzy@galemail.com</a>
', N'suzy@galemail.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4087<br>
                        
                        
                            Fax: 020 7219 6828<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:galerj@parliament.uk">galerj@parliament.uk</a>
                    ', N'www.rogergale.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (88, N'Rt Hon Michael Fallon MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25169.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sevenoaks', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6482<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:michael.fallon.mp@parliament.uk">michael.fallon.mp@parliament.uk</a>
', N'michael.fallon.mp@parliament.uk', N'
                        Becket House, 13 Vestry Road, Sevenoaks, TN14 5EL<br>

                        
                            Tel: 01732 452261<br>
                        
                        
                            Fax: 0870 051 8023<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@sevenoakstory.org.uk">office@sevenoakstory.org.uk</a>
                    ', N'www.michaelfallonmp.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (91, N'Mr Mark Spencer MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72306.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sherwood', N'
                        Sherwood Constituency Office, Room 3, Under One Roof, 3A Vine Terrace, Hucknall, Nottingham, NG15 7HN<br>

                        
                            Tel: 0115 968 1186<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7143<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mark.spencer.mp@parliament.uk">mark.spencer.mp@parliament.uk</a>
                    ', N'www.markspencermp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (93, N'Rt Hon Sir Tony Baldry MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25155.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Banbury', N'
                        Alexandra House, Church Passage, Banbury, OX16 5JZ<br>

                        
                            Tel: 01295 673873<br>
                        
                        
                            Fax: 01295 263376<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6465<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tony.baldry.mp@parliament.uk">tony.baldry.mp@parliament.uk</a>
                    ', N'www.tonybaldry.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (95, N'Rt Hon Andrew Smith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25823.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Oxford East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5102<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:smithad@parliament.uk">smithad@parliament.uk</a>
', N'smithad@parliament.uk', N'
                        Unit A, Bishop Mews, Transport Way, Oxford, OX4 6HD<br>

                        
                            Tel: 01865 595790<br>
                        
                        
                            Fax: 01865 595799<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:andrewsmith.mp@gmail.com">andrewsmith.mp@gmail.com</a>
                    ', N'www.andrewsmithmp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (98, N'Rt Hon Shaun Woodward MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25256.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'St Helens South and Whiston', N'
                        Century House, Hardshaw Street, St Helens, WA10 1QW<br>

                        
                            Tel: 01744 24226<br>
                        
                        
                            Fax: 01744 24306<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2680<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:woodwardsh@parliament.uk">woodwardsh@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (103, N'Sir Paul Beresford MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25158.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mole Valley', N'
                        Mole Valley Conservative Association, 86 South Street, Dorking, RH4 2EW<br>

                        
                            Tel: 01306 883312<br>
                        
                        
                            Fax: 01306 885194<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mvca@btconnect.com">mvca@btconnect.com</a>
', N'mvca@btconnect.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5018<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:dukem@parliament.uk">dukem@parliament.uk</a>
                    ', N'www.molevalleyconservatives.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (104, N'Mr Crispin Blunt MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25811.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Reigate', N'
                        83 Bell Street, Reigate, RH2 7AN<br>

                        
                            Tel: 01737 222756<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2254<br>
                        
                        
                            Fax: 020 7219 3373<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:crispinbluntmp@parliament.uk">crispinbluntmp@parliament.uk</a>
                    ', N'www.blunt4reigate.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (105, N'Rt Hon Philip Hammond MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25564.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Runnymede and Weybridge', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4055<br>
                        
                        
                            Fax: 020 7219 5851<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hammondp@parliament.uk">hammondp@parliament.uk</a>
', N'hammondp@parliament.uk', N'
                        Runnymede, Spelthorne and Weybridge Conservative Association, 55 Cherry Orchard, Staines, TW18 2DQ<br>

                        
                            Tel: 01784 453544<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@runnymedeweybridgeconservatives.com">office@runnymedeweybridgeconservatives.com</a>
                    ', N'www.runnymedeweybridgeconservatives.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (111, N'Mr Nick Gibb MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25544.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bognor Regis and Littlehampton', N'
                        2 Flansham Business Centre, Hoe Lane, Bognor Regis, PO22 8NJ<br>

                        
                            Tel: 01243 587016<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6374<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:gibbn@parliament.uk">gibbn@parliament.uk</a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (112, N'Mr Andrew Tyrie MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25231.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chichester', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6371<br>
                        
                        
                            Fax: 020 7219 0625<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:tyriea@parliament.uk">tyriea@parliament.uk</a>
', N'tyriea@parliament.uk', N'
                        St. John''s House, St. John''s Street, Chichester, PO19 1UU<br>

                        
                            Tel: 01243 783519<br>
                        
                        
                            Fax: 01243 536848<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@chichester.conservatives.com">office@chichester.conservatives.com</a>
                    ', N'www.andrewtyrie.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (114, N'Tim Loughton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25471.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Worthing and Shoreham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4471<br>
                        
                        
                            Fax: 020 7219 0461<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:loughtont@parliament.uk">loughtont@parliament.uk</a>
', N'loughtont@parliament.uk', N'
                        88A High Street, Shoreham, BN43 5DB<br>

                        
                            Tel: 01273 757182<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:eastworthingandshoreham@tory.org">eastworthingandshoreham@tory.org</a>
                    ', N'www.timloughton.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (115, N'Rt Hon Francis Maude MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25444.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Horsham', N'
                        Gough House, Madeira Avenue, Horsham, RH12 1RL<br>

                        
                            Tel: 01403 242000<br>
                        
                        
                            Fax: 01403 210600<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2494<br>
                        
                        
                            Fax: 020 7219 2990<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:francismaudemp@parliament.uk">francismaudemp@parliament.uk</a>
                    ', N'www.francismaude.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (116, N'Sir Robert Smith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25313.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Aberdeenshire and Kincardine', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5106<br>
                        
                        
                            Fax: 020 7219 4526<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:robert.smith.mp@parliament.uk">robert.smith.mp@parliament.uk</a>
', N'robert.smith.mp@parliament.uk', N'
                        Banchory Business Centre, Burn O''Bennie Road, Banchory, AB31 5ZU<br>

                        
                            Tel: 01330 826549<br>
                        
                        
                            Fax: 01330 820338<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.scotlibdems.org.uk/people/mps/smith', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (117, N'Sir Peter Bottomley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25739.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Worthing West', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5060<br>
                        
                        
                            Fax: 020 7219 1212<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:bottomleyp@parliament.uk">bottomleyp@parliament.uk</a>
                    ', N'www.sirpeterbottomley.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (123, N'Rt Hon Andrew Lansley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25555.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Cambridgeshire', N'
                        153 St Neots Road, Hardwick, Cambridge, CB23 7QJ<br>

                        
                            Tel: 01954 212707<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:lansleya@parliament.uk">lansleya@parliament.uk</a>
                    ', N'www.andrewlansley.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (124, N'Rt Hon Sir James Paice MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25397.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South East Cambridgeshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1347<br>
                        
                        
                            Fax: 020 7219 3804<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:james.paice.mp@parliament.uk">james.paice.mp@parliament.uk</a>
', N'james.paice.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jamespaicemp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (126, N'Mr Keith Simpson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25206.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Broadland', N'
                        Broadland Conservative Association, The Stable, Church Farm, Attlebridge, NR9 5ST<br>

                        
                            Tel: 01603 865763<br>
                        
                        
                            Fax: 01603 865762<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:organiser@broadlandconservatives.org.uk">organiser@broadlandconservatives.org.uk</a>
', N'organiser@broadlandconservatives.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4053<br>
                        
                        
                            Fax: 020 7219 0975<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:keith.simpson.mp@parliament.uk">keith.simpson.mp@parliament.uk</a>
                    ', N'www.keithsimpson.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (133, N'Mr David Ruffley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25360.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bury St Edmunds', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2880<br>
                        
                        
                            Fax: 020 7219 3998<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david.ruffley.mp@parliament.uk">david.ruffley.mp@parliament.uk</a>
', N'david.ruffley.mp@parliament.uk', N'
                        10 Hatter Street, Bury St Edmunds, Suffolk, IP33 1LZ<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@telldavidruffley.com">info@telldavidruffley.com</a>
                    ', N'www.davidruffleymp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (136, N'Mr Tim Yeo MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25224.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Suffolk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6353<br>
                        
                        
                            Fax: 020 7219 4857<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:timyeomp@parliament.uk">timyeomp@parliament.uk</a>
', N'timyeomp@parliament.uk', N'
                        4 Byford Road, Sudbury, Suffolk, CO10 2YG<br>

                        
                            Tel: 01787 312363<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:peter@ss-ca.org.uk">peter@ss-ca.org.uk</a>
                    ', N'www.timyeo.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (140, N'Mr Mark Hoban MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25572.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Fareham', N'
                        14 East Street, Fareham, PO16 0BN<br>

                        
                            Tel: 01329 233573<br>
                        
                        
                            Fax: 01329 234197<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:help@markhoban.com">help@markhoban.com</a>
', N'help@markhoban.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8191<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hobanm@parliament.uk">hobanm@parliament.uk</a>
                    ', N'www.markhoban.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (146, N'Barry Gardiner MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25651.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Brent North', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4046<br>
                        
                        
                            Fax: 020 7219 2495<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:gardinerb@parliament.uk">gardinerb@parliament.uk</a>
                    ', N'www.barrygardiner.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (150, N'Rt Hon Harriet Harman QC MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25677.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Camberwell and Peckham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4218<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:harmanh@parliament.uk">harmanh@parliament.uk</a>
', N'harmanh@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.harrietharman.org/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (151, N'Rt Hon Tom Brake MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25726.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Carshalton and Wallington', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0924<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:braket@parliament.uk">braket@parliament.uk</a>
', N'braket@parliament.uk', N'
                        Kennedy House, 5 Nightingale Road, Carshalton, SM5 2DN<br>

                        
                            Tel: 020 8255 8155<br>
                        
                        
                            Fax: 020 8395 4453<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@tombrake.co.uk">info@tombrake.co.uk</a>
                    ', N'www.tombrake.co.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (152, N'Rt Hon Iain Duncan Smith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25640.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chingford and Woodford Green', N'
                        64A Station Road, Chingford, London, E4 7BE<br>

                        
                            Tel: 020 8524 4344<br>
                        
                        
                            Fax: 020 8523 9697<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:cwgca@tory.org">cwgca@tory.org</a>
', N'cwgca@tory.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2667<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nashj@parliament.uk">nashj@parliament.uk</a>
                    ', N'www.gov.uk/government/organisations/department-for-work-pensions', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (155, N'Geraint Davies', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25772.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Swansea West', N'
                        011 - 9th Floor, Princess House, Princess Way, Swansea, SA1 3LW<br>

                        
                            Tel: 01792 475943<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:info@geraintdavies.org">info@geraintdavies.org</a>
', N'info@geraintdavies.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7166<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:geraint.davies.mp@parliament.uk">geraint.davies.mp@parliament.uk</a>
                    ', N'www.geraintdavies.org.uk/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (157, N'Rt Hon Sir Richard Ottaway MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25394.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Croydon South', N'
                        Croydon South Conservative Association, 36 Brighton Road, Purley, CR8 2LG<br>

                        
                            Tel: 020 8660 0491<br>
                        
                        
                            Fax: 020 8763 9686<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:croydonconservatives@tory.org">croydonconservatives@tory.org</a>
', N'croydonconservatives@tory.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6392<br>
                        
                        
                            Fax: 020 7219 2256<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ottawayr@parliament.uk">ottawayr@parliament.uk</a>
                    ', N'www.richardottaway.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (159, N'Rt Hon Dame  Tessa Jowell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25526.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dulwich and West Norwood', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3409<br>
                        
                        
                            Fax: 020 7219 2702<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jowellt@parliament.uk">jowellt@parliament.uk</a>
', N'jowellt@parliament.uk', N'
                        264 Rosendale Road, London, SE24 9DL<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.tessajowell.net', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (161, N'Dr Daniel Poulter MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83524.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Central Suffolk and North Ipswich', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7038<br>
                        
                        
                            Fax: 020 7219 1192<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:daniel.poulter.mp@parliament.uk">daniel.poulter.mp@parliament.uk</a>
', N'daniel.poulter.mp@parliament.uk', N'
                        23 The Business Centre, Earl Soham, IP13 7SA<br>

                        
                            Tel: 01728 685148<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.drdanielpoulter.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (163, N'Rt Hon Stephen Timms MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25278.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Ham', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen@stephentimms.org.uk">stephen@stephentimms.org.uk</a>
', N'stephen@stephentimms.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4000<br>
                        
                        
                            Fax: 020 7219 2949<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:timmss@parliament.uk">timmss@parliament.uk</a>
                    ', N'www.stephentimms.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (164, N'Mr Andrew Love MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25495.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Edmonton', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6377<br>
                        
                        
                            Fax: 020 7219 6623<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andy.love.mp@parliament.uk">andy.love.mp@parliament.uk</a>
', N'andy.love.mp@parliament.uk', N'
                        Broad House, 205 Fore Street, Edmonton, London, N18 2TZ<br>

                        
                            Tel: 020 8803 0574<br>
                        
                        
                            Fax: 020 8807 1673<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:andylovemp@aol.com">andylovemp@aol.com</a>
                    ', N'www.andylovemp.com', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (165, N'Clive Efford MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25662.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Eltham', N'
                        132 Westmount Road, Eltham, London, SE9 1UT<br>

                        
                            Tel: 020 8850 5744<br>
                        
                        
                            Fax: 020 8294 2166<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:clive@cliveefford.org.uk">clive@cliveefford.org.uk</a>
', N'clive@cliveefford.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4057<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:effordc@parliament.uk">effordc@parliament.uk</a>
                    ', N'www.cliveefford.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (167, N'Stephen Twigg MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25294.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Liverpool, West Derby', N'
                        229 Eaton Road, Liverpool, L12 2AG<br>

                        
                            Tel: 0151 230 0853<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7103<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stephen.twigg.mp@parliament.uk">stephen.twigg.mp@parliament.uk</a>
                    ', N'stephentwiggmp.co.uk', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (171, N'Rt Hon Sir John Randall MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25199.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Uxbridge and South Ruislip', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:randallj@parliament.uk">randallj@parliament.uk</a>
', N'randallj@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6885<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:john.randall.mp@parliament.uk">john.randall.mp@parliament.uk</a>
                    ', N'www.johnrandallmp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (172, N'Ms Diane Abbott MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25790.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hackney North and Stoke Newington', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4426<br>
                        
                        
                            Fax: 020 7219 4964<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:chalkiasg@parliament.uk">chalkiasg@parliament.uk</a>
', N'chalkiasg@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.dianeabbott.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (175, N'Huw Irranca-Davies MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25197.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ogmore', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4027<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:irrancadaviesh@parliament.uk">irrancadaviesh@parliament.uk</a>
', N'irrancadaviesh@parliament.uk', N'
                        Unit 2, 112-113 Commercial Street, Maesteg, CF34 9DL<br>

                        
                            Tel: 01656 737777<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.huwirranca-davies.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (177, N'Mr Mike Thornton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/103183.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Eastleigh', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 0207 219 7334<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mike.thornton.mp@parliament.uk">mike.thornton.mp@parliament.uk</a>
', N'mike.thornton.mp@parliament.uk', N'', N'@mike4eastleigh', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (178, N'John McDonnell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25474.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hayes and Harlington', N'
                        Pump Lane, Hayes, UB3 3NB<br>

                        
                            Tel: 020 8569 0010<br>
                        
                        
                            Fax: 020 8569 0109<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6908<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mcdonnellj@parliament.uk">mcdonnellj@parliament.uk</a>
                    ', N'www.john-mcdonnell.net', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (180, N'Rt Hon Frank Dobson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25657.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Holborn and St Pancras', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5840<br>
                        
                        
                            Fax: 020 7219 6956<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:patelm@parliament.uk">patelm@parliament.uk</a>
', N'patelm@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (181, N'John Cryer', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25579.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leyton and Wanstead', N'
                        877 High Road, Leytonstone, London, E11 1HR<br>

                        
                            Tel: 020 8989 5249<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7100<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:john.cryer.mp@parliament.uk">john.cryer.mp@parliament.uk</a>
                    ', N'www.leytonandwansteadlabour.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (184, N'Mike Gapes MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25650.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ilford South', N'
                        No constituency address publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6485<br>
                        
                        
                            Fax: 020 7219 0978<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mike.gapes.mp@parliament.uk">mike.gapes.mp@parliament.uk</a>
                    ', N'www.mikegapes.org.uk', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (185, N'Jeremy Corbyn MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25692.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Islington North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3545<br>
                        
                        
                            Fax: 020 7219 2328<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:corbynj@parliament.uk">corbynj@parliament.uk</a>
', N'corbynj@parliament.uk', N'
                        Constituency office not publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jeremycorbyn.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (188, N'Rt Hon Edward Davey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25694.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kingston and Surbiton', N'
                        Liberal Democrats, 21 Berrylands Road, Surbiton, KT5 8QX<br>

                        
                            Tel: 020 8288 0161<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:edward.davey.mp@parliament.uk">edward.davey.mp@parliament.uk</a>
', N'edward.davey.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3512<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:edward.davey.mp@parliament.uk">edward.davey.mp@parliament.uk</a>
                    ', N'www.edwarddavey.co.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (189, N'Jim Dowd MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25700.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Lewisham West and Penge', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4617<br>
                        
                        
                            Fax: 020 7219 2686<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:dowdj@parliament.uk">dowdj@parliament.uk</a>
', N'dowdj@parliament.uk', N'
                        43 Sunderland Road, Forest Hill, London, SE23 2PS<br>

                        
                            Tel: 020 8699 2001<br>
                        
                        
                            Fax: 020 8699 2001<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jimdowd.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (190, N'Rt Hon Dame Joan Ruddock MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25364.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Lewisham, Deptford', N'
                        No constituency office publicised<br>

                        
                            Tel: 020 8691 5992; 020 8691 1400<br>
                        
                        
                            Fax: 020 8691 8242<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4513<br>
                        
                        
                            Fax: 020 7219 6045<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:joan.ruddock.mp@parliament.uk">joan.ruddock.mp@parliament.uk</a>
                    ', N'www.joanruddock.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (193, N'Siobhain McDonagh MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25421.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mitcham and Morden', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4678<br>
                        
                        
                            Fax: 020 7219 0986<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mcdonaghs@parliament.uk">mcdonaghs@parliament.uk</a>
', N'mcdonaghs@parliament.uk', N'
                        1 Crown Road, Morden, SM4 5DD<br>

                        
                            Tel: 020 8542 4835<br>
                        
                        
                            Fax: 020 8544 0377<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:siobhain@mmlp.org.uk">siobhain@mmlp.org.uk</a>
                    ', N'@siobhainmp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (194, N'Robert Jenrick MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/77370.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newark', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7335<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'', N'@robertjenrick', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (197, N'Jim Fitzpatrick MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25612.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Poplar and Limehouse', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5085; 0207 219 6215<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jim.fitzpatrick.mp@parliament.uk">jim.fitzpatrick.mp@parliament.uk</a>
', N'jim.fitzpatrick.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jimfitzpatrickmp.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (199, N'Ms Karen Buck MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25747.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Westminster North', N'
                        The Labour Party, 4G Shirland Mews, London, W9 3DY<br>

                        
                            Tel: 020 8968 7999<br>
                        
                        
                            Fax: 020 8960 0150<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:buckk@parliament.uk">buckk@parliament.uk</a>
', N'buckk@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                            Fax: 020 7219 3664<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:buckk@parliament.uk">buckk@parliament.uk</a>
                    ', N'www.karenbuck.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (204, N'Rt Hon Paul Burstow MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25753.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sutton and Cheam', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1196<br>
                        
                        
                            Fax: 020 7219 0974<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:burstowp@parliament.uk">burstowp@parliament.uk</a>
', N'burstowp@parliament.uk', N'
                        234 Gander Green Lane, Cheam, SM3 9QF<br>

                        
                            Tel: 020 8288 6550<br>
                        
                        
                            Fax: 020 8288 6553<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:paul@paulburstow.org.uk">paul@paulburstow.org.uk</a>
                    ', N'www.paulburstow.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (206, N'Rt Hon David Lammy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25459.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Tottenham', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mail@davidlammy.co.uk">mail@davidlammy.co.uk</a>
', N'mail@davidlammy.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0767<br>
                        
                        
                            Fax: 020 7219 0357<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:lammyd@parliament.uk">lammyd@parliament.uk</a>
                    ', N'www.davidlammy.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (207, N'Rt Hon Vince Cable MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25665.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Twickenham', N'
                        2a Lion Road, Twickenham, TW1 4JQ<br>

                        
                            Tel: 020 8892 0215<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:cablev@parliament.uk">cablev@parliament.uk</a>
', N'cablev@parliament.uk', N'
                        Department for Business, Innovation and Skills, 1 Victoria Street, London , SW1H 0ET<br>

                        
                            Tel: 020 7215 5000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:enquiries@bis.gsi.gov.uk">enquiries@bis.gsi.gov.uk</a>
                    ', N'www.vincentcable.org.uk/en/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (209, N'Mr Dominic Raab MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83496.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Esher and Walton', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:dominic.raab.mp@parliament.uk">dominic.raab.mp@parliament.uk</a>
', N'dominic.raab.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7069<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:dominic.raab.mp@parliament.uk">dominic.raab.mp@parliament.uk</a>
                    ', N'www.dominicraab.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (210, N'Mrs Sharon Hodgson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35910.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Washington and Sunderland West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5160<br>
                        
                        
                            Fax: 020 7219 4493<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sharon.hodgson.mp@parliament.uk">sharon.hodgson.mp@parliament.uk</a>
', N'sharon.hodgson.mp@parliament.uk', N'
                        Unit 1, Vermont House, Concord, Washington, NE37 2SQ<br>

                        
                            Tel: 0191 417 2000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:brownjea@parliament.uk">brownjea@parliament.uk</a>
                    ', N'www.sharonhodgson.org/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (214, N'Rt Hon Don Foster MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25170.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bath', N'
                        31 James Street West, Bath, BA1 2BT<br>

                        
                            Tel: 01225 338973<br>
                        
                        
                            Fax: 01225 463630<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:donfoster@parliament.uk">donfoster@parliament.uk</a>
', N'donfoster@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4805<br>
                        
                        
                            Fax: 020 7219 2695<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:fosterd@parliament.uk">fosterd@parliament.uk</a>
                    ', N'www.donfoster.co.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (217, N'Rt Hon Dawn Primarolo MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25333.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bristol South', N'
                        PO Box 1002, Bristol, BS99 1WH<br>

                        
                            Tel: 0117 909 0063<br>
                        
                        
                            Fax: 0117 909 0064<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4343<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:primarolod@parliament.uk">primarolod@parliament.uk</a>
                    ', N'www.bristolsouthlabourparty.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (220, N'Steve Webb MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25230.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Thornbury and Yate', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4378<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:webbs@parliament.uk">webbs@parliament.uk</a>
', N'webbs@parliament.uk', N'
                        Poole Court, Poole Court Drive, Yate, Bristol , BS37 5PP<br>

                        
                            Tel: 01454 322100<br>
                        
                        
                            Fax: 01454 866515<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:steve@stevewebb.org.uk">steve@stevewebb.org.uk</a>
                    ', N'www.stevewebb.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (223, N'Rt Hon Dr Liam Fox MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25617.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Somerset', N'
                        71 High Street, Nailsea, BS48 1AW<br>

                        
                            Tel: 01275 790090<br>
                        
                        
                            Fax: 01275 790091<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:admin@northsomersetconservatives.com">admin@northsomersetconservatives.com</a>
', N'admin@northsomersetconservatives.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4198<br>
                        
                        
                            Fax: 020 7219 2167<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:douglasi@parliament.uk">douglasi@parliament.uk</a>
                    ', N'@liamfoxmp', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (227, N'Andrew George MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25653.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'St Ives', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4588<br>
                        
                        
                            Fax: 020 7219 5572<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andrew.george.mp@parliament.uk">andrew.george.mp@parliament.uk</a>
', N'andrew.george.mp@parliament.uk', N'
                        Trewella, 18 Mennaye Road, Penzance, TR18 4NG<br>

                        
                            Tel: 01736 360020<br>
                        
                        
                            Fax: 01736 332866<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:andrew@andrewgeorge.org.uk">andrew@andrewgeorge.org.uk</a>
                    ', N'www.andrewgeorge.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (230, N'Rt Hon Ben Bradshaw MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25160.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Exeter', N'
                        Labour HQ, 26B Clifton Hill, Exeter, EX1 2DJ<br>

                        
                            Tel: 01392 424464<br>
                        
                        
                            Fax: 01392 435523<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6597<br>
                        
                        
                            Fax: 020 7219 0950<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:bradshawb@parliament.uk">bradshawb@parliament.uk</a>
                    ', N'www.benbradshaw.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (231, N'Sir Nick Harvey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25569.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Devon', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6232<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mail@nickharveymp.com">mail@nickharveymp.com</a>
', N'mail@nickharveymp.com', N'
                        The Castle Centre, Barnstaple, EX31 1DR<br>

                        
                            Tel: 01271 328631<br>
                        
                        
                            Fax: 01271 345664<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mail@nickharveymp.com">mail@nickharveymp.com</a>
                    ', N'www.nickharveymp.com', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (234, N'Mr Gary Streeter MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25211.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South West Devon', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5033<br>
                        
                        
                            Fax: 020 7219 2414<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:deans@parliament.uk">deans@parliament.uk</a>
', N'deans@parliament.uk', N'
                        Old Newnham Farm, Plymouth, PL7 5BL<br>

                        
                            Tel: 01752 335666<br>
                        
                        
                            Fax: 01752 338401<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mail@garystreeter.co.uk">mail@garystreeter.co.uk</a>
                    ', N'www.garystreeter.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (237, N'Mr Adrian Sanders MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25377.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Torbay', N'
                        69 Belgrave Road, Torquay, TQ2 5HZ<br>

                        
                            Tel: 01803 200036<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6304<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:sandersa@parliament.uk">sandersa@parliament.uk</a>
                    ', N'www.adriansanders.org', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (242, N'Rehman Chishti MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35922.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Gillingham and Rainham', N'
                        Gillingham and Rainham Conservatives, Burden House, 200 Canterbury Street, Gillingham, ME7 5XG<br>

                        
                            Tel: 01634 570118<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7075<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:rehman.chishti.mp@parliament.uk">rehman.chishti.mp@parliament.uk</a>
                    ', N'www.rehmanchishti.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (244, N'Mr Robert Walter MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25331.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Dorset', N'
                        The Stables, White Cliff Gardens, Blandford Forum, DT11 7BU<br>

                        
                            Tel: 01258 452585<br>
                        
                        
                            Fax: 01258 459614<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6981<br>
                        
                        
                            Fax: 020 7219 2608<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:robert.walter.mp@parliament.uk">robert.walter.mp@parliament.uk</a>
                    ', N'www.bobwaltermp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (245, N'Mr Robert Syms MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25262.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Poole', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4601<br>
                        
                        
                            Fax: 020 7219 6867<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:guyn@parliament.uk">guyn@parliament.uk</a>
', N'guyn@parliament.uk', N'
                        Poole Conservative Association, 38 Sandbanks Road, Poole, BH14 8BX<br>

                        
                            Tel: 01202 739922<br>
                        
                        
                            Fax: 01202 739944<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:symsr@pooleconservatives.org">symsr@pooleconservatives.org</a>
                    ', N'@robertsymsmp', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (247, N'Chris Leslie MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25414.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Nottingham East', N'
                        Ground Floor, 12 Regent Street, Nottingham, NG1 5BQ<br>

                        
                            Tel: 0115 711 7666<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:chris.leslie@parliament.uk">chris.leslie@parliament.uk</a>
                    ', N'www.chrisleslie.org', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (249, N'Rt Hon Nick Clegg MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/27859.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sheffield, Hallam', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        85 Nethergreen Road, Sheffield, S11 7EH<br>

                        
                            Tel: 0114 230 9002<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.nickclegg.org.uk/', 1308, 1)
GO
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (253, N'Mr Laurence Robertson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25350.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Tewkesbury', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4196<br>
                        
                        
                            Fax: 020 7219 2325<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:robertsonl@parliament.uk">robertsonl@parliament.uk</a>
', N'robertsonl@parliament.uk', N'
                        22 High Street, Tewkesbury, GL20 5AL<br>

                        
                            Tel: 01684 291640<br>
                        
                        
                            Fax: 01684 291759<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.laurencerobertsonmp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (255, N'Mr David Heath MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25588.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Somerton and Frome', N'
                        17 Bath Street, Frome, BA11 1DN<br>

                        
                            Tel: 01373 473618<br>
                        
                        
                            Fax: 01373 455152<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:davidheath@davidheath.co.uk">davidheath@davidheath.co.uk</a>
', N'davidheath@davidheath.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6245<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.davidheath.co.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (261, N'Mr James Gray MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25542.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Wiltshire', N'
                        North Wilts Conservative Association, 4 Forest Gate, Pewsham, Chippenham, SN15 3RS<br>

                        
                            Tel: 01249 446209<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jenny@nwca.tory.org.uk">jenny@nwca.tory.org.uk</a>
', N'jenny@nwca.tory.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6237<br>
                        
                        
                            Fax: 020 7219 1163<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jamesgraymp@parliament.uk">jamesgraymp@parliament.uk</a>
                    ', N'www.jamesgray.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (268, N'Sir Peter Luff MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25497.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mid Worcestershire', N'
                        No constituency office publicised<br>

                        
                            Tel: 01905 763952<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:peter.luff.mp@parliament.uk">peter.luff.mp@parliament.uk</a>
                    ', N'www.peterluff.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (274, N'Rt Hon Owen Paterson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25405.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Shropshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5185<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        No constituency office publicised<br>

                        
                            Tel: 01978 710073<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gov.uk/government/organisations/department-for-environment-food-rural-affairs', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (280, N'Michael Fabricant MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25587.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Lichfield', N'
                        8 Bore Street, Lichfield, WS13 6LL<br>

                        
                            Tel: 01543 417868<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5022<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.michael.fabricant.mp.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (286, N'Joan Walley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25330.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stoke-on-Trent North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4524<br>
                        
                        
                            Fax: 020 7219 4397<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:walleyj@parliament.uk">walleyj@parliament.uk</a>
', N'walleyj@parliament.uk', N'
                        Unit 5, Burslem Enterprise Centre, Moorland Road, Burslem, Stoke-on-Trent, ST6 1JN<br>

                        
                            Tel: 01782 577900<br>
                        
                        
                            Fax: 01782 836462<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:landona@parliament.uk">landona@parliament.uk</a>
                    ', N'www.joanwalleymp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (288, N'Mr Douglas Carswell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31685.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Clacton', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8397<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        84 Station Road, Clacton-on-Sea, CO15 1SP<br>

                        
                            Tel: 01255 423112<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:douglas@douglascarswell.com">douglas@douglascarswell.com</a>
                    ', N'www.douglascarswell.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (295, N'Sir Richard Shepherd MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25205.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Aldridge-Brownhills', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5004<br>
                        
                        
                            Fax: 020 7219 0083<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:shepherdr@parliament.uk">shepherdr@parliament.uk</a>
', N'shepherdr@parliament.uk', N'
                        82 Walsall Road, Aldridge, Walsall, WS9 0JW<br>

                        
                            Tel: 01922 452228<br>
                        
                        
                            Fax: 01922 452228<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ald-browncons@tory.org">ald-browncons@tory.org</a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (296, N'Ms Gisela Stuart MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25260.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birmingham, Edgbaston', N'
                        No constituency office publicised<br>

                        
                            Tel: 0121 454 5430<br>
                        
                        
                            Fax: 0121 454 3167<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4853<br>
                        
                        
                            Fax: 020 7219 0317<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stuartg@parliament.uk">stuartg@parliament.uk</a>
                    ', N'www.giselastuartmp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (298, N'Steve McCabe MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25185.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birmingham, Selly Oak', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3509<br>
                        
                        
                            Fax: 020 7219 0367<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mccabes@parliament.uk">mccabes@parliament.uk</a>
', N'mccabes@parliament.uk', N'
                        No constituency office publicised<br>

                        
                            Tel: 0121 443 3878<br>
                        
                        
                            Fax: 0121 441 4779<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.stevemccabe-mp.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (301, N'Richard Burden MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25748.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birmingham, Northfield', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2318<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:richard.burden.mp@parliament.uk">richard.burden.mp@parliament.uk</a>
', N'richard.burden.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                            Tel: 0121 477 7746<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.richardburden.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (304, N'Mr Roger Godsiff MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25655.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birmingham, Hall Green', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5191<br>
                        
                        
                            Fax: 020 7219 2221<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:godsiffr@parliament.uk">godsiffr@parliament.uk</a>
', N'godsiffr@parliament.uk', N'
                        No Postal Address publicised<br>

                        
                            Tel: 07889 650544; 0121 603 2299<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:novellm@parliament.uk">novellm@parliament.uk</a>
                    ', N'www.rogergodsiffmp.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (306, N'Adam Afriyie MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35516.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Windsor', N'
                        87 St Leonards Road, Windsor, SL4 3BZ<br>

                        
                            Tel: 01753 678693<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8023<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:adam.afriyie.mp@parliament.uk">adam.afriyie.mp@parliament.uk</a>
                    ', N'www.adamafriyie.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (307, N'Mr Geoffrey Robinson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25201.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Coventry North West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4083<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:robinsong@parliament.uk">robinsong@parliament.uk</a>
', N'robinsong@parliament.uk', N'
                        Transport House, Short Street, Coventry, CV1 2LS<br>

                        
                            Tel: 024 7625 7870<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:geoffrey@newstatesman.co.uk">geoffrey@newstatesman.co.uk</a>
                    ', N'www.labourincoventry.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (308, N'Mr Jim Cunningham MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25575.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Coventry South', N'
                        Ground Floor, Rear of Queens House, 16 Queens Road, Coventry, CV1 3EG<br>

                        
                            Tel: 024 7655 3159<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gannond@parliament.uk">gannond@parliament.uk</a>
', N'gannond@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6362<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:eleanorm.connolly@parliament.uk">eleanorm.connolly@parliament.uk</a>
                    ', N'www.jimcunningham.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (312, N'Rt Hon John Spellar', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25322.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Warley', N'
                        Brandhall Labour Club, Tame Road, Oldbury, B68 0JT<br>

                        
                            Tel: 0121 423 2933<br>
                        
                        
                            Fax: 0121 423 2933<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:john.spellar@btconnect.com;%20colyerl@parliament.uk">john.spellar@btconnect.com; colyerl@parliament.uk</a>
', N'john.spellar@btconnect.com; colyerl@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0674<br>
                        
                        
                            Fax: 020 7219 2113<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:john.spellar.mp@parliament.uk">john.spellar.mp@parliament.uk</a>
                    ', N'www.johnspellar.labour.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (316, N'Mr David Winnick', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25248.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Walsall North', N'
                        47 Field Road, Bloxwich, Walsall, WS3 3JD<br>

                        
                            Tel: 01922 492084<br>
                        
                        
                            Fax: 01922 408307<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5003<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:winnickd@parliament.uk">winnickd@parliament.uk</a>
                    ', N'www.davidwinnick.webs.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (318, N'Anna Soubry MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35353.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Broxtowe', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7211<br>
                        
                        
                            Fax: 020 7219 6176<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:anna.soubry.mp@parliament.uk">anna.soubry.mp@parliament.uk</a>
', N'anna.soubry.mp@parliament.uk', N'
                        Barton House, 61 High Road, Chilwell, Nottingham, NG9 4AJ<br>

                        
                            Tel: 0115 943 6507<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.anna4broxtowe.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (320, N'Mr Adrian Bailey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25800.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Bromwich West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6060<br>
                        
                        
                            Fax: 020 7219 1202<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:baileya@parliament.uk">baileya@parliament.uk</a>
', N'baileya@parliament.uk', N'
                        Terry Duffy House, Thomas Street, West Bromwich, BR70 6NT<br>

                        
                            Tel: 01215 691926<br>
                        
                        
                            Fax: 01215 691936<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:cromptonm@parliament.uk">cromptonm@parliament.uk</a>
                    ', N'www.adrianbaileymp.org/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (325, N'Mr Dennis Skinner MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25309.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bolsover', N'
                        1 Elmhurst Close, South Normanton, Alfreton, DE55 3NF<br>

                        
                            Tel: 01773 581027<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5107<br>
                        
                        
                            Fax: 020 7219 0028<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:skinnerd@parliament.uk">skinnerd@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (328, N'Rt Hon Margaret Beckett MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25805.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Derby South', N'
                        Labour Party Agent (no constituency office publicised)<br>

                        
                            Tel: 01332 345636<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:james@derbylabourparty.co.uk">james@derbylabourparty.co.uk</a>
', N'james@derbylabourparty.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5135; 020 7219 2088; Fax 020 7219 4780<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:beckettm@parliament.uk">beckettm@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (333, N'Rt Hon Patrick McLoughlin MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25481.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Derbyshire Dales', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3511<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:patrick.mcloughlin.mp@parliament.uk">patrick.mcloughlin.mp@parliament.uk</a>
', N'patrick.mcloughlin.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gov.uk/government/organisations/department-for-transport', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (334, N'Rt Hon Andrew Robathan MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25346.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Leicestershire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3459<br>
                        
                        
                            Fax: 020 7219 0096<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:thompsondm@parliament.uk">thompsondm@parliament.uk</a>
', N'thompsondm@parliament.uk', N'
                        51 Main Street, Broughton Astley, LE9 6RE<br>

                        
                            Tel: 01455 283594<br>
                        
                        
                            Fax: 01455 286159<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:southleicscons@btconnect.com">southleicscons@btconnect.com</a>
                    ', N'www.gov.uk/government/organisations/northern-ireland-office', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (335, N'David Tredinnick MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25288.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bosworth', N'
                        Bosworth Conservative Association, 10a Priory Walk, Hinckley, LE10 1HU<br>

                        
                            Tel: 01455 635741<br>
                        
                        
                            Fax: 01455 612023<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4514<br>
                        
                        
                            Fax: 020 7219 4901<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tredinnickd@parliament.uk">tredinnickd@parliament.uk</a>
                    ', N'www.bosworthconservatives.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (336, N'Rt Hon Stephen Dorrell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25769.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Charnwood', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4472<br>
                        
                        
                            Fax: 020 7219 5838<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen.dorrell.mp@parliament.uk">stephen.dorrell.mp@parliament.uk</a>
', N'stephen.dorrell.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@stephendorrell.org.uk">info@stephendorrell.org.uk</a>
                    ', N'www.stephendorrell.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (337, N'Sir Edward Garnier QC MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25652.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Harborough', N'
                        24 Nelson Street, Market Harborough, LE16 9AY<br>

                        
                            Tel: 01858 464146<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@harboroughconservatives.com">office@harboroughconservatives.com</a>
', N'office@harboroughconservatives.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4034<br>
                        
                        
                            Fax: 020 7219 6735<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:garniere@parliament.uk">garniere@parliament.uk</a>
                    ', N'www.edwardgarnier.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (338, N'Rt Hon Keith Vaz MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25232.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leicester East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4605<br>
                        
                        
                            Fax: 020 7219 3922<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:vazk@parliament.uk">vazk@parliament.uk</a>
', N'vazk@parliament.uk', N'
                        Keith Vaz Casework Centre, 294 Victoria Road East, Leicester , LE5 0LF<br>

                        
                            Tel: 0116 246 0163<br>
                        
                        
                            Fax: 0116 246 1135<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:casework@live.co.uk">casework@live.co.uk</a>
                    ', N'www.keithvazmp.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (343, N'Rt Hon Alan Duncan MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25590.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rutland and Melton', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5204<br>
                        
                        
                            Fax: 020 7219 2529<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alan.duncan.mp@parliament.uk">alan.duncan.mp@parliament.uk</a>
', N'alan.duncan.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                            Tel: 01664 411211<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.alanduncan.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (345, N'Sir Edward Leigh MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25183.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Gainsborough', N'
                        Office 1, 20 Union Street, Market Rasen, LN8 3AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6480<br>
                        
                        
                            Fax: 020 7219 4883<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:edward.leigh.mp@parliament.uk">edward.leigh.mp@parliament.uk</a>
                    ', N'www.edwardleigh.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (348, N'Rt Hon Sir Peter Tapsell', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25264.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Louth and Horncastle', N'
                        Cannon Street House, Cannon Street, Louth, LN11 9NL<br>

                        
                            Tel: 01507 609840<br>
                        
                        
                            Fax: 01507 608091<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4477<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (350, N'Rt Hon John Hayes MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25491.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Holland and The Deepings', N'
                        Office 1, Broad Street Business Centre, 10 Broad Street, Spalding, PE11 1TB<br>

                        
                            Tel: 01775 711534<br>
                        
                        
                            Fax: 01775 713905<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:davieshm@parliament.uk">davieshm@parliament.uk</a>
', N'davieshm@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1389<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hayesj@parliament.uk">hayesj@parliament.uk</a>
                    ', N'www.gov.uk/government/organisations/cabinet-office', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (360, N'Vernon Coaker MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25758.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Gedling', N'
                        2A Parkyn Road, Daybrook, Nottingham, NG5 6BG<br>

                        
                            Tel: 0115 920 4224<br>
                        
                        
                            Fax: 0115 920 4500<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:robertsc@parliament.uk">robertsc@parliament.uk</a>
', N'robertsc@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6627<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:vernon.coaker.mp@parliament.uk">vernon.coaker.mp@parliament.uk</a>
                    ', N'www.vernon-coaker-mp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (361, N'Sir Alan Meale MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25454.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mansfield', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4159<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mealea@parliament.uk">mealea@parliament.uk</a>
', N'mealea@parliament.uk', N'
                        85 West Gate, Mansfied, NG18 1RT<br>

                        
                            Tel: 01623 660531<br>
                        
                        
                            Fax: 01623 420495<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:enquiries@alanmeale.co.uk">enquiries@alanmeale.co.uk</a>
                    ', N'www.alanmeale.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (364, N'Mr Graham Allen MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25774.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Nottingham North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5065<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:allengw@parliament.uk">allengw@parliament.uk</a>
', N'allengw@parliament.uk', N'
                        No constituency office publicised<br>

                        
                            Tel: 0115 975 2377<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@grahamallenmp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (366, N'Katy Clark MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/36523.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Ayrshire and Arran', N'
                        53 Main Street, Kilbirnie, KA25 7BX<br>

                        
                            Tel: 01505 684127<br>
                        
                        
                            Fax: 01505 684349<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:percivalj@parliament.uk">percivalj@parliament.uk</a>
', N'percivalj@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4113<br>
                        
                        
                            Fax: 020 7219 4002<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:clarkk@parliament.uk">clarkk@parliament.uk</a>
                    ', N'www.katyclark.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (372, N'Austin Mitchell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25372.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Great Grimsby', N'
                        13 Bargate, Grimsby, DN34 4SS<br>

                        
                            Tel: 01472 342145<br>
                        
                        
                            Fax: 01472 251484<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bentonjo@parliament.uk">bentonjo@parliament.uk</a>
', N'bentonjo@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4559<br>
                        
                        
                            Fax: 020 7219 4843<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mitchellav@parliament.uk">mitchellav@parliament.uk</a>
                    ', N'www.austinmitchell.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (373, N'Rt Hon David Davis', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25643.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Haltemprice and Howden', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5900<br>
                        
                        
                            Fax: 020 7219 4183<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david.davis.mp@parliament.uk">david.davis.mp@parliament.uk</a>
', N'david.davis.mp@parliament.uk', N'
                        Spaldington Court, Spaldington, Howden, DN14 7NG<br>

                        
                            Tel: 01430 430365<br>
                        
                        
                            Fax: 01430 430253<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:davisdm@parliament.uk">davisdm@parliament.uk</a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (376, N'Rt Hon Alan Johnson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25517.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kingston upon Hull West and Hessle', N'
                        Goodwin Resource Centre, Icehouse Road, Hull, HU3 2HQ<br>

                        
                            Tel: 01482 219211<br>
                        
                        
                            Fax: 01482 219211<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:windlet@parliament.uk">windlet@parliament.uk</a>
', N'windlet@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6637<br>
                        
                        
                            Fax: 020 7219 1305<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:johnsona@parliament.uk">johnsona@parliament.uk</a>
                    ', N'www.alanjohnson.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (379, N'Rt Hon William Hague MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25550.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Richmond (Yorks)', N'
                        Unit 1, Omega Business Village, Thurston Road, Northallerton, DL6 2NJ<br>

                        
                            Tel: 01609 779093<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:william.hague.mp@parliament.uk">william.hague.mp@parliament.uk</a>
', N'william.hague.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4611<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:william.hague.mp@parliament.uk">william.hague.mp@parliament.uk</a>
                    ', N'www.gov.uk/government/organisations/foreign-commonwealth-office', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (384, N'Miss Anne McIntosh MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25530.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Thirsk and Malton', N'
                        Thirsk and Malton Conservative Association, 109 Town Street, Old Malton, YO17 7HD<br>

                        
                            Tel: 01845 523835<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:info@thirskandmaltonconservatives.com">info@thirskandmaltonconservatives.com</a>
', N'info@thirskandmaltonconservatives.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3541<br>
                        
                        
                            Fax: 020 7219 0972<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mcintosha@parliament.uk">mcintosha@parliament.uk</a>
                    ', N'thirskandmaltonconservatives.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (385, N'Hugh Bayley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25803.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'York Central', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6824<br>
                        
                        
                            Fax: 020 7219 0346<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hugh.bayley.mp@parliament.uk">hugh.bayley.mp@parliament.uk</a>
', N'hugh.bayley.mp@parliament.uk', N'
                        59 Holgate Road, York, YO24 4AA<br>

                        
                            Tel: 01904 623713<br>
                        
                        
                            Fax: 01904 623260<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:dellaganal@parliament.uk">dellaganal@parliament.uk</a>
                    ', N'www.hughbayley.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (389, N'Rt Hon Caroline Flint MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25645.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Don Valley', N'
                        Meteor House, First Avenue, Auckley, Doncaster, DN9 3GA<br>

                        
                            Tel: 01302 623330<br>
                        
                        
                            Fax: 01302 775099<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4407<br>
                        
                        
                            Fax: 020 7219 1277<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:caroline.flint.mp@parliament.uk">caroline.flint.mp@parliament.uk</a>
                    ', N'carolineflintdonvalley.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (390, N'Rt Hon Rosie Winterton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25253.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Doncaster Central', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:rosie.winterton.mp@parliament.uk">rosie.winterton.mp@parliament.uk</a>
', N'rosie.winterton.mp@parliament.uk', N'
                        Doncaster Trades, 19 South Mall, Frenchgate, Doncaster, DN1 1LL<br>

                        
                            Tel: 01302 326297<br>
                        
                        
                            Fax: 01302 342921<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.rosiewinterton.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (392, N'Rt Hon Kevin Barron MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25731.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rother Valley', N'
                        9 Lordens Hill, Dinnington, Sheffield, S25 2QE<br>

                        
                            Tel: 01909 568611<br>
                        
                        
                            Fax: 01909 569974<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4432; 020 7219 6306<br>
                        
                        
                            Fax: 020 7219 5952<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:barronk@parliament.uk">barronk@parliament.uk</a>
                    ', N'www.kevinbarronmp.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (394, N'Mr Clive Betts MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25722.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sheffield South East', N'
                        First Floor, Barkers Pool House, Burgess Street, Sheffield, S1 2HF<br>

                        
                            Tel: 0114 275 7788<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5114<br>
                        
                        
                            Fax: 020 7219 2289<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:clive.betts.mp@parliament.uk">clive.betts.mp@parliament.uk</a>
                    ', N'www.clivebetts.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (395, N'Rt Hon David Blunkett MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25810.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sheffield, Brightside and Hillsborough', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4043<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:blunkettd@parliament.uk">blunkettd@parliament.uk</a>
', N'blunkettd@parliament.uk', N'
                        Second floor, MidCity House, 17-21 Furnival Gate, Sheffield, S1 4QR<br>

                        
                            Tel: 0114 273 5987<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:dbconstituency@aol.com">dbconstituency@aol.com</a>
                    ', N'davidblunkett.typepad.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (400, N'Rt Hon John Healey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25567.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wentworth and Dearne', N'
                        79 High Street, Wath upon Dearne, Rotherham, S63 7QB<br>

                        
                            Tel: 01709 875943<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hartleyi@parliament.uk">hartleyi@parliament.uk</a>
', N'hartleyi@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6359<br>
                        
                        
                            Fax: 020 7219 2451<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:healeyj@parliament.uk">healeyj@parliament.uk</a>
                    ', N'www.johnhealeymp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (401, N'Mike Wood MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25255.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Batley and Spen', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4125<br>
                        
                        
                            Fax: 020 7219 2861<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:woodm@parliament.uk">woodm@parliament.uk</a>
', N'woodm@parliament.uk', N'
                        Tom Myer''s House, 9 Cross Crown Street, Cleckheaton, BD19 3HW<br>

                        
                            Tel: 01274 335233<br>
                        
                        
                            Fax: 01274 335235<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.mikewood.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (403, N'Mr Gerry Sutcliffe MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25257.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bradford South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3247<br>
                        
                        
                            Fax: 020 7219 1227<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sutcliffeg@parliament.uk">sutcliffeg@parliament.uk</a>
', N'sutcliffeg@parliament.uk', N'
                        Gumption Centre, Glydegate, Bradford , BD5 0BQ<br>

                        
                            Tel: 01274 288688<br>
                        
                        
                            Fax: 01274 288689<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gerrysutcliffe.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (410, N'Jon Trickett MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25290.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hemsworth', N'
                        1a Highfield Road, Hemsworth, Pontefract, WF9 4DP<br>

                        
                            Tel: 01977 722290<br>
                        
                        
                            Fax: 01977 722290<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jtrickett@jontrickett.org.uk">jtrickett@jontrickett.org.uk</a>
', N'jtrickett@jontrickett.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5074<br>
                        
                        
                            Fax: 020 7219 2133<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:trickettj@parliament.uk">trickettj@parliament.uk</a>
                    ', N'@jon_trickett', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (411, N'Mr Barry Sheerman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25297.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Huddersfield', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5037<br>
                        
                        
                            Fax: 020 7219 2404<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sheermanb@parliament.uk">sheermanb@parliament.uk</a>
', N'sheermanb@parliament.uk', N'
                        F18 The Media Centre, 7 Northumberland Street, Huddersfield, HD7 1RL<br>

                        
                            Tel: 01484 487970<br>
                        
                        
                            Fax: 01484 487971<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:osullivang@parliament.uk">osullivang@parliament.uk</a>
                    ', N'www.barrysheerman.co.uk/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (413, N'Rt Hon Hilary Benn MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25735.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leeds Central', N'
                        2 Blenheim Terrace, Leeds, LS2 9JG<br>

                        
                            Tel: 0113 244 1097<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:boxj@parliament.uk">boxj@parliament.uk</a>
', N'boxj@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5770<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:bennh@parliament.uk">bennh@parliament.uk</a>
                    ', N'@hilarybennmp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (414, N'Mr George Mudie MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25370.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leeds East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4411<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:george.mudie.mp@parliament.uk">george.mudie.mp@parliament.uk</a>
', N'george.mudie.mp@parliament.uk', N'
                        The Former Presbytery, Our Lady of Good Council, Rosgill Drive, Leeds, LS14 6QY<br>

                        
                            Tel: 0113 232 3266<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:george.mudie.2nd@parliament.uk">george.mudie.2nd@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (415, N'Fabian Hamilton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25563.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leeds North East', N'
                        335 Roundhay Road, Leeds, LS8 4HT<br>

                        
                            Tel: 0113 249 6600<br>
                        
                        
                            Fax: 0113 235 9866<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:fabian@leedsne.co.uk">fabian@leedsne.co.uk</a>
', N'fabian@leedsne.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3493<br>
                        
                        
                            Fax: 020 7219 5540<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:fabian.hamilton.mp@parliament.uk">fabian.hamilton.mp@parliament.uk</a>
                    ', N'www.leedsne.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (420, N'Rt Hon Yvette Cooper MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25691.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Normanton, Pontefract and Castleford', N'
                        1 York Street, Castleford, WF10 1RB<br>

                        
                            Tel: 01977 553388<br>
                        
                        
                            Fax: 01977 559753<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5080<br>
                        
                        
                            Fax: 020 7219 0912<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:coopery@parliament.uk">coopery@parliament.uk</a>
                    ', N'www.yvettecooper.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (422, N'Dr Julian Lewis MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25560.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'New Forest East', N'
                        3 The Parade, Southampton Road, Cadnam, SO40 2NG<br>

                        
                            Tel: 023 8081 4817<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'', N'www.julianlewis.net', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (427, N'Rt Hon Stephen O''Brien MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25193.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Eddisbury', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6315<br>
                        
                        
                            Fax: 020 7219 0584<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:obriens@parliament.uk">obriens@parliament.uk</a>
', N'obriens@parliament.uk', N'
                        Eddisbury Conservative Association, 4 Church Walk, High Street, Tarporley, CW6 0AJ<br>

                        
                            Tel: 01829 733243<br>
                        
                        
                            Fax: 01829 733243<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@eddisburyconservatives.co.uk">office@eddisburyconservatives.co.uk</a>
                    ', N'www.stephenobrien.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (428, N'Andrew Miller MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25451.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ellesmere Port and Neston', N'
                        Whitby Hall Lodge, Stanney Lane, Ellesmere Port, CH65 6QY<br>

                        
                            Tel: 0151 357 3019<br>
                        
                        
                            Fax: 0151 356 8226<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3580<br>
                        
                        
                            Fax: 020 7219 3796<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:millera@parliament.uk">millera@parliament.uk</a>
                    ', N'www.andrew-miller-mp.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (429, N'Derek Twigg MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25293.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Halton', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1039<br>
                        
                        
                            Fax: 020 7219 3642<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:derek.twigg.mp@parliament.uk">derek.twigg.mp@parliament.uk</a>
', N'derek.twigg.mp@parliament.uk', N'
                        Bridge Business Centre, Suite E, Cheshire House, Gorsey Lane, Widnes, Cheshire, WA8 0RP,  (Please send post to House of Commons)<br>

                        
                            Tel: 0151 424 7030<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.derektwigg.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (432, N'Helen Jones MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25523.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Warrington North', N'
                        Suite 9, Gilbert Wakefield House, 67 Bewsey Street, Warrington, Cheshire, WA2 7JQ<br>

                        
                            Tel: 01925 232480<br>
                        
                        
                            Fax: 01925 232239<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4048<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jonesh@parliament.uk">jonesh@parliament.uk</a>
                    ', N'@helenjonesmp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (435, N'Mr Graham Brady MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25725.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Altrincham and Sale West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1260<br>
                        
                        
                            Fax: 020 7219 1649<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:crowthers@parliament.uk">crowthers@parliament.uk</a>
', N'crowthers@parliament.uk', N'
                        Altrincham and Sale West Conservative Association, Thatcher House, Delahays Farm, Green Lane, Timperley, WA15 8QW<br>

                        
                            Tel: 0161 904 8828<br>
                        
                        
                            Fax: 0161 904 8868<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@altsaletory.demon.co.uk">office@altsaletory.demon.co.uk</a>
                    ', N'www.grahambradymp.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (437, N'Mr David Crausby MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25156.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bolton North East', N'
                        426 Blackburn Road, Bolton, BL1 8NL<br>

                        
                            Tel: 01204 303340<br>
                        
                        
                            Fax: 01204 303340<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4092<br>
                        
                        
                            Fax: 020 7219 3713<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:crausbyd@parliament.uk">crausbyd@parliament.uk</a>
                    ', N'www.davidcrausby.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (441, N'Brandon Lewis MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/43555.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Great Yarmouth', N'
                        Sussex Road Business Centre, Sussex Road, Gorleston, Norfolk, NR31 6PF<br>

                        
                            Tel: 01493 652928<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@brandonlewis.co">office@brandonlewis.co</a>
', N'office@brandonlewis.co', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7231<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:justine.duggan@parliament.uk">justine.duggan@parliament.uk</a>
                    ', N'www.brandonlewis.co/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (445, N'Rt Hon Sir Andrew Stunell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25261.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hazel Grove', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5136<br>
                        
                        
                            Fax: 020 7219 2302<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stunella@parliament.uk">stunella@parliament.uk</a>
', N'stunella@parliament.uk', N'
                        34 Stockport Road, Romiley, Stockport, SK6 3AA<br>

                        
                            Tel: 0161 406 7070<br>
                        
                        
                            Fax: 0161 494 2425<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:enquiries@andrewstunell.org.uk">enquiries@andrewstunell.org.uk</a>
                    ', N'www.andrewstunell.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (446, N'Jim Dobbin MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25656.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Heywood and Middleton', N'
                        45 York Street, Heywood, OL10 4NN<br>

                        
                            Tel: 01706 361135<br>
                        
                        
                            Fax: 01706 625703<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:lambertc@parliament.uk">lambertc@parliament.uk</a>
', N'lambertc@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4530<br>
                        
                        
                            Fax: 020 7219 2696<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:dobbinj@parliament.uk">dobbinj@parliament.uk</a>
                    ', N'', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (449, N'Graham Stringer MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25259.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Blackley and Broughton', N'
                        North Manchester Sixth Form College, Rochdale Road, Manchester, M9 4AF<br>

                        
                            Tel: 0161 202 6600<br>
                        
                        
                            Fax: 0161 202 6626<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:connollym@parliament.uk">connollym@parliament.uk</a>
', N'connollym@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5235<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stringerg@parliament.uk">stringerg@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (451, N'Rt Hon Sir Gerald Kaufman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25528.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Manchester, Gorton', N'
                        No constituency office publicised<br>

                        
                            Tel: 07745 823990<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:cootesn@parliament.uk">cootesn@parliament.uk</a>
', N'cootesn@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5145<br>
                        
                        
                            Fax: 020 7219 6825<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:kaufmang@parliament.uk">kaufmang@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (454, N'Rt Hon Michael Meacher MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25453.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Oldham West and Royton', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4532; 020 7219 6461<br>
                        
                        
                            Fax: 020 7219 5945<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:michael.meacher.mp@parliament.uk">michael.meacher.mp@parliament.uk</a>
', N'michael.meacher.mp@parliament.uk', N'
                        11 Church Lane, Oldham, OL1 3AN<br>

                        
                            Tel: 0161 626 5779<br>
                        
                        
                            Fax: 0161 626 8572<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:buckleysd@parliament.uk">buckleysd@parliament.uk</a>
                    ', N'www.michaelmeacher.info', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (456, N'Rt Hon Hazel Blears MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25738.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Salford and Eccles', N'
                        201 Langworthy Road, Salford, M6 5PW<br>

                        
                            Tel: 0161 925 0705<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6595<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:blearsh@parliament.uk">blearsh@parliament.uk</a>
                    ', N'www.hazelblears.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (458, N'Ann Coffey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25759.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stockport', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4546<br>
                        
                        
                            Fax: 020 7219 2342<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ann.coffey.mp@parliament.uk">ann.coffey.mp@parliament.uk</a>
', N'ann.coffey.mp@parliament.uk', N'
                        207a Bramhall Lane, Stockport, SK2 6JA<br>

                        
                            Tel: 0161 483 2600<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:dunbarb@parliament.uk">dunbarb@parliament.uk</a>
                    ', N'www.anncoffeymp.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (463, N'Rt Hon Jack Straw MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25353.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Blackburn', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5070<br>
                        
                        
                            Fax: 020 7219 2310<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:strawj@parliament.uk">strawj@parliament.uk</a>
', N'strawj@parliament.uk', N'
                        Richmond Chambers, Richmond Terrace, Blackburn, BB1 7AS<br>

                        
                            Tel: 01254 52317<br>
                        
                        
                            Fax: 01254 682213<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jack.straw@blackburnlabour.org">jack.straw@blackburnlabour.org</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (465, N'Mr Gordon Marsden MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25439.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Blackpool South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1262<br>
                        
                        
                            Fax: 020 7219 5859<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gordonmarsdenmp@parliament.uk">gordonmarsdenmp@parliament.uk</a>
', N'gordonmarsdenmp@parliament.uk', N'
                        304 Highfield Road, Blackpool, FY4 3JX<br>

                        
                            Tel: 01253 344143<br>
                        
                        
                            Fax: 01253 344940<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gordonmarsden.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (467, N'Rt Hon Lindsay Hoyle MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25512.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chorley', N'
                        35-39 Market Street, Chorley, PR7 2SW<br>

                        
                            Tel: 01257 271555<br>
                        
                        
                            Fax: 01257 277462<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:goreb@parliament.uk">goreb@parliament.uk</a>
', N'goreb@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3515<br>
                        
                        
                            Fax: 020 7219 3831<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:wilsonp@parliament.uk">wilsonp@parliament.uk</a>
                    ', N'@lindsayhoyle_mp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (473, N'Mark Hendrick MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25602.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Preston', N'
                        PTMC, Marsh Lane, Preston, PR1 8UQ<br>

                        
                            Tel: 01772 883575<br>
                        
                        
                            Fax: 01772 887188<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:warhurste@parliament.uk">warhurste@parliament.uk</a>
', N'warhurste@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4791<br>
                        
                        
                            Fax: 020 7219 5220<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mark.hendrick.mp@parliament.uk">mark.hendrick.mp@parliament.uk</a>
                    ', N'www.prestonmp.co.uk', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (474, N'Mr Nigel Evans MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25719.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ribble Valley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6939<br>
                        
                        
                            Fax: 020 7219 2568<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:evansn@parliament.uk">evansn@parliament.uk</a>
', N'evansn@parliament.uk', N'
                        9 Railway View, Clitheroe, BB7 2HA<br>

                        
                            Tel: 01200 425939<br>
                        
                        
                            Fax: 01200 422904<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (478, N'Rt Hon Frank Field MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25534.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birkenhead', N'
                        No constituency office publicised<br>

                        
                            Tel: 0800 028 0293<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5193<br>
                        
                        
                            Fax: 020 7219 0601<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:fieldf@parliament.uk">fieldf@parliament.uk</a>
                    ', N'www.frankfield.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (479, N'Mr Joe Benton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25807.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bootle', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6973<br>
                        
                        
                            Fax: 020 7219 3895<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bentonj@parliament.uk">bentonj@parliament.uk</a>
', N'bentonj@parliament.uk', N'
                        Second Floor, St Hugh''s House, Stanley Road (corner of Trinity Road), Bootle, Bootle L20 3QQ<br>

                        
                            Tel: 0151 933 8432 Fax: 0151 933 4746<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (481, N'Stewart Hosie MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/38317.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dundee East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8164<br>
                        
                        
                            Fax: 020 7219 6716<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hosies@parliament.uk">hosies@parliament.uk</a>
', N'hosies@parliament.uk', N'
                        95 High Street, Carnoustie, DD7 9EA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.stewarthosie.com/', 1311, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (483, N'Maria Eagle MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25660.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Garston and Halewood', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0551<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:eaglem@parliament.uk">eaglem@parliament.uk</a>
', N'eaglem@parliament.uk', N'
                        Unit House, Speke Boulevard, Liverpool, L24 9HZ<br>

                        
                            Tel: 0151 448 1167<br>
                        
                        
                            Fax: 0151 448 0976<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.mariaeaglemp.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (484, N'Mrs Louise Ellman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25701.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Liverpool, Riverside', N'
                        515 The Cotton Exchange Building, Old Hall Street, Liverpool, L3 9LQ<br>

                        
                            Tel: 0151 236 2969<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:lloydju@parliament.uk;%20rob.carney@parliament.uk">lloydju@parliament.uk; rob.carney@parliament.uk</a>
', N'lloydju@parliament.uk; rob.carney@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5210<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:louise.ellman.mp@parliament.uk;%20alex.mayes@parliament.uk;%20becky.rowland@parliament.uk">louise.ellman.mp@parliament.uk; alex.mayes@parliament.uk; becky.rowland@parliament.uk</a>
                    ', N'www.louiseellman.co.uk/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (489, N'Mr Dave Watts MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25229.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'St Helens North', N'
                        Sixth Floor, Century House, Hardshaw Street, St Helens, WA10 1QU<br>

                        
                            Tel: 01744 21336<br>
                        
                        
                            Fax: 01744 21343<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:davewattsmp@hotmail.com">davewattsmp@hotmail.com</a>
', N'davewattsmp@hotmail.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6325<br>
                        
                        
                            Fax: 020 7219 0913<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:wattsd@parliament.uk">wattsd@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (491, N'Ms Angela Eagle MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25642.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wallasey', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3843; 020 7219 5057<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:eaglea@parliament.uk">eaglea@parliament.uk</a>
', N'eaglea@parliament.uk', N'
                        Sherlock House, 6 Manor Road, Liscard, Wallasey, Wirral, CH45 4JB<br>

                        
                            Tel: 0151 637 1979<br>
                        
                        
                            Fax: 0151 638 5861<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.angelaeaglemp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (513, N'Rt Hon Sir Alan Beith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25716.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Berwick-upon-Tweed', N'
                        54 Bondgate Within, Alnwick, NE66 1JD<br>

                        
                            Tel: 01665 602901<br>
                        
                        
                            Fax: 01665 604435<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alanbeith@berwicklibdems.org.uk">alanbeith@berwicklibdems.org.uk</a>
', N'alanbeith@berwicklibdems.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3540<br>
                        
                        
                            Fax: 020 7219 5890<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:cheesemang@parliament.uk">cheesemang@parliament.uk</a>
                    ', N'berwicklibdems.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (514, N'Rt Hon Sir Menzies Campbell QC MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25795.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North East Fife', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6910<br>
                        
                        
                            Fax: 020 7219 0559<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:menzies.campbell.mp@parliament.uk">menzies.campbell.mp@parliament.uk</a>
', N'menzies.campbell.mp@parliament.uk', N'
                        North East Fife Liberal Democrats, 16 Millgate, Cupar, KY15 5EG<br>

                        
                            Tel: 01334 656361<br>
                        
                        
                            Fax: 01334 654045<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:fife_office@mingcampbell.org.uk">fife_office@mingcampbell.org.uk</a>
                    ', N'www.mingcampbell.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (520, N'Mr Stephen Hepburn MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25595.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Jarrow', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4134<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hepburns@parliament.uk">hepburns@parliament.uk</a>
', N'hepburns@parliament.uk', N'
                        141 Tedco Business Centre, Viking Industrial Estate, Jarrow, NE32 3DT<br>

                        
                            Tel: 0191 420 0648<br>
                        
                        
                            Fax: 0191 489 7531<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hepburn4jarrow@btinternet.com">hepburn4jarrow@btinternet.com</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (523, N'Rt Hon Nicholas Brown MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25744.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newcastle upon Tyne East', N'
                        1 Mosley Street, Newcastle upon Tyne, NE1 1YE<br>

                        
                            Tel: 0191 261 1408<br>
                        
                        
                            Fax: 0191 261 1409<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6814<br>
                        
                        
                            Fax: 020 7219 5941<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nickbrownmp@parliament.uk">nickbrownmp@parliament.uk</a>
                    ', N'www.nickbrownmp.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (529, N'Mr Alan Campbell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25786.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Tynemouth', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6619<br>
                        
                        
                            Fax: 020 7219 3006<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:campbellal@parliament.uk">campbellal@parliament.uk</a>
', N'campbellal@parliament.uk', N'
                        99 Howard Street, North Shields, NE30 1NA<br>

                        
                            Tel: 0191 257 1927<br>
                        
                        
                            Fax: 0191 257 6537<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.alancampbellmp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (533, N'Rt Hon David Hanson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25676.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Delyn', N'
                        64 Chester Street, Flint, CH6 5DH<br>

                        
                            Tel: 01352 763159<br>
                        
                        
                            Fax: 01352 730140<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5064<br>
                        
                        
                            Fax: 020 7219 2671<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:david.hanson.mp@parliament.uk">david.hanson.mp@parliament.uk</a>
                    ', N'www.davidhanson.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (534, N'Chris Ruane MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25363.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Vale of Clwyd', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6378<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ruanec@parliament.uk">ruanec@parliament.uk</a>
', N'ruanec@parliament.uk', N'
                        25 Kinmel Street, Rhyl, LL18 1AH<br>

                        
                            Tel: 01745 354626<br>
                        
                        
                            Fax: 01745 334827<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ruanec@parliament.uk">ruanec@parliament.uk</a>
                    ', N'www.chrisruane.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (545, N'Paul Flynn MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25647.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newport West', N'
                        No constituency office publicised<br>

                        
                            Tel: 01633 262348<br>
                        
                        
                            Fax: 01633 760532<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:paulflynnmp@talk21.com">paulflynnmp@talk21.com</a>
', N'paulflynnmp@talk21.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3478<br>
                        
                        
                            Fax: 020 7219 2433<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.paulflynnmp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (546, N'Rt Hon Paul Murphy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25383.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Torfaen', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3463<br>
                        
                        
                            Fax: 020 7219 3819<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:paul.murphy.mp@parliament.uk">paul.murphy.mp@parliament.uk</a>
', N'paul.murphy.mp@parliament.uk', N'
                        73 Upper Trosnant Street, Pontypool, Torfaen, NP4 8AU<br>

                        
                            Tel: 01495 750078<br>
                        
                        
                            Fax: 01495 752584<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.paulmurphymp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (549, N'Rt Hon Elfyn Llwyd MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25418.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dwyfor Meirionnydd', N'
                        Angorfa, Heol Meurig, Dolgellau, LL40 1LN<br>

                        
                            Tel: 01341 422661<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jenkinssh@parliament.uk">jenkinssh@parliament.uk</a>
', N'jenkinssh@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3555<br>
                        
                        
                            Fax: 020 7219 2633<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:elfyn.llwyd.mp@parliament.uk">elfyn.llwyd.mp@parliament.uk</a>
                    ', N'', 1309, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (553, N'Rt Hon Ann Clwyd MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25755.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cynon Valley', N'
                        4th Floor, Crown Buildings, Aberdare, CF44 7DW<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6609<br>
                        
                        
                            Fax: 020 7219 5943<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ann.clwyd.mp@parliament.uk">ann.clwyd.mp@parliament.uk</a>
                    ', N'@annclwyd', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (566, N'Sir William Cash MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25682.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stone', N'
                        50 High Street, Stone, ST15 8AU<br>

                        
                            Tel: 01785 811000<br>
                        
                        
                            Fax: 01785 811000<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:orgsec.stonecons@btconnect.com">orgsec.stonecons@btconnect.com</a>
', N'orgsec.stonecons@btconnect.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6330<br>
                        
                        
                            Fax: 020 7219 3935<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mcconaloguej@parliament.uk">mcconaloguej@parliament.uk</a>
                    ', N'www.billcashmp.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (567, N'Rt Hon Peter Hain MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25173.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Neath', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3925<br>
                        
                        
                            Fax: 020 7219 3816<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hainp@parliament.uk">hainp@parliament.uk</a>
', N'hainp@parliament.uk', N'
                        39 Windsor Road, Neath, SA11 1NB<br>

                        
                            Tel: 01639 630152<br>
                        
                        
                            Fax: 01639 641196<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.peterhain.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (570, N'Mr Frank Doran', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25699.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Aberdeen North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3481<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:doranf@parliament.uk">doranf@parliament.uk</a>
', N'doranf@parliament.uk', N'
                        69 Dee Street, Aberdeen, AB11 6EE<br>

                        
                            Tel: 01224 252715<br>
                        
                        
                            Fax: 01224 252716<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:dilloni@parliament.uk">dilloni@parliament.uk</a>
                    ', N'www.frankdoran.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (572, N'Dame  Anne Begg MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25157.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Aberdeen South', N'
                        39-41 Victoria Road, Torry, Aberdeen, AB11 9LS<br>

                        
                            Tel: 01224 974404<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:anne.begg.mp@parliament.uk">anne.begg.mp@parliament.uk</a>
', N'anne.begg.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2140<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:anne.begg.mp@parliament.uk">anne.begg.mp@parliament.uk</a>
                    ', N'www.annebegg.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (576, N'Sandra Osborne MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25393.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ayr, Carrick and Cumnock', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:osbornes@parliament.uk">osbornes@parliament.uk</a>
', N'osbornes@parliament.uk', N'
                        139 Main Street, Ayr, KA8 8BX<br>

                        
                            Tel: 01292 262906<br>
                        
                        
                            Fax: 01292 885661<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.sandraosborne.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (582, N'Mr Adam Holloway MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35356.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Gravesham', N'
                        No constituency office publicised<br>

                        
                            Tel: 01474 332097<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8402<br>
                        
                        
                            Fax: 020 7219 2871<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hollowaya@parliament.uk">hollowaya@parliament.uk</a>
                    ', N'www.adamholloway.co.uk/', 1302, 1)
GO
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (583, N'Rt Hon Kenneth Clarke QC MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25686.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rushcliffe', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 0207 219 5189<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:clarkek@parliament.uk;%20suggd@parliament.uk%20">clarkek@parliament.uk; suggd@parliament.uk </a>
', N'clarkek@parliament.uk; suggd@parliament.uk ', N'
                        Rushcliffe House, 17/19 Rectory Road, West Bridgford, Nottingham, NG2 6BE<br>

                        
                            Tel: 0115 981 7224<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.rushcliffeconservatives.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (586, N'Mr Brian H. Donohoe MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25698.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Central Ayrshire', N'
                        17 Townhead, Irvine, Ayrshire, KA12 0BL<br>

                        
                            Tel: 01294 276844<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:brownrm@parliament.uk;%20mairs@parliament.uk">brownrm@parliament.uk; mairs@parliament.uk</a>
', N'brownrm@parliament.uk; mairs@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6230<br>
                        
                        
                            Fax: 020 7219 5388<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:brian.donohoe.mp@parliament.uk">brian.donohoe.mp@parliament.uk</a>
                    ', N'www.briandonohoemp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (588, N'Mr Russell Brown MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25161.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dumfries and Galloway', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4429<br>
                        
                        
                            Fax: 020 7219 0922<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:russell.brown.mp@parliament.uk">russell.brown.mp@parliament.uk</a>
', N'russell.brown.mp@parliament.uk', N'
                        13 Hanover Street, Stranraer, DG9 7SB<br>

                        
                            Tel: 01776 705254<br>
                        
                        
                            Fax: 01776 703006<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:russell@russellbrownmp.com">russell@russellbrownmp.com</a>
                    ', N'www.russellbrownmp.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (591, N'Rt Hon Gordon Brown MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25779.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kirkcaldy and Cowdenbeath', N'
                        Carlyle House, Carlyle Road, Kirkcaldy, Fife, KY1 1DB<br>

                        
                            Tel: 01592 263792<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2968<br>
                        
                        
                            Fax: 020 7219 5734<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:browng@parliament.uk">browng@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (595, N'Rt Hon Jim Murphy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25192.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Renfrewshire', N'
                        Suite 4, 1 Spiersbridge Way, Thornliebank, Glasgow, G46 8NG<br>

                        
                            Tel: 0141 620 6310<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4615<br>
                        
                        
                            Fax: 020 7219 5657<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jimmurphymp@parliament.uk">jimmurphymp@parliament.uk</a>
                    ', N'www.jimmurphymp.com/default.aspx', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (596, N'Rt Hon Alistair Darling MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25690.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Edinburgh South West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4584<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alistair.darling.mp@parliament.uk">alistair.darling.mp@parliament.uk</a>
', N'alistair.darling.mp@parliament.uk', N'
                        CBC House, 24 Canning Street, Edinburgh, EH3 8EG<br>

                        
                            Tel: 0131 272 2727<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (602, N'Michael Connarty MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25631.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Linlithgow and East Falkirk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5071<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:michael.connarty.mp@parliament.uk">michael.connarty.mp@parliament.uk</a>
', N'michael.connarty.mp@parliament.uk', N'
                        Room 8, 5 Kerse Road, Grangemouth, FK3 8HQ<br>

                        
                            Tel: 01324 474832<br>
                        
                        
                            Fax: 01324 666811<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.mconnartymp.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (603, N'Eric Joyce MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25527.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Falkirk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2779<br>
                        
                        
                            Fax: 020 7219 2090<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:eric.joyce.mp@parliament.uk">eric.joyce.mp@parliament.uk</a>
', N'eric.joyce.mp@parliament.uk', N'
                        37 Church walk, Denny, Falkirk, FK6 6DF<br>

                        
                            Tel: 01324 823200<br>
                        
                        
                            Fax: 01324 823200<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'ericjoycemp.wordpress.com', 1305, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (605, N'John Robertson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25349.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Glasgow North West', N'
                        131 Dalsetter Avenue, Drumchapel, Glasgow, G15 8TE<br>

                        
                            Tel: 0141 944 7298<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jrmpoffice@btinternet.com">jrmpoffice@btinternet.com</a>
', N'jrmpoffice@btinternet.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6964<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.john-robertson.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (609, N'Mr George Galloway', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25649.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bradford West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6940<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:george.galloway.mp@parliament.uk">george.galloway.mp@parliament.uk</a>
', N'george.galloway.mp@parliament.uk', N'', N'www.georgegalloway.com/', 1310, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (611, N'Mr Ian Davidson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25767.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Glasgow South West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2006<br>
                        
                        
                            Fax: 020 7219 2238<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:iandavidsonmp@parliament.uk">iandavidsonmp@parliament.uk</a>
', N'iandavidsonmp@parliament.uk', N'
                        3 Kilmuir Drive, Glasgow, G46 8BW<br>

                        
                            Tel: 0141 621 2216<br>
                        
                        
                            Fax: 0141 621 4217<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.iandavidsonmp.com/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (615, N'Rt Hon Sir Malcolm Bruce MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25668.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Gordon', N'
                        67 High Street, Inverurie, AB51 3QJ<br>

                        
                            Tel: 01467 623413<br>
                        
                        
                            Fax: 01467 624994<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:milsomr@parliament.uk;%20info@malcolmbruce.org.uk">milsomr@parliament.uk; info@malcolmbruce.org.uk</a>
', N'milsomr@parliament.uk; info@malcolmbruce.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6233<br>
                        
                        
                            Fax: 020 7219 2334<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@malcolmbruce.org.uk">info@malcolmbruce.org.uk</a>
                    ', N'www.malcolmbruce.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (626, N'Mr Frank Roy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25362.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Motherwell and Wishaw', N'
                        265 Main Street, Wishaw, ML2 7NE<br>

                        
                            Tel: 01698 303040<br>
                        
                        
                            Fax: 01698 303060<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1038<br>
                        
                        
                            Fax: 020 7219 8233<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:royf@parliament.uk">royf@parliament.uk</a>
                    ', N'www.frankroy.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (627, N'Mr Gregory Campbell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25794.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Londonderry', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8495<br>
                        
                        
                            Fax: 020 7219 1953<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:fieldingm@parliament.uk">fieldingm@parliament.uk</a>
', N'fieldingm@parliament.uk', N'
                        25 Bushmills Road, Coleraine, BT52 2BP<br>

                        
                            Tel: 028 7032 7327<br>
                        
                        
                            Fax: 028 7032 7328<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:wilkinsonh@parliament.uk">wilkinsonh@parliament.uk</a>
                    ', N'www.duplondonderry.co.uk/campbell.html', 1303, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (632, N'Rt Hon Danny Alexander MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/32109.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Inverness, Nairn, Badenoch and Strathspey', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2300<br>
                        
                        
                            Fax: 020 7219 1438<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alexanderdg@parliament.uk">alexanderdg@parliament.uk</a>
', N'alexanderdg@parliament.uk', N'
                        45 Huntly Street, Inverness, IV3 5HR<br>

                        
                            Tel: 01463 711280<br>
                        
                        
                            Fax: 01463 714960<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:danny@highlandlibdems.org.uk">danny@highlandlibdems.org.uk</a>
                    ', N'www.dannyalexander.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (634, N'Rt Hon Charles Kennedy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25680.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ross, Skye and Lochaber', N'
                        5 MacGregor''s Court, Dingwall, IV15 9HS<br>

                        
                            Tel: 01349 862152<br>
                        
                        
                            Fax: 01349 866829<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:charles@highlandlibdems.org.uk">charles@highlandlibdems.org.uk</a>
', N'charles@highlandlibdems.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0356<br>
                        
                        
                            Fax: 020 7219 4881<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:kennedyc@parliament.uk">kennedyc@parliament.uk</a>
                    ', N'www.charleskennedy.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (636, N'Rt Hon Anne McGuire MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25529.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stirling', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5014<br>
                        
                        
                            Fax: 020 7219 2503; 020 7219 0792<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:anne.mcguire.mp@parliament.uk">anne.mcguire.mp@parliament.uk</a>
', N'anne.mcguire.mp@parliament.uk', N'
                        22 Viewfield Street, Stirling, FK8 1UA<br>

                        
                            Tel: 01786 446515<br>
                        
                        
                            Fax: 01786 446513<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.annemcguiremp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (638, N'Rt Hon Michael Moore MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25455.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Berwickshire, Roxburgh and Selkirk', N'
                        Parliamentary Office, 11 Island Street, Galashiels, TD1 1NZ<br>

                        
                            Tel: 01896 663650<br>
                        
                        
                            Fax: 01896 663655<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2236<br>
                        
                        
                            Fax: 020 7219 0263<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:michaelmooremp@parliament.uk">michaelmooremp@parliament.uk</a>
                    ', N'www.michaelmoore.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (639, N'Owen Smith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/54340.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Pontypridd', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7128<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:owen.smith.mp@parliament.uk">owen.smith.mp@parliament.uk</a>
', N'owen.smith.mp@parliament.uk', N'
                        Office of Owen Smith MP, GMB House, Morgan Street, Pontypridd, CF37 2DS<br>

                        
                            Tel: 01443 401122<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'owensmithmp.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (650, N'Rt Hon Jeffrey M. Donaldson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25697.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Lagan Valley', N'
                        The Old Town Hall, 29 Castle Street, Lisburn, BT27 4DH<br>

                        
                            Tel: 028 9266 8001<br>
                        
                        
                            Fax: 028 9267 1845<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jeffreydonaldsonmp@laganvalley.net">jeffreydonaldsonmp@laganvalley.net</a>
', N'jeffreydonaldsonmp@laganvalley.net', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3407<br>
                        
                        
                            Fax: 020 7219 0696<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jeffrey.donaldson.mp@parliament.uk">jeffrey.donaldson.mp@parliament.uk</a>
                    ', N'@j_donaldson_mp', 1303, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (655, N'Dr William McCrea MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33616.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Antrim', N'
                        5-7 School Street, The Square, Ballyclare, BT39 9BE<br>

                        
                            Tel: 028 9334 2727<br>
                        
                        
                            Fax: 028 9334 2707<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8525<br>
                        
                        
                            Fax: 020 7219 2347<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:william.mccrea.mp@parliament.uk">william.mccrea.mp@parliament.uk</a>
                    ', N'', 1303, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1166, N'Jonathan Evans', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/28051.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cardiff North', N'
                        54 Old Church Road, Whitchurch, Cardiff, CF14 1AB<br>

                        
                            Tel: 029 2061 3539<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:vivienne.ward@parliament.uk">vivienne.ward@parliament.uk</a>
', N'vivienne.ward@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7205<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jonathan.evans.mp@parliament.uk">jonathan.evans.mp@parliament.uk</a>
                    ', N'www.jonathanevans.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1170, N'Sarah Teather MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25227.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Brent Central', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8147<br>
                        
                        
                            Fax: 020 7219 0041<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:teathers@parliament.uk">teathers@parliament.uk</a>
', N'teathers@parliament.uk', N'
                        70 Walm Lane, Willesden Green, London, NW2 4RA<br>

                        
                            Tel: 020 8459 0455<br>
                        
                        
                            Fax: 020 8830 3280<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.brentlibdems.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1171, N'Rt Hon Liam Byrne MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/30771.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birmingham, Hodge Hill', N'
                        No constituency office publicised<br>

                        
                            Tel: 0121 789 7287<br>
                        
                        
                            Fax: 0121 789 9824<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6953<br>
                        
                        
                            Fax: 020 7219 1431<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:byrnel@parliament.uk">byrnel@parliament.uk</a>
                    ', N'www.liambyrne.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1191, N'Rt Hon Sir Malcolm Rifkind QC', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31729.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kensington', N'
                        1A Chelsea Manor Street, London, SW3 5RP<br>

                        
                            Tel: 020 7352 0102<br>
                        
                        
                            Fax: 020 7351 5885<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5683<br>
                        
                        
                            Fax: 020 7219 4213<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:shaylorc@parliament.uk">shaylorc@parliament.uk</a>
                    ', N'www.malcolmrifkind.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1198, N'Mr David Evennett', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31706.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bexleyheath and Crayford', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8403<br>
                        
                        
                            Fax: 020 7219 2163<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david.evennett.mp@parliament.uk">david.evennett.mp@parliament.uk</a>
', N'david.evennett.mp@parliament.uk', N'
                        17 Church Road, Bexleyheath, Kent, DA7 4DD<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'davidevennett.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1200, N'Rt Hon Sir Greg Knight MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25553.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Yorkshire', N'
                        18 Exchange Street, Driffield, YO25 6LJ<br>

                        
                            Tel: 0845 090 0203<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sothcottt@parliament.uk">sothcottt@parliament.uk</a>
', N'sothcottt@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8417<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:sothcottt@parliament.uk">sothcottt@parliament.uk</a>
                    ', N'www.gregknight.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1201, N'Rt Hon Alistair Burt MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25162.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North East Bedfordshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8132<br>
                        
                        
                            Fax: 020 7219 1740<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alistair.burt.mp@parliament.uk">alistair.burt.mp@parliament.uk</a>
', N'alistair.burt.mp@parliament.uk', N'
                        Biggleswade Conservative Club, St Andrews Street, Biggleswade, SG18 8BA<br>

                        
                            Tel: 01767 313385<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nebca@northeastbedsconservatives.com">nebca@northeastbedsconservatives.com</a>
                    ', N'www.alistair-burt.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1211, N'Rt Hon Andrew Mitchell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25190.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sutton Coldfield', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8516<br>
                        
                        
                            Fax: 020 7219 1981<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andrew.mitchell.mp@parliament.uk">andrew.mitchell.mp@parliament.uk</a>
', N'andrew.mitchell.mp@parliament.uk', N'
                        Sutton Coldfield Conservative Association, 36 High Street, Sutton Coldfield, B72 1UP<br>

                        
                            Tel: 0121 354 2229<br>
                        
                        
                            Fax: 0121 321 1762<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@sutton-coldfield-tories.org.uk">info@sutton-coldfield-tories.org.uk</a>
                    ', N'www.andrew-mitchell-mp.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1220, N'Charles Hendry MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25603.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wealden', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8238<br>
                        
                        
                            Fax: 020 7219 1977<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:charles.hendry.mp@parliament.uk">charles.hendry.mp@parliament.uk</a>
', N'charles.hendry.mp@parliament.uk', N'
                        Wealden Conservative Association, The Granary, Bales Green Farm, Arlington, BN27 6SH<br>

                        
                            Tel: 01323 489289<br>
                        
                        
                            Fax: 01323 484847<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.charleshendry.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1382, N'Dr Hywel Francis MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25618.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Aberavon', N'
                        Unit 7, Water Street Business Centre, Gwyn Terrace, Aberavon, SA12 6LG<br>

                        
                            Tel: 01639 897660<br>
                        
                        
                            Fax: 01639 891725<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8121<br>
                        
                        
                            Fax: 020 7219 6223<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:francish@parliament.uk">francish@parliament.uk</a>
                    ', N'www.hywelfrancis.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1383, N'Mark Tami MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25263.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Alyn and Deeside', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8174<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:tamim@parliament.uk">tamim@parliament.uk</a>
', N'tamim@parliament.uk', N'
                        70 High Street, Connah''s Quay, Flintshire, CH5 4DD<br>

                        
                            Tel: 01244 819854<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.marktami.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1384, N'Mr Mike Weir MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25219.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Angus', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8125<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:weirm@parliament.uk">weirm@parliament.uk</a>
', N'weirm@parliament.uk', N'
                        Arbroath Office, 16 Brothock Bridge, Arbroath, DD11 1NG<br>

                        
                            Tel: 01241 874522<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:frasers@parliament.uk">frasers@parliament.uk</a>
                    ', N'www.angussnp.org', 1311, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1385, N'Mr Alan Reid MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25342.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Argyll and Bute', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8127<br>
                        
                        
                            Fax: 020 7219 1737<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:reida@parliament.uk">reida@parliament.uk</a>
', N'reida@parliament.uk', N'
                        95 Alexandra Parade, Dunoon, PA23 8AL<br>

                        
                            Tel: 01369 704840<br>
                        
                        
                            Fax: 01369 701212<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1386, N'Sir Gerald Howarth MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25505.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Aldershot', N'
                        Not for correspondence<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        Conservative Club, Victoria Road, Aldershot, GU11 1JX<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.geraldhowarth.org', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1387, N'John Mann MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25485.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bassetlaw', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8345<br>
                        
                        
                            Fax: 020 7219 5965<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mannj@parliament.uk">mannj@parliament.uk</a>
', N'mannj@parliament.uk', N'
                        Office of John Mann MP, Stanley Street, Worksop, Nottinghamshire, S81 7HX<br>

                        
                            Tel: 01909 506200<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:whitej@parliament.uk">whitej@parliament.uk</a>
                    ', N'www.johnmannmp.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1388, N'Rt Hon Nigel Dodds MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25658.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Belfast North', N'
                        39 Shore Road, Belfast, BT15 3PG<br>

                        
                            Tel: 028 9077 4774<br>
                        
                        
                            Fax: 028 9077 7685<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ndodds@dup-belfast.co.uk">ndodds@dup-belfast.co.uk</a>
', N'ndodds@dup-belfast.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8419<br>
                        
                        
                            Fax: 020 7219 2347<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:doddsn@parliament.uk">doddsn@parliament.uk</a>
                    ', N'@nigeldoddsmp', 1303, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1389, N'Rt Hon Gregory Barker MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25710.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bexhill and Battle', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1852<br>
                        
                        
                            Fax: 020 7219 1971<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:barkerg@parliament.uk">barkerg@parliament.uk</a>
', N'barkerg@parliament.uk', N'
                        6a Amherst Road, Bexhill on Sea, TN40 1QJ<br>

                        
                            Tel: 01424 736861<br>
                        
                        
                            Fax: 01424 734910<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gregorybarker.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1390, N'Mr John Baron MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25729.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Basildon and Billericay', N'
                        13 Bentalls Business Park, Basildon, SS14 3BN<br>

                        
                            Tel: 01268 520765<br>
                        
                        
                            Fax: 01268 524009<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:turnerja@parliament.uk">turnerja@parliament.uk</a>
', N'turnerja@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8138<br>
                        
                        
                            Fax: 020 7219 1743<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:baronj@parliament.uk">baronj@parliament.uk</a>
                    ', N'www.johnbaron.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1392, N'Mr Khalid Mahmood MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25438.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birmingham, Perry Barr', N'
                        18 Heathfield Road, Handsworth, Birmingham, B19 1HB<br>

                        
                            Tel: 0121 356 8264; 0121 356 8268<br>
                        
                        
                            Fax: 0121 356 8278<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8141<br>
                        
                        
                            Fax: 020 7219 1745<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mahmoodk@parliament.uk">mahmoodk@parliament.uk</a>
                    ', N'www.khalidmahmoodmp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1393, N'Mark Simmonds MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25302.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Boston and Skegness', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8143<br>
                        
                        
                            Fax: 020 7219 1746<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mark.simmonds.mp@parliament.uk">mark.simmonds.mp@parliament.uk</a>
', N'mark.simmonds.mp@parliament.uk', N'
                        5 Church Close, Boston, PE21 6NA<br>

                        
                            Tel: 01205 751414<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.marksimmonds.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1394, N'Roger Williams MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25249.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Brecon and Radnorshire', N'
                        4 Watergate, Brecon, LD3 9AN<br>

                        
                            Tel: 01874 625739<br>
                        
                        
                            Fax: 01874 625635<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:info@rogerwilliams.org.uk">info@rogerwilliams.org.uk</a>
', N'info@rogerwilliams.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8145<br>
                        
                        
                            Fax: 020 7219 1747<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:williamsr@parliament.uk">williamsr@parliament.uk</a>
                    ', N'www.rogerwilliams.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1396, N'Mr Ian Liddell-Grainger MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25472.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bridgwater and West Somerset', N'
                        16 Northgate, Bridgwater, TA6 3EU<br>

                        
                            Tel: 01278 458383<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8149<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ianlg@parliament.uk">ianlg@parliament.uk</a>
                    ', N'www.liddellgrainger.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1397, N'Hywel Williams MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25221.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Arfon', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8150<br>
                        
                        
                            Fax: 020 7219 3705<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hywel.williams.mp@parliament.uk">hywel.williams.mp@parliament.uk</a>
', N'hywel.williams.mp@parliament.uk', N'
                        8 Castle Street, Caernarfon, LL55 1SE<br>

                        
                            Tel: 01286 672076<br>
                        
                        
                            Fax: 01286 672003<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.hywelwilliams.org/', 1309, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1398, N'Wayne David MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25696.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Caerphilly', N'
                        Community Council Offices, Newport Road, Bedwas, Caerphilly, CF83 8YB<br>

                        
                            Tel: 029 2088 1061<br>
                        
                        
                            Fax: 029 2088 1954<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8152<br>
                        
                        
                            Fax: 020 7219 1751<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:davidw@parliament.uk">davidw@parliament.uk</a>
                    ', N'www.waynedavid.labour.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1399, N'John Thurso MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25277.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Caithness, Sutherland and Easter Ross', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1752<br>
                        
                        
                            Fax: 020 7219 3797<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:john.thurso.mp@parliament.uk">john.thurso.mp@parliament.uk</a>
', N'john.thurso.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:john@johnthurso.org.uk">john@johnthurso.org.uk</a>
                    ', N'www.johnthurso.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1400, N'Kevin Brennan MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25777.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cardiff West', N'
                        33-35 Cathedral Road, Cardiff, CF11 9HB<br>

                        
                            Tel: 029 2022 3207<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:simmonse@parliament.uk">simmonse@parliament.uk</a>
', N'simmonse@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8156<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:brennank@parliament.uk">brennank@parliament.uk</a>
                    ', N'www.kevinbrennan.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1405, N'Mark Field MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25535.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cities of London and Westminster', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8155<br>
                        
                        
                            Fax: 020 7219 1980<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:fieldm@parliament.uk">fieldm@parliament.uk</a>
', N'fieldm@parliament.uk', N'
                        90 Ebury Street, London, SW1W 9QD<br>

                        
                            Tel: 020 7730 8181<br>
                        
                        
                            Fax: 020 7730 4520<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@westminsterconservatives.co.uk">office@westminsterconservatives.co.uk</a>
                    ', N'www.markfieldmp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1406, N'Jon Cruddas MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25577.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dagenham and Rainham', N'
                        50-52 New Road, Dagenham, RM9 6YS<br>

                        
                            Tel: 020 8984 7854<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mullanem@parliament.uk">mullanem@parliament.uk</a>
', N'mullanem@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8161<br>
                        
                        
                            Fax: 020 7219 1756<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:cruddasj@parliament.uk">cruddasj@parliament.uk</a>
                    ', N'www.joncruddas.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1408, N'Rt Hon Hugo Swire MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25212.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Devon', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8173<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hugo.swire.mp@parliament.uk">hugo.swire.mp@parliament.uk</a>
', N'hugo.swire.mp@parliament.uk', N'
                        Foreign and Commonwealth Office, King Charles Street, London, SW1A 2AH<br>

                        
                            Tel: 020 7008 1500<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:FCOcorrespondence@fco.gsi.gov.uk">FCOcorrespondence@fco.gsi.gov.uk</a>
                    ', N'www.hugoswire.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1409, N'Geoffrey Clifton-Brown MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25708.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'The Cotswolds', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5147<br>
                        
                        
                            Fax: 020 7219 2550<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:cliftonbrowng@parliament.uk">cliftonbrowng@parliament.uk</a>
', N'cliftonbrowng@parliament.uk', N'', N'www.cliftonbrown.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1411, N'Mark Lazarowicz MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25467.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Edinburgh North and Leith', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8222<br>
                        
                        
                            Fax: 020 7219 1761<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mark.lazarowicz.mp@parliament.uk">mark.lazarowicz.mp@parliament.uk</a>
', N'mark.lazarowicz.mp@parliament.uk', N'
                        5 Croall Place, Edinburgh, EH7 4LT<br>

                        
                            Tel: 0131 557 0577<br>
                        
                        
                            Fax: 0131 557 5759<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mark@marklazarowicz.org.uk">mark@marklazarowicz.org.uk</a>
                    ', N'www.marklazarowicz.org.uk', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1413, N'Rt Hon Chris Grayling MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25172.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Epsom and Ewell', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8194<br>
                        
                        
                            Fax: 020 7219 1763<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:graylingc@parliament.uk">graylingc@parliament.uk</a>
', N'graylingc@parliament.uk', N'
                        PO Box 164, Ashtead, Surrey, KT21 9BS<br>

                        
                            Tel: 01372 271036<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:martinverdinosv@parliament.uk">martinverdinosv@parliament.uk</a>
                    ', N'www.chrisgrayling.net', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1414, N'Damian Hinds MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35480.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Hampshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7057<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:damian.hinds.mp@parliament.uk">damian.hinds.mp@parliament.uk</a>
', N'damian.hinds.mp@parliament.uk', N'
                        14a Butts Road, Alton, GU34 1ND<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.damianhinds.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1415, N'Rt Hon Hugh Robertson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25348.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Faversham and Mid Kent', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2643<br>
                        
                        
                            Fax: 020 7219 1765<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:staintonjamesa@parliament.uk">staintonjamesa@parliament.uk</a>
', N'staintonjamesa@parliament.uk', N'
                        11 The Square, Lenham, Kent, ME17 2PQ<br>

                        
                            Tel: 01622 851616<br>
                        
                        
                            Fax: 01622 850294<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.hughrobertson.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1416, N'Michelle Gildernew MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25626.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Fermanagh and South Tyrone', N'
                        Thomas Clarke House, 60 Irish Street, Dungannon, BT70 1QD<br>

                        
                            Tel: 028 8772 2776<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:michelle.gildernew@sinn-fein.ie">michelle.gildernew@sinn-fein.ie</a>
', N'michelle.gildernew@sinn-fein.ie', N'
                        5 Market Street, Enniskillen, Co. Fermanagh, BT74  7DS<br>

                        
                            Tel: 028 6632 8214<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@gildernewmp', 1312, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1418, N'Mr Tom Harris MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25568.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Glasgow South', N'
                        c/o Cathcart Old Parish Church, 119 Carmunnock Road, Mount Florida, Glasgow, G44 5UW<br>

                        
                            Tel: 0141 637 1962<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8187<br>
                        
                        
                            Fax: 020 7219 1769<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tomharrismp@parliament.uk">tomharrismp@parliament.uk</a>
                    ', N'tomharris.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1419, N'Ann McKechin MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25431.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Glasgow North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8239<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ann.mckechin.mp@parliament.uk">ann.mckechin.mp@parliament.uk</a>
', N'ann.mckechin.mp@parliament.uk', N'
                        154 Raeberry Street, Glasgow, G20 6EA<br>

                        
                            Tel: 0141 946 1300<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:christopher.kerr@parliament.uk">christopher.kerr@parliament.uk</a>
                    ', N'www.annmckechinmp.net', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1424, N'Mr Mark Prisk MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25334.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hertford and Stortford', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6358<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hunterj@parliament.uk">hunterj@parliament.uk</a>
', N'hunterj@parliament.uk', N'
                        Hertford and Stortford Conservative Association GF04, Room GF04 Harlow Enterprise Hub, Kao Hockham Building, Edinburgh Way, Harlow, Essex, CM20 2NQ<br>

                        
                            Tel: 01279 512 197<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@hertford-stortfordconservatives.net%20">office@hertford-stortfordconservatives.net </a>
                    ', N'www.markprisk.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1425, N'Mr Jonathan Djanogly MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25639.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Huntingdon', N'
                        Castle Hill House, High Street, Huntingdon, PE29 3TE<br>

                        
                            Tel: 01480 437840<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jonathan.djanogly.mp@parliament.uk">jonathan.djanogly.mp@parliament.uk</a>
', N'jonathan.djanogly.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2367<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jonathan.djanogly.mp@parliament.uk">jonathan.djanogly.mp@parliament.uk</a>
                    ', N'www.jonathandjanogly.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1426, N'Mr Andrew Turner MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25287.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Isle of Wight', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8490<br>
                        
                        
                            Fax: 020 7219 0174<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        The Riverside Centre, The Quay, Newport, Isle of Wight, PO30 2QR<br>

                        
                            Tel: 01983 530808<br>
                        
                        
                            Fax: 01983 822266<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mail@islandmp.org">mail@islandmp.org</a>
                    ', N'www.islandmp.org', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1427, N'Rt Hon Andy Burnham MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25787.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leigh', N'
                        10 Market Street, Leigh, WN7 1DS<br>

                        
                            Tel: 01942 682353<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andy.burnham.mp@parliament.uk">andy.burnham.mp@parliament.uk</a>
', N'andy.burnham.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8250<br>
                        
                        
                            Fax: 020 7219 4381<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:andy.burnham.mp@parliament.uk">andy.burnham.mp@parliament.uk</a>
                    ', N'www.andyburnham.net/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1428, N'Bill Wiggin MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25244.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Herefordshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8175<br>
                        
                        
                            Fax: 020 7219 1893<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bill.wiggin.mp@parliament.uk">bill.wiggin.mp@parliament.uk</a>
', N'bill.wiggin.mp@parliament.uk', N'
                        8 Corn Square, Leominster, HR6 8LR<br>

                        
                            Tel: 01568 612565<br>
                        
                        
                            Fax: 01568 610320<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.billwiggin.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1430, N'Mr Dai Havard MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25489.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Merthyr Tydfil and Rhymney', N'
                        Unit 4, Triangle Business Park, Pentrebach, Merthyr Tydfil, CF48 4TQ<br>

                        
                            Tel: 01685 379247<br>
                        
                        
                            Fax: 01685 387563<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:toomeyd@parliament.uk">toomeyd@parliament.uk</a>
', N'toomeyd@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8255<br>
                        
                        
                            Fax: 020 7219 1449<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:dai.havard.mp@parliament.uk">dai.havard.mp@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1431, N'Annette Brooke MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25778.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mid Dorset and North Poole', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8193<br>
                        
                        
                            Fax: 020 7219 1898<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:brookea@parliament.uk">brookea@parliament.uk</a>
', N'brookea@parliament.uk', N'
                        Broadstone Liberal Hall, 14 York Road, Broadstone, BH18 8ET<br>

                        
                            Tel: 01202 693555<br>
                        
                        
                            Fax: 01202 658420<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mackayj@parliament.uk">mackayj@parliament.uk</a>
                    ', N'www.annettebrooke.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1432, N'Mr David Hamilton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25494.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Midlothian', N'
                        PO Box 11, 95 High Street, Dalkeith, EH22 1HL<br>

                        
                            Tel: 0131 654 1585<br>
                        
                        
                            Fax: 0131 654 1586<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8257<br>
                        
                        
                            Fax: 020 7219 2532<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hamiltonda@parliament.uk">hamiltonda@parliament.uk</a>
                    ', N'www.davidhamiltonmp.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1433, N'Angus Robertson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25347.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Moray', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8259<br>
                        
                        
                            Fax: 020 7219 1781<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:robertsona@parliament.uk">robertsona@parliament.uk</a>
', N'robertsona@parliament.uk', N'
                        Moray Parliamentary Office, 9 Wards Road, Elgin, IV30 1NL<br>

                        
                            Tel: 01343 551111<br>
                        
                        
                            Fax: 01343 556355<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:moraymp@googlemail.com">moraymp@googlemail.com</a>
                    ', N'www.angusrobertson.org', 1311, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1436, N'Paul Farrelly MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25533.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newcastle-under-Lyme', N'
                        Waterloo Buildings, 79-81 Dunkirk, Newcastle-under-Lyme, ST5 2SW<br>

                        
                            Tel: 01782 715033<br>
                        
                        
                            Fax: 01782 613174<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hopwoodc@parliament.uk">hopwoodc@parliament.uk</a>
', N'hopwoodc@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8391<br>
                        
                        
                            Fax: 020 7219 1986<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:paul.farrelly.mp@parliament.uk">paul.farrelly.mp@parliament.uk</a>
                    ', N'www.paulfarrelly.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1437, N'Lady Hermon MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25175.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Down', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8491<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sylvia.hermon.mp@parliament.uk">sylvia.hermon.mp@parliament.uk</a>
', N'sylvia.hermon.mp@parliament.uk', N'
                        17a Hamilton Road, Bangor, BT20 4LF<br>

                        
                            Tel: 028 9127 5858<br>
                        
                        
                            Fax: 028 9127 5747<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jamisons@parliament.uk">jamisons@parliament.uk</a>
                    ', N'www.sylviahermon.org/', 1305, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1438, N'Mr Kevan Jones MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25525.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Durham', N'
                        Fulforth Centre, Front Street, Sacriston, Co. Durham, DH7 6JT<br>

                        
                            Tel: 0191 371 8834<br>
                        
                        
                            Fax: 0191 371 8834<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8219<br>
                        
                        
                            Fax: 020 7219 1759<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:kevanjonesmp@parliament.uk">kevanjonesmp@parliament.uk</a>
                    ', N'www.kevanjonesmp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1439, N'Norman Lamb MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25458.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Norfolk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0542<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:norman.lamb.mp@parliament.uk">norman.lamb.mp@parliament.uk</a>
', N'norman.lamb.mp@parliament.uk', N'
                        Unit 4, North Walsham Garden Centre, Nursery Drive, Norwich Road, North Walsham, NR28 0DR<br>

                        
                            Tel: 01692 403752<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.normanlamb.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1440, N'Pete Wishart MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25254.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Perth and North Perthshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8303<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:wishartp@parliament.uk">wishartp@parliament.uk</a>
', N'wishartp@parliament.uk', N'
                        63 Glasgow Road, Perth, PH2 0PE<br>

                        
                            Tel: 01738 639598<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.petewishartmp.com', 1311, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1441, N'Mr Henry Bellingham MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25734.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North West Norfolk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8234<br>
                        
                        
                            Fax: 020 7219 2844<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bellinghamh@parliament.uk">bellinghamh@parliament.uk</a>
', N'bellinghamh@parliament.uk', N'
                        North West Norfolk Conservative Association, First Floor, 12 London Road, King''s Lynn, PE30 5PY<br>

                        
                            Tel: 01485 600559<br>
                        
                        
                            Fax: 01485 600292<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.henrybellingham.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1442, N'Mr Ronnie Campbell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25669.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Blyth Valley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4216<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ronnie.campbell.mp@parliament.uk">ronnie.campbell.mp@parliament.uk</a>
', N'ronnie.campbell.mp@parliament.uk', N'
                        42 Renwick Road, Blyth, NE24 2LQ<br>

                        
                            Tel: 01670 363050<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1444, N'Rt Hon Mark Francois MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25619.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rayleigh and Wickford', N'
                        25 Bellingham Lane, Rayleigh, SS6 7ED<br>

                        
                            Tel: 01268 742044<br>
                        
                        
                            Fax: 01268 741833<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8287<br>
                        
                        
                            Fax: 020 7219 1858<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mark.francois.mp@parliament.uk">mark.francois.mp@parliament.uk</a>
                    ', N'www.gov.uk/government/organisations/ministry-of-defence', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1446, N'Chris Bryant MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25745.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rhondda', N'
                        Oxford House, Dunraven Street, Tonypandy, CF40 1AU<br>

                        
                            Tel: 01443 442521<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:morganke@parliament.uk">morganke@parliament.uk</a>
', N'morganke@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8315<br>
                        
                        
                            Fax: 020 7219 1792<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:bryantc@parliament.uk">bryantc@parliament.uk</a>
                    ', N'www.chris-bryant.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1447, N'Andrew Rosindell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25356.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Romford', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8475 ; 020 7219 8499<br>
                        
                        
                            Fax: 020 7219 1960<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andrew.rosindell.mp@parliament.uk">andrew.rosindell.mp@parliament.uk</a>
', N'andrew.rosindell.mp@parliament.uk', N'
                        Margaret Thatcher House, 85 Western Road, Romford, RM1 3LS<br>

                        
                            Tel: 01708 766700 ; 01708 761186<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:andrew@rosindell.com">andrew@rosindell.com</a>
                    ', N'@andrewrosindell', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1448, N'Meg Munn MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25376.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sheffield, Heeley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8316<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:meg.munn.mp@parliament.uk">meg.munn.mp@parliament.uk</a>
', N'meg.munn.mp@parliament.uk', N'
                        PO Box 4333, Sheffield, S8 2EY<br>

                        
                            Tel: 0114 258 2010<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.megmunnmp.org.uk', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1451, N'Mr Richard Bacon MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25783.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Norfolk', N'
                        Grasmere, Denmark Street, Diss, IP22 4LE<br>

                        
                            Tel: 01379 643728, 01379 642769<br>
                        
                        
                            Fax: 01379 642220<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:reevet@parliament.uk;%20rigbym@parliament.uk">reevet@parliament.uk; rigbym@parliament.uk</a>
', N'reevet@parliament.uk; rigbym@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8301<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:richardbaconmp@parliament.uk">richardbaconmp@parliament.uk</a>
                    ', N'www.richardbacon.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1453, N'Andrew Selous MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25301.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South West Bedfordshire', N'
                        6c Princes Street, Dunstable, LU6 3AX<br>

                        
                            Tel: 01582 662821<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8134<br>
                        
                        
                            Fax: 020 7219 1741<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:andrew.selous.mp@parliament.uk">andrew.selous.mp@parliament.uk</a>
                    ', N'www.andrewselous.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1454, N'Mark Pritchard MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31594.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'The Wrekin', N'
                        25 Church Street, Wellington, TF1 1DG<br>

                        
                            Tel: 01952 256080<br>
                        
                        
                            Fax: 01952 256080<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8494<br>
                        
                        
                            Fax: 020 7219 5969<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:pritchardm@parliament.uk">pritchardm@parliament.uk</a>
                    ', N'www.markpritchard.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1458, N'Rt Hon George Osborne MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25194.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Tatton', N'
                        Tatton Conservative Association, Manchester Road, Knutsford, WA16 0LT<br>

                        
                            Tel: 01565 873037<br>
                        
                        
                            Fax: 01565 873039<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:robertsonbj@parliament.uk">robertsonbj@parliament.uk</a>
', N'robertsonbj@parliament.uk', N'
                        HM Treasury, 1 Horse Guards Road, London, SW1A 2HQ<br>

                        
                            Tel: 020 7270 5000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:public.enquiries@hm-treasury.gov.uk">public.enquiries@hm-treasury.gov.uk</a>
                    ', N'www.georgeosborne.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1461, N'David Wright MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25269.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Telford', N'
                        35B High Street, Dawley, Telford, TF4 2EX<br>

                        
                            Tel: 01952 507747<br>
                        
                        
                            Fax: 01952 506064<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:info@fromtelfordfortelford.com">info@fromtelfordfortelford.com</a>
', N'info@fromtelfordfortelford.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8331<br>
                        
                        
                            Fax: 020 7219 1979<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:wrightda@parliament.uk">wrightda@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1462, N'Dame  Angela Watkinson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25236.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hornchurch and Upminster', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8267<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:angela.watkinson.mp@parliament.uk">angela.watkinson.mp@parliament.uk</a>
', N'angela.watkinson.mp@parliament.uk', N'
                        23 Butts Green Road, Hornchurch, RM11 2JS<br>

                        
                            Tel: 01708 475252<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.angelawatkinsonmp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1463, N'Mr Tom Watson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25228.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Bromwich East', N'
                        Terry Duffy House, 1 Thomas Street, West Bromwich, B70 6NT<br>

                        
                            Tel: 0121 569 1904<br>
                        
                        
                            Fax: 0121 553 2043<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8335<br>
                        
                        
                            Fax: 020 7219 1943<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tom.watson.mp@parliament.uk">tom.watson.mp@parliament.uk</a>
                    ', N'www.tom-watson.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1464, N'Jim Sheridan MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25305.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Paisley and Renfrewshire North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8314<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sheridanj@parliament.uk">sheridanj@parliament.uk</a>
', N'sheridanj@parliament.uk', N'
                        Mirren Court Three, Ground Floor, 123 Renfrew Road, Paisley, PA3 4EA<br>

                        
                            Tel: 0141 847 1457<br>
                        
                        
                            Fax: 0141 847 1395<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jim@jimsheridanmp.org.uk">jim@jimsheridanmp.org.uk</a>
                    ', N'www.jimsheridanmp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1465, N'Mr Pat Doherty MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25659.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Tyrone', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8159<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:dohertyp@parliament.uk">dohertyp@parliament.uk</a>
', N'dohertyp@parliament.uk', N'
                        4 James Street, Omagh, BT78 1DH<br>

                        
                            Tel: 028 8225 3040<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.westtyronesinnfein.com/', 1312, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1466, N'Dr Andrew Murrison MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25384.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South West Wiltshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:murrisona@parliament.uk">murrisona@parliament.uk</a>
', N'murrisona@parliament.uk', N'
                        Suite 1, Holloway House, Epsom Square, White Horse Business Park, Trowbridge, BA14 0XG<br>

                        
                            Tel: 01225 358584<br>
                        
                        
                            Fax: 01225 358583<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.andrewmurrison.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1467, N'Rt Hon David Cameron MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25752.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Witney', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3475<br>
                        
                        
                            Fax: 020 7219 0918<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        West Oxfordshire Conservative Association, Waterloo House, 58-60 High Street, Witney, Oxfordshire, OX28 6HJ<br>

                        
                            Tel: 01993 702302<br>
                        
                        
                            Fax: 01993 776639<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.davidcameronmp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1469, N'Sir Tony Cunningham MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25576.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Workington', N'
                        Moss Bay House, 40 Peart Road, Derwent Howe, Workington, CA14 3YT<br>

                        
                            Tel: 01900 65815<br>
                        
                        
                            Fax: 01900 68348<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:smithjmt@parliament.uk">smithjmt@parliament.uk</a>
', N'smithjmt@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6905<br>
                        
                        
                            Fax: 020 7219 1245<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:cunninghamt@parliament.uk">cunninghamt@parliament.uk</a>
                    ', N'www.tonycunningham.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1470, N'Ian Lucas MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25496.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wrexham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8346<br>
                        
                        
                            Fax: 020 7219 1948<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:lucasi@parliament.uk">lucasi@parliament.uk</a>
', N'lucasi@parliament.uk', N'
                        Vernon House, 41 Rhosddu Road, Wrexham, LL11 2NS<br>

                        
                            Tel: 01978 355743<br>
                        
                        
                            Fax: 01978 310051<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.ianlucas.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1473, N'Rt Hon David Laws MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25557.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Yeovil', N'
                        5 Church Street, Yeovil, BA20 1HB<br>

                        
                            Tel: 01935 425025; 01935 423284<br>
                        
                        
                            Fax: 01935 433652<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:enquiries@yeovil-libdems.org.uk">enquiries@yeovil-libdems.org.uk</a>
', N'enquiries@yeovil-libdems.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8413<br>
                        
                        
                            Fax: 020 7219 8188<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:lawsd@parliament.uk">lawsd@parliament.uk</a>
                    ', N'www.gov.uk/government/organisations/department-for-education', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1474, N'Albert Owen MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25395.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ynys Môn', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8415<br>
                        
                        
                            Fax: 020 7219 1951<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:albert.owen.mp@parliament.uk">albert.owen.mp@parliament.uk</a>
', N'albert.owen.mp@parliament.uk', N'
                        18a Thomas Street, Holyhead, LL65 1RR<br>

                        
                            Tel: 01407 765750<br>
                        
                        
                            Fax: 01407 764336<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:albertowen.labour@gmail.com">albertowen.labour@gmail.com</a>
                    ', N'@albertowenmp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1476, N'Mr Nick Hurd MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/37707.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ruislip, Northwood and Pinner', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1053<br>
                        
                        
                            Fax: 020 7219 4854<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nick.hurd.mp@parliament.uk">nick.hurd.mp@parliament.uk</a>
', N'nick.hurd.mp@parliament.uk', N'
                        Ruislip Northwood &amp; Pinner Conservative Association, 32 High Street, Northwood, HA6 1BN<br>

                        
                            Tel: 01923 822876<br>
                        
                        
                            Fax: 01923 841514<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:annrnca@aol.com">annrnca@aol.com</a>
                    ', N'www.nickhurd.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1478, N'Mr Iain Wright MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/34603.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hartlepool', N'
                        23 South Road, Hartlepool, TS26 9HD<br>

                        
                            Tel: 01429 224403<br>
                        
                        
                            Fax: 01429 864775<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5587<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:wrighti@parliament.uk">wrighti@parliament.uk</a>
                    ', N'www.iainwrightmp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1479, N'Rt Hon Nick Herbert MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40839.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Arundel and South Downs', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4080<br>
                        
                        
                            Fax: 020 7219 1295<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:herbertn@parliament.uk">herbertn@parliament.uk</a>
', N'herbertn@parliament.uk', N'
                        Arundel and South Downs Conservative Association, The Old Town Hall, 38 High Street, Arundel, BN44 3YE<br>

                        
                            Tel: 01903 816880<br>
                        
                        
                            Fax: 01903 810348<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nick@nickherbert.com">nick@nickherbert.com</a>
                    ', N'www.nickherbert.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1480, N'Rt Hon Maria Miller MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31595.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Basingstoke', N'
                        The Mount, Bounty Road, Basingstoke, RG21 3DD<br>

                        
                            Tel: 01256 322207<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:maria.miller.mp@parliament.uk">maria.miller.mp@parliament.uk</a>
', N'maria.miller.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5749<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:maria.miller.mp@parliament.uk">maria.miller.mp@parliament.uk</a>
                    ', N'www.mariamiller.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1481, N'Nadine Dorries MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41312.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mid Bedfordshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5928<br>
                        
                        
                            Fax: 020 7219 6428<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:dorriesn@parliament.uk">dorriesn@parliament.uk</a>
', N'dorriesn@parliament.uk', N'
                        Mid-Bedfordshire Conservative Association, St Michaels Close, High Street, Shefford, SG17 5DD<br>

                        
                            Tel: 01462 811 449<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nadine.dorries.mp@parliament.uk">nadine.dorries.mp@parliament.uk</a>
                    ', N'blog.dorries.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1482, N'Mr Graham Stuart MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31707.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Beverley and Holderness', N'
                        9 Cross Street, Beverley, HU17 9AX<br>

                        
                            Tel: 01482 679687<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:graham@grahamstuart.com">graham@grahamstuart.com</a>
', N'graham@grahamstuart.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4340<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:graham.stuart.mp@parliament.uk">graham.stuart.mp@parliament.uk</a>
                    ', N'www.grahamstuart.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1483, N'John Hemming MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31572.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birmingham, Yardley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6314<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hemmingj@parliament.uk">hemmingj@parliament.uk</a>
', N'hemmingj@parliament.uk', N'
                        1772 Coventry Road, Birmingham, B26 1PB<br>

                        
                            Tel: 0121 722 3417<br>
                        
                        
                            Fax: 0121 722 3437<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:john.hemming@jhc.co.uk">john.hemming@jhc.co.uk</a>
                    ', N'john.hemming.name/', 1308, 1)
GO
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1484, N'Helen Goodman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40782.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bishop Auckland', N'
                        1 Cockton Hill Road, Bishop Auckland, DL14 6EN<br>

                        
                            Tel: 01388 603075<br>
                        
                        
                            Fax: 01388 603075<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4346<br>
                        
                        
                            Fax: 020 7219 0444<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:goodmanh@parliament.uk">goodmanh@parliament.uk</a>
                    ', N'helengoodman.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1486, N'Mr David Anderson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35863.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Blaydon', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4348<br>
                        
                        
                            Fax: 020 7219 8276<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andersonda@parliament.uk">andersonda@parliament.uk</a>
', N'andersonda@parliament.uk', N'
                        St Cuthbert''s Community Hall, Shibdon Road, Blaydon on Tyne, NE21 5PT<br>

                        
                            Tel: 0191 414 2488<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.daveanderson.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1487, N'Mr Tobias Ellwood MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31646.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bournemouth East', N'
                        Bournemouth East Conservative Association, Haviland Road West, Boscombe, Bournemouth, BH1 4JW<br>

                        
                            Tel: 01202 397047<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:becaoffice@btconnect.com">becaoffice@btconnect.com</a>
', N'becaoffice@btconnect.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4349<br>
                        
                        
                            Fax: 020 7219 0946<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tobias.ellwood.mp@parliament.uk">tobias.ellwood.mp@parliament.uk</a>
                    ', N'www.tobiasellwood.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1488, N'Mr Brooks Newmark MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31699.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Braintree', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3464<br>
                        
                        
                            Fax: 020 7219 5245<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:brooks.newmark.mp@parliament.uk">brooks.newmark.mp@parliament.uk</a>
', N'brooks.newmark.mp@parliament.uk', N'
                        Avenue Lodge, The Avenue, Witham, CM8 2DL<br>

                        
                            Tel: 01376 512386<br>
                        
                        
                            Fax: 01376 516475<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:theoffice@braintreeconservatives.co.uk">theoffice@braintreeconservatives.co.uk</a>
                    ', N'www.brooksnewmark.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1490, N'Mrs Madeleine Moon MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35871.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bridgend', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0814<br>
                        
                        
                            Fax: 020 7219 6488<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:moonm@parliament.uk">moonm@parliament.uk</a>
', N'moonm@parliament.uk', N'
                        47 Nolton Street, Bridgend, CF31 3AA<br>

                        
                            Tel: 01656 750002<br>
                        
                        
                            Fax: 01656 660081<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.madeleinemoonmp.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1491, N'Kerry McCarthy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/39440.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bristol East', N'
                        326A Church Road, St George, Bristol, BS5 8AJ<br>

                        
                            Tel: 0117 939 9901<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4510<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:kerry.mccarthy.mp@parliament.uk">kerry.mccarthy.mp@parliament.uk</a>
                    ', N'www.kerrymccarthymp.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1492, N'Stephen Williams MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31736.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bristol West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8416<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen.williams.mp@parliament.uk">stephen.williams.mp@parliament.uk</a>
', N'stephen.williams.mp@parliament.uk', N'
                        PO Box 2500, Bristol, BS6 9AH<br>

                        
                            Tel: 0117 942 3494<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stephen.williams.mp@parliament.uk">stephen.williams.mp@parliament.uk</a>
                    ', N'www.stephenwilliams.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1493, N'Mr Charles Walker MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/37676.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Broxbourne', N'
                        57-59 High Street, Hoddesdon, Hertfordshire, EN11 8TQ<br>

                        
                            Tel: 01992 479972<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:broxbourne@tory.org">broxbourne@tory.org</a>
', N'broxbourne@tory.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0338<br>
                        
                        
                            Fax: 020 7219 0505<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:charles.walker.mp@parliament.uk">charles.walker.mp@parliament.uk</a>
                    ', N'www.charleswalker.org', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1496, N'Mr Shailesh Vara MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31684.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North West Cambridgeshire', N'
                        North West Cambridgeshire Conservative Association, The Old Barn, Hawthorn Farm, Ashton, Stamford, PE9 3BA<br>

                        
                            Tel: 01733 380 089<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nwcca@tory.org">nwcca@tory.org</a>
', N'nwcca@tory.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6050<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:shailesh.vara.mp@parliament.uk">shailesh.vara.mp@parliament.uk</a>
                    ', N'www.shaileshvara.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1497, N'Jenny Willott MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35533.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cardiff Central', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8418<br>
                        
                        
                            Fax: 020 7219 0694<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:willottj@parliament.uk">willottj@parliament.uk</a>
', N'willottj@parliament.uk', N'
                        38 The Parade, Roath, Cardiff, CF24 3AD<br>

                        
                            Tel: 029 20 462276<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jenny@jennywillott.com">jenny@jennywillott.com</a>
                    ', N'www.jennywillott.co.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1498, N'Mr Mark Williams MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31723.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ceredigion', N'
                        32 North Parade, Aberystwyth, Ceredigion, SY23 2NF<br>

                        
                            Tel: 01970 627721<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8469<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:williamsmf@parliament.uk">williamsmf@parliament.uk</a>
                    ', N'www.markwilliams.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1499, N'Kris Hopkins MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31576.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Keighley', N'
                        Churchill House, North Street, Keighley, BD21 3AF<br>

                        
                            Tel: 01535 211152<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:kris.hopkins.mp@parliament.uk">kris.hopkins.mp@parliament.uk</a>
                    ', N'www.krishopkins.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1500, N'Rt Hon Theresa Villiers MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/29179.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chipping Barnet', N'
                        163 High Street, Barnet, EN5 5SU<br>

                        
                            Tel: 020 8449 7345<br>
                        
                        
                            Fax: 020 8449 7346<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:theresa@theresavilliers.co.uk">theresa@theresavilliers.co.uk</a>
', N'theresa@theresavilliers.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6212<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:theresa@theresavilliers.co.uk">theresa@theresavilliers.co.uk</a>
                    ', N'www.theresavilliers.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1501, N'Roberta Blackman-Woods MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35889.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'City of Durham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4982<br>
                        
                        
                            Fax: 020 7219 8018<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:woodsr@parliament.uk">woodsr@parliament.uk</a>
', N'woodsr@parliament.uk', N'
                        The Miners'' Hall, Redhills, Flass Street, Durham, DH1 4BD<br>

                        
                            Tel: 0191 374 1915<br>
                        
                        
                            Fax: 0191 374 1916<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mail@roberta.org.uk">mail@roberta.org.uk</a>
                    ', N'www.roberta.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1502, N'Rt Hon David Jones MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31694.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Clwyd West', N'
                        3 Llewelyn Road, Colwyn Bay, Conwy, LL29 7AP<br>

                        
                            Tel: 01492 535845<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:georgeb@parliament.uk">georgeb@parliament.uk</a>
', N'georgeb@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8070<br>
                        
                        
                            Fax: 020 7219 0142<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jonesdi@parliament.uk">jonesdi@parliament.uk</a>
                    ', N'@davidjonesmp', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1503, N'Rt Hon John Redwood MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25340.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wokingham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4205<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:john.redwood.mp@parliament.uk">john.redwood.mp@parliament.uk</a>
', N'john.redwood.mp@parliament.uk', N'
                        30 Rose Street, Wokingham, RG40 1XU<br>

                        
                            Tel: 0118 962 9501<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@wokinghamconservatives.org.uk">office@wokinghamconservatives.org.uk</a>
                    ', N'www.johnredwoodsdiary.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1504, N'Dan Rogerson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31629.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Cornwall', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4707<br>
                        
                        
                            Fax: 020 7219 1018<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        4 Tower Street, Launceston, PL15 8BQ<br>

                        
                            Tel: 01566 777123<br>
                        
                        
                            Fax: 01566 772122<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:contact@danrogerson.org">contact@danrogerson.org</a>
                    ', N'www.danrogerson.org', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1506, N'Andrew Gwynne MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35894.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Denton and Reddish', N'
                        Town Hall, Market Street, Denton, M34 2AP<br>

                        
                            Tel: 0161 320 1504<br>
                        
                        
                            Fax: 0161 320 1503<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4708<br>
                        
                        
                            Fax: 020 7219 4548<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:gwynnea@parliament.uk">gwynnea@parliament.uk</a>
                    ', N'www.andrewgwynne.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1507, N'Natascha Engel MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35958.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North East Derbyshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1015<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:natascha.engel.mp@parliament.uk">natascha.engel.mp@parliament.uk</a>
', N'natascha.engel.mp@parliament.uk', N'
                        62 Market Street, Eckington, S21 4JH<br>

                        
                            Tel: 01246 439018<br>
                        
                        
                            Fax: 01246 439024<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.nataschaengelmp.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1508, N'Mr Geoffrey Cox QC MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31700.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Torridge and West Devon', N'
                        2 Bridge Chambers, Lower Bridge Street, Bideford, EX39 2BU<br>

                        
                            Tel: 01237 459001<br>
                        
                        
                            Fax: 01237 459003<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:tellgeoffrey@geoffreycox.co.uk">tellgeoffrey@geoffreycox.co.uk</a>
', N'tellgeoffrey@geoffreycox.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4719<br>
                        
                        
                            Fax: 020 7219 4307<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:coxg@parliament.uk">coxg@parliament.uk</a>
                    ', N'www.geoffreycox.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1510, N'Rt Hon Edward Miliband MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40791.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Doncaster North', N'
                        Hutton Business Centre, Bridge Works, Bentley, Doncaster, DN5 9QP<br>

                        
                            Tel: 01302 875462<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4778<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ed.miliband.mp@parliament.uk">ed.miliband.mp@parliament.uk</a>
                    ', N'@ed_miliband', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1511, N'Ian Austin MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/19494.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dudley North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8012<br>
                        
                        
                            Fax: 020 7219 4488<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:austini@parliament.uk">austini@parliament.uk</a>
', N'austini@parliament.uk', N'
                        Turner House, 157-185 Wrens Nest Road, Dudley, DY1 3RU<br>

                        
                            Tel: 01384 342503/4<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:austini@parliament.uk">austini@parliament.uk</a>
                    ', N'www.ianaustin.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1512, N'Rt Hon David Mundell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/32679.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dumfriesshire, Clydesdale and Tweeddale', N'
                        2 Holm Street, Moffat, DG10 9EB<br>

                        
                            Tel: 01683 222746<br>
                        
                        
                            Fax: 01683 222796<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david@davidmundell.com">david@davidmundell.com</a>
', N'david@davidmundell.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4895<br>
                        
                        
                            Fax: 020 7219 2707<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:david.mundell.mp@parliament.uk">david.mundell.mp@parliament.uk</a>
                    ', N'www.davidmundell.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1513, N'Jo Swinson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31661.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Dunbartonshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8088<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jo.swinson.mp@parliament.uk">jo.swinson.mp@parliament.uk</a>
', N'jo.swinson.mp@parliament.uk', N'
                        126 Drymen Road, Bearsden, G61 3RB<br>

                        
                            Tel: 0141 943 1568<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.joswinson.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1514, N'Martin Horwood MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/38427.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cheltenham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4784<br>
                        
                        
                            Fax: 020 7219 1185<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:martin.horwood.mp@parliament.uk">martin.horwood.mp@parliament.uk</a>
', N'martin.horwood.mp@parliament.uk', N'
                        16 Hewlett Road, Cheltenham, GL52 6AA<br>

                        
                            Tel: 01242 224889<br>
                        
                        
                            Fax: 01242 256658<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:martin@martinhorwood.net">martin@martinhorwood.net</a>
                    ', N'www.martinhorwood.net/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1515, N'Jim McGovern MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/38863.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dundee West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4938<br>
                        
                        
                            Fax: 020 7219 4812<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mcgovernj@parliament.uk">mcgovernj@parliament.uk</a>
', N'mcgovernj@parliament.uk', N'
                        7 West Wynd (off Perth Road), Dundee, DD1 4JQ<br>

                        
                            Tel: 01382 322100<br>
                        
                        
                            Fax: 01382 322696<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jimmcgovern.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1516, N'Andy Slaughter MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35897.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hammersmith', N'
                        28 Greyhound Road, London, W6 8NX<br>

                        
                            Tel: 020 7610 1950<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andy@andyslaughter.com">andy@andyslaughter.com</a>
', N'andy@andyslaughter.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4990<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:slaughtera@parliament.uk">slaughtera@parliament.uk</a>
                    ', N'www.andyslaughter.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1518, N'Mr David Burrowes MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31648.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Enfield, Southgate', N'
                        1C Chaseville Parade, Chaseville Park Road, Winchmore Hill, London, N21 1PG<br>

                        
                            Tel: 020 8360 0234<br>
                        
                        
                            Fax: 020 8364 2766<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david@davidburrowes.com">david@davidburrowes.com</a>
', N'david@davidburrowes.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8144<br>
                        
                        
                            Fax: 020 7219 5289<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:burrowesd@parliament.uk">burrowesd@parliament.uk</a>
                    ', N'www.davidburrowes.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1520, N'Mr Mark Harper MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31687.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Forest of Dean', N'
                        

                        
                            Tel: 01594 823482<br>
                        
                        
                            Fax: 01594 823623<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5056<br>
                        
                        
                            Fax: 020 7219 0937<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mark.harper.mp@parliament.uk">mark.harper.mp@parliament.uk</a>
                    ', N'www.markharper.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1521, N'Rt Hon Margaret Hodge MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25573.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Barking', N'
                        102 North Street, Barking, IG11 8LA<br>

                        
                            Tel: 020 8594 1333<br>
                        
                        
                            Fax: 020 8594 1131<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:margarethodge@hotmail.co.uk">margarethodge@hotmail.co.uk</a>
', N'margarethodge@hotmail.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6666<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hodgem@parliament.uk">hodgem@parliament.uk</a>
                    ', N'margaret-hodge.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1522, N'Mr Philip Hollobone MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31683.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kettering', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8373<br>
                        
                        
                            Fax: 020 7219 8802<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:philip.hollobone.mp@parliament.uk">philip.hollobone.mp@parliament.uk</a>
', N'philip.hollobone.mp@parliament.uk', N'
                        Tel: 01536 414715; 07979 850126<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1523, N'Anne Milton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35360.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Guildford', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8392<br>
                        
                        
                            Fax: 020 7219 5239<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:anne.milton.mp@parliament.uk">anne.milton.mp@parliament.uk</a>
', N'anne.milton.mp@parliament.uk', N'
                        17a Home Farm, Loseley Park, Guildford, GU3 1HS<br>

                        
                            Tel: 01483 300330<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:anne@annemilton.com">anne@annemilton.com</a>
                    ', N'www.guildfordconservatives.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1524, N'David Heyes MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25502.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ashton-under-Lyne', N'
                        4 St Michael''s Court, St Michael''s Square, Stamford Street, Ashton-Under-Lyne, OL6 6XN<br>

                        
                            Tel: 0161 331 9307<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8129<br>
                        
                        
                            Fax: 020 7219 1738<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:heyesd@parliament.uk">heyesd@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1525, N'Mrs Linda Riordan MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35915.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Halifax', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5399<br>
                        
                        
                            Fax: 020 7219 1513<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:riordanl@parliament.uk">riordanl@parliament.uk</a>
', N'riordanl@parliament.uk', N'
                        2-4 Shaw Lodge House, Halifax, HX3 9ET<br>

                        
                            Tel: 01422 251800<br>
                        
                        
                            Fax: 01422 251888<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.lindariordanmp.com/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1526, N'Greg Hands MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35363.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chelsea and Fulham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mail@greghands.com">mail@greghands.com</a>
', N'mail@greghands.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0809<br>
                        
                        
                            Fax: 020 7219 6801<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.greghands.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1527, N'Neil Carmichael MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35481.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stroud', N'
                        38-39 Palace Chambers, London Road, Stroud, GL5 2AJ<br>

                        
                            Tel: 01453 751572<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:neil.carmichael.mp@parliament.uk">neil.carmichael.mp@parliament.uk</a>
                    ', N'www.neilcarmichael.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1528, N'Stephen Pound MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25409.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ealing North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4312<br>
                        
                        
                            Fax: 020 7219 5982<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stevepoundmp@parliament.uk">stevepoundmp@parliament.uk</a>
', N'stevepoundmp@parliament.uk', N'', N'www.stevepound.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1529, N'Mr David Gauke MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31647.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South West Hertfordshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4459<br>
                        
                        
                            Fax: 020 7219 4759<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gauked@parliament.uk">gauked@parliament.uk</a>
', N'gauked@parliament.uk', N'
                        South West Hertfordshire Conservative Association, Scots Bridge House, Scots Hill, Rickmansworth, WD3 3BB<br>

                        
                            Tel: 01923 771781<br>
                        
                        
                            Fax: 01923 779471<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:david@davidgauke.com">david@davidgauke.com</a>
                    ', N'www.davidgauke.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1530, N'James Brokenshire MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35372.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Old Bexley and Sidcup', N'
                        19 Station Road, Sidcup, DA15 7EB<br>

                        
                            Tel: 020 8300 3471<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8400<br>
                        
                        
                            Fax: 020 7219 2043<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:james.brokenshire.mp@parliament.uk">james.brokenshire.mp@parliament.uk</a>
                    ', N'www.jamesbrokenshire.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1531, N'Lynne Featherstone MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31718.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hornsey and Wood Green', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8401<br>
                        
                        
                            Fax: 020 7219 0008<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:featherstonel@parliament.uk">featherstonel@parliament.uk</a>
', N'featherstonel@parliament.uk', N'
                        First Floor Offices, 62 High Street, London, N8 7BX<br>

                        
                            Tel: 020 8340 5459<br>
                        
                        
                            Fax: 020 8340 5459<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:lynne@lynnefeatherstone.org">lynne@lynnefeatherstone.org</a>
                    ', N'www.lynnefeatherstone.org', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1533, N'Diana Johnson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35928.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kingston upon Hull North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5647<br>
                        
                        
                            Fax: 020 7219 0959<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:johnsond@parliament.uk">johnsond@parliament.uk</a>
', N'johnsond@parliament.uk', N'
                        Sycamore Suite, Community Enterprise Centre, Cottingham Road, Hull, HU5 2DH<br>

                        
                            Tel: 01482 319135<br>
                        
                        
                            Fax: 01482 319137<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.dianajohnson.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1534, N'Mr Lee Scott MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31722.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ilford North', N'
                        9 Sevenways Parade, Gants Hill, Ilford, IG2 6XH<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8326<br>
                        
                        
                            Fax: 020 7219 0970<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:scottle@parliament.uk">scottle@parliament.uk</a>
                    ', N'www.lee-scott.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1535, N'Peter Aldous MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35512.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Waveney', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7182<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:peter.aldous.mp@parliament.uk">peter.aldous.mp@parliament.uk</a>
', N'peter.aldous.mp@parliament.uk', N'
                        15 Surrey Street, Lowestoft, Suffolk, NR32 1LU<br>

                        
                            Tel: 01502 586568<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.peteraldous.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1536, N'Mr Gareth Thomas MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25273.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Harrow West', N'
                        132 Blenheim Road, West Harrow, HA2 7AA<br>

                        
                            Tel: 020 8861 6300<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gareth.thomas@harrowlabour.org">gareth.thomas@harrowlabour.org</a>
', N'gareth.thomas@harrowlabour.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4243<br>
                        
                        
                            Fax: 020 7219 1154<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:thomasgr@parliament.uk">thomasgr@parliament.uk</a>
                    ', N'www.gareththomas.org/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1537, N'George Hollingbery MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35515.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Meon Valley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7109<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:george.hollingbery.mp@parliament.uk">george.hollingbery.mp@parliament.uk</a>
', N'george.hollingbery.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                            Tel: 01962 734076<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.georgehollingbery.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1538, N'Rosie Cooper MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/38898.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Lancashire', N'
                        Suite 108, Malthouse Business Centre, 48 Southport Road, Ormskirk, L39 1QR<br>

                        
                            Tel: 01695 570094<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:rosie@rosiecooper.net">rosie@rosiecooper.net</a>
', N'rosie@rosiecooper.net', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@rosie4westlancs', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1539, N'Mr Ben Wallace MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35379.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wyre and Preston North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5804<br>
                        
                        
                            Fax: 020 7219 5901<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:wallaceb@parliament.uk">wallaceb@parliament.uk</a>
', N'wallaceb@parliament.uk', N'
                        Ben Wallace MP, Great Eccleston Village Centre, 59 High Street, Great Eccleston, PR3 0YB<br>

                        
                            Tel: 01995 672977<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.benwallacemp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1540, N'Greg Mulholland MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35617.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leeds North West', N'
                        Wainwright House, 12 Holt Park Centre, Holt Road, Leeds, LS16 7SR<br>

                        
                            Tel: 0113 226 6519<br>
                        
                        
                            Fax: 0113 226 2237<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:info@gregmulholland.org">info@gregmulholland.org</a>
', N'info@gregmulholland.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3833<br>
                        
                        
                            Fax: 020 7219 2810<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:greg.mulholland.mp@parliament.uk">greg.mulholland.mp@parliament.uk</a>
                    ', N'www.gregmulholland.org', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1541, N'Nia Griffith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/37590.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Llanelli', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4903<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nia.griffith.mp@parliament.uk">nia.griffith.mp@parliament.uk</a>
', N'nia.griffith.mp@parliament.uk', N'
                        6 Queen Victoria Road, Llanelli, SA15 2TL<br>

                        
                            Tel: 01554 756374<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'niagriffith.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1542, N'Mr Philip Dunne MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35388.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ludlow', N'
                        Ludlow Constituency Conservative Association, 54 Broad Street, Ludlow, SY8 1GP<br>

                        
                            Tel: 01584 872187<br>
                        
                        
                            Fax: 01584 876345<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:philip@philipdunne.com">philip@philipdunne.com</a>
', N'philip@philipdunne.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2388<br>
                        
                        
                            Fax: 020 7219 0788<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:philip.dunne.mp@parliament.uk">philip.dunne.mp@parliament.uk</a>
                    ', N'www.philipdunne.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1543, N'Mr John Leech MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/38488.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Manchester, Withington', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8353<br>
                        
                        
                            Fax: 020 7219 0442<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:john.leech.mp@parliament.uk">john.leech.mp@parliament.uk</a>
', N'john.leech.mp@parliament.uk', N'
                        8 Gawsworth Avenue, East Didsbury, Manchester, M20 5NF<br>

                        
                            Tel: 0161 434 3334<br>
                        
                        
                            Fax: 0161 434 3206<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.john-leech.libdems.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1544, N'Mark Lancaster MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31593.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Milton Keynes North', N'
                        Suite 102, Milton Keynes Business Park, Foxhunter Drive, Linford Wood, MK14 6GD<br>

                        
                            Tel: 01908 686830<br>
                        
                        
                            Fax: 01908 686831<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8414<br>
                        
                        
                            Fax: 020 7219 6685<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:officeofmarklancaster@parliament.uk">officeofmarklancaster@parliament.uk</a>
                    ', N'www.lancaster4mk.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1545, N'David T. C. Davies MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33522.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Monmouth', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david.davies.mp@parliament.uk">david.davies.mp@parliament.uk</a>
', N'david.davies.mp@parliament.uk', N'
                        The Grange, 16 Maryport Street, Usk, Monmouthshire, NP15 1AB<br>

                        
                            Tel: 01291 672817<br>
                        
                        
                            Fax: 01291 672737<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.david-daviesmp.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1546, N'Mr Angus Brendan MacNeil MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/38376.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Na h-Eileanan an Iar', N'
                        31 Bayhead Street, Stornoway, Isle of Lewis, Outer Hebrides, HS1 2DU<br>

                        
                            Tel: 01851 702272<br>
                        
                        
                            Fax: 01851 701767<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:macdonaldrm@parliament.uk">macdonaldrm@parliament.uk</a>
', N'macdonaldrm@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8476<br>
                        
                        
                            Fax: 020 7219 6111<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:macneila@parliament.uk">macneila@parliament.uk</a>
                    ', N'@angusmacneilmp', 1311, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1547, N'Richard Benyon MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35394.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newbury', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8319<br>
                        
                        
                            Fax: 020 7219 4509<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:benyonr@parliament.uk">benyonr@parliament.uk</a>
', N'benyonr@parliament.uk', N'
                        6 Cheap Street, Newbury, RG14 5DD<br>

                        
                            Tel: 01635 551070<br>
                        
                        
                            Fax: 01635 569690<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mp@richardbenyon.com">mp@richardbenyon.com</a>
                    ', N'www.richardbenyon.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1548, N'Jessica Morden MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/39384.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newport East', N'
                        Suite 2, 7th Floor, Clarence House, Clarence Place, Newport, NP19 7AA<br>

                        
                            Tel: 01633 841725<br>
                        
                        
                            Fax: 01633 841727<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6213<br>
                        
                        
                            Fax: 020 7219 6196<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mordenj@parliament.uk">mordenj@parliament.uk</a>
                    ', N'www.jessicamorden.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1549, N'Rt Hon Ed Balls MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/19497.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Morley and Outwood', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4115<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ed.balls.mp@parliament.uk">ed.balls.mp@parliament.uk</a>
', N'ed.balls.mp@parliament.uk', N'
                        Albion Chambers, Albion Street, Morley, LS27 8DT<br>

                        
                            Tel: 0113 253 9466<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ed@edballs.com">ed@edballs.com</a>
                    ', N'www.edballs.com', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1550, N'Mr Brian Binley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35405.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Northampton South', N'
                        Northampton South Conservative Association, White Lodge, 42 Billing Road, Northampton, NN1 5DA<br>

                        
                            Tel: 01604 633414<br>
                        
                        
                            Fax: 01604 250252<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nsca@devlinfisher.co.uk">nsca@devlinfisher.co.uk</a>
', N'nsca@devlinfisher.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8298<br>
                        
                        
                            Fax: 020 7219 2265<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:brian.binley.mp@parliament.uk">brian.binley.mp@parliament.uk</a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1551, N'Glenda Jackson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25521.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hampstead and Kilburn', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4008<br>
                        
                        
                            Fax: 020 7219 2112<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jacksong@parliament.uk">jacksong@parliament.uk</a>
                    ', N'www.glenda-jackson.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1552, N'Alison Seabeck MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40799.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Plymouth, Moor View', N'
                        No constituency office publicised<br>

                        
                            Tel: 01752 365617<br>
                        
                        
                            Fax: 01752 364325<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jamessi@parliament.uk">jamessi@parliament.uk</a>
', N'jamessi@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6431<br>
                        
                        
                            Fax: 020 7219 0883<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:alison.seabeck.mp@parliament.uk">alison.seabeck.mp@parliament.uk</a>
                    ', N'www.alisonseabeckmp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1554, N'Stephen Crabb MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31575.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Preseli Pembrokeshire', N'
                        Suite 1, 20 Upper Market Street, Haverfordwest, Pembrokeshire, SA61 1QA<br>

                        
                            Tel: 01437 767555<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jonesad@parliament.uk">jonesad@parliament.uk</a>
', N'jonesad@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6518<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stephen.crabb.mp@parliament.uk">stephen.crabb.mp@parliament.uk</a>
                    ', N'www.stephencrabb.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1555, N'Rt Hon Justine Greening MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31669.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Putney', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8300<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:greeningj@parliament.uk">greeningj@parliament.uk</a>
', N'greeningj@parliament.uk', N'
                        3 Summerstown, London, SW17 0BQ<br>

                        
                            Tel: 020 8946 4557<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.justinegreening.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1556, N'Mr Rob Wilson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31582.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Reading East', N'
                        12a South View Park, Marsack Street, Reading, RG4 5AF<br>

                        
                            Tel: 0118 375 9785<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@readingeastconservatives.com">office@readingeastconservatives.com</a>
', N'office@readingeastconservatives.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2498<br>
                        
                        
                            Fax: 020 7219 6519<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:robwilsonmp@parliament.uk">robwilsonmp@parliament.uk</a>
                    ', N'www.robwilsonmp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1559, N'James Duddridge MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31686.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rochford and Southend East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:james@jamesduddridge.com">james@jamesduddridge.com</a>
', N'james@jamesduddridge.com', N'
                        Rochford and Southend East Conservative Association, Suite 1, Strand House, 742 Southchurch Road, Southend on Sea, SS1 2PS<br>

                        
                            Tel: 01702 616135<br>
                        
                        
                            Fax: 01702 619071<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jamesduddridge.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1560, N'Jeremy Wright MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35436.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kenilworth and Southam', N'
                        Jubilee House, Smalley Place, Kenilworth, CV8 1QG<br>

                        
                            Tel: 01926 853650<br>
                        
                        
                            Fax: 01926 854615<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:barnespa@parliament.uk">barnespa@parliament.uk</a>
', N'barnespa@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8299<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:pickeringc@parliament.uk">pickeringc@parliament.uk</a>
                    ', N'www.jeremywrightmp.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1561, N'Dr Julian Huppert MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35613.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cambridge', N'
                        16 Signet Court, Cambridge, CB5 8LA<br>

                        
                            Tel: 01223 304421<br>
                        
                        
                            Fax: 01223 312148<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:julianhuppertmp@gmail.com">julianhuppertmp@gmail.com</a>
', N'julianhuppertmp@gmail.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0647<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:julian.huppert.mp@parliament.uk">julian.huppert.mp@parliament.uk</a>
                    ', N'www.julianhuppert.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1562, N'Mr Robert Goodwill MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/28181.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Scarborough and Whitby', N'
                        21 Huntriss Row, Scarborough, YO11 2ED<br>

                        
                            Tel: 01723 365656<br>
                        
                        
                            Fax: 01723 362577<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8268<br>
                        
                        
                            Fax: 020 7219 8108<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:robert.goodwill.mp@parliament.uk">robert.goodwill.mp@parliament.uk</a>
                    ', N'www.robertgoodwill.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1563, N'Rt Hon Tom Clarke MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25687.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Coatbridge, Chryston and Bellshill', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6997<br>
                        
                        
                            Fax: 020 7219 6094<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:clarket@parliament.uk">clarket@parliament.uk</a>
', N'clarket@parliament.uk', N'
                        Municipal Buildings, Kildonan Street, Coatbridge, ML5 3LF<br>

                        
                            Tel: 01236 600800<br>
                        
                        
                            Fax: 01236 600808<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hartys@parliament.uk">hartys@parliament.uk</a>
                    ', N'www.tomclarke.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1564, N'Angela Smith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40274.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Penistone and Stocksbridge', N'
                        The Arc, Town Hall, Manchester Road, Stocksbridge, Sheffield, S36 2DT<br>

                        
                            Tel: 0114 283 1855<br>
                        
                        
                            Fax: 0114 283 1850<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:wilsonst@parliament.uk">wilsonst@parliament.uk</a>
', N'wilsonst@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6713<br>
                        
                        
                            Fax: 020 7219 8598<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:smithac@parliament.uk">smithac@parliament.uk</a>
                    ', N'www.angelasmith-mp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1565, N'Philip Davies MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35440.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Shipley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8264<br>
                        
                        
                            Fax: 020 7219 8389<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:daviesp@parliament.uk">daviesp@parliament.uk</a>
', N'daviesp@parliament.uk', N'
                        Shipley Conservatives Association, 76 Otley Road, Shipley, BD18 3SA<br>

                        
                            Tel: 01274 592248<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:philipdavies@shipleyconservatives.fsnet.co.uk">philipdavies@shipleyconservatives.fsnet.co.uk</a>
                    ', N'www.philip-davies.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1566, N'Daniel Kawczynski MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31584.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Shrewsbury and Atcham', N'
                        Shrewsbury &amp; Atcham West Conservative Association, Unit 1, Benbow Business Park, Harlescott Lane, Shrewsbury, SY1 3FA<br>

                        
                            Tel: 01743 466477<br>
                        
                        
                            Fax: 01743 465774<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mail@daniel4shrewsbury.co.uk">mail@daniel4shrewsbury.co.uk</a>
', N'mail@daniel4shrewsbury.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6249<br>
                        
                        
                            Fax: 020 7219 1047<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:kawczynskid@parliament.uk">kawczynskid@parliament.uk</a>
                    ', N'www.daniel4shrewsbury.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1567, N'Lorely Burt MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31605.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Solihull', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8269<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:lorely.burt.mp@parliament.uk">lorely.burt.mp@parliament.uk</a>
', N'lorely.burt.mp@parliament.uk', N'
                        81 Warwick Road, Solihull, B92 7HP<br>

                        
                            Tel: 0121 706 9593<br>
                        
                        
                            Fax: 0121 706 9365<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'lorelyburt.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1568, N'Mrs Anne Main MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35461.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'St Albans', N'
                        104 High Street, London Colney, St Albans, AL2 1QL<br>

                        
                            Tel: 01727 825100<br>
                        
                        
                            Fax: 01727 828404<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8270<br>
                        
                        
                            Fax: 020 7219 3058<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:maina@parliament.uk">maina@parliament.uk</a>
                    ', N'www.annemain.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1569, N'Robert Flello MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/37613.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stoke-on-Trent South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:flellor@parliament.uk">flellor@parliament.uk</a>
', N'flellor@parliament.uk', N'
                        Travers Court, City Road, Fenton, Stoke-on-Trent, ST4 2PY<br>

                        
                            Tel: 01782 844810<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.robertflello.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1571, N'Rt Hon Michael Gove MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35482.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Surrey Heath', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6804<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:michael.gove.mp@parliament.uk">michael.gove.mp@parliament.uk</a>
', N'michael.gove.mp@parliament.uk', N'
                        Curzon House, Church Road, Windlesham, GU20 6BH<br>

                        
                            Tel: 01276 472468<br>
                        
                        
                            Fax: 01276 451602<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@shca.org.uk">office@shca.org.uk</a>
                    ', N'www.michaelgove.com/index.php', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1572, N'Rt Hon Simon Hughes MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25596.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bermondsey and Old Southwark', N'
                        4 Market Place, London, SE16 3UQ<br>

                        
                            Tel: 020 7232 2557<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6256<br>
                        
                        
                            Fax: 020 7219 6567<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:simon@simonhughes.org.uk">simon@simonhughes.org.uk</a>
                    ', N'simonhughesmp.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1573, N'Margot James MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35371.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stourbridge', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7226<br>
                        
                        
                            Fax: 020 7219 6434<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:margot.james.mp@parliament.uk">margot.james.mp@parliament.uk</a>
', N'margot.james.mp@parliament.uk', N'
                        15-17 Lawn Avenue, Stourbridge, DY8 3UR<br>

                        
                            Tel: 01384 370574<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.margotjames.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1575, N'Mr Jeremy Browne MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35697.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Taunton Deane', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5181<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:brownej@parliament.uk">brownej@parliament.uk</a>
', N'brownej@parliament.uk', N'
                        Liberal Democrat Office, Masons House, Magdalene Street, Taunton, TA1 1SG<br>

                        
                            Tel: 01823 337874<br>
                        
                        
                            Fax: 01823 559823<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.tauntonlibdems.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1576, N'Mr Steve Reed MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/101345.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Croydon North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7297<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:steve.reed.mp@parliament.uk">steve.reed.mp@parliament.uk</a>
', N'steve.reed.mp@parliament.uk', N'', N'www.stevereedmp.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1577, N'Rt Hon Sadiq Khan MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35774.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Tooting', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6967<br>
                        
                        
                            Fax: 020 7219 6477<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sadiqkhanmp@parliament.uk">sadiqkhanmp@parliament.uk</a>
', N'sadiqkhanmp@parliament.uk', N'
                        273 Balham High Road, London, SW17 7BD<br>

                        
                            Tel: 020 8682 2897<br>
                        
                        
                            Fax: 020 8682 3416<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:sadiqkhanmp@parliament.uk">sadiqkhanmp@parliament.uk</a>
                    ', N'www.sadiqkhan.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1578, N'Mr James Clappison MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25164.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hertsmere', N'
                        104 High Street, London Colney, St Albans, AL2 1QL<br>

                        
                            Tel: 01727 828221<br>
                        
                        
                            Fax: 01727 828404<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4152<br>
                        
                        
                            Fax: 020 7219 0514<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tilleye@parliament.uk">tilleye@parliament.uk</a>
                    ', N'www.jamesclappison.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1579, N'Mary Creagh MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/36019.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wakefield', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6984<br>
                        
                        
                            Fax: 020 7219 4257<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:creaghm@parliament.uk">creaghm@parliament.uk</a>
', N'creaghm@parliament.uk', N'
                        20-22 Cheapside, Wakefield, WF1 2TF<br>

                        
                            Tel: 01924 386124<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mary@marycreagh.co.uk">mary@marycreagh.co.uk</a>
                    ', N'www.marycreagh.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1580, N'Mr Edward Vaizey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35502.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wantage', N'
                        Wallingford Town Hall, Wallingford, Oxon, OX10 0EG<br>

                        
                            Tel: 01491 838816<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:vaizeye@parliament.uk">vaizeye@parliament.uk</a>
', N'vaizeye@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6350<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:vaizeye@parliament.uk">vaizeye@parliament.uk</a>
                    ', N'www.vaizey.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1581, N'Mr Peter Bone MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31691.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wellingborough', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8496<br>
                        
                        
                            Fax: 020 7219 0301<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bonep@parliament.uk">bonep@parliament.uk</a>
', N'bonep@parliament.uk', N'
                        21 High Street, Wellingborough, NN8 4JZ<br>

                        
                            Tel: 01933 279343<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:wrca@tory.org">wrca@tory.org</a>
                    ', N'www.wellingboroughconservatives.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1582, N'Rt Hon Grant Shapps MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31715.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Welwyn Hatfield', N'
                        Welwyn Hatfield Conservative Association, Maynard House, The Common, Hatfield, AL10 0NF<br>

                        
                            Tel: 01707 262632<br>
                        
                        
                            Fax: 01707 263892<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sandra@welhatconservatives.com">sandra@welhatconservatives.com</a>
', N'sandra@welhatconservatives.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8497<br>
                        
                        
                            Fax: 020 7219 0659<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:shappsg@parliament.uk">shappsg@parliament.uk</a>
                    ', N'www.shapps.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1583, N'Lyn Brown MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/39376.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Ham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6999<br>
                        
                        
                            Fax: 020 7219 0864<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:brownl@parliament.uk">brownl@parliament.uk</a>
', N'brownl@parliament.uk', N'
                        306 High Street, E15 1AJ<br>

                        
                            Tel: 020 8470 3463<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:lyn@lynbrown.org.uk">lyn@lynbrown.org.uk</a>
                    ', N'www.lynbrown.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1584, N'Mike Penning MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31734.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hemel Hempstead', N'
                        The Bury, Queensway, Hemel Hempstead, HP1 1HR<br>

                        
                            Tel: 01442 251126<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mike@penning4hemel.com">mike@penning4hemel.com</a>
', N'mike@penning4hemel.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:penningm@parliament.uk">penningm@parliament.uk</a>
                    ', N'www.mikepenning.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1585, N'Stephen Hammond MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31703.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wimbledon', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3401<br>
                        
                        
                            Fax: 020 7219 0462<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen.hammond.mp@parliament.uk">stephen.hammond.mp@parliament.uk</a>
', N'stephen.hammond.mp@parliament.uk', N'
                        Wimbledon Conservative Association, c/o 1 Summerstown, London, SW17 0BQ<br>

                        
                            Tel: 020 8944 2905<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:agent@wimbledonconservatives.co.uk">agent@wimbledonconservatives.co.uk</a>
                    ', N'www.stephenhammondmp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1586, N'Nigel Adams MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35435.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Selby and Ainsty', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7141<br>
                        
                        
                            Fax: 020 7219 3992<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nigel.adams.mp@parliament.uk">nigel.adams.mp@parliament.uk</a>
', N'nigel.adams.mp@parliament.uk', N'
                        17 High Street, Tadcaster, LS24 9AP<br>

                        
                            Tel: 01937 838088<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.selbyandainsty.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1587, N'Rt Hon Pat McFadden MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/18145.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wolverhampton South East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4036<br>
                        
                        
                            Fax: 020 7219 5665<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mcfaddenp@parliament.uk">mcfaddenp@parliament.uk</a>
', N'mcfaddenp@parliament.uk', N'
                        Crescent House, Broad Street, Bilston, WV14 0BZ<br>

                        
                            Tel: 01902 405762<br>
                        
                        
                            Fax: 01902 402381<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.patmcfadden.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1588, N'Barbara Keeley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/36036.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Worsley and Eccles South', N'
                        No constituency office publicised<br>

                        
                            Tel: 0161 799 4159<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8025<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.barbarakeeley.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1589, N'Gordon Banks MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/38890.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ochil and South Perthshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8275<br>
                        
                        
                            Fax: 020 7219 8693<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:banksgr@parliament.uk">banksgr@parliament.uk</a>
', N'banksgr@parliament.uk', N'
                        Unit 3, Penny Lane, Church Street, Crieff, PH7 3LE<br>

                        
                            Tel: 01764 654738<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gordonbanks.info/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1590, N'Rt Hon Greg Clark MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40488.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Tunbridge Wells', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6977<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gregclarkmp@parliament.uk">gregclarkmp@parliament.uk</a>
', N'gregclarkmp@parliament.uk', N'
                        Cabinet Office, Correspondence Team, 70 Whitehall, London, SW1A 2AS<br>

                        
                            Tel: 020 7276 1234<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:publiccorrespondence@cabinet-office.gsi.gov.uk">publiccorrespondence@cabinet-office.gsi.gov.uk</a>
                    ', N'www.gregclark.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1591, N'Tim Farron MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31716.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Westmorland and Lonsdale', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8498<br>
                        
                        
                            Fax: 020 7219 2810<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:farront@parliament.uk">farront@parliament.uk</a>
', N'farront@parliament.uk', N'
                        Acland House, Yard 2, Stricklandgate, Kendal, LA9 4ND<br>

                        
                            Tel: 01539 723403<br>
                        
                        
                            Fax: 01539 740800<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tim@timfarron.co.uk">tim@timfarron.co.uk</a>
                    ', N'www.timfarron.co.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1593, N'Sammy Wilson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33564.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Antrim', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8523<br>
                        
                        
                            Fax: 020 7219 3671<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:barronj@parliament.uk">barronj@parliament.uk</a>
', N'barronj@parliament.uk', N'
                        East Antrim DUP, 116 Main Street, Larne, BT40 1RG<br>

                        
                            Tel: 028 2826 7722<br>
                        
                        
                            Fax: 028 2826 9922<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.sammywilson.org', 1303, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1594, N'Mark Durkan MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33606.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Foyle', N'
                        23 Bishop Street, Derry, BT48 6PR<br>

                        
                            Tel: 028 7136 0700<br>
                        
                        
                            Fax: 028 7136 0808<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:m.durkan@sdlp.ie">m.durkan@sdlp.ie</a>
', N'm.durkan@sdlp.ie', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5096<br>
                        
                        
                            Fax: 020 7219 2694<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mark.durkan.mp@parliament.uk">mark.durkan.mp@parliament.uk</a>
                    ', N'www.markdurkan.net/', 1313, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1595, N'Conor Murphy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33626.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newry and Armagh', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8534<br>
                        
                        
                            Fax: 020 7219 6107<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:murphyc@parliament.uk">murphyc@parliament.uk</a>
', N'murphyc@parliament.uk', N'
                        1 Kilmorey Terrace, Patrick Street, Newry, BT35 6DW<br>

                        
                            Tel: 028 3026 1693<br>
                        
                        
                            Fax: 028 3026 8283<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.newryarmaghsf.com', 1312, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1596, N'Dr Alasdair McDonnell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33617.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Belfast South', N'
                        120A Ormeau Road, Belfast, BT7 2EB<br>

                        
                            Tel: 028 9024 2474<br>
                        
                        
                            Fax: 028 9043 9935<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8528; 020 7219 8510<br>
                        
                        
                            Fax: 020 7219 2832<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:alasdair.mcdonnell.mp@parliament.uk">alasdair.mcdonnell.mp@parliament.uk</a>
                    ', N'www.alasdairmcdonnell.com', 1313, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1597, N'David Simpson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33635.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Upper Bann', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8533<br>
                        
                        
                            Fax: 020 7219 2347<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:simpsond@parliament.uk">simpsond@parliament.uk</a>
', N'simpsond@parliament.uk', N'
                        13 Thomas Street, Portadown, Craigavon, BT62 3NP<br>

                        
                            Tel: 028 3833 2234<br>
                        
                        
                            Fax: 028 3833 2123<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:davidsimpson@upperbanndup.co.uk">davidsimpson@upperbanndup.co.uk</a>
                    ', N'', 1303, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1598, N'Tristram Hunt MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/85049.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stoke-on-Trent Central', N'
                        88 Lonsdale Street, Stoke, ST4 4DP<br>

                        
                            Tel: 01782 410455<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:tristramhunt@parliament.uk">tristramhunt@parliament.uk</a>
', N'tristramhunt@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1179<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tristram.hunt.mp@parliament.uk">tristram.hunt.mp@parliament.uk</a>
                    ', N'www.tristramhunt.com/web/', 1306, 1)
GO
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1601, N'Robert Neill MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/34498.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bromley and Chislehurst', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8169<br>
                        
                        
                            Fax: 020 7219 8089<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bob.neill.mp@parliament.uk">bob.neill.mp@parliament.uk</a>
', N'bob.neill.mp@parliament.uk', N'
                        Bromley and Chislehurst Conservative Association, 5 White Horse Hill, Chislehurst, BR7 6DG<br>

                        
                            Tel: 020 8295 2639<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@bromleyconservatives.com">office@bromleyconservatives.com</a>
                    ', N'www.bobneillmp.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1603, N'Phil Wilson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/61609.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sedgefield', N'
                        4 Beveridge Walkway, Newton Aycliffe, DL5 4EE<br>

                        
                            Tel: 01325 321603<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:brownmar@parliament.uk">brownmar@parliament.uk</a>
', N'brownmar@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4966<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:phil.wilson.mp@parliament.uk">phil.wilson.mp@parliament.uk</a>
                    ', N'@philwilsonmp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1604, N'Mr Virendra Sharma MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/61606.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ealing, Southall', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6080<br>
                        
                        
                            Fax: 020 7219 3969<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sharmav@parliament.uk">sharmav@parliament.uk</a>
', N'sharmav@parliament.uk', N'
                        112A The Green, Southall, UB2 4BQ<br>

                        
                            Tel: 020 8571 1003<br>
                        
                        
                            Fax: 020 8571 9991<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.virendrasharma.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1605, N'Mr Edward Timpson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62790.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Crewe and Nantwich', N'
                        30 Victoria Street, Crewe, CW1 2JE<br>

                        
                            Tel: 01270 501725<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8027<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:timpsone@parliament.uk">timpsone@parliament.uk</a>
                    ', N'www.edwardtimpson.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1606, N'John Howell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72787.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Henley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6676<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:howelljm@parliament.uk">howelljm@parliament.uk</a>
', N'howelljm@parliament.uk', N'
                        PO Box 84, Watlington, OX49 5XD<br>

                        
                            Tel: 01491 613072<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.johnhowellmp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1608, N'Lindsay Roy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/74691.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Glenrothes', N'
                        83a Woodside Way, Glenrothes, KY7 5DW<br>

                        
                            Tel: 01592 751549<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:lroymp@parliament.uk">lroymp@parliament.uk</a>
', N'lroymp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8273<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:royl@parliament.uk">royl@parliament.uk</a>
                    ', N'@lindsayroymp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1609, N'Chloe Smith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72302.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Norwich North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8449<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:chloe.smith.mp@parliament.uk">chloe.smith.mp@parliament.uk</a>
', N'chloe.smith.mp@parliament.uk', N'
                        Diamond House, Vulcan Road, Norwich, NR6 6AQ<br>

                        
                            Tel: 01603 414756<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:chloe@chloesmith.org.uk">chloe@chloesmith.org.uk</a>
                    ', N'www.chloesmith.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (1610, N'Mr William Bain MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/80778.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Glasgow North East', N'
                        Flemington House, 110 Flemington Street, Glasgow, G21 4BX<br>

                        
                            Tel: 0141 557 2513<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:willie@williebain.com">willie@williebain.com</a>
', N'willie@williebain.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7527<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:willie.bain.mp@parliament.uk">willie.bain.mp@parliament.uk</a>
                    ', N'www.williebain.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3909, N'Pamela Nash MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84901.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Airdrie and Shotts', N'
                        100 Stirling St, Airdrie, ML6 0AS<br>

                        
                            Tel: 01236 753795<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7003<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:pamela.nash.mp@parliament.uk">pamela.nash.mp@parliament.uk</a>
                    ', N'@pamela_nash', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3910, N'Guto Bebb MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35317.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Aberconwy', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7002<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:guto.bebb.mp@parliament.uk">guto.bebb.mp@parliament.uk</a>
', N'guto.bebb.mp@parliament.uk', N'
                        1 Ashdown House, Riverside Business Park, Benarth Road, Conwy, LL32 8YX<br>

                        
                            Tel: 01492 583094<br>
                        
                        
                            Fax: 01492 592721<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@gutobebbmp.co.uk">office@gutobebbmp.co.uk</a>
                    ', N'gutobebbmp.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3911, N'Dr Eilidh Whiteford MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/74736.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Banff and Buchan', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7005<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:eilidh.whiteford.mp@parliament.uk">eilidh.whiteford.mp@parliament.uk</a>
', N'eilidh.whiteford.mp@parliament.uk', N'
                        Office 7, Burnside Business Centre, Burnside Road, Peterhead, AB42 3AW<br>

                        
                            Tel: 01779 822022<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.eilidhwhiteford.info', 1311, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3912, N'Richard Fuller MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35241.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bedford', N'
                        135 Midland Road, Bedford, MK40 1DN<br>

                        
                            Tel: 01234 261487<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7012<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:richard.fuller.mp@parliament.uk">richard.fuller.mp@parliament.uk</a>
                    ', N'www.richardfuller.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3913, N'Jack Dromey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/47128.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birmingham, Erdington', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0903<br>
                        
                        
                            Fax: 020 7219 0477<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jack.dromey.mp@parliament.uk">jack.dromey.mp@parliament.uk</a>
', N'jack.dromey.mp@parliament.uk', N'
                        77 Mason Road, Erdington, Birmingham, B24 9EH<br>

                        
                            Tel: 0121 350 6077<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jackdromey.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3914, N'Shabana Mahmood MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72715.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Birmingham, Ladywood', N'
                        No constituency address publicised<br>

                        
                            Tel: 0121 706 8999 Fax:0121 706 8596<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7818<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:shabana.mahmood.mp@parliament.uk">shabana.mahmood.mp@parliament.uk</a>
                    ', N'www.shabanamahmood.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3915, N'Gloria De Piero MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/85008.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ashfield', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7004<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gloria.depiero.mp@parliament.uk">gloria.depiero.mp@parliament.uk</a>
', N'gloria.depiero.mp@parliament.uk', N'
                        8 Station Street, Kirkby-in-Ashfield, NG17 7AR<br>

                        
                            Tel: 01623 720399<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gloria-de-piero.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3916, N'Michael Dugher MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/16861.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Barnsley East', N'
                        West Bank House, West Street, Hoyland, Barnsley, S74 9EE<br>

                        
                            Tel: 01226 743483<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7006<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:michael.dugher.mp@parliament.uk">michael.dugher.mp@parliament.uk</a>
                    ', N'www.michaeldugher.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3917, N'John Woodcock MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/32028.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Barrow and Furness', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:john.woodcock.mp@parliament.uk">john.woodcock.mp@parliament.uk</a>
', N'john.woodcock.mp@parliament.uk', N'
                        22 Hartington Street, Barrow-in-Furness, LA14 5SL<br>

                        
                            Tel: 01229 431204<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:john.woodcock.mp@parliament.uk">john.woodcock.mp@parliament.uk</a>
                    ', N'www.johnwoodcock.org', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3918, N'Jane Ellison MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35418.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Battersea', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7010<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jane.ellison.mp@parliament.uk">jane.ellison.mp@parliament.uk</a>
', N'jane.ellison.mp@parliament.uk', N'
                        1-3 Summerstown, London, SW17 0QB<br>

                        
                            Tel: 020 8944 2065<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.janeellison.net', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3919, N'John Stevenson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62784.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Carlisle', N'
                        2 Currie Street, Carlisle, CA1 1HH<br>

                        
                            Tel: 01228 550684<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@johnstevensonmp.co.uk">office@johnstevensonmp.co.uk</a>
', N'office@johnstevensonmp.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:john.stevenson.mp@parliament.uk">john.stevenson.mp@parliament.uk</a>
                    ', N'www.johnstevensonmp.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3920, N'Naomi Long MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33560.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Belfast East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7013; 0207 219 2340<br>
                        
                        
                            Fax: 0207 219 0451<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:naomi.long.mp@parliament.uk">naomi.long.mp@parliament.uk</a>
', N'naomi.long.mp@parliament.uk', N'
                        56 Upper Newtownards Road, Belfast, BT4 3EL<br>

                        
                            Tel: 028 9047 2004<br>
                        
                        
                            Fax: 028 9065 6408<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:belfast.east@allianceparty.org;%20naomi.long@allianceparty.org">belfast.east@allianceparty.org; naomi.long@allianceparty.org</a>
                    ', N'@naomi_long', 1301, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3921, N'Dr Phillip Lee MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40500.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bracknell', N'
                        10 Milbanke Court, Milbanke Way, Western Road, Bracknell, RG12 1RP<br>

                        
                            Tel: 01344 868894<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:phillip@phillip-lee.com">phillip@phillip-lee.com</a>
', N'phillip@phillip-lee.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1270<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:phillip.lee.mp@parliament.uk">phillip.lee.mp@parliament.uk</a>
                    ', N'www.phillip-lee.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3922, N'Conor Burns MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31713.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bournemouth West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7021<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:conor.burns.mp@parliament.uk">conor.burns.mp@parliament.uk</a>
', N'conor.burns.mp@parliament.uk', N'
                        Bournemouth West Conservatives, 135 Hankinson Road, Bournemouth, BH9 1HR<br>

                        
                            Tel: 01202 533553<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:conor@localconservatives.com">conor@localconservatives.com</a>
                    ', N'www.conorburns.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3923, N'Mr David Ward MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/37625.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bradford East', N'
                        458-460 Killinghall Road, Bradford, BD2 4SL<br>

                        
                            Tel: 01274 458010<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david@davidward.org.uk">david@davidward.org.uk</a>
', N'david@davidward.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7213<br>
                        
                        
                            Fax: 020 7219 6204<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:david.ward.mp@parliament.uk">david.ward.mp@parliament.uk</a>
                    ', N'davidward.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3924, N'John Pugh MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25198.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Southport', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8318<br>
                        
                        
                            Fax: 020 7219 1794<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:pughj@parliament.uk">pughj@parliament.uk</a>
', N'pughj@parliament.uk', N'
                        35 Shakespeare Street, Southport, PR8 5AB<br>

                        
                            Tel: 01704 533555<br>
                        
                        
                            Fax: 01704 884160<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:pughjo@parliament.uk">pughjo@parliament.uk</a>
                    ', N'www.johnpughmp.com', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3925, N'Meg Hillier MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35914.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hackney South and Shoreditch', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5325<br>
                        
                        
                            Fax: 020 7219 8768<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:meghilliermp@parliament.uk">meghilliermp@parliament.uk</a>
                    ', N'www.meghillier.com/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3926, N'Paul Maynard MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35499.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Blackpool North and Cleveleys', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7017<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:paul.maynard.mp@parliament.uk">paul.maynard.mp@parliament.uk</a>
', N'paul.maynard.mp@parliament.uk', N'
                        16 Queen Street, BLackpool, FY1 1PD<br>

                        
                            Tel: 01253 473070<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.paulmaynard.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3927, N'Mary Macleod MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62722.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Brentford and Isleworth', N'
                        c/o Brentford and Isleworth Conservatives, 433 Chiswick High Road, London, W4 4AU<br>

                        
                            Tel: 020 8994 1406<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7023<br>
                        
                        
                            Fax: 020 7219 4049<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mary.macleod.mp@parliament.uk">mary.macleod.mp@parliament.uk</a>
                    ', N'www.marymacleod.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3928, N'Bob Stewart MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83527.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Beckenham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7011<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bob.stewart.mp@parliament.uk">bob.stewart.mp@parliament.uk</a>
', N'bob.stewart.mp@parliament.uk', N'', N'www.bobstewartmp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3929, N'Simon Kirby MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62711.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Brighton, Kemptown', N'
                        370 South Coast Road, Telscombe Cliffs, East Sussex, BN10 7ES<br>

                        
                            Tel: 01273 589178<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:simon.kirby.mp@parliament.uk">simon.kirby.mp@parliament.uk</a>
', N'simon.kirby.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7024<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:simon.kirby.mp@parliament.uk">simon.kirby.mp@parliament.uk</a>
                    ', N'www.simonkirby.org', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3930, N'Caroline Lucas MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/28490.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Brighton, Pavilion', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7025<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:caroline.lucas.mp@parliament.uk">caroline.lucas.mp@parliament.uk</a>
', N'caroline.lucas.mp@parliament.uk', N'
                        Brighton Media Centre, 15-17 Middle Street, Brighton, BN1 1AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:brightonoffice@parliament.uk">brightonoffice@parliament.uk</a>
                    ', N'www.carolinelucas.com', 1304, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3931, N'Mark Hunter MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/42986.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cheadle', N'
                        Hillson House, 3 Gillbent Road, Cheadle Hulme, Cheadle, Stockport, SK8 7LE<br>

                        
                            Tel: 0161 486 1359<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:info@cheadle-libdems.org.uk">info@cheadle-libdems.org.uk</a>
', N'info@cheadle-libdems.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3889<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:hunterm@parliament.uk">hunterm@parliament.uk</a>
                    ', N'www.markhunter.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3932, N'Christopher Pincher MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35491.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Tamworth', N'
                        23 Albert Road, Tamworth, B79 7JS<br>

                        
                            Tel: 01827 312778<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7169<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:christopher.pincher.mp@parliament.uk">christopher.pincher.mp@parliament.uk</a>
                    ', N'www.christopherpincher.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3933, N'Charlotte Leslie MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62717.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bristol North West', N'
                        184 Henleaze Road, Bristol, BS9 4NE<br>

                        
                            Tel: 0117 962 9427<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7026<br>
                        
                        
                            Fax: 020 7219 0921<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:charlotte.leslie.mp@parliament.uk">charlotte.leslie.mp@parliament.uk</a>
                    ', N'charlotteleslie.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3934, N'George Eustice MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/77092.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Camborne and Redruth', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7032<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:george.eustice.mp@parliament.uk">george.eustice.mp@parliament.uk</a>
', N'george.eustice.mp@parliament.uk', N'
                        1 Trevenson Street, Camborne, TR14 8JD<br>

                        
                            Tel: 01209 713355<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:camborneredruthconservatives@googlemail.com">camborneredruthconservatives@googlemail.com</a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3935, N'Mel Stride MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62786.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Central Devon', N'
                        2A Manaton Court, Manaton Close, Matford Business Park, Exeter, EX2 8PF<br>

                        
                            Tel: 01392 823306<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mel.stride.mp@parliament.uk">mel.stride.mp@parliament.uk</a>
', N'mel.stride.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7037<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mel.stride.mp@parliament.uk">mel.stride.mp@parliament.uk</a>
                    ', N'melstridemp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3936, N'Andrew Griffiths MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/42358.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Burton', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7029<br>
                        
                        
                            Fax: 020 7219 0911<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andrew.griffiths.mp@parliament.uk">andrew.griffiths.mp@parliament.uk</a>
', N'andrew.griffiths.mp@parliament.uk', N'
                        Gothard House, 9 St Paul''s Square, Burton-upon-Trent, DE14 2EF<br>

                        
                            Tel: 01283 564934<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.andrewgriffithsmp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3937, N'Mr Aidan Burley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72840.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cannock Chase', N'
                        6 High Green Court, Newhall Street, Cannock, WS11 1GR<br>

                        
                            Tel: 01543 502447<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7034<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:aidan.burley.mp@parliament.uk">aidan.burley.mp@parliament.uk</a>
                    ', N'www.aidanburleymp.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3938, N'Rt Hon Sir Nicholas Soames MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25208.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mid Sussex', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4143<br>
                        
                        
                            Fax: 020 7219 2998<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nicholas.soames.mp@parliament.uk">nicholas.soames.mp@parliament.uk</a>
', N'nicholas.soames.mp@parliament.uk', N'
                        5 Hazelgrove Road, Haywards Heath, RH16 3PH<br>

                        
                            Tel: 01444 452590<br>
                        
                        
                            Fax: 01444 415766<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@msca.org.uk">info@msca.org.uk</a>
                    ', N'www.nicholassoames.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3939, N'John Penrose MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31711.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Weston-super-Mare', N'
                        24-26 Alexandra Parade, Weston-Super-Mare, BS23 1QX<br>

                        
                            Tel: 01934 613841<br>
                        
                        
                            Fax: 01934 632955<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:john@johnpenrose.org">john@johnpenrose.org</a>
', N'john@johnpenrose.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2385<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:beauperec@parliament.uk">beauperec@parliament.uk</a>
                    ', N'www.johnpenrose.org', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3940, N'Craig Whittaker MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62798.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Calder Valley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7031<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:craig.whittaker.mp@parliament.uk">craig.whittaker.mp@parliament.uk</a>
', N'craig.whittaker.mp@parliament.uk', N'
                        First Floor, Spring Villa, 16 Church Lane, Brighouse, HD6 1AT<br>

                        
                            Tel: 01484 711260<br>
                        
                        
                            Fax: 01484 718288<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@craigwhittakermp.co.uk">office@craigwhittakermp.co.uk</a>
                    ', N'www.craigwhittakermp.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3942, N'Andrew Stephenson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62783.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Pendle', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7222<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andrew.stephenson.mp@parliament.uk">andrew.stephenson.mp@parliament.uk</a>
', N'andrew.stephenson.mp@parliament.uk', N'
                        9 Cross Street, Nelson, BB9 7EN<br>

                        
                            Tel: 01282 614748<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@andrew4pendle', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3943, N'Jonathan Edwards MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84388.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Carmarthen East and Dinefwr', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jonathan.edwards.mp@parliament.uk">jonathan.edwards.mp@parliament.uk</a>
', N'jonathan.edwards.mp@parliament.uk', N'
                        37 Wind Street, Ammanford, SA18 3DN<br>

                        
                            Tel: 01269 597677<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jonathanedwards.org.uk/', 1309, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3944, N'Simon Hart MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/53927.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Carmarthen West and South Pembrokeshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7228<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:simon.hart.mp@parliament.uk">simon.hart.mp@parliament.uk</a>
', N'simon.hart.mp@parliament.uk', N'
                        15 St John Street, Whitland, SA34 0AN<br>

                        
                            Tel: 01994 342002<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.simon-hart.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3945, N'Dan Jarvis MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/91415.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Barnsley Central', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 0207 219 1082<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:dan.jarvis.mp@parliament.uk">dan.jarvis.mp@parliament.uk</a>
', N'dan.jarvis.mp@parliament.uk', N'
                        Corporate Mail Room, PO Box 634, Barnsley, S70 9GG<br>

                        
                            Tel: 01226 787893<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:officeofdan.jarvis.mp@parliament.uk">officeofdan.jarvis.mp@parliament.uk</a>
                    ', N'www.danjarvismp.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3946, N'Gordon Birtwistle MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/38423.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Burnley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7028<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gordon.birtwistle.mp@parliament.uk">gordon.birtwistle.mp@parliament.uk</a>
', N'gordon.birtwistle.mp@parliament.uk', N'
                        23 St James Row, Burnley, BB11 1EY<br>

                        
                            Tel: 01282 704430<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:gbirtwistlemp@gmail.com">gbirtwistlemp@gmail.com</a>
                    ', N'www.burnleylibdems.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3948, N'Rebecca Harris MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/73064.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Castle Point', N'
                        c/o Castle Point Conservatives, Bernard Braine House, 8 Green Road, Benfleet, SS7 5JT<br>

                        
                            Tel: 01268 792992<br>
                        
                        
                            Fax: 01268 792992<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@castlepointconservatives.com">office@castlepointconservatives.com</a>
', N'office@castlepointconservatives.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7206<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:rebecca.harris.mp@parliament.uk">rebecca.harris.mp@parliament.uk</a>
                    ', N'www.castlepointconservatives.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3949, N'Gregg McClymont MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72286.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cumbernauld, Kilsyth and Kirkintilloch East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7045<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gregg.mcclymont.mp@parliament.uk">gregg.mcclymont.mp@parliament.uk</a>
', N'gregg.mcclymont.mp@parliament.uk', N'
                        Lennox House, Lennox Road, Cumbernauld, Seafar, G67 1LL<br>

                        
                            Tel: 01236 457788<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.greggmcclymont.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3950, N'Tracey Crouch MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/54004.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chatham and Aylesford', N'
                        6-8 Revenge Road, Lordswood, Chatham, ME5 8UD<br>

                        
                            Tel: 01634 673180<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7203<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tracey.crouch.mp@parliament.uk">tracey.crouch.mp@parliament.uk</a>
                    ', N'@tracey_crouch', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3951, N'Duncan Hames MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31676.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chippenham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7039<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:duncan.hames.mp@parliament.uk">duncan.hames.mp@parliament.uk</a>
', N'duncan.hames.mp@parliament.uk', N'
                        Avonbridge House, Bath Road, Chippenham, SN15 2BB<br>

                        
                            Tel: 01249 454110<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:duncan@duncanhames.org.uk">duncan@duncanhames.org.uk</a>
                    ', N'www.duncanhames.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3952, N'Andrew Percy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40470.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Brigg and Goole', N'
                        81-83 Pasture Road, Goole, North Humberside, DN14 6BP<br>

                        
                            Tel: 01724 720800/01405 767969<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:brigg.goole@gmail.com">brigg.goole@gmail.com</a>
', N'brigg.goole@gmail.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:andrew.percy.mp@parliament.uk">andrew.percy.mp@parliament.uk</a>
                    ', N'www.andrewpercy.org', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3953, N'Jason McCartney MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62727.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Colne Valley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7041<br>
                        
                        
                            Fax: 020 7219 6474<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jason.mccartney.mp@parliament.uk">jason.mccartney.mp@parliament.uk</a>
', N'jason.mccartney.mp@parliament.uk', N'
                        Upperbridge House, 24 Huddersfield Road, Holmfirth, HD9 2JS<br>

                        
                            Tel: 01484 688364; 01484 688378<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'jasonmccartney.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3954, N'Stephen Mosley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/64332.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'City of Chester', N'
                        Unionist Buildings, Nicholas Street, Chester, CH1 2NX<br>

                        
                            Tel: 01244 458120<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen@chestermp.com">stephen@chestermp.com</a>
', N'stephen@chestermp.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7207<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stephen.mosley.mp@parliament.uk">stephen.mosley.mp@parliament.uk</a>
                    ', N'www.chestermp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3955, N'Gavin Barwell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72243.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Croydon Central', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2119<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gavin.barwell.mp@parliament.uk">gavin.barwell.mp@parliament.uk</a>
', N'gavin.barwell.mp@parliament.uk', N'
                        133 Wickham Road, Croydon, CR0 8TE<br>

                        
                            Tel: 020 8663 8741<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gavinbarwell.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3956, N'Susan Elan Jones MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83418.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Clwyd South', N'
                        Enterprise Centre, Well Street, Cefn Mawr, Wrexham, LL14 3YD<br>

                        
                            Tel: 01978 824288<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 0920<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:susan.jones.mp@parliament.uk">susan.jones.mp@parliament.uk</a>
                    ', N'www.susanelanjones.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3957, N'Martin Vickers MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40371.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cleethorpes', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7212<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:martin.vickers.mp@parliament.uk">martin.vickers.mp@parliament.uk</a>
', N'martin.vickers.mp@parliament.uk', N'
                        62 St Peter''s Avenue, Cleethorpes, DN35 8HP<br>

                        
                            Tel: 01472 603554<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.martinvickers.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3958, N'Fiona Bruce MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35510.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Congleton', N'
                        Riverside, Mountbatten Way, Congleton, Cheshire, CW12 1DY<br>

                        
                            Tel: 01260 274044<br>
                        
                        
                            Fax: 01260 278293<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7042<br>
                        
                        
                            Fax: 020 7219 1258<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:fiona.bruce.mp@parliament.uk">fiona.bruce.mp@parliament.uk</a>
                    ', N'fionabruce.mp/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3960, N'Henry Smith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35320.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Crawley', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7043<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:henry.smith.mp@parliament.uk">henry.smith.mp@parliament.uk</a>
', N'henry.smith.mp@parliament.uk', N'
                        01293 934554<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'henrysmith.info/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3962, N'Thomas Docherty MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/43149.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dunfermline and West Fife', N'
                        Unit 14, Dunfermline Business Centre, Izatt Avenue, Dunfermline, Fife, KY11 3BZ<br>

                        
                            Tel: 01383 626669<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:thomas.docherty.mp@parliament.uk">thomas.docherty.mp@parliament.uk</a>
                    ', N'@thomas_docherty', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3963, N'Mr Michael McCann MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83834.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Kilbride, Strathaven and Lesmahagow', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7058<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:michael.mccann.mp@parliament.uk">michael.mccann.mp@parliament.uk</a>
', N'michael.mccann.mp@parliament.uk', N'
                        Civic Centre, Andrew Street, East Kilbride, G74 1AB<br>

                        
                            Tel: 01355 239642<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.michaelmccann.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3964, N'Fiona O''Donnell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/85410.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Lothian', N'
                        65 High Street, Tranent, EH33 1LN<br>

                        
                            Tel: 01875 824779<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7059<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:fiona.odonnell.mp@parliament.uk">fiona.odonnell.mp@parliament.uk</a>
                    ', N'www.fionaodonnellmp.org/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3965, N'Sheila Gilmore MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/59373.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Edinburgh East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sheila.gilmore.mp@parliament.uk">sheila.gilmore.mp@parliament.uk</a>
', N'sheila.gilmore.mp@parliament.uk', N'
                        84 Niddrie Mains Road, Edinburgh, EH16 4DT<br>

                        
                            Tel: 0131 661 7522<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.sheilagilmore.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3966, N'Ian Murray MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/85111.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Edinburgh South', N'
                        31 Minto Street, Edinburgh, EH9 2BT<br>

                        
                            Tel: 0131 662 4520<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ian@ianmurraymp.co.uk">ian@ianmurraymp.co.uk</a>
', N'ian@ianmurraymp.co.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7064<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ian.murray.mp@parliament.uk">ian.murray.mp@parliament.uk</a>
                    ', N'www.ianmurraymp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3967, N'Mike Crockart MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35598.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Edinburgh West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7063<br>
                        
                        
                            Fax: 020 7219 2096<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mike.crockart.mp@parliament.uk">mike.crockart.mp@parliament.uk</a>
', N'mike.crockart.mp@parliament.uk', N'
                        185 St John’s Road, Edinburgh, EH12 7SL<br>

                        
                            Tel: 0131 339 0339<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.mikecrockartmp.com/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3968, N'Stephen Lloyd MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31663.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Eastbourne', N'
                        100 Seaside Road, Eastbourne, BN21 3PF<br>

                        
                            Tel: 01323 733030<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen@stephenlloyd.org.uk">stephen@stephenlloyd.org.uk</a>
', N'stephen@stephenlloyd.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7061<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stephen.lloyd.mp@parliament.uk">stephen.lloyd.mp@parliament.uk</a>
                    ', N'eastbournelibdems.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3969, N'Julie Hilling MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/78784.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bolton West', N'
                        The Old Surgery, 108 Market Street, Westhoughton, BL5 3AZ<br>

                        
                            Tel: 01942 813468<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7020<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:julie.hilling.mp@parliament.uk">julie.hilling.mp@parliament.uk</a>
                    ', N'www.juliehilling.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3970, N'Gareth Johnson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35325.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dartford', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7047<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gareth.johnson.mp@parliament.uk">gareth.johnson.mp@parliament.uk</a>
', N'gareth.johnson.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.garethjohnsonmp.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3971, N'Charlie Elphicke MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41646.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dover', N'
                        c/o Dover and Deal Conservative Association, 54 The Strand, Walmer, Deal, CT14 7DP<br>

                        
                            Tel: 01304 379669<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:charlie@elphicke.com">charlie@elphicke.com</a>
', N'charlie@elphicke.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7052<br>
                        
                        
                            Fax: 020 7219 6917<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:charlie.elphicke.mp@parliament.uk">charlie.elphicke.mp@parliament.uk</a>
                    ', N'www.elphicke.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3972, N'Sarah Champion MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/101347.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rotherham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7295<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sarah.champion.mp@parliament.uk">sarah.champion.mp@parliament.uk</a>
', N'sarah.champion.mp@parliament.uk', N'
                        Unit 35, Mooregate Crofts Business Centre, South Grove, Rotherham, S60 2DH<br>

                        
                            Tel: 01709 331035/331036<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.sarahchampionmp.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3973, N'Grahame M. Morris MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/64630.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Easington', N'
                        The Glebe Centre Annex, Durham Place, Murton, Seaham, County Durham, SR7 9BX<br>

                        
                            Tel: 0191 526 2828<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1283<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:grahame.morris.mp@parliament.uk">grahame.morris.mp@parliament.uk</a>
                    ', N'@grahamemorris', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3974, N'Toby Perkins MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/64625.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Chesterfield', N'
                        113 Saltergate, Chesterfield, S40 1NF<br>

                        
                            Tel: 01246 386286<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 2320<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:toby.perkins.mp@parliament.uk">toby.perkins.mp@parliament.uk</a>
                    ', N'www.tobyperkins.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3975, N'Chris Kelly MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/64308.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dudley South', N'
                        No constituency office publicised<br>

                        
                            Tel: 01384 211 041<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@chriskelly.mp">office@chriskelly.mp</a>
', N'office@chriskelly.mp', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7053<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@chriskelly.mp">office@chriskelly.mp</a>
                    ', N'chriskelly.mp', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3976, N'Chris Williamson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84368.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Derby North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7049<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:chris.williamson.mp@parliament.uk">chris.williamson.mp@parliament.uk</a>
', N'chris.williamson.mp@parliament.uk', N'
                        9a Theatre Walk, Westfield Centre, Derby, DE1 2NG<br>

                        
                            Tel: 01332 205126<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.chriswilliamson.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3977, N'Chris Heaton-Harris MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/28244.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Daventry', N'
                        78 St Georges Avenue, Northampton, NN2 6JF<br>

                        
                            Tel: 01604 859721<br>
                        
                        
                            Fax: 01604 859329<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7048<br>
                        
                        
                            Fax: 020 7219 1375<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:chris.heatonharris.mp@parliament.uk">chris.heatonharris.mp@parliament.uk</a>
                    ', N'www.heatonharris.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3978, N'Simon Reevell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/78807.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Dewsbury', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7210<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:simon.reevell.mp@parliament.uk">simon.reevell.mp@parliament.uk</a>
', N'simon.reevell.mp@parliament.uk', N'
                        5 Northgate, Dewsbury, WF13 1DS<br>

                        
                            Tel: 01924 465008<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.simonreevell.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3979, N'Angie Bray MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/47234.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ealing Central and Acton', N'
                        39 Broughton Road, London, W13 8QW<br>

                        
                            Tel: 020 8810 0579<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7055<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:angie.bray.mp@parliament.uk">angie.bray.mp@parliament.uk</a>
                    ', N'www.angiebray.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3980, N'Mr Sam Gyimah MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/53938.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'East Surrey', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3504<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sam@samgyimah.com">sam@samgyimah.com</a>
', N'sam@samgyimah.com', N'
                        c/o East Surrey Conservative Association, 2 Hoskins Road, Oxted, RH8 9HT<br>

                        
                            Tel: 01883 715782<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:sam@samgyimah.com">sam@samgyimah.com</a>
                    ', N'www.samgyimah.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3981, N'Anas Sarwar MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/58360.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Glasgow Central', N'
                        9 Scotland Street, Glasgow, G5 8NB<br>

                        
                            Tel: 0141 429 6027<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7076<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:anas.sarwar.mp@parliament.uk">anas.sarwar.mp@parliament.uk</a>
                    ', N'www.scottishlabour.org.uk/mp/anas_sarwar/277/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3982, N'Margaret Curran MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/32611.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Glasgow East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                            Fax: 020 7219 6656<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:margaret.curran.mp@parliament.uk">margaret.curran.mp@parliament.uk</a>
', N'margaret.curran.mp@parliament.uk', N'
                        Academy House, 1346 Shettleston Road, Glasgow, G32 9AT<br>

                        
                            Tel: 0141 778 8993<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:rachel.mcgee@parliament.uk">rachel.mcgee@parliament.uk</a>
                    ', N'margaretcurran.org/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3983, N'Amber Rudd MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41303.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hastings and Rye', N'
                        Creative Media Centre, 45 Robertson Street, Hastings, TN34 1HL<br>

                        
                            Tel: 01424 205435<br>
                        
                        
                            Fax: 01424 205401 <br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7229<br>
                        
                        
                            Fax: 020 7219 6545<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:amber.rudd.mp@parliament.uk">amber.rudd.mp@parliament.uk</a>
                    ', N'www.amberrudd.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3984, N'Mike Weatherley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31588.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hove', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7216<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mike.weatherley.mp@parliament.uk">mike.weatherley.mp@parliament.uk</a>
', N'mike.weatherley.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.mikeweatherleymp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3985, N'Robert Halfon MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31730.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Harlow', N'
                        Harlow Enterprise Hub, Kao-Hockham Building, Edinburgh Way, Harlow, CM20 2NQ<br>

                        
                            Tel: 01279 311451<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:halfon4harlow@roberthalfon.com">halfon4harlow@roberthalfon.com</a>
', N'halfon4harlow@roberthalfon.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7223<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.roberthalfon.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3986, N'Damian Collins MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35404.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Folkestone and Hythe', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7072<br>
                        
                        
                            Fax: 020 7219 2213<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:damian.collins.mp@parliament.uk">damian.collins.mp@parliament.uk</a>
', N'damian.collins.mp@parliament.uk', N'
                        4 West Cliff Gardens, Folkstone, CT20 1SP<br>

                        
                            Tel: 01303 253524<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.damiancollins.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3987, N'Jenny Chapman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83499.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Darlington', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 0207 219 7046<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jenny.chapman.mp@parliament.uk">jenny.chapman.mp@parliament.uk</a>
', N'jenny.chapman.mp@parliament.uk', N'
                        40a Coniscliffe Road, Darlington, DL3 7RG<br>

                        
                            Tel: 01325 382345<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@jennychapman', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3988, N'Ben Gummer MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62691.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Ipswich', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7090<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ben.gummer.mp@parliament.uk">ben.gummer.mp@parliament.uk</a>
', N'ben.gummer.mp@parliament.uk', N'
                        9 Fore Street, Ipswich, Suffolk, IP4 1JW<br>

                        
                            Tel: 01473 232883<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ben@bengummer.com">ben@bengummer.com</a>
                    ', N'www.bengummer.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3989, N'Jack Lopresti MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41628.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Filton and Bradley Stoke', N'
                        Unit 2B, First Floor East, The Willowbrook Centre, Savages Wood Road, Bradley Stoke, BS32 8BS<br>

                        
                            Tel: 01454 617 783<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7070<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jack.lopresti.mp@parliament.uk">jack.lopresti.mp@parliament.uk</a>
                    ', N'www.jacklopresti.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3990, N'Richard Graham MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62688.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Gloucester', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7077<br>
                        
                        
                            Fax: 020 7219 2299<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:richard.graham.mp@parliament.uk">richard.graham.mp@parliament.uk</a>
', N'richard.graham.mp@parliament.uk', N'
                        2nd Floor, St Peter''s House, 2 College Street, Gloucester, GL1 2NE<br>

                        
                            Tel: 01452 501167<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.richardgraham.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3991, N'Jesse Norman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/51624.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hereford and South Herefordshire', N'
                        Suite 3, Penn House, Broad Street, Hereford, HR4 9AP<br>

                        
                            Tel: 01432 276422<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7084<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jesse.norman.mp@parliament.uk">jesse.norman.mp@parliament.uk</a>
                    ', N'www.jessenorman.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3992, N'James Morris MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/74640.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Halesowen and Rowley Regis', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8715<br>
                        
                        
                            Fax: 020 7219 1429<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:james.morris.mp@parliament.uk">james.morris.mp@parliament.uk</a>
', N'james.morris.mp@parliament.uk', N'
                        Trinity Point, New Road, Halesowen, B63 3HY<br>

                        
                            Tel: 0121 550 6777<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:lisa.townsend@parliament.uk">lisa.townsend@parliament.uk</a>
                    ', N'www.jamesmorrismp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3993, N'Jessica Lee MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35262.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Erewash', N'
                        73 Derby Road, Westgate, Long Eaton, Nottingham, NG10 1LU<br>

                        
                            Tel: 0115 972 2419<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7067<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jessica.lee.mp@parliament.uk">jessica.lee.mp@parliament.uk</a>
                    ', N'www.jessicaleemp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3994, N'Andrew Bingham MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35370.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'High Peak', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8479<br>
                        
                        
                            Fax: 020 7219 2413<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andrew.bingham.mp@parliament.uk">andrew.bingham.mp@parliament.uk</a>
', N'andrew.bingham.mp@parliament.uk', N'
                        20 Broadwalk, Buxton, Derbyshire, SK17 6JR<br>

                        
                            Tel: 01298 26698 <br>
                        
                        
                            Fax: 01298 27112<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.andrewbingham.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3995, N'Nick Boles MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35373.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Grantham and Stamford', N'
                        G&amp;S Constituency Office, Suite 3, The George Centre, Grantham, Lincolnshire, NG31 6LH<br>

                        
                            Tel: 01476 978121<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nick.boles.mp@parliament.uk">nick.boles.mp@parliament.uk</a>
', N'nick.boles.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7079<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nick.boles.mp@parliament.uk">nick.boles.mp@parliament.uk</a>
                    ', N'www.gov.uk/government/organisations/department-for-communities-and-local-government', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3996, N'Andrew Jones MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41851.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Harrogate and Knaresborough', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andrew.jones.mp@parliament.uk">andrew.jones.mp@parliament.uk</a>
', N'andrew.jones.mp@parliament.uk', N'
                        57 East Parade, Harrogate, North Yorkshire, HG1 5LQ<br>

                        
                            Tel: 01423 529614<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.andrewjonesmp.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3997, N'Alec Shelbrooke MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35500.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Elmet and Rothwell', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alec.shelbrooke.mp@parliament.uk">alec.shelbrooke.mp@parliament.uk</a>
', N'alec.shelbrooke.mp@parliament.uk', N'
                        Tel: 01937 589 002<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.alecshelbrooke.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3998, N'Mark Menzies MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31587.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Fylde', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7073<br>
                        
                        
                            Fax: 020 7219 2235<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mark.menzies.mp@parliament.uk">mark.menzies.mp@parliament.uk</a>
', N'mark.menzies.mp@parliament.uk', N'
                        Office of Mark Menzies MP, 28 Orchard Road, Lytham St Annes, FY8 1PF<br>

                        
                            Tel: 01253 729846<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.conservatives.com/people/members_of_parliament/menzies_mark.aspx', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (3999, N'Graham Jones MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83620.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hyndburn', N'
                        50 Abbey Street, Accrington, Lancashire, BB5 1EE<br>

                        
                            Tel: 01254 382283<br>
                        
                        
                            Fax: 01254 398089<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7089<br>
                        
                        
                            Fax: 020 7219 2492<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:graham.jones.mp@parliament.uk">graham.jones.mp@parliament.uk</a>
                    ', N'hhgrahamjones.blogspot.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4000, N'Ian Mearns MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84679.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Gateshead', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7074<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ian.mearns.mp@parliament.uk">ian.mearns.mp@parliament.uk</a>
', N'ian.mearns.mp@parliament.uk', N'
                        12 Regent Terrace, Gateshead, NE8 1LU<br>

                        
                            Tel: 0191 477 0651<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ian.mearns.mp@parliament.uk">ian.mearns.mp@parliament.uk</a>
                    ', N'www.ianmearns.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4002, N'Nick de Bois MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35347.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Enfield North', N'
                        605 Hertford Road, Enfield, EN3 6UP<br>

                        
                            Tel: 01992 678255<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7066<br>
                        
                        
                            Fax: 020 7219 2153<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nick.debois.mp@parliament.uk">nick.debois.mp@parliament.uk</a>
                    ', N'www.nickdebois.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4003, N'Teresa Pearce MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/79865.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Erith and Thamesmead', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6936<br>
                        
                        
                            Fax: 020 7219 2190<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:teresa.pearce.mp@parliament.uk">teresa.pearce.mp@parliament.uk</a>
', N'teresa.pearce.mp@parliament.uk', N'
                        315 Bexley Road, Erith, DA8 3EX<br>

                        
                            Tel: 01322 342991<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.teresapearce.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4004, N'Mike Freer MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35366.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Finchley and Golders Green', N'
                        Finchley and Golders Green Conservatives, 212 Ballards Lane, Finchley, London, N3 2LX<br>

                        
                            Tel: 020 8445 4292<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mike.freer.mp@parliament.uk">mike.freer.mp@parliament.uk</a>
', N'mike.freer.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7071<br>
                        
                        
                            Fax: 020 7219 2211<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mike.freer.mp@parliament.uk">mike.freer.mp@parliament.uk</a>
                    ', N'@mikefreermp', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4005, N'Bob Blackman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/37675.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Harrow East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7082<br>
                        
                        
                            Fax: 020 7219 2336<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bob.blackman.mp@parliament.uk">bob.blackman.mp@parliament.uk</a>
', N'bob.blackman.mp@parliament.uk', N'
                        209 Headstone Lane, Harrow, Middlesex, HA2 6ND<br>

                        
                            Tel: 0208 421 3323<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.bobblackmanmp.com/', 1302, 1)
GO
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4006, N'Dr Matthew Offord MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/42618.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hendon', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7083<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:matthew.offord.mp@parliament.uk">matthew.offord.mp@parliament.uk</a>
', N'matthew.offord.mp@parliament.uk', N'
                        Churchill House, 120 Bunns Lane, Mill Hill, London, NW7 2AS<br>

                        
                            Tel: 020 3114 2131<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.matthewofford.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4007, N'Yasmin Qureshi MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35869.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bolton South East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7019<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:yasmin.qureshi.mp@parliament.uk">yasmin.qureshi.mp@parliament.uk</a>
', N'yasmin.qureshi.mp@parliament.uk', N'
                        60 St Georges Road, Bolton, BL1 2DD<br>

                        
                            Tel: 01204 371202<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.yasminqureshi.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4008, N'Caroline Dinenage MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35430.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Gosport', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7078; 020 7219 0198<br>
                        
                        
                            Fax: 020 7219 6874<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:caroline.dinenage.mp@parliament.uk">caroline.dinenage.mp@parliament.uk</a>
', N'caroline.dinenage.mp@parliament.uk', N'
                        167 Stoke Road, Gosport, PO12 1SE<br>

                        
                            Tel: 023 9252 2121<br>
                        
                        
                            Fax: 023 9252 0900<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'caroline4gosport.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4009, N'Mrs Emma Lewell-Buck MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/104265.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Shields', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4468<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:emma.lewell-buck.mp@parliament.uk">emma.lewell-buck.mp@parliament.uk</a>
', N'emma.lewell-buck.mp@parliament.uk', N'
                        Ede House, 143 Westoe Road, South Shields, NE33 3PD<br>

                        
                            Tel: 0191 427 1240<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@emmalewellbuck', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4011, N'Mrs Siân C. James MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35773.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Swansea East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6954; 020 7219 8223<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jamessc@parliament.uk">jamessc@parliament.uk</a>
', N'jamessc@parliament.uk', N'
                        485 Llangyfelach Road, Brynhyfryd, Swansea, SA5 9EA<br>

                        
                            Tel: 01792 455089<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:harriscat@parliament.uk">harriscat@parliament.uk</a>
                    ', N'', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4012, N'Graeme Morrice MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35070.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Livingston', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:graeme.morrice.mp@parliament.uk">graeme.morrice.mp@parliament.uk</a>
', N'graeme.morrice.mp@parliament.uk', N'
                        Geddes House, Kirkton North Road, Livingston, EH54 6GU<br>

                        
                            Tel: 01506 410109<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.graememorricemp.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4013, N'Gavin Shuker MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83624.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Luton South', N'
                        c/o Luton Labour Party, 3 Union Street, Luton, LU1 3AN<br>

                        
                            Tel: 01582 457774<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:office@gavinshuker.org">office@gavinshuker.org</a>
', N'office@gavinshuker.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1130<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:gavin.shuker.mp@parliament.uk">gavin.shuker.mp@parliament.uk</a>
                    ', N'www.gavinshuker.org', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4014, N'Alok Sharma MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62774.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Reading West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7131<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alok.sharma.mp@parliament.uk">alok.sharma.mp@parliament.uk</a>
', N'alok.sharma.mp@parliament.uk', N'
                        16C Upton Road, Tilehurst, Reading, RG30 4BJ<br>

                        
                            Tel: 0118 941 3803<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.aloksharma.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4015, N'Iain Stewart MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31728.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Milton Keynes South', N'
                        Suite 102, Milton Keynes Business Centre, Foxhunter Drive, Linford Wood, MK14 6GD<br>

                        
                            Tel: 01908 686830<br>
                        
                        
                            Fax: 01908 686831<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:iain.stewart.mp@parliament.uk">iain.stewart.mp@parliament.uk</a>
', N'iain.stewart.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7230<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:iain.stewart.mp@parliament.uk">iain.stewart.mp@parliament.uk</a>
                    ', N'ias4mks.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4016, N'Kate Hoey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25608.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Vauxhall', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5989<br>
                        
                        
                            Fax: 020 7219 5985<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hoeyk@parliament.uk">hoeyk@parliament.uk</a>
', N'hoeyk@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.katehoey.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4017, N'Penny Mordaunt MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35429.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Portsmouth North', N'
                        Portsmouth North Conservatives, 379 London Road, Portsmouth, PO2 9LD<br>

                        
                            Tel: 023 9269 7266<br>
                        
                        
                            Fax: 023 9269 9578<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:info@pennymordaunt.com">info@pennymordaunt.com</a>
', N'info@pennymordaunt.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7129<br>
                        
                        
                            Fax: 020 7219 3592<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:penny.mordaunt.mp@parliament.uk">penny.mordaunt.mp@parliament.uk</a>
                    ', N'www.pennymordaunt.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4018, N'Mrs Helen Grant MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72264.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Maidstone and The Weald', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7107<br>
                        
                        
                            Fax: 020 7219 2806<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:helen.grant.mp@parliament.uk">helen.grant.mp@parliament.uk</a>
', N'helen.grant.mp@parliament.uk', N'
                        Maidstone and The Weald Conservative Association, 3 Albion Place, Maidstone, ME14 5DY<br>

                        
                            Tel: 01622 769898<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:helen.grant.mp@parliament.uk">helen.grant.mp@parliament.uk</a>
                    ', N'www.helengrant.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4019, N'Nicola Blackwood MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62819.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Oxford West and Abingdon', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7126<br>
                        
                        
                            Fax: 020 7219 8122<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nicola.blackwood.mp@parliament.uk">nicola.blackwood.mp@parliament.uk</a>
                    ', N'www.nicolablackwood.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4020, N'George Freeman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35463.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mid Norfolk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1940<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:george.freeman.mp@parliament.uk">george.freeman.mp@parliament.uk</a>
', N'george.freeman.mp@parliament.uk', N'
                        8 Damgate Street, Wymondham, Norfolk, NR18 0BQ<br>

                        
                            Tel: 01953 600617<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:george@georgefreeman.co.uk">george@georgefreeman.co.uk</a>
                    ', N'www.georgefreeman.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4021, N'Chris Skidmore MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62314.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kingswood', N'
                        47 High street, Kingswood, Bristol, BS15 4AA<br>

                        
                            Tel: 0117 908 1524<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7094<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:chris.skidmore.mp@parliament.uk">chris.skidmore.mp@parliament.uk</a>
                    ', N'www.chrisskidmore.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4022, N'Oliver Colvile MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31698.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Plymouth, Sutton and Devonport', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7219<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:oliver.colvile.mp@parliament.uk">oliver.colvile.mp@parliament.uk</a>
', N'oliver.colvile.mp@parliament.uk', N'
                        202 Exeter Street, Plymouth, PL4 0NH<br>

                        
                            Tel: 01752 600108<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.olivercolvile.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4023, N'Karen Lumley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31724.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Redditch', N'
                        Grosvenor House, Prospect Hill, Redditch, B97 4DL<br>

                        
                            Tel: 01527 591334<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:karen@tellkaren.com">karen@tellkaren.com</a>
', N'karen@tellkaren.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7133<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:karen.lumley.mp@parliament.uk">karen.lumley.mp@parliament.uk</a>
                    ', N'tellkaren.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4024, N'Mr Marcus Jones MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/76806.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Nuneaton', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7123<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:marcus.jones.mp@parliament.uk">marcus.jones.mp@parliament.uk</a>
', N'marcus.jones.mp@parliament.uk', N'
                        13-17 Hollybush House, Bondgate, Nuneaton, Warwickshire, CV11 4AR<br>

                        
                            Tel: 024 7634 8482<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.marcusjones.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4025, N'Pauline Latham MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41721.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mid Derbyshire', N'
                        The Old Station, Station Road, Spondon, Derby, DE21 7NE<br>

                        
                            Tel: 01332 676679<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7110<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:pauline.latham.mp@parliament.uk">pauline.latham.mp@parliament.uk</a>
                    ', N'www.paulinelatham.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4026, N'Liz Kendall MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/36386.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leicester West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:liz.kendall.mp@parliament.uk">liz.kendall.mp@parliament.uk</a>
', N'liz.kendall.mp@parliament.uk', N'
                        42 Narborough Road, Leicester, LE3 0BQ<br>

                        
                            Tel: 0116 204 4980<br>
                        
                        
                            Fax: 0116 204 4989<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.lizkendall.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4027, N'Rt Hon Nicky Morgan MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31696.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Loughborough', N'
                        3/3a Nottingham Road, Loughborough, LE11 1ER<br>

                        
                            Tel: 01509 262723<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7224<br>
                        
                        
                            Fax: 020 7219 6414<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nicky.morgan.mp@parliament.uk">nicky.morgan.mp@parliament.uk</a>
                    ', N'www.nickymorgan.org', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4028, N'Karl McCartney MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35387.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Lincoln', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7221<br>
                        
                        
                            Fax: 020 7219 1964<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:karl.mccartney.mp@parliament.uk">karl.mccartney.mp@parliament.uk</a>
', N'karl.mccartney.mp@parliament.uk', N'
                        c/o Lincoln Conservatives, 1a Farrier Road, Lincoln, LN6 3RU<br>

                        
                            Tel: 01522 687261<br>
                        
                        
                            Fax: 01522 687261<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@lincolnconservatives.co.uk">info@lincolnconservatives.co.uk</a>
                    ', N'www.karlmccartney.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4029, N'Lilian Greenwood MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72265.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Nottingham South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7122<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:lilian.greenwood.mp@parliament.uk">lilian.greenwood.mp@parliament.uk</a>
', N'lilian.greenwood.mp@parliament.uk', N'
                        1st Floor, 12 Regent Street, Nottingham, NG1 5BQ<br>

                        
                            Tel: 0115 711 7000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.liliangreenwood.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4030, N'Karl Turner MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72313.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kingston upon Hull East', N'
                        430 Holderness Road, Hull, HU9 3DW<br>

                        
                            Tel: 01482 781019<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7088<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:karl.turner.mp@parliament.uk">karl.turner.mp@parliament.uk</a>
                    ', N'www.karlturnermp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4031, N'Rachel Reeves MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35872.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leeds West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7097<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:rachel.reeves.mp@parliament.uk">rachel.reeves.mp@parliament.uk</a>
', N'rachel.reeves.mp@parliament.uk', N'
                        Unit 10, Armley Park Court, Stanningley Road, Leeds, LS12 2AE<br>

                        
                            Tel: 0113 263 0411<br>
                        
                        
                            Fax: 0113 263 0411<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:rachel.reeves.mp@parliament.uk">rachel.reeves.mp@parliament.uk</a>
                    ', N'www.rachelreeves.net', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4032, N'Stuart Andrew MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62806.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Pudsey', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7130<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stuart.andrew.mp@parliament.uk">stuart.andrew.mp@parliament.uk</a>
', N'stuart.andrew.mp@parliament.uk', N'
                        The Shaw Rooms, 98 Thornhill Street, Calverley, Leeds, LS28 5PD<br>

                        
                            Tel: 0113 204 7954<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@stuartandrewmp', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4033, N'David Rutley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83409.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Macclesfield', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7106<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david.rutley.mp@parliament.uk">david.rutley.mp@parliament.uk</a>
', N'david.rutley.mp@parliament.uk', N'
                        Macclesfield Conservative Association, West Bank Road, Macclesfield, SK10 3BT<br>

                        
                            Tel: 01625 422848<br>
                        
                        
                            Fax: 01625 617066<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.davidrutley.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4034, N'Yvonne Fovargue MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84359.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Makerfield', N'
                        Wigan Investment Centre, Waterside Drive, Off Swan Meadow Rd, Wigan, WN3 5BA<br>

                        
                            Tel: 01942 824029<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:yvonne.fovargue.mp@parliament.uk">yvonne.fovargue.mp@parliament.uk</a>
                    ', N'@y_fovarguemp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4035, N'Steve Rotheram MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/85412.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Liverpool, Walton', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7101<br>
                        
                        
                            Fax: 020 7219 2739<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:steve.rotheram.mp@parliament.uk">steve.rotheram.mp@parliament.uk</a>
', N'steve.rotheram.mp@parliament.uk', N'
                        330 Rice Lane, Walton, Liverpool, L9 2BL<br>

                        
                            Tel: 0151 525 5025<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:gary.booth@parliament.uk">gary.booth@parliament.uk</a>
                    ', N'steverotherammp.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4036, N'Luciana Berger MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83726.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Liverpool, Wavertree', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7102<br>
                        
                        
                            Fax: 020 7219 2770<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:luciana.berger.mp@parliament.uk">luciana.berger.mp@parliament.uk</a>
', N'luciana.berger.mp@parliament.uk', N'
                        UCATT Building, 56 Derwent Road East, Liverpool, L13 6QR<br>

                        
                            Tel: 0151 228 1628<br>
                        
                        
                            Fax: 0151 228 2519<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:luciana4wavertree@hotmail.co.uk">luciana4wavertree@hotmail.co.uk</a>
                    ', N'www.lucianaberger.com/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4037, N'Tom Blenkinsop MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/85090.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Middlesbrough South and East Cleveland', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        Harry Tout House, 8 Wilson Street, Guisborough, TS14 6NA<br>

                        
                            Tel: 01287 610878<br>
                        
                        
                            Fax: 01287 631894<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@tomblenkinsop.com">info@tomblenkinsop.com</a>
                    ', N'www.tomblenkinsop.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4038, N'Rt Hon Douglas Alexander MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25705.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Paisley and Renfrewshire South', N'
                        2014 Mile End Mill, Abbey Mill Business Centre, Paisley, PA1 1JS<br>

                        
                            Tel: 0141 561 0333<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mp@douglasalexander.org.uk">mp@douglasalexander.org.uk</a>
', N'mp@douglasalexander.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1345<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:alexanderd@parliament.uk">alexanderd@parliament.uk</a>
                    ', N'www.douglasalexander.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4039, N'Joseph Johnson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83602.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Orpington', N'
                        Orpington Conservative Association, 6 Sevenoaks Road, Orpington, BR6 9JJ<br>

                        
                            Tel: 01689 820347<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7125<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jo.johnson.mp@parliament.uk">jo.johnson.mp@parliament.uk</a>
                    ', N'www.jo-johnson.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4040, N'Chris Evans MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84900.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Islwyn', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7091<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:chris.evans.mp@parliament.uk">chris.evans.mp@parliament.uk</a>
', N'chris.evans.mp@parliament.uk', N'
                        6 Woodfieldside Business Park, Penmaen Road, Pontllanfraith, Blackwood, NP12 2DG<br>

                        
                            Tel: 01495 231990<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.chrisevansmp.co.uk/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4041, N'Glyn Davies MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33513.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Montgomeryshire', N'
                        20 High Street, Welshpool, Powys, SY21 7JP<br>

                        
                            Tel: 01938 552315/01938 554037<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:pamela.williams@parliament.uk;%20turnerjaa@parliament.uk">pamela.williams@parliament.uk; turnerjaa@parliament.uk</a>
', N'pamela.williams@parliament.uk; turnerjaa@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7112<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:phillip.carlick@parliament.uk">phillip.carlick@parliament.uk</a>
                    ', N'www.glyn-davies.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4042, N'Nick Smith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72304.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Blaenau Gwent', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7018<br>
                        
                        
                            Fax: 020 7219 0565<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nick.smith.mp@parliament.uk">nick.smith.mp@parliament.uk</a>
', N'nick.smith.mp@parliament.uk', N'
                        23 Beaufort Street, Brynmawr, NP23 4AQ<br>

                        
                            Tel: 01495 313232<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'nicksmithmp.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4043, N'Kelvin Hopkins MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25583.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Luton North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6670<br>
                        
                        
                            Fax: 020 7219 0957<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hopkinsk@parliament.uk">hopkinsk@parliament.uk</a>
', N'hopkinsk@parliament.uk', N'
                        3 Union Street, Luton, LU1 3AN<br>

                        
                            Tel: 01582 488208<br>
                        
                        
                            Fax: 01582 480990<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.kelvinhopkinsmp.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4044, N'Rt Hon Sir John Stanley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25325.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Tonbridge and Malling', N'
                        No constituency office publicised.<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5977<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:john.stanley.mp@parliament.uk">john.stanley.mp@parliament.uk</a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4045, N'Ian Swales MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/38504.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Redcar', N'
                        Room 103, The Innovation Centre, Vienna Court, Kirkleatham Business Park, Redcar, TS10 5SH<br>

                        
                            Tel: 01642 777940<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ian@ianswales.com">ian@ianswales.com</a>
', N'ian@ianswales.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7132<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ian.swales.mp@parliament.uk">ian.swales.mp@parliament.uk</a>
                    ', N'www.ianswales.com', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4046, N'Stephen Phillips QC MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83523.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sleaford and North Hykeham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7146; 020 7219 6487<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen.phillips.mp@parliament.uk">stephen.phillips.mp@parliament.uk</a>
', N'stephen.phillips.mp@parliament.uk', N'
                        Sleaford and North Hykham Conservatives, 6 Market Place, Sleaford, NG34 7SD<br>

                        
                            Tel: 01529 419000<br>
                        
                        
                            Fax: 01529 419019<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:admin@snhca.co.uk">admin@snhca.co.uk</a>
                    ', N'www.stephenphillips.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4047, N'Tom Greatrex MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/60543.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rutherglen and Hamilton West', N'
                        Blantyre Miners'' Community Centre, 3 Calder Street  , Blantyre, G72 0AU<br>

                        
                            Tel: 01698 821380<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8974<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tom.greatrex.mp@parliament.uk">tom.greatrex.mp@parliament.uk</a>
                    ', N'www.tomgreatrex.org/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4048, N'Caroline Nokes MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31721.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Romsey and Southampton North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7218<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:caroline.nokes.mp@parliament.uk">caroline.nokes.mp@parliament.uk</a>
', N'caroline.nokes.mp@parliament.uk', N'
                        Room 4, 13 Market Place, Romsey, SO51 8NA<br>

                        
                            Tel: 01794 521155<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.carolinenokes.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4049, N'Rt Hon Nick Raynsford', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25344.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Greenwich and Woolwich', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5895<br>
                        
                        
                            Fax: 020 7219 2619<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nick.raynsford.mp@parliament.uk">nick.raynsford.mp@parliament.uk</a>
', N'nick.raynsford.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.nickraynsford.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4050, N'Gordon Henderson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31697.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sittingbourne and Sheppey', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7144<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gordon.henderson.mp@parliament.uk">gordon.henderson.mp@parliament.uk</a>
', N'gordon.henderson.mp@parliament.uk', N'
                        Top Floor, Unit 10, Periwinkle Court Business Centre, Sittingbourne, Kent, ME10 2JZ<br>

                        
                            Tel: 01795 423199<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gordonhendersonmp.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4051, N'John Glen MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/43043.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Salisbury', N'
                        The Morrison Hall, 12 Brown Street, Salisbury, SP1 1HE<br>

                        
                            Tel: 01722 323050<br>
                        
                        
                            Fax: 01722 327080<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7138<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:john.glen.mp@parliament.uk">john.glen.mp@parliament.uk</a>
                    ', N'www.johnglenmp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4052, N'Mark Pawsey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35411.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rugby', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7136<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mark.pawsey.mp@parliament.uk">mark.pawsey.mp@parliament.uk</a>
', N'mark.pawsey.mp@parliament.uk', N'
                        Albert Buildings, 2 Castle Mews, Rugby, CV21 2XL<br>

                        
                            Tel: 01788 579499<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.markpawsey.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4053, N'Heather Wheeler MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35319.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Derbyshire', N'
                        SDCA, The Nissen Hut, Church Street, Swadlincote, DE11 8LF<br>

                        
                            Tel: 01283 225365<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1184<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:heather.wheeler.mp@parliament.uk">heather.wheeler.mp@parliament.uk</a>
                    ', N'www.heatherwheeler.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4054, N'Claire Perry MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83522.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Devizes', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7050<br>
                        
                        
                            Fax: 0207 219 1385<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:claire.perry.mp@parliament.uk">claire.perry.mp@parliament.uk</a>
', N'claire.perry.mp@parliament.uk', N'
                        Renelec House, 46 New Park Street, Devizes, Wiltshire, SN10 1DT<br>

                        
                            Tel: 01380 729358<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@devizesconservatives.org.uk">office@devizesconservatives.org.uk</a>
                    ', N'www.claireperry.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4055, N'Rt Hon Caroline Spelman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25318.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Meriden', N'
                        631 Warwick Road, Solihull, B91 1AR<br>

                        
                            Tel: 0121 711 7029<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:caroline@carolinespelman.com">caroline@carolinespelman.com</a>
', N'caroline@carolinespelman.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4189<br>
                        
                        
                            Fax: 020 7219 0378<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:spelmanc@parliament.uk">spelmanc@parliament.uk</a>
                    ', N'www.carolinespelman.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4056, N'Nic Dakin MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83408.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Scunthorpe', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7139<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nic.dakin.mp@parliament.uk">nic.dakin.mp@parliament.uk</a>
', N'nic.dakin.mp@parliament.uk', N'
                        18A Ethel Court, Scunthorpe, DN15 6RP<br>

                        
                            Tel: 01724 842000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.nicdakin.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4057, N'Rushanara Ali MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/58561.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bethnal Green and Bow', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7200<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:rushanara.ali.mp@parliament.uk">rushanara.ali.mp@parliament.uk</a>
', N'rushanara.ali.mp@parliament.uk', N'', N'www.rushanaraali.org/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4058, N'Paul Blomfield MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72247.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sheffield Central', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7142<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:paul.blomfield.mp@parliament.uk">paul.blomfield.mp@parliament.uk</a>
', N'paul.blomfield.mp@parliament.uk', N'
                        Unit 4, Edmund Road Business Centre, 135 Edmund Road, Sheffield, S2 4ED<br>

                        
                            Tel: 0114 272 2882  Fax: 0114 272 2442<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.paulblomfield.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4059, N'Simon Danczuk MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/64653.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rochdale', N'
                        26 St Mary''s Gate, Rochdale, OL16 1DZ<br>

                        
                            Tel: 01706 750135<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:simon.danczuk.mp@parliament.uk">simon.danczuk.mp@parliament.uk</a>
                    ', N'www.simondanczuk.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4060, N'Jake Berry MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62817.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Rossendale and Darwen', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7214<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jake.berry.mp@parliament.uk">jake.berry.mp@parliament.uk</a>
', N'jake.berry.mp@parliament.uk', N'
                        4 Mount Terrace, Rawtenstall, Rossendale, BB4 8SF<br>

                        
                            Tel: 01706 215547<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'jakeberry.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4061, N'Bill Esterson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83727.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sefton Central', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4403<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bill.esterson.mp@parliament.uk">bill.esterson.mp@parliament.uk</a>
', N'bill.esterson.mp@parliament.uk', N'
                        29 Liverpool Road North, Maghull, L31 2HB<br>

                        
                            Tel: 0151 531 8433<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.billesterson.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4062, N'Zac Goldsmith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62682.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Richmond Park', N'
                        372 Upper Richmond Road West, London, SW14 7JU<br>

                        
                            Tel: 020 8939 0321<br>
                        
                        
                            Fax: 020 8939 0331<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:zac@zacgoldsmith.com">zac@zacgoldsmith.com</a>
', N'zac@zacgoldsmith.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.zacgoldsmith.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4063, N'Gemma Doyle MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/76856.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Dunbartonshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gemma.doyle.mp@parliament.uk">gemma.doyle.mp@parliament.uk</a>
', N'gemma.doyle.mp@parliament.uk', N'
                        11 Castle Street, Dumbarton, G82 1QS<br>

                        
                            Tel: 01389 734214<br>
                        
                        
                            Fax: 01389 761498<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@gemmadoyle.org.uk">info@gemmadoyle.org.uk</a>
                    ', N'www.gemmadoyle.org.uk/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4064, N'Steve Baker MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83404.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wycombe', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3547, 020 7219 5099<br>
                        
                        
                            Fax: 020 7219 4614<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:steve.baker.mp@parliament.uk">steve.baker.mp@parliament.uk</a>
', N'steve.baker.mp@parliament.uk', N'
                        c/o Wycombe Conservative Association, 150A West Wycombe Road, High Wycombe, HP12 3AE<br>

                        
                            Tel: 01494 448408<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:steve@stevebaker.info">steve@stevebaker.info</a>
                    ', N'www.stevebaker.info', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4065, N'Jackie Doyle-Price MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41326.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Thurrock', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7171<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jackie.doyleprice.mp@parliament.uk">jackie.doyleprice.mp@parliament.uk</a>
', N'jackie.doyleprice.mp@parliament.uk', N'
                        2 Orsett Business Centre, Stanford Road, Grays, Essex, RM16 3BX<br>

                        
                            Tel: 01375 802029<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jackiedoyleprice.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4066, N'Priti Patel MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/37703.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Witham', N'
                        Witham Conservative Office, Avenue Lodge, The Avenue, Witham, Essex, CM8 2DL<br>

                        
                            Tel: 01376 520649<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3528<br>
                        
                        
                            Fax: 020 7219 5192<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:priti.patel.mp@parliament.uk">priti.patel.mp@parliament.uk</a>
                    ', N'www.priti4witham.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4067, N'Steve Brine MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62824.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Winchester', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7189<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:steve.brine.mp@parliament.uk">steve.brine.mp@parliament.uk</a>
', N'steve.brine.mp@parliament.uk', N'
                        9 Stockbridge Road, Winchester, SO22 6RN<br>

                        
                            Tel: 01962 791110<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:steve.brine.mp@parliament.uk">steve.brine.mp@parliament.uk</a>
                    ', N'www.stevebrine.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4068, N'Richard Harrington MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/78037.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Watford', N'
                        30 The Avenue, Watford, WD17 4NS<br>

                        
                            Tel: 01923 296790<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:richard@richardharrington.org.uk">richard@richardharrington.org.uk</a>
', N'richard@richardharrington.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7180<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:richard.harrington.mp@parliament.uk">richard.harrington.mp@parliament.uk</a>
                    ', N'www.richardharrington.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4069, N'Rt Hon Bob Ainsworth MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25703.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Coventry North East', N'
                        Bayley House, 22-23 Bayley Lane, Coventry, CV1 5RJ<br>

                        
                            Tel: 02476 226707<br>
                        
                        
                            Fax: 02476 433401<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:wisec@parliament.uk">wisec@parliament.uk</a>
', N'wisec@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4047<br>
                        
                        
                            Fax: 020 7219 2889<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ainsworthr@parliament.uk">ainsworthr@parliament.uk</a>
                    ', N'www.bobainsworthmp.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4070, N'Matthew Hancock MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84378.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Suffolk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7186<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:matthew.hancock.mp@parliament.uk">matthew.hancock.mp@parliament.uk</a>
', N'matthew.hancock.mp@parliament.uk', N'
                        Unit 8, Swan Lane Business Park, Exning, Newmarket, CB8 7FN<br>

                        
                            Tel: 01638 576692; 01787 211380<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@westsuffolkconservatives.com">office@westsuffolkconservatives.com</a>
                    ', N'www.matthewhancock.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4071, N'Sarah Newton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62750.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Truro and Falmouth', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7174<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sarah.newton.mp@parliament.uk">sarah.newton.mp@parliament.uk</a>
', N'sarah.newton.mp@parliament.uk', N'
                        Rooms 5 and 6, 18 Lemon Street, Truro, Cornwall, TR1 2LZ<br>

                        
                            Tel: 01872 274760<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.sarahnewton.org.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4072, N'Neil Parish MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/28723.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Tiverton and Honiton', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7172<br>
                        
                        
                            Fax: 020 7219 5005<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:neil.parish.mp@parliament.uk">neil.parish.mp@parliament.uk</a>
', N'neil.parish.mp@parliament.uk', N'
                        9C Mill Park Industrial Estate, White Cross Road, Woodbury Salterton, Exeter, EX5 1EL<br>

                        
                            Tel: 01395 233503<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.neilparish.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4073, N'Dr Sarah Wollaston MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/81217.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Totnes', N'
                        Totnes Conservative Club, Station Road, Totnes, TQ9 5HW<br>

                        
                            Tel: 01803 868 378<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nina.smith@parliament.uk">nina.smith@parliament.uk</a>
', N'nina.smith@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5129<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:sarah.wollaston.mp@parliament.uk">sarah.wollaston.mp@parliament.uk</a>
                    ', N'www.drsarah.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4074, N'Mark Garnier MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35520.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wyre Forest', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mark.garnier.mp@parliament.uk">mark.garnier.mp@parliament.uk</a>
', N'mark.garnier.mp@parliament.uk', N'
                        9a Lower Mill Street, Kidderminster, DY11 6UU<br>

                        
                            Tel: 01562 746771<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mark@wyreforestconservatives.com">mark@wyreforestconservatives.com</a>
                    ', N'www.markgarnier.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4075, N'Rt Hon Eric Pickles MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25404.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Brentwood and Ongar', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4428<br>
                        
                        
                            Fax: 020 7219 2783<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:picklese@parliament.uk">picklese@parliament.uk</a>
', N'picklese@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.ericpickles.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4076, N'Valerie Vaz MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84905.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Walsall South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7176<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:valerie.vaz.mp@parliament.uk">valerie.vaz.mp@parliament.uk</a>
', N'valerie.vaz.mp@parliament.uk', N'
                        16A Lichfield Street, Walsall, WS1 1TJ<br>

                        
                            Tel: 01922 635835<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.valerievazmp.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4077, N'Emma Reynolds MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/52382.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wolverhampton North East', N'
                        492A Stafford Road, Wolverhampton, WV10 6AN<br>

                        
                            Tel: 01902 397698<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6919<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:emma.reynolds.mp@parliament.uk">emma.reynolds.mp@parliament.uk</a>
                    ', N'www.emmareynolds.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4078, N'Paul Uppal MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41292.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wolverhampton South West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7195<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:paul.uppal.mp@parliament.uk">paul.uppal.mp@parliament.uk</a>
', N'paul.uppal.mp@parliament.uk', N'
                        Gresham Chambers, Second Floor, 14 Lichfield Street, Wolverhampton, WV1 1DG<br>

                        
                            Tel: 01902 712134<br>
                        
                        
                            Fax: 01902 238931<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.pauluppal.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4079, N'Julian Sturdy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35437.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'York Outer', N'
                        York Conservatives, 1 Ash Street, York, YO26 4UR<br>

                        
                            Tel: 01904 784847<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7199<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:julian.sturdy.mp@parliament.uk">julian.sturdy.mp@parliament.uk</a>
                    ', N'www.juliansturdy.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4080, N'David Mowat MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62743.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Warrington South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7178<br>
                        
                        
                            Fax: 020 7219 5067<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david.mowat.mp@parliament.uk">david.mowat.mp@parliament.uk</a>
', N'david.mowat.mp@parliament.uk', N'
                        Warrington South Conservative Association, 1 Stafford Road, Warrington, WA4 6RP<br>

                        
                            Tel: 01925 231267<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:wsca@talktalk.net">wsca@talktalk.net</a>
                    ', N'www.davidmowat.org', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4081, N'Graham Evans MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40501.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Weaver Vale', N'
                        The Bungalow, Sheath Street, Northwich, Cheshire, CW9 5BH<br>

                        
                            Tel: 01606 350323<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:help@grahamevansmp.com">help@grahamevansmp.com</a>
', N'help@grahamevansmp.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7183<br>
                        
                        
                            Fax: 020 7219 5079<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mail@grahamevansmp.com">mail@grahamevansmp.com</a>
                    ', N'www.grahamevansmp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4082, N'Lisa Nandy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/54056.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wigan', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7188<br>
                        
                        
                            Fax: 020 7219 5152<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:lisa.nandy.mp@parliament.uk">lisa.nandy.mp@parliament.uk</a>
', N'lisa.nandy.mp@parliament.uk', N'
                        2nd Floor, Wigan Investment Centre, Waterside Drive, Wigan, WN3 5BA<br>

                        
                            Tel: 01942 242 047<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.lisanandy.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4083, N'Alison McGovern MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84353.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wirral South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7190<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alison.mcgovern.mp@parliament.uk">alison.mcgovern.mp@parliament.uk</a>
', N'alison.mcgovern.mp@parliament.uk', N'
                        99 New Chester Road, New Ferry, Wirral, CH62 4RA<br>

                        
                            Tel: 0151 645 6590<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@alison_mcgovern', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4084, N'Esther McVey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35517.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wirral West', N'
                        The Parade, Hoylake Community Centre, Hoyle Road, Hoylake, Wirral, CH47 3AG<br>

                        
                            Tel: 0151 632 4348<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:officeofesthermcveymp@parliament.uk">officeofesthermcveymp@parliament.uk</a>
', N'officeofesthermcveymp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7191<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:esther.mcvey.mp@parliament.uk">esther.mcvey.mp@parliament.uk</a>
                    ', N'www.esthermcvey.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4086, N'Alun Cairns MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33463.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Vale of Glamorgan', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7175<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alun.cairns.mp@parliament.uk">alun.cairns.mp@parliament.uk</a>
', N'alun.cairns.mp@parliament.uk', N'
                        29 High Street, Barry, CF62 7EB<br>

                        
                            Tel: 01446 403814<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.aluncairns.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4088, N'Stella Creasy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72256.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Walthamstow', N'
                        Walthamstow Labour Party, 23 Orford Road, Walthamstow, London, E17 9NL<br>

                        
                            Tel: 020 8521 1223<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stella@workingforwalthamstow.org.uk">stella@workingforwalthamstow.org.uk</a>
', N'stella@workingforwalthamstow.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6980<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stella.creasy.mp@parliament.uk">stella.creasy.mp@parliament.uk</a>
                    ', N'www.workingforwalthamstow.org.uk/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4089, N'Tessa Munt MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31619.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wells', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4024<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:tessa.munt.mp@parliament.uk">tessa.munt.mp@parliament.uk</a>
', N'tessa.munt.mp@parliament.uk', N'
                        Hodge''s Chamber, Cheddar Road, Wedmore, BS28 4EH<br>

                        
                            Tel: 01934 710748<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:tessa.munt.mp@parliament.uk">tessa.munt.mp@parliament.uk</a>
                    ', N'www.tessamunt.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4090, N'Jonathan Lord MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83146.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Woking', N'
                        Woking Conservatives, Churchill House, Chobham Road, Woking, GU21 4AA<br>

                        
                            Tel: 01483 773384<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6913<br>
                        
                        
                            Fax: 020 7219 5198<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jonathan.lord.mp@parliament.uk">jonathan.lord.mp@parliament.uk</a>
                    ', N'www.wokingconservatives.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4091, N'Mr Robin Walker MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62794.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Worcester', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7196<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:robin.walker.mp@parliament.uk">robin.walker.mp@parliament.uk</a>
', N'robin.walker.mp@parliament.uk', N'
                        Office of Robin Walker MP, Guildhall, High Street, Worcester, WR1 2EY<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:robin@walker4worcester.com">robin@walker4worcester.com</a>
                    ', N'www.walker4worcester.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4092, N'Stephen Metcalfe MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35374.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Basildon and East Thurrock', N'
                        South Basildon and East Thurrock Conservatives, 2 Orsett Business Centre, Stanford Road, Grays, Essex, RM16 1BX<br>

                        
                            Tel: 01268 200430<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7009<br>
                        
                        
                            Fax: 020 7219 0306<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stephen.metcalfe.mp@parliament.uk">stephen.metcalfe.mp@parliament.uk</a>
                    ', N'www.stephenmetcalfemp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4093, N'Stephen McPartland MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62730.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stevenage', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7156<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen.mcpartland.mp@parliament.uk">stephen.mcpartland.mp@parliament.uk</a>
', N'stephen.mcpartland.mp@parliament.uk', N'
                        No constituency address publicised<br>

                        
                            Tel: 01438 728139<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:stephen@stephenmcpartland.co.uk">stephen@stephenmcpartland.co.uk</a>
                    ', N'www.stephenmcpartland.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4094, N'Laura Sandys MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62773.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Thanet', N'
                        16A Grange Road, Ramsgate, CT11 9LR<br>

                        
                            Tel: 01843 589434<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:laura@sandys.org.uk">laura@sandys.org.uk</a>
', N'laura@sandys.org.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:laura.sandys.mp@parliament.uk">laura.sandys.mp@parliament.uk</a>
                    ', N'telllaura.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4095, N'Stephen Barclay MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/43499.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North East Cambridgeshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7117<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen.barclay.mp@parliament.uk">stephen.barclay.mp@parliament.uk</a>
', N'stephen.barclay.mp@parliament.uk', N'
                        Cromwell House, Wisbech Road, March, PE15 8EB<br>

                        
                            Tel: 01354 656 635<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.stevebarclay.net/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4096, N'Simon Wright MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62804.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Norwich South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7120<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:simon.wright.mp@parliament.uk">simon.wright.mp@parliament.uk</a>
', N'simon.wright.mp@parliament.uk', N'
                        2 Douro Place, Norwich, NR2 4BG<br>

                        
                            Tel: 01603 627660<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@simonwright.org.uk">office@simonwright.org.uk</a>
                    ', N'www.simonwright.org.uk', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4097, N'Elizabeth Truss MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40370.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South West Norfolk', N'
                        The Limes, 32 Bridge Street, Thetford, IP24 3AG<br>

                        
                            Tel: 01842 757345<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7151<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:elizabeth.truss.mp@parliament.uk">elizabeth.truss.mp@parliament.uk</a>
                    ', N'www.elizabethtruss.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4098, N'Dr Thérèse Coffey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/37744.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Suffolk Coastal', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7164<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:therese.coffey.mp@parliament.uk">therese.coffey.mp@parliament.uk</a>
', N'therese.coffey.mp@parliament.uk', N'
                        Suite 110, York House, 2-4 York Road, Felixstowe, Suffolk, IP11 7QG<br>

                        
                            Tel: 01394 285 218<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.theresecoffeymp.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4099, N'Jacob Rees-Mogg MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/42117.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North East Somerset', N'
                        North East Somerset Conservative Association, Rear of 16 High Street, Keynsham, Bristol, BS31 1DQ<br>

                        
                            Tel: 0117 987 2313<br>
                        
                        
                            Fax: 0117 987 2322<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7118<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jacob.reesmogg.mp@parliament.uk">jacob.reesmogg.mp@parliament.uk</a>
                    ', N'www.northeastsomersetconservatives.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4100, N'Sheryll Murray MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62747.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South East Cornwall', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:sheryll.murray.mp@parliament.uk">sheryll.murray.mp@parliament.uk</a>
', N'sheryll.murray.mp@parliament.uk', N'
                        Windsor Place, Liskeard, PL14 4BH<br>

                        
                            Tel: 01579 344428<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:sheryll@sheryllmurray.com">sheryll@sheryllmurray.com</a>
                    ', N'sheryllmurray.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4101, N'Stephen Gilbert MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62678.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'St Austell and Newquay', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7153<br>
                        
                        
                            Fax: 020 7219 4180<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen.gilbert.mp@parliament.uk">stephen.gilbert.mp@parliament.uk</a>
', N'stephen.gilbert.mp@parliament.uk', N'
                        c/o St Austell and Newquay Liberal Democrats, 10 South Street, St Austell, PL25 5BH<br>

                        
                            Tel: 01726 63443<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:steve@stevegilbert.info">steve@stevegilbert.info</a>
                    ', N'stephengilbert.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4103, N'Pat Glass MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83728.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North West Durham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7054<br>
                        
                        
                            Fax: 020 7219 1652<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:pat.glass.mp@parliament.uk">pat.glass.mp@parliament.uk</a>
', N'pat.glass.mp@parliament.uk', N'
                        1 Gledstone House, 26 Newmarket Street, Consett, DH8 5LQ<br>

                        
                            Tel: 01207 501782<br>
                        
                        
                            Fax: 01207 501791<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.patglassmp.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4104, N'Rt Hon Alistair Carmichael MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/24991.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Orkney and Shetland', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8181<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:carmichaela@parliament.uk">carmichaela@parliament.uk</a>
', N'carmichaela@parliament.uk', N'
                        Orkney: 14 Palace Road , Kirkwall, KW15 1PA<br>

                        
                            Tel: 01856 876541<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:flettb@parliament.uk">flettb@parliament.uk</a>
                    ', N'www.alistaircarmichael.org.uk/', 1308, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4105, N'Justin Tomlinson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35402.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Swindon', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7167<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:justin.tomlinson.mp@parliament.uk">justin.tomlinson.mp@parliament.uk</a>
', N'justin.tomlinson.mp@parliament.uk', N'
                        Swindon Conservative MP''s Office, First Floor, 1 Milton Road, Swindon, SN1 5JE<br>

                        
                            Tel: 01793 533393<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.justintomlinson.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4106, N'Mr Robert Buckland MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35445.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Swindon', N'
                        Swindon Conservatives, 1 Milton Road, Swindon, SN1 5JE<br>

                        
                            Tel: 01793 533393<br>
                        
                        
                            Fax: 01793 533393<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7168<br>
                        
                        
                            Fax: 020 7219 4849<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:robert.buckland.mp@parliament.uk">robert.buckland.mp@parliament.uk</a>
                    ', N'www.robertbuckland.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4107, N'Harriett Baldwin MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40489.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Worcestershire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                            Fax: 020 7219 5151<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:harriett.baldwin.mp@parliament.uk">harriett.baldwin.mp@parliament.uk</a>
', N'harriett.baldwin.mp@parliament.uk', N'
                        Malvern Hills Science Park, Geraldine Road, Malvern, WR14 3SZ<br>

                        
                            Tel: 01684 585165<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.harriettbaldwin.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4108, N'Gavin Williamson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35247.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Staffordshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7150<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:gavin.williamson.mp@parliament.uk">gavin.williamson.mp@parliament.uk</a>
', N'gavin.williamson.mp@parliament.uk', N'
                        2 Lychgate House, High Street, Pattingham, South Staffordshire, WV6 7BQ<br>

                        
                            Tel: 01902 701479<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.gavinwilliamson.org', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4109, N'Jeremy Lefroy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/37699.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stafford', N'
                        Unit 15, Pearl House, Anson Court, Staffordshire Technology Park, Beaconside, Stafford, ST18 8GB<br>

                        
                            Tel: 01785 252477<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7154<br>
                        
                        
                            Fax: 020 7219 4186<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jeremy.lefroy.mp@parliament.uk">jeremy.lefroy.mp@parliament.uk</a>
                    ', N'www.jeremylefroy.org.uk/', 1302, 1)
GO
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4110, N'Karen Bradley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41311.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Staffordshire Moorlands', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7215<br>
                        
                        
                            Fax: 020 7219 6222<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:karen.bradley.mp@parliament.uk">karen.bradley.mp@parliament.uk</a>
', N'karen.bradley.mp@parliament.uk', N'
                        Unit 24, The Smithfield Centre, Haywood Street, Leek, ST13 5JW<br>

                        
                            Tel: 01538 382421<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.karenbradley.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4111, N'Rt Hon Jeremy Hunt MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35446.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South West Surrey', N'
                        SW Surrey Conservative Association, 2 Royal Parade, Tilford Road, Hindhead, GU26 6TD<br>

                        
                            Tel: 01428 609416<br>
                        
                        
                            Fax: 01428 607498<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6813<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:huntj@parliament.uk">huntj@parliament.uk</a>
                    ', N'www.jeremyhunt.org/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4112, N'Dan Byles MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62829.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Warwickshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7179<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:dan.byles.mp@parliament.uk">dan.byles.mp@parliament.uk</a>
', N'dan.byles.mp@parliament.uk', N'
                        8 Kingsway House, 4 King Street, Bedworth, CV12 8HY<br>

                        
                            Tel: 02476 315233<br>
                        
                        
                            Fax: 02476 315233<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:sandra.trickett@parliament.uk">sandra.trickett@parliament.uk</a>
                    ', N'www.danbyles.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4113, N'Nadhim Zahawi MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/53078.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stratford-on-Avon', N'
                        First Floor, 3 Trinity Street, Stratford-upon-Avon, CV37 6BL<br>

                        
                            Tel: 01789 264362; 01789 292723<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:nadhim@zahawi.com">nadhim@zahawi.com</a>
', N'nadhim@zahawi.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7159<br>
                        
                        
                            Fax: 020 7219 4462<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nadhim.zahawi.mp@parliament.uk">nadhim.zahawi.mp@parliament.uk</a>
                    ', N'www.zahawi.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4114, N'Chris White MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31580.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Warwick and Leamington', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7201<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:chris.white.mp@parliament.uk">chris.white.mp@parliament.uk</a>
', N'chris.white.mp@parliament.uk', N'
                        43a Clemens Street, Leamington Spa, CV31 2DP<br>

                        
                            Tel: 01926 315888<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.chriswhitemp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4115, N'Mr Stewart Jackson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31714.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Peterborough', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5046/6062<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stewart.jackson.mp@parliament.uk">stewart.jackson.mp@parliament.uk</a>
', N'stewart.jackson.mp@parliament.uk', N'
                        Peterborough Conservative Association, 193 Dogsthorpe Road, Peterborough, PE1 3AT<br>

                        
                            Tel: 01733 891080<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.stewartjackson.org.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4116, N'Michael Ellis MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62852.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Northampton North', N'
                        78 St George''s Avenue, Northampton, NN2 6JF<br>

                        
                            Tel: 01604 858539<br>
                        
                        
                            Fax: 01604 712372<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7220<br>
                        
                        
                            Fax: 020 7219 6375<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:michael.ellis.mp@parliament.uk">michael.ellis.mp@parliament.uk</a>
                    ', N'www.michaelellis.co.uk/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4117, N'Andrea Leadsom MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40456.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Northamptonshire', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7149<br>
                        
                        
                            Fax: 020 7219 4045<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andrea.leadsom.mp@parliament.uk">andrea.leadsom.mp@parliament.uk</a>
', N'andrea.leadsom.mp@parliament.uk', N'
                        78 St. George''s Avenue, Northampton, NN2 6JF<br>

                        
                            Tel: 01604 859721<br>
                        
                        
                            Fax: 01604 859329<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.andrealeadsom.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4118, N'Julian Smith MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83149.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Skipton and Ripon', N'
                        19 Otley Street, Skipton, BD23 1DY<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7145<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:julian.smith.mp@parliament.uk">julian.smith.mp@parliament.uk</a>
                    ', N'www.juliansmithmp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4119, N'Jonathan Reynolds MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84903.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stalybridge and Hyde', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7155<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jonathan.reynolds.mp@parliament.uk">jonathan.reynolds.mp@parliament.uk</a>
', N'jonathan.reynolds.mp@parliament.uk', N'
                        Hyde Town Hall, Market Street, Hyde, SK14 1AL<br>

                        
                            Tel: 0161 367 8077<br>
                        
                        
                            Fax: 0161 367 0050<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jonathanreynolds.org.uk', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4120, N'Kate Green MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84849.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stretford and Urmston', N'
                        Stretford and Urmston Labour Party, The Morris Hall, 9 Atkinson Road, Urmston, Manchester, M41 9AD<br>

                        
                            Tel: 0161 749 9120<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7162<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:kate.green.mp@parliament.uk">kate.green.mp@parliament.uk</a>
                    ', N'www.kategreen.org/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4121, N'Lorraine Fullbrook MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35444.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Ribble', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7217<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:lorraine.fullbrook.mp@parliament.uk">lorraine.fullbrook.mp@parliament.uk</a>
', N'lorraine.fullbrook.mp@parliament.uk', N'
                        South Ribble Conservative Association, Unit 6, Enterprise House, Meadowcroft, Pope Lane, Whitestake, Preston, PR4 4BA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:info@lorrainefullbrook.com">info@lorrainefullbrook.com</a>
                    ', N'', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4122, N'Alex Cunningham MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72257.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stockton North', N'
                        Stockton Business Centre, Brunswick Street, Stockton on Tees, TS18 1DW<br>

                        
                            Tel: 01642 345291<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:alex@stockton-north.com">alex@stockton-north.com</a>
', N'alex@stockton-north.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7157<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:alex.cunningham.mp@parliament.uk">alex.cunningham.mp@parliament.uk</a>
                    ', N'www.alexcunninghammp.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4123, N'James Wharton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62797.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Stockton South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7236<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:james.wharton.mp@parliament.uk">james.wharton.mp@parliament.uk</a>
', N'james.wharton.mp@parliament.uk', N'
                        Stockton Conservatives, Suite 6, DTV Business Centre, Orde Wingate Way, Stockton-on-Tees, TS19 0GD<br>

                        
                            Tel: 01642 636235<br>
                        
                        
                            Fax: 01642 636234<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:james@jameswharton.co.uk">james@jameswharton.co.uk</a>
                    ', N'www.jameswharton.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4124, N'Chi Onwurah MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83135.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newcastle upon Tyne Central', N'
                        Unit 25, 7-15 Pink Lane, Newcastle-upon-Tyne, NE1 5DW<br>

                        
                            Tel: 0191 232 5838<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7114<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:chi.onwurah.mp@parliament.uk">chi.onwurah.mp@parliament.uk</a>
                    ', N'chionwurahmp.com/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4125, N'Catherine McKinnell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83507.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newcastle upon Tyne North', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7115<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:catherine.mckinnell.mp@parliament.uk">catherine.mckinnell.mp@parliament.uk</a>
', N'catherine.mckinnell.mp@parliament.uk', N'
                        The Lemington Centre, Tyne View, Newcastle, NE15 8RZ<br>

                        
                            Tel: 0191 229 0352<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.catherinemckinnellmp.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4126, N'Mrs Mary Glindon MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84517.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Tyneside', N'
                        Suites 1 &amp; 2, Salisbury House, 2 Buddle Street, Wallsend, NE28 6EH<br>

                        
                            Tel: 0191 234 2493<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                            Fax: 020 7219 3272<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:mary.glindon.mp@parliament.uk">mary.glindon.mp@parliament.uk</a>
                    ', N'www.maryglindonmp.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4127, N'Julie Elliott MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/78855.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Sunderland Central', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7165<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:julie.elliott.mp@parliament.uk">julie.elliott.mp@parliament.uk</a>
', N'julie.elliott.mp@parliament.uk', N'
                        10 Norfolk Street, Sunderland, SR1 1EA<br>

                        
                            Tel: 0191 565 5327<br>
                        
                        
                            Fax: 0191 565 9848<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'julie4sunderland.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4128, N'Mr Chuka Umunna MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/72314.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Streatham', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:chuka.umunna.mp@parliament.uk">chuka.umunna.mp@parliament.uk</a>
', N'chuka.umunna.mp@parliament.uk', N'
                        3a Mount Ephraim Road, Streatham, London, SW16 1NQ<br>

                        
                            Tel: 020 8769 5063<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.chuka.org.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4129, N'Ian Paisley MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33629.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North Antrim', N'
                        9-11 Church Street, Ballymena, BT43 6DD<br>

                        
                            Tel: 028 2564 1421<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ian.paisley.mp@parliament.uk">ian.paisley.mp@parliament.uk</a>
', N'ian.paisley.mp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7116<br>
                        
                        
                            Fax: 020 7219 2996<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:ian.paisley.mp@parliament.uk">ian.paisley.mp@parliament.uk</a>
                    ', N'www.ianpaisleymp.co.uk/', 1303, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4130, N'Ms Margaret Ritchie MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33632.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Down', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:margaret.ritchie.mp@parliament.uk">margaret.ritchie.mp@parliament.uk</a>
', N'margaret.ritchie.mp@parliament.uk', N'
                        32 Saul Street, Downpatrick, Co Down, BT30 6NQ<br>

                        
                            Tel: 028 4461 2882<br>
                        
                        
                            Fax: 028 4461 9574<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:m.ritchie@sdlp.ie">m.ritchie@sdlp.ie</a>
                    ', N'www.margaretritchie.com', 1313, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4131, N'Jim Shannon MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33634.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Strangford', N'
                        34A Frances Street, Newtownards, BT23 7DN<br>

                        
                            Tel: 028 9182 7990<br>
                        
                        
                            Fax: 028 9182 7991<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7160<br>
                        
                        
                            Fax: 020 7219 2347<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:jim.shannon.mp@parliament.uk">jim.shannon.mp@parliament.uk</a>
                    ', N'www.dup.org.uk/myprofile.asp?memberid=62', 1303, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4132, N'Richard Drax MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62850.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'South Dorset', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7051<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:richard.drax.mp@parliament.uk">richard.drax.mp@parliament.uk</a>
', N'richard.drax.mp@parliament.uk', N'
                        Unit 2C, Purbeck Buildings, Dorset Green Technology Park, Winfrith Newburgh, Dorchester, DT2 8ZB<br>

                        
                            Tel: 01305 851900<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.richarddrax.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4133, N'Andrew Bridgen MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62823.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'North West Leicestershire', N'
                        6 Elford Street, Ashby De La Zouch, Leicestershire, LE65 1HH<br>

                        
                            Tel: 01530 417736<br>
                        
                        
                            Fax: 01530 560896<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:andrew@andrewbridgen.com">andrew@andrewbridgen.com</a>
', N'andrew@andrewbridgen.com', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7238<br>
                        
                        
                            Fax: 020 7219 6819<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:andrew.bridgen.mp@parliament.uk">andrew.bridgen.mp@parliament.uk</a>
                    ', N'www.andrewbridgen.com/', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4134, N'Kwasi Kwarteng MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35251.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Spelthorne', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4017<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:kwasi.kwarteng.mp@parliament.uk">kwasi.kwarteng.mp@parliament.uk</a>
', N'kwasi.kwarteng.mp@parliament.uk', N'
                        Spelthorne Conservative Association, 55 Cherry Orchard, Staines, TW18 2SQ<br>

                        
                            Tel: 01784 453544<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:office@spelthorneconservatives.org.uk">office@spelthorneconservatives.org.uk</a>
                    ', N'www.kwart2010.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4135, N'David Morris MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31581.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Morecambe and Lunesdale', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7234<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:david.morris.mp@parliament.uk">david.morris.mp@parliament.uk</a>
', N'david.morris.mp@parliament.uk', N'
                        Office 204, Riverway House, Morecambe Road, Skerton, LA1 2RX<br>

                        
                            Tel: 01524 841225<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:david.morris.mp@parliament.uk">david.morris.mp@parliament.uk</a>
                    ', N'www.davidmorrismp.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4136, N'Nigel Mills MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62731.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Amber Valley', N'
                        Thomas Henry House, Suite 101, 1-5 Church Street, Ripley, DE5 3BU<br>

                        
                            Tel: 01773 744341<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7233<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:nigel.mills.mp@parliament.uk">nigel.mills.mp@parliament.uk</a>
                    ', N'www.nigelmillsmp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4137, N'Rory Stewart MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83410.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Penrith and The Border', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7127<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:rory.stewart.mp@parliament.uk">rory.stewart.mp@parliament.uk</a>
', N'rory.stewart.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:rory@rorystewart.co.uk">rory@rorystewart.co.uk</a>
                    ', N'rorystewart.co.uk', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4138, N'Heidi Alexander MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/83154.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Lewisham East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7099<br>
                        
                        
                            Fax: 020 7219 2677<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:heidi.alexander.mp@parliament.uk">heidi.alexander.mp@parliament.uk</a>
', N'heidi.alexander.mp@parliament.uk', N'
                        No constituency office publicised<br>

                        
                            Tel: 020 8461 4733<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:heidi@heidialexander.org.uk">heidi@heidialexander.org.uk</a>
                    ', N'www.heidialexander.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4139, N'Ian Lavery MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/84518.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wansbeck', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:ian.lavery.mp@parliament.uk">ian.lavery.mp@parliament.uk</a>
', N'ian.lavery.mp@parliament.uk', N'
                        7 Esther COurt, Wansbeck Business park, Ashington, Northumberland, NE63 8QZ<br>

                        
                            Tel: 01670 852494<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.ianlavery.co.uk', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4140, N'Mr David Nuttall MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/31712.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Bury North', N'
                        15 St Mary''s Place, Bury, BL9 0DZ<br>

                        
                            Tel: 0161 797 5007<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7030<br>
                        
                        
                            Fax: 020 7219 2409<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:david.nuttall.mp@parliament.uk">david.nuttall.mp@parliament.uk</a>
                    ', N'davidnuttall.info', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4141, N'Eric Ollerenshaw MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62757.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Lancaster and Fleetwood', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7096<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:eric.ollerenshaw.mp@parliament.uk">eric.ollerenshaw.mp@parliament.uk</a>
', N'eric.ollerenshaw.mp@parliament.uk', N'
                        The Village Centre, 59 High Street, Great Eccleston, PR3 0YB<br>

                        
                            Tel: 01995 672975<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:lancasterwyre@tory.org">lancasterwyre@tory.org</a>
                    ', N'www.ericollerenshaw.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4142, N'Guy Opperman MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/41334.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Hexham', N'
                        Hexham Conservatives, 1 Meal Market, Hexham, NE46 1NF<br>

                        
                            Tel: 01434 603777<br>
                        
                        
                            Fax: 01434 601659<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:hexham@tory.org">hexham@tory.org</a>
', N'hexham@tory.org', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7227<br>
                        
                        
                            Fax: 020 7219 6435<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:guy.opperman.mp@parliament.uk">guy.opperman.mp@parliament.uk</a>
                    ', N'guyopperman.blogspot.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4212, N'Debbie Abrahams MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/80556.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Oldham East and Saddleworth', N'
                        Lord Chambers, 11 Church Lane, Oldham, OL1 3AN<br>

                        
                            Tel: 0161 624 4248<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:abrahamsd@parliament.uk">abrahamsd@parliament.uk</a>
', N'abrahamsd@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 1041 Fax: 0207 219 2405<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:abrahamsd@parliament.uk">abrahamsd@parliament.uk</a>
                    ', N'www.debbieabrahams.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4243, N'Cathy Jamieson MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/32645.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Kilmarnock and Loudoun', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8456<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:cathy.jamieson.mp@parliament.uk">cathy.jamieson.mp@parliament.uk</a>
', N'cathy.jamieson.mp@parliament.uk', N'
                        32 Grange Street, Kilmarnock, KA1 2DD<br>

                        
                            Tel: 01563 522361<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.cathyjamieson.com', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4244, N'Jonathan Ashworth MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/27620.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Leicester South', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 0207 219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:jon.ashworth.mp@parliament.uk">jon.ashworth.mp@parliament.uk</a>
', N'jon.ashworth.mp@parliament.uk', N'
                        60 Charles Street, Leicester, LE1 1FB<br>

                        
                            Tel: 0116 251 1927<br>
                        
                        
                            Fax: 0116 262 6329<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.jonashworth.org/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4245, N'Paul Maskey MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/58939.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Belfast West', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 0207 219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mailto:paul.maskey.mp@parliament.uk">mailto:paul.maskey.mp@parliament.uk</a>
', N'mailto:paul.maskey.mp@parliament.uk', N'
                        c/o 53 Falls Road , Belfast, BT12 4PD<br>

                        
                            Tel: 028 9034 7350<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:westbelfastmp@ireland.com">westbelfastmp@ireland.com</a>
                    ', N'www.sinnfein.ie/', 1312, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4246, N'Iain McKenzie MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/92542.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Inverclyde', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8446<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:iain.mckenzie.mp@parliament.uk">iain.mckenzie.mp@parliament.uk</a>
', N'iain.mckenzie.mp@parliament.uk', N'
                        The Parliamentary Office, 20 Union Street, Greenock, PA16 8JL<br>

                        
                            Tel: 01475 791820<br>
                        
                        
                            Fax: 01475 791821<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@inverclydemp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4249, N'Anne Marie Morris MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/62737.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Newton Abbot', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:annemarie.morris.mp@parliament.uk">annemarie.morris.mp@parliament.uk</a>
', N'annemarie.morris.mp@parliament.uk', N'
                        2 Salisbury House, Salisbury Road, Newton Abbot, TQ12 2DF<br>

                        
                            Tel: 01626 368277<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:annemarie@annemariemorris.co.uk">annemarie@annemariemorris.co.uk</a>
                    ', N'@ammorrismp', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4253, N'Seema Malhotra MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/95400.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Feltham and Heston', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 8957<br>
                        
                        
                            Fax: 020 7219 2578<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:seema.malhotra.mp@parliament.uk">seema.malhotra.mp@parliament.uk</a>
', N'seema.malhotra.mp@parliament.uk', N'
                        Labour Hall, Manor Place, Feltham, London, TW14 9BT<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.seemamalhotra.com/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4263, N'Lucy Powell MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/60214.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Manchester Central', N'
                        

                        
                            Tel: 0161 232 0872<br>
                        
                        
                            Fax: 0161 232 1865<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4402<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'lucypowell.org.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4264, N'Stephen Doughty MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/61112.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Cardiff South and Penarth', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5348<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:stephen.doughty.mp@parliament.uk">stephen.doughty.mp@parliament.uk</a>
', N'stephen.doughty.mp@parliament.uk', N'
                        Ground Floor, Mount Stuart House, Mount Stuart Square, Cardiff Bay, CF10 5FQ<br>

                        
                            Tel: 02920 452 072<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.stephendoughty.org.uk/', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4265, N'Andy Sawford MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/49717.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Corby', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 6134<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        Grosvenor House, George Street, Corby, NN17 1QB<br>

                        
                            Tel: 01536 264 194<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@andysawfordmp', 1307, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4267, N'Martin Caton MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25683.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Gower', N'
                        9 Pontardulais Road, Gorseinon, Swansea, SA4 4FE<br>

                        
                            Tel: 01792 892100<br>
                        
                        
                            Fax: 01792 892375<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:daviesl@parliament.uk">daviesl@parliament.uk</a>
', N'daviesl@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5111<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:martin.caton.mp@parliament.uk">martin.caton.mp@parliament.uk</a>
                    ', N'www.martin-caton.co.uk/', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4268, N'Mr Jamie Reed MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/40273.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Copeland', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4706<br>
                        
                        
                            Fax: 020 7219 4870<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:reedjr@parliament.uk">reedjr@parliament.uk</a>
', N'reedjr@parliament.uk', N'
                        Phoenix Enterprise Centre, Phoenix House, Jacktrees Road, Cleator Moor, CA25 5BD<br>

                        
                            Tel: 01946 816723<br>
                        
                        
                            Fax: 01946 816743<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:andersenj@parliament.uk">andersenj@parliament.uk</a>
                    ', N'@jreedmp', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4269, N'Andy McDonald MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/101338.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Middlesbrough', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4995<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:info@andymcdonaldmp.org">info@andymcdonaldmp.org</a>
', N'info@andymcdonaldmp.org', N'
                        Unit 4, Broadcasting House, Newport Road, Middlesbrough, TS1 5JA<br>

                        
                            Tel: 01642 246574<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.andymcdonaldmp.org', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4273, N'Emily Thornberry MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/35925.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Islington South and Finsbury', N'
                        65 Barnsbury Street, Islington, London, N1 1EJ<br>

                        
                            Tel: 020 7697 9307<br>
                        
                        
                            Fax: 020 7697 4587<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:emilythornberrymp@parliament.uk">emilythornberrymp@parliament.uk</a>
', N'emilythornberrymp@parliament.uk', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 5676<br>
                        
                        
                            Fax: 020 7219 5955<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:thornberrye@parliament.uk">thornberrye@parliament.uk</a>
                    ', N'www.emilythornberry.com', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4274, N'Francie Molloy MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/33623.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Mid Ulster', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 3000<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail"></a>
', N'', N'
                        26 Burn Road, Cookstown, Co Tyrone, BT80 8DN<br>

                        
                            Tel: 028 86765850<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:sinnfeincookstown@yahoo.com">sinnfeincookstown@yahoo.com</a>
                    ', N'@franciemolloy', 1312, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4277, N'Rt Hon Oliver Letwin MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25415.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'West Dorset', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:letwino@parliament.uk;%20charlesa@parliament.uk">letwino@parliament.uk; charlesa@parliament.uk</a>
', N'letwino@parliament.uk; charlesa@parliament.uk', N'
                        Cabinet Office, 9 Downing Street, London, SW1A 2AG<br>

                        
                            Tel: 020 7276 1234<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail" href="mailto:publiccorrespondence@cabinet-office.gsi.gov.uk">publiccorrespondence@cabinet-office.gsi.gov.uk</a>
                    ', N'www.oliverletwinmp.com', 1302, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4316, N'Mike Kane MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/109778.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Wythenshawe and Sale East', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 7524<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:mike.kane.mp@parliament.uk">mike.kane.mp@parliament.uk</a>
', N'mike.kane.mp@parliament.uk', N'
                        Unit A, Etrop Court, Wythenshawe Town Centre, Manchester, M22 5RG<br>

                        
                            Tel: 0161 499 7900 Fax: 0161 499 7911<br>
                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'@mjpkane', 1306, 1)
INSERT [dbo].[Politicians] ([PoliticianID], [Name], [Bio], [ImageFilePath], [DateCreated], [Constituency], [ConstituencyContact], [ConstituencyEmail], [ParliamentaryContact], [WebsiteSocialMedia], [Party_PartyID], [Government_EntityID]) VALUES (4320, N'Mr Bernard Jenkin MP', NULL, N'http://assets3.parliament.uk/ext/mnis-bio-person/www.dodspeople.com/photos/25515.jpg.jpg', CAST(0x0000A35A012294EC AS DateTime), N'Harwich and North Essex', N'
                        House of Commons, London, SW1A 0AA<br>

                        
                            Tel: 020 7219 4029<br>
                        
                        
                            Fax: 020 7219 5963<br>
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl00_hypEmail" href="mailto:bernard.jenkin.mp@parliament.uk">bernard.jenkin.mp@parliament.uk</a>
', N'bernard.jenkin.mp@parliament.uk', N'
                        Harwich and North Essex Conservative Association, Unit C2, East Gores Farm, Salmons Lane, Coggeshall, Colchester, CO6 1RZ<br>

                        
                        
                        <a id="ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_ctlContactDetails_rptPhysicalAddresses_ctl01_hypEmail"></a>
                    ', N'www.bernardjenkinmp.com', 1302, 1)
SET IDENTITY_INSERT [dbo].[Politicians] OFF
