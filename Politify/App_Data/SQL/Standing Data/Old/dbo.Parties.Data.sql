USE [Politify]
GO
SET IDENTITY_INSERT [dbo].[Parties] ON 

INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1301, N'Alliance', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1302, N'Conservative', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1303, N'Democratic Unionist', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1304, N'Green', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1305, N'Independent', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1306, N'Labour', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1307, N'Labour (Co-op)', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1308, N'Liberal Democrat', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1309, N'Plaid Cymru', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1310, N'Respect', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1311, N'Scottish National', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1312, N'Sinn Fein', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1313, N'Social Democratic & Labour Party', CAST(0x0000A35A01252E43 AS DateTime))
INSERT [dbo].[Parties] ([PartyID], [Name], [DateCreated]) VALUES (1314, N'Speaker', CAST(0x0000A35A01252E43 AS DateTime))
SET IDENTITY_INSERT [dbo].[Parties] OFF
