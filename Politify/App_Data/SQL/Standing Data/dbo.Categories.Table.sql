SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (1, 1, N'Advice and benefits', N'For discussions around advice and benefits.', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (4, 2, N'Business', N'For discussions around business development.', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (5, 3, N'Community & Living', N'For discussions around your community.', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (6, 5, N'Education & Learning', N'For discussions around local education', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (7, 4, N'Council & Democracy', N'For discussions around council operations and structures.', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (8, 6, N'Environment & Planning', N'For discussions around your local environment.', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (9, 7, N'Health & Social Care', N'For discussions around health and care in the area.', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (10, 8, N'Housing', N'For discussions around housing.', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (11, 9, N'Jobs & Careers', N'For discussions around jobs and careers in your area.', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (12, 10, N'Leisure & Culture', N'For discussions around leisure and culture in your area.', 4)
INSERT [dbo].[Categories] ([CategoryId], [Order], [Title], [Description], [OrganisationType]) VALUES (13, 11, N'Transport & Streets', N'For discussions around public transport and your transport routes.', 4)
SET IDENTITY_INSERT [dbo].[Categories] OFF
