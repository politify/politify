declare @cats table(CategoryId int)

insert into @cats
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 5

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats c on 1 = 1
  where OrganisationType = 5