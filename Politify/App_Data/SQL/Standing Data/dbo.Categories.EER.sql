SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT INTO [dbo].[Categories]([CategoryId],[Order],[Title],[Description],[OrganisationType]) VALUES (69,1,'Current Discussion','Give your opinion on politics as it happens.',5)
INSERT INTO [dbo].[Categories]([CategoryId],[Order],[Title],[Description],[OrganisationType]) VALUES (70,2,'Legislation','Discussion around previously established legislation.',5)
INSERT INTO [dbo].[Categories]([CategoryId],[Order],[Title],[Description],[OrganisationType]) VALUES (71,3,'Ideas & Future','Do you have ideas that aren''t covered by current or past legislation.',5)
INSERT INTO [dbo].[Categories]([CategoryId],[Order],[Title],[Description],[OrganisationType]) VALUES (72,4,'Political Structure','What you think about how politics is currently run.',5)
INSERT INTO [dbo].[Categories]([CategoryId],[Order],[Title],[Description],[OrganisationType]) VALUES (73,5,'Praise','What is this organisation doing right?',5)
INSERT INTO [dbo].[Categories]([CategoryId],[Order],[Title],[Description],[OrganisationType]) VALUES (74,6,'Your Cases','Have you come up against a piece of legislation, tell us your story.',5)

SET IDENTITY_INSERT [dbo].[Categories] OFF