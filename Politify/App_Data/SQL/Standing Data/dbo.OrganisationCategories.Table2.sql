declare @cats table(CategoryId int)

insert into @cats
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 7

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats c on 1 = 1
  where OrganisationType = 7

  
declare @cats2 table(CategoryId int)

insert into @cats2
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 2

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats2 c on 1 = 1
  where OrganisationType = 2

  
  
declare @cats3 table(CategoryId int)

insert into @cats3
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 3

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats3 c on 1 = 1
  where OrganisationType = 3

  
  
declare @cats4 table(CategoryId int)

insert into @cats4
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 8

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats4 c on 1 = 1
  where OrganisationType = 8
  
  
declare @cats5 table(CategoryId int)

insert into @cats5
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 9

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats5 c on 1 = 1
  where OrganisationType = 9
  
declare @cats6 table(CategoryId int)

insert into @cats6
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 6

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats6 c on 1 = 1
  where OrganisationType = 6
  
declare @cats7 table(CategoryId int)

insert into @cats7
SELECT [CategoryId]
  FROM [Politify].[dbo].[Categories] where organisationtype = 1

  insert into Politify.dbo.OrganisationCategories
  select c.CategoryId, OrganisationId from politify.dbo.organisations
  join @cats7 c on 1 = 1
  where OrganisationType = 1