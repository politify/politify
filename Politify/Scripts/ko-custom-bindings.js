﻿ko.bindingHandlers.timeago = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).timeago();
    }
};

ko.bindingHandlers.loading = {
    update: function (element, valueAccessor) {
        var value = valueAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(value);

        var ele = $(element);

        if (valueUnwrapped === true) {

            $.each(ele.children(), function (i, child) {
                $(child).css('display', 'none');
            });

            var spinnerEle = $('<div class="dynamicSpinnerWrapper"><span class="glyphicon glyphicon-refresh spin"></span></div>');
            var padding = Math.max((parseInt(ele.css('height')) / 2 - 33), 30) + 'px';
            spinnerEle.css('margin-top', padding);
            spinnerEle.css('margin-bottom', padding);
            ele.prepend(spinnerEle);
        }
        else {
            ele.find('.dynamicSpinnerWrapper').remove();
            $.each(ele.children(), function (i, child) {
                $(child).css('display', 'block');
            });
        }
    }
};