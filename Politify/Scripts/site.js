﻿if (!window.location.origin) { //IE fix for lack of setting this variable
    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
}

$('.datepicker-input').datepicker({
    format: "dd/mm/yyyy"
});

function getCurrentDateTimeOffset() {
    var currDate = new Date();
    //"2012-07-19T14:30:00+09:30"
    return currDate.getFullYear() + '-' + ensureNumberOfDigits(currDate.getMonth() + 1, 2) + '-' +
        ensureNumberOfDigits(currDate.getDate(), 2) + 'T' +
        ensureNumberOfDigits(currDate.getHours(), 2) + ':' +
        ensureNumberOfDigits(currDate.getMinutes(), 2) + ':' +
        ensureNumberOfDigits(currDate.getSeconds(), 2) + '+00:00';
}

function ensureNumberOfDigits(number, digits) {
    var numStr = number.toString();

    if (numStr.length == digits) return number;
    else if (numStr.length > digits) throw "ensureNumberOfDigits error: provided number '" + number + "' is greater than number of digits required '" + digits + "'";

    return generateString0s(digits - numStr.length) + numStr;
}

function generateString0s(num0s) {
    var string0s = '';
    for (i = 0; i < num0s; i++) {
        string0s += '0';
    }
    return string0s;
}

function showHideEl(el) {

    var ele = $(el);

    if (!ele) return;

    var currDisplay = ele.css('display');

    ele.style.removeAttribute('display');

    if (currDisplay == 'none') {
        ele.style.setProperty('display', 'block', 'important');
    }
    else {
        ele.style.setProperty('display', 'none', 'important');
    }
}

envocal.functions.toggleLoaderAnimation = function () {
    var loaderAnimations = $('.loader-animation');

    if (loaderAnimations.length > 0) {
        $.each(loaderAnimations, function (i, la) {
            window.showHideEl(la);
        });
    }
};

envocal.functions.getCurrUserObj = function () {

    var u = envocal.user;

    if (!u.isAuthenticated) return null;

    return {
        FirstName: u.firstName,
        Surname: u.surname,
        UserName: u.username,
        User_Id: u.id
    }
};

envocal.consts.orgTypes = ko.observableArray([    
    /*{ name: 'Unknown', val: 0},*/
    { name: 'Ward', val: 1},
    { name: 'District', val: 2},
    { name: 'County', val: 3},
    { name: 'UnitaryAuthority', val: 4},
    { name: 'Constituency', val: 5},
    { name: 'Government', val: 6},
    { name: 'LondonBorough', val: 7},
    { name: 'MetropolitanDistrict', val: 8},
    { name: 'Envocal', val: 9}
]);

envocal.consts.orgTypeCSharpNameSpace = 'Politify.Models.OrganisationType';