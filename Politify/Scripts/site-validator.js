﻿//
//############# Datepicker #############
//
$(function ($) {
    $.validator.addMethod('date',
    function (value, element) {
        if (this.optional(element)) {
            return true;
        }

        var ok = true;
        try {
            $.fn.datepicker.DPGlobal.parseDate(value, 'dd/mm/yy');
        }
        catch (err) {
            ok = false;
        }
        return ok;
    });
});