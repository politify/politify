﻿var politiciansVM = {
    entityName: 'PoliticianVmApi',
    partyName: 'PartyVmApi',
    viewPageSize: 60,
    viewPageNumber: 1,
    viewMoreVisible: ko.observable(true),
    politicians: ko.observableArray(),
    parties: ko.observableArray(),
    orderBy: ko.observable('Score desc'),
    partySelected: ko.observable(null),
    load: function (options) {
        if (options) {
            if (options.viewPageSize) this.viewPageSize = options.viewPageSize;
        }
        $.when(this.refreshView(), this.getParties());
    },
    orderByChange: function () {
        $.when(this.refreshView());
    },
    partyChange: function () {
        $.when(this.refreshView());
    },
    btnViewMore: function () {
        var self = this;

        $.when(ajaxOdataRequest(this.entityName, null, this.orderBy(), this.viewPageSize, this.viewPageNumber * this.viewPageSize, morphPartyToFilter(this.partySelected())))
            .then(this.checkViewMoreDataLength)
            .then(this.addPoliticians)
            .then(this.incrementPageNumber)
            .fail(odataErrorHandler);
    },
    refreshView: function () {
        var self = this;

        return ajaxOdataRequest(this.entityName, null, this.orderBy(), this.viewPageSize, null, morphPartyToFilter(this.partySelected()))
            .then(this.clearPoliticians)
            .then(this.checkViewMoreDataLength)
            .then(this.addPoliticians)
            .then(this.resetPageNumber)
            .fail(odataErrorHandler)
            .promise();
    },
    clearPoliticians: function (data, textStatus, jqXHR) {
        politiciansVM.politicians.removeAll();
        return data;
    },
    addPoliticians: function (data) {
        if (data.value.length == 0) return;
        $.each(data.value, function (i, pol) {
            politiciansVM.politicians.push(morphPolitician(pol));
        });
    },
    getParties: function () {
        var self = this;

        return ajaxOdataRequest(this.partyName, null, 'Name asc')
            .then(this.addParties)
            .fail(odataErrorHandler)
            .promise();
    },
    addParties: function (data) {
        if (data == null || data.value.length == 0) return;
        $.each(data.value, function (i, party) {
            politiciansVM.parties.push(party);
        });
    },
    resetPageNumber : function(){
        politiciansVM.viewPageNumber = 1;
    },
    incrementPageNumber: function () {
        politiciansVM.viewPageNumber += 1;
    },
    checkViewMoreDataLength: function (data, textStatus, jqXHR) {
        if (data && data.value){
            if (data.value.length < politiciansVM.viewPageSize) {
                politiciansVM.viewMoreVisible(false);
                if (data.value.length == 0) return;
            }
            else politiciansVM.viewMoreVisible(true);
            return data;
        }
        else return;
    }
};

var morphPartyToFilter = function (partySelected) {
    if (partySelected == null) return null;
    return 'PartyID eq ' + partySelected;
}

var morphPolitician = function (pol) {
    if (pol == null) return null;

    pol.PositiveCount = (pol.Positives == null ? 0 : pol.Positives);
    pol.NegativeCount = (pol.Negatives == null ? 0 : pol.Negatives);
    pol.Score = (pol.Score == null ? 0 : pol.Score);
    pol.DetailsUrl = window.location.origin + '/Politician/Details/' + pol.PoliticianID;
    pol.DetailsLinkTitle = 'View Politician, ' + pol.Name

    return pol;
}

