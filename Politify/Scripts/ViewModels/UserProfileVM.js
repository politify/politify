﻿var userProfileVM = {
    userId: null,
    isCurrentUser: false,
    userWardEntity: 'UserLocationOrgs',
    userWorkWardEntity: 'UserWorkLocationOrgs',
    discussionEntity: 'DiscussionVM',
    discussions: {
        values: ko.observableArray([]),
        loading: ko.observable(false)
    },
    orgs:{
        values: ko.observableArray([]),
        loading: ko.observable(false)
    },
    workOrgs: {
        values: ko.observableArray([]),
        loading: ko.observable(false)
    },
    setup: function(){
        userProfileVM.userId = getEntityId();
        if (userProfileVM.userId == null || userProfileVM.userId == 'UserProfile') userProfileVM.userId = envocal.user.id;

        userProfileVM.isCurrentUser = userProfileVM.userId == envocal.user.id;

        userProfileVM.load();
    },
    load: function () {
        userProfileVM.discussions.loading(true);
        userProfileVM.orgs.loading(true);
        userProfileVM.workOrgs.loading(true);

        $.when(this.refreshView());
    },
    refreshView: function () {

        var requestActions = [];

        requestActions.push(
            ajaxOdataRequest(userProfileVM.userWardEntity + "(userId='" + userProfileVM.userId + "')", null, null, null, null, null, 'Parents($expand=Parents($expand=Parents($expand=Parents($expand=Parents))))')
                .then(userProfileVM.bindWard)
                .always(function () {
                    userProfileVM.orgs.loading(false);
                })
                .promise()
        );

        requestActions.push(
            ajaxOdataRequest(userProfileVM.userWorkWardEntity + "(userId='" + userProfileVM.userId + "')", null, null, null, null, null, 'Parents($expand=Parents($expand=Parents($expand=Parents($expand=Parents))))')
                .then(userProfileVM.bindWorkWard)
                .always(function () {
                    userProfileVM.workOrgs.loading(false);
                })
                .promise()
            );

        requestActions.push(
            ajaxOdataRequest(userProfileVM.discussionEntity, null, 'DateCreated desc', null, null, "User_Id eq '" + userProfileVM.userId + "'", 'User,Section')
                .then(userProfileVM.bindDiscussions)
                .always(function () {
                    userProfileVM.discussions.loading(false);
                })
                .promise()
        );

        return requestActions;
    },
    bindWard: function (resp) {
        if (resp.value != null) userProfileVM.orgs.values(morphOrganisations(resp.value));
    },
    bindWorkWard: function (resp) {
        if (resp.value != null) userProfileVM.workOrgs.values(morphOrganisations(resp.value));
    },
    bindDiscussions: function(discussions){
        userProfileVM.discussions.values($.each(discussions.value, function (i, d) {
            return morphDiscussion(d);
        }));
    }
}