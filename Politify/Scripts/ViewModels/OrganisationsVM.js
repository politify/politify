﻿var organisationsVM = {
    organisationEntity: 'OrganisationVM',
    isAuthenticated: ko.observable(false),
    organisations: ko.observableArray(),
    count: 50,
    loading: ko.observable(false),
    organisationType: ko.observable(),
    searchText: ko.observable(),
    setup: function (options) {
        assignOptions(options, organisationsVM);
        organisationVM.isAuthenticated(envocal.user != null && envocal.user.isAuthenticated);

        organisationsVM.load();
        organisationsVM.filter.subscribe(organisationsVM.load);
    },
    load: function () {

        organisationsVM.loading(true);

        $.when(organisationsVM.refreshView())
            .then(function () {
                organisationsVM.loading(false);
            });
    },
    refreshView: function () {
        //entity, options, orderby, top, skip, filter, expand, select
        return ajaxOdataRequest(organisationsVM.organisationEntity, null, null, organisationsVM.count, null, organisationsVM.filter())
            .then(this.bindOrganisations)
            .fail(odataErrorHandler)
            .promise();
    },
    bindOrganisations: function (data) {
        if (data == null) return;

        organisationsVM.organisations(morphOrganisations(data.value));
    }
};

organisationsVM.filter = ko.computed(function () {
    var filterTerms = [];

    if (organisationsVM.searchText() != null) filterTerms.push("contains(Name, '" + organisationsVM.searchText() + "')");
    if (organisationsVM.organisationType() != null)
        filterTerms.push("OrganisationType eq " + envocal.consts.orgTypeCSharpNameSpace + "'" + organisationsVM.organisationType() + "'");

    if (filterTerms.length == 0) return null;

    return filterTerms.join(' and ');
});

var morphOrganisations = function (orgs) {

    return $.each(orgs, function (i, org) {
        return morphOrganisation(org);
    });
};