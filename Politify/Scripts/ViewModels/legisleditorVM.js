﻿//todo: make this method not so lame. Currently runs on every tab change on all 4 tabs.
var setLePanelDimensions = function () {
    var lePanel = $($('#le-panel')[0]);
    var navBar = $($('.navbar')[0]);
    var navBarHeight = parseInt(navBar.css('height'));
    var lePanelHeight = window.innerHeight - navBarHeight;

    lePanel.css('top', navBarHeight);
    lePanel.css('height', lePanelHeight);    

    var lePanelTop = $($('#le-panel-top')[0]);
    var tabContent = $($('.tab-content')[0]);
    var tabContentHeight = lePanelHeight - parseInt(lePanelTop.css('height'));
    
    tabContent.css('height', tabContentHeight);

    var tabContentHeightLessPadding = tabContentHeight - parseInt(tabContent.css('padding-top')) - parseInt(tabContent.css('padding-bottom'));
    var sectionContainers = $('.section-container');
    var tabHeaders = $('.tab-header');
    var tabHeaderHeight = $(tabHeaders[0]).css('height');

    for (var i = 0; i < sectionContainers.length; i++) {
        var sectionContainer = $(sectionContainers[i]);

        sectionContainer.css('height', tabContentHeightLessPadding - parseInt(tabHeaderHeight));
    }

}

$(window).resize(setLePanelDimensions);

var legisleditorVM = {
    entity: 'DiscussionVM',
    sectionEntity: 'SectionVM',
    sectionInputEntity: 'SectionIM',
    sectionEditInputEntity: 'SectionEditIM',
    sectionCommentInputEntity: 'SectionCommentIM',
    sectionExplanationInputEntity: 'SectionExplanationIM',
    sectionEvidenceInputEntity: 'SectionEvidenceIM',
    sectionVoteInputEntity: 'SectionVoteIM',
    canAddEntity: 'CanUserAddToOrgCat',
    entityId: getEntityId(),
    focusedSection: ko.observable(),
    hoveredSection: ko.observable(),
    discussion: ko.observable(),
    isAuthenticated: ko.observable(false),
    loading: ko.observable(false),
    canAdd: ko.observable(false),
    setup: function (options) {
        assignOptions(options, legisleditorVM);

        legisleditorVM.isAuthenticated(envocal.user != null && envocal.user.isAuthenticated);

        legisleditorVM.load();

    },
    load: function () {
        legisleditorVM.loading(true);

        $.when(legisleditorVM.refreshView())
            .then(function () {
                if (legisleditorVM.isAuthenticated()) {
                    if (legisleditorVM.discussion() != null && legisleditorVM.discussion().OrgCat_OrganisationCategoryId)
                        return $.when(ajaxOdataRequest(legisleditorVM.canAddEntity + '(orgCatId=' + legisleditorVM.discussion().OrgCat_OrganisationCategoryId + ')')
                                .success(function (data) {
                                    legisleditorVM.canAdd(data.value);
                                    legisleditorVM.loading(false);
                                }));
                }
            });

        //add global on click handler
        $(document).click(function (e) {
            //hide legisleditor panel if click is not one of: on le panel, on navbar, on a section.
            if (!($('#le-panel').has(e.target).length > 0 || $('.navbar-fixed-top').has(e.target).length > 0 || $(e.target).closest('.section').length > 0)) {                
                $("#le-panel").hide(200, "linear", "default");
            }
        });
    },
    refreshView: function () {
        var self = this;

        return ajaxOdataRequest(this.entity + '(' + this.entityId + ')', null, null, null, null, null, 'Section($expand=Children($expand=Children($expand=Children)))')
            .then(this.bind)
            .fail(odataErrorHandler)
            .promise();
    },
    bind: function (data) {
        if (data == null) return;
        legisleditorVM.discussion(morphLegisleditorDiscussion(data));
    },
    hideLePanel: function (){
        if ($("#le-panel").not(":hidden")) $("#le-panel").toggle(200, "linear", "default");
    },
    sectionClick: function (section) {
        if ($("#le-panel").is(":hidden")) $("#le-panel").toggle(200, "linear", "default");

        if (legisleditorVM.focusedSection() && legisleditorVM.focusedSection().SectionID == section.SectionID) return;

        if (legisleditorVM.focusedSection() != null) {
            legisleditorVM.focusedSection().IsFocused(false);
        }
        section.IsFocused(true);

        if (section.IsFetched) legisleditorVM.bindSection(section);
        else legisleditorVM.getAndBindSection(section);

            setTimeout(setLePanelDimensions, 300);
    },
    getAndBindSection: function (section) {
        $.when(ajaxOdataRequest(this.sectionEntity + '(' + section.SectionID + ')', null, null, null, null, null, 'Comments($expand=Comments),SectionEdits,SectionExplanations,SectionEvidences'))
            .then(function (fetchedSection) {
                legisleditorVM.bindSection(section, fetchedSection);
            });
    },
    bindSection: function (section, fetchedSection) {
        section = morphSection(section);

        if (fetchedSection) {
            $.each(fetchedSection.Comments, function(i, comm){
                section.Comments.push(morphSectionComment(comm));
            });
            $.each(fetchedSection.SectionEdits, function (i, edit) {
                section.SectionEdits.push(morphSectionEdit(edit));
            });
            $.each(fetchedSection.SectionExplanations, function (i, expl) {
                section.SectionExplanations.push(morphSectionExplanation(expl));
            });
            $.each(fetchedSection.SectionEvidences, function (i, evidence) {
                section.SectionEvidences.push(morphSectionEvidence(evidence));
            });
            section.IsFetched = true;
        }

        legisleditorVM.focusedSection(section);
    },
    displayAddSection: function(section){
        section.IsAdding(!section.IsAdding());
        section.AddHasFocus(true);
    },
    addSection: function(section) {
        if (section == null) return;
        if (section.AddSectionText() == null || section.AddSectionText() == '') {
            toastr.error('You must enter some text.');
            return;
        }
        
        var newSection = {
            'Text': section.AddSectionText(),
            'DateCreated': getCurrentDateTimeOffset(),
            'User_ID': envocal.user.id,
            'ParentId': section.SectionID,
            OrganisationCategoryId: legisleditorVM.discussion().OrgCat_OrganisationCategoryId
        };
        
        ajaxOdataPost(legisleditorVM.sectionInputEntity, newSection).success(
            function (resp) {
                section.Children.push(morphSection(morphNewSectionCommon(resp)));
                section.IsAdding(false);
                section.AddSectionText(null);
            }
        );
    },
    addSectionComment: function (section) {
        if (section == null) return;
        if (section.CommentText() == null || section.CommentText() == '') {
            toastr.error('You must enter some text.');
            return;
        }

        var comment = {
            'Text': section.CommentText(),
            'CreatedDate': getCurrentDateTimeOffset(),
            'SectionID': section.SectionID,
            'User_ID': envocal.user.id,
            OrganisationCategoryId: legisleditorVM.discussion().OrgCat_OrganisationCategoryId
        };

        ajaxOdataPost(legisleditorVM.sectionCommentInputEntity, comment).success(
            function (resp) {
                section.Comments.push(morphSectionComment(morphNewSectionCommon(resp)));
                section.CommentText(null)
            }
        );
    },
    addSectionEdit: function (section) {
        if (section == null) return;
        if (section.EditedText() == null || section.EditedText() == '' || section.EditedText() == section.Text) {
            toastr.error('You must enter a valid edit.');
            return;
        }

        var edit = {
            'Text': section.EditedText(),
            'DateCreated': getCurrentDateTimeOffset(),
            'SectionID': section.SectionID,
            'User_ID': envocal.user.id,
            OrganisationCategoryId: legisleditorVM.discussion().OrgCat_OrganisationCategoryId
        };

        var resp = ajaxOdataPost(legisleditorVM.sectionEditInputEntity, edit).success(
            function (resp) {
                section.SectionEdits.push(morphSectionEdit(morphNewSectionCommon(resp)));
                section.EditedText(null);
            }
        );
    },
    addSectionExplanation: function (section) {
        if (section == null) return;
        if (section.ExplanationText() == null || section.ExplanationText() == '' || section.ExplanationText() == section.Text) {
            toastr.error('You must enter some text.');
            return;
        }

        var explanation = {
            'Text': section.ExplanationText(),
            'DateCreated': getCurrentDateTimeOffset(),
            'SectionID': section.SectionID,
            'User_ID': envocal.user.id,
            OrganisationCategoryId: legisleditorVM.discussion().OrgCat_OrganisationCategoryId
        };

        var resp = ajaxOdataPost(legisleditorVM.sectionExplanationInputEntity, explanation).success(
            function (resp) {
                section.SectionExplanations.push(morphSectionExplanation(morphNewSectionCommon(resp)));
                section.ExplanationText(null);
            }
        );
    },
    addSectionEvidence: function (section) {
        if (section == null) return;
        if (section.EvidenceText() == null || section.EvidenceText() == '') {
            toastr.error('You must enter some text.');
            return;
        }

        var evidence = {
            'Text': section.EvidenceText(),
            'DateCreated': getCurrentDateTimeOffset(),
            'SectionID': section.SectionID,
            'User_ID': envocal.user.id,
            OrganisationCategoryId: legisleditorVM.discussion().OrgCat_OrganisationCategoryId
        };

        var resp = ajaxOdataPost(legisleditorVM.sectionEvidenceInputEntity, evidence).success(
            function (resp) {
                section.SectionEvidences.push(morphSectionEvidence(morphNewSectionCommon(resp)));
                section.EvidenceText(null);
            }
        );
    },
    sectionVoteUp: function (entity) {
        if (entity == null) return;

        var postEntity = getVotePostDataFromSectionEntity(entity);
        postEntity.IsPositive = true;
        
        var resp = ajaxOdataPost(legisleditorVM.sectionVoteInputEntity, postEntity).success(
            function (resp) {
                var voteValue = 1;

                if (resp.wasExistingVote) voteValue -= (resp.existingVoteWasPositive ? 1 : -1);

                entity.Positives(entity.Positives() + voteValue);
            }
        );
    },
    sectionVoteDown: function (entity) {
        if (entity == null) return;
        
        var postEntity = getVotePostDataFromSectionEntity(entity);
        postEntity.IsPositive = false;

        var resp = ajaxOdataPost(legisleditorVM.sectionVoteInputEntity, postEntity).success(
            function (resp) {
                var voteValue = 1;

                if (resp.wasExistingVote) voteValue += (resp.existingVoteWasPositive ? 1 : -1);

                entity.Negatives(entity.Negatives() + voteValue);
            }
        );
    }
};

var getVotePostDataFromSectionEntity = function (entity) {

    var sectionId = 0;
    if (typeof (entity.SectionID) !== 'undefined') sectionId = entity.SectionID;
    else if (typeof (entity.Section) !== 'undefined' && typeof (entity.Section.SectionID) !== 'undefined') sectionId = entity.Section.SectionID;

    return {
        'CreatedDate': getCurrentDateTimeOffset(),
        'SectionID': sectionId,
        'SectionEditID': typeof (entity.SectionEditId) == 'undefined' ? 0 : entity.SectionEditId,
        'SectionCommentID': typeof (entity.CommentID) == 'undefined' ? 0 : entity.CommentID,
        'SectionExplanationID': typeof (entity.SectionExplanationId) == 'undefined' ? 0 : entity.SectionExplanationId,
        'SectionEvidenceID': typeof (entity.SectionEvidenceId) == 'undefined' ? 0 : entity.SectionEvidenceId,
        'User_ID': envocal.user.id,
        OrganisationCategoryId: legisleditorVM.discussion().OrgCat_OrganisationCategoryId
    };
}

var morphLegisleditorDiscussion = function (discussion) {

    if (discussion == null) return discussion;

    discussion.Section = morphSection(discussion.Section);

    return morphDiscussion(discussion);
}

var morphSection = function (section) {

    if (section.IsMorphed) return section;

    section.EditedText = ko.observable(section.Text);
    section.ExplanationText = ko.observable();
    section.CommentText = ko.observable('');
    section.EvidenceText = ko.observable();
    section.IsFetched = false;
    section.SectionEdits = ko.observableArray();
    section.Comments = ko.observableArray();
    section.SectionExplanations = ko.observableArray();
    section.SectionEvidences = ko.observableArray();
    section.EditCount = ko.computed(function () { return section.SectionEdits().length; });
    section.CommentCount = ko.computed(function () { return section.Comments().length; });
    section.ExplanationCount = ko.computed(function () { return section.SectionExplanations().length; });
    section.EvidenceCount = ko.computed(function () { return section.SectionEvidences().length; });
    section.IsAdding = ko.observable(false);
    section.AddSectionText = ko.observable(null);
    section.AddHasFocus = ko.observable(false);
    section.IsFocused = ko.observable(false);
    
    if (section.Children != null && section.Children.length != 0)
        section.Children = ko.observableArray($.each(section.Children, function (i, child) {
            return morphSection(child);
        }));
    else section.Children = ko.observableArray([]);

    return morphSectionCommon(section);
}

var morphSectionComment = function (sectionComment) {

    if (!sectionComment || sectionComment.IsMorphed) return sectionComment;

    if (!sectionComment.Comments) sectionComment.Comments = [];
    else {
        var tmpComm = [];
        $.each(sectionComment.Comments, function (i, comment) {
            tmpComm.push(morphSectionComment(comment));
        });
        sectionComment.Comments = tmpComm;
    }

    return morphSectionCommon(sectionComment);
}

var morphSectionEdit = function (sectionEdit) {

    if (!sectionEdit || sectionEdit.IsMorphed) return sectionEdit;

    if (!sectionEdit.Comments) sectionEdit.Comments = [];
    else {
        var tmpComm = [];
        $.each(sectionEdit.Comments, function (i, comment) {
            tmpComm.push(morphSectionComment(comment));
        });
        sectionEdit.Comments = tmpComm;
    }

    return morphSectionCommon(sectionEdit);
}

var morphSectionExplanation = function (sectionExplanation) {

    if (!sectionExplanation || sectionExplanation.IsMorphed) return sectionExplanation;

    if (!sectionExplanation.Comments) sectionExplanation.Comments = [];
    else {
        var tmpComm = [];
        $.each(sectionExplanation.Comments, function (i, comment) {
            tmpComm.push(morphSectionComment(comment));
        });
        sectionExplanation.Comments = tmpComm;
    }

    return morphSectionCommon(sectionExplanation);
}

var morphSectionEvidence = function (sectionEvidence) {

    if (!sectionEvidence || sectionEvidence.IsMorphed) return sectionEvidence;

    if (!sectionEvidence.Comments) sectionEvidence.Comments = [];
    else {
        var tmpComm = [];
        $.each(sectionEvidence.Comments, function (i, comment) {
            tmpComm.push(morphSectionComment(comment));
        });
        sectionEvidence.Comments = tmpComm;
    }

    return morphSectionCommon(sectionEvidence);
}

var morphSectionCommon = function (sectionCommon) {
    sectionCommon.IsMorphed = true;
    sectionCommon.Positives = ko.observable(!sectionCommon.Positives ? 0 : sectionCommon.Positives);
    sectionCommon.Negatives = ko.observable(!sectionCommon.Negatives ? 0 : sectionCommon.Negatives);
    sectionCommon.Score = ko.computed(
        function () {
            return sectionCommon.Positives() - sectionCommon.Negatives();
        });

    return sectionCommon;
}

var morphNewSectionCommon = function (sectionCommon) {
    if (!sectionCommon.FirstName) sectionCommon.FirstName = envocal.user.firstName;
    if (!sectionCommon.Surname) sectionCommon.Surname = envocal.user.surname;
    if (!sectionCommon.Username) sectionCommon.Username = envocal.user.username;
    if (!sectionCommon.User_ID) sectionCommon.User_ID = envocal.user.id;
    return sectionCommon;
}