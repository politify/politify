﻿var organisationVM = {
    organisationEntity: 'OrganisationVM',
    discussionEntity: 'DiscussionVM',
    discussionInputEntity: 'DiscussionIM',
    canAddEntity: 'CanUserAddToOrg',
    organisation: ko.observable(),
    entityId: getEntityId(),
    isAuthenticated: ko.observable(false),
    canAdd: ko.observable(false),
    focusedCat: ko.observable(),
    loading: ko.observable(false),
    load: function () {
        organisationVM.isAuthenticated(envocal.user != null && envocal.user.isAuthenticated);


        organisationVM.loading(true);

        $.when(this.refreshView())
            .then(function () {
                if (organisationVM.organisation().Categories() != null && organisationVM.organisation().Categories().length > 0)
                    organisationVM.categoryClick(organisationVM.organisation().Categories()[0]);

                if (organisationVM.isAuthenticated()) {
                    if (organisationVM.organisation() != null && organisationVM.organisation().OrganisationId)
                        return $.when(ajaxOdataRequest(organisationVM.canAddEntity + '(orgId=' + organisationVM.organisation().OrganisationId + ')')
                                .success(function (data) {
                                    organisationVM.canAdd(data.value);
                                    organisationVM.loading(false);
                                }));
                }
            });
    },
    refreshView: function () {
        var self = this;
        //entity, options, orderby, top, skip, filter, expand, select
        return ajaxOdataRequest(this.organisationEntity + '(' + this.entityId + ')', null, null, null, null, null, 'Children,SignatureCounts,Staff,Parents,Categories')
            .then(this.bindOrganisation)
            .then(initialiseMenus)
            .fail(odataErrorHandler)
            .promise();
    },
    bindOrganisation: function (data) {
        if (data == null) return;

        organisationVM.organisation(morphOrganisation(data));
    },
    addDiscussion: function (category) {
        if (!category) return;
        if(!category.CreateDiscussionText() || category.CreateDiscussionText().length == 0) {
            toastr.error('You must enter valid text.');
            return;
        }

        var date = getCurrentDateTimeOffset();

        var discussion = {
            Text: category.CreateDiscussionText(),
            Html: null,
            OrderQualifier: '1',
            User_Id: envocal.user.id,
            DateCreated: date,
            OrganisationCategoryId: category.OrganisationCategoryId
        };

        ajaxOdataPost(organisationVM.discussionInputEntity, discussion).success(
            function (resp) {
                resp.User = envocal.functions.getCurrUserObj();
                category.Discussions.unshift(morphDiscussion(resp));
                category.CreateDiscussionText(null);
            }
        );
    },
    categoryClick: function (cat) {
        if (organisationVM.focusedCat() != null) organisationVM.focusedCat().IsActive(false);
        cat.IsActive(true);

        organisationVM.categoryActivate(cat);

        organisationVM.focusedCat(cat);
    },
    categoryActivate: function (cat) {

        if (cat.Loaded()) return;

        cat.loading(true);

        $.when(ajaxOdataRequest(organisationVM.discussionEntity, 'DateCreated desc', null, null, null, 'OrgCat_OrganisationCategoryId eq ' + cat.OrganisationCategoryId, 'User,Section'))
            .then(function (data) {
                cat.Discussions($.each(data.value, function (i, d) {
                    return morphDiscussion(d);
                }));
                cat.DiscussionCount(data.value.length);
                cat.loading(false);
            });

        cat.Loaded(true);
    },
    discussionVote: function (entity) {
        if (entity == null) return;

        var postEntity = getVotePostDataFromDiscussion(entity);
        postEntity.IsPositive = true;

        var resp = ajaxOdataPost(legisleditorVM.sectionVoteInputEntity, postEntity).success(
            function (resp) {
                var voteValue = 1;

                if (resp.wasExistingVote) voteValue -= (resp.existingVoteWasPositive ? 1 : -1);

                entity.Positives(entity.Positives() + voteValue);
            }
        );
    },
    discussionVoteDown: function (entity) {
        if (entity == null) return;

        var postEntity = getVotePostDataFromDiscussion(entity);
        postEntity.IsPositive = false;

        var resp = ajaxOdataPost(legisleditorVM.sectionVoteInputEntity, postEntity).success(
            function (resp) {
                var voteValue = 1;

                if (resp.wasExistingVote) voteValue += (resp.existingVoteWasPositive ? 1 : -1);

                entity.Negatives(entity.Negatives() + voteValue);
            }
        );
    }
};

var getVotePostDataFromDiscussion = function (entity) {

    var sectionId = 0;
    if (typeof (entity.SectionID) !== 'undefined' && entity.SectionID != null) sectionId = entity.SectionID;
    else if (typeof (entity.Section) !== 'undefined' && typeof (entity.Section.SectionID) !== 'undefined' && entity.Section.SectionID != null)
        sectionId = entity.Section.SectionID;
    else if (typeof (entity.Section_SectionID) !== 'undefined' && entity.Section_SectionID != null)
        sectionId = entity.Section_SectionID;

    return {
        'CreatedDate': getCurrentDateTimeOffset(),
        'SectionID': sectionId,
        'SectionEditID': typeof (entity.SectionEditId) == 'undefined' ? 0 : entity.SectionEditId,
        'SectionCommentID': typeof (entity.CommentID) == 'undefined' ? 0 : entity.CommentID,
        'SectionExplanationID': typeof (entity.SectionExplanationId) == 'undefined' ? 0 : entity.SectionExplanationId,
        'SectionEvidenceID': typeof (entity.SectionEvidenceId) == 'undefined' ? 0 : entity.SectionEvidenceId,
        'User_ID': envocal.user.id,
        OrganisationCategoryId: entity.OrgCat_OrganisationCategoryId
    };
}

var morphOrganisation = function (o) {
    if (o == null) return;

    o.hasEmailAddress = ko.observable(o.EmailAddress != null);
    o.hasPicture = ko.computed(function () {
        return this.ProfilePicture != null;
    }, o);

    if (typeof o.Parents !== 'undefined')
        o.Parents = ko.observableArray($.each(o.Parents, function (i, parent) {
            return morphOrganisation(parent);
        }));
    else o.Parents = null;

    if (typeof o.Children !== 'undefined')
        o.Children = ko.observableArray($.each(o.Children, function (i, child) {
            return morphOrganisation(child);
        }));

    if (typeof o.Categories !== 'undefined')
        o.Categories = ko.observableArray($.each(o.Categories, function (i, cat) {
            return morphCategory(cat);
        }));

    o.Link = virtualPath + 'Organisation/Details/' + o.OrganisationId;

    return o;
}

var morphCategory = function (c) {
    if (c == null) return;

    c.ElementId = c.Title.replace(/[^a-z1-9A-z]+/g, '');
    c.CreateDiscussionText = ko.observable();
    c.IsActive = ko.observable(false);
    c.Loaded = ko.observable(false);
    c.loading = ko.observable(false);
    c.Discussions = ko.observableArray();
    c.DiscussionCount = ko.observable(0);

    return c;
}

var morphDiscussion = function (d) {
    if (d == null) return;

    d.Link = (d.DiscussionId != null ? virtualPath + 'Discussion/Details/' + d.DiscussionId : null);
    d.User = morphUser(d.User);
    d.Positives = ko.observable(d.Positives);
    d.Negatives = ko.observable(d.Negatives);
    d.Score = ko.computed(function () {
        return d.Positives() - d.Negatives();
    });

    return d;
}

var morphUser = function (u) {
    if (u == null) return;

    u.Link = virtualPath + 'Account/UserProfile/' + u.Id;
    u.DisplayName = u.FirstName + ' ' + u.Surname;

    return u;
}

var initialiseMenus = function () {
    $('.auto-menu').each(function () {
        // For each set of menu, we want to keep track of
        // which tab is active and it's associated content
        var $active, $content, $links = $(this).find('a');

        if ($links.length == 0) return;

        // If the location.hash matches one of the links, use that as the active tab.
        // If no match is found, use the first link as the initial active tab.
        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
        $active.addClass('active');

        $content = $($active[0].hash);

        // Hide the remaining content
        $links.not($active).each(function () {
            $(this.hash).hide();
        });

        // Bind the click event handler
        $(this).on('click', 'a', function (e) {
            // Make the old tab inactive.
            $active.removeClass('active');
            $content.hide();

            // Update the variables with the new link and content
            $active = $(this);
            $content = $(this.hash);

            // Make the tab active.
            $active.addClass('active');
            $content.show();

            // Prevent the anchor's default click action
            e.preventDefault();

        });
    });
};