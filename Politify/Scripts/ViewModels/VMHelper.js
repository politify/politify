﻿var apiPath = window.location.origin + virtualPath + 'odata/';

var ajaxOdataRequest = function (entity, options, orderby, top, skip, filter, expand, select) {

    var orderByQuery = (orderby == null ? '' : '$orderby=' + orderby);
    var topQuery = (top == null ? '' : '&$top=' + top);
    var skipQuery = (skip == null ? '' : '&$skip=' + skip);
    var filterQuery = (filter == null ? '' : '&$filter=' + filter);
    var expandQuery = (expand == null ? '' : '&$expand=' + expand);
    var selectQuery = (select == null ? '' : '&$select=' + select);
    var url = apiPath + entity + '?' + orderByQuery + topQuery + skipQuery + filterQuery + expandQuery + selectQuery;

    return $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: ko.toJSON(options),
        contentType: 'application/json',
        success: function (result) {
            return result;
        },
        error: function (err) {
            toastr.error("We're sorry, something has gone wrong.");
            return err;
        }
    });
}

var ajaxOdataPost = function (entity, data) {

    var url = apiPath + entity;

    return $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: ko.toJSON(data),
        contentType: 'application/json',
        success: function (result) {
            return result;
        },
        error: function (err) {
            toastr.error("We're sorry, something has gone wrong.");
            return err;
        }
    });
}

var odataErrorHandler = function (e) {
    var errorMsg;
    try {
        errorMsg = e.responseJSON['odata.error'].message.value;
    } catch (e) { errorMsg = e; }

    console.log('odata error:', errorMsg);
};

var getEntityId = function (pathDepth) {

    var pathSegments = cleanArray(window.location.pathname.split('/'));
    if (pathDepth == null) {

        return pathSegments.pop();
    }
    else {
        if (pathSegments.length < pathDepth) return null;

        return pathSegments[pathDepth - 1];
    }
}

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}

function assignOptions(o, vm) {
    if (o == null || vm == null) return;

    for (var name in o) {
        vm[name] = o[name];
    }

    return;
}