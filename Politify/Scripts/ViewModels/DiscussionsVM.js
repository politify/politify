﻿var discussionsVM = {
    discussionEntity: 'DiscussionVM',
    isAuthenticated: ko.observable(false),
    discussions: ko.observableArray(),
    count: 50,
    loading: ko.observable(false),
    searchText: ko.observable(),
    orderBy: ko.observable('DateCreated desc'),
    orderByOptions: [{ name: 'Newest', value: 'DateCreated desc'}, { name: 'Best', value: 'Positives desc'} ],
    setup: function(options) {
        assignOptions(options, discussionsVM);

        discussionsVM.isAuthenticated(envocal.user != null && envocal.user.isAuthenticated);

        discussionsVM.load();
        discussionsVM.filter.subscribe(discussionsVM.load);
        discussionsVM.orderBy.subscribe(discussionsVM.load);
    },
    load: function(){
        discussionsVM.loading(true);

        $.when(discussionsVM.refreshView())
            .then(function () {
                discussionsVM.loading(false);
            });
    },
    refreshView: function () {
        //entity, options, orderby, top, skip, filter, expand, select
        return ajaxOdataRequest(discussionsVM.discussionEntity, null, discussionsVM.orderBy(), this.count, null, discussionsVM.filter(), 'User,OrgCat($expand=Organisation)')
            .then(this.bindDiscussions)
            .fail(odataErrorHandler)
            .promise();
    },
    bindDiscussions: function (data) {
        if (data == null) return;

        discussionsVM.discussions(morphDiscussions(data.value));
    }
}

discussionsVM.filter = ko.computed(function () {
    var filterTerms = [];

    if (discussionsVM.searchText() != null) filterTerms.push("contains(Text, '" + discussionsVM.searchText() + "')");

    if (filterTerms.length == 0) return null;

    return filterTerms.join(' and ');
});

var morphDiscussions = function (orgs) {

    return $.each(orgs, function (i, org) {
        return morphDiscussion(org);
    });
};