﻿using System.Web;
using System.Web.Optimization;

namespace Politify
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/Lib/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/Lib/jquery.validate*",
                "~/Scripts/site-validator.js"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/Lib/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                "~/Scripts/Lib/bootstrap.js"
                , "~/Scripts/Lib/bootstrap-datepicker.js"
                , "~/Scripts/Lib/respond.js"
                , "~/Scripts/Lib/jquery.timeago.js"
                , "~/Scripts/Lib/knockout-3.2.0.js"
                , "~/Scripts/Lib/toastr.js"
                , "~/Scripts/site.js"
                , "~/Scripts/ko-custom-bindings.js"));

            bundles.Add(new ScriptBundle("~/bundles/viewModels").Include(
                "~/Scripts/ViewModels/VMHelper.js"
                , "~/Scripts/ViewModels/OrganisationVM.js"
                , "~/Scripts/ViewModels/legisleditorVM.js"
                , "~/Scripts/ViewModels/UserProfileVM.js"
                , "~/Scripts/ViewModels/OrganisationsVM.js"
                , "~/Scripts/ViewModels/DiscussionsVM.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css"
                , "~/Content/bootstrap-datepicker.css"
                , "~/Content/bootstrap-datepicker3.css"
                , "~/Content/site.css"
                , "~/Content/legistyle.css"
                , "~/Content/organisation.css"
                , "~/Content/toastr.css"));
            
#if DEBUG
            BundleTable.EnableOptimizations = false;
#endif
        }
    }
}
