﻿using Microsoft.AspNet.Identity.EntityFramework;
using Politify.Models;
using Politify.Models.Comments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Query;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;

namespace Politify
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            ODataModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<DiscussionVM>("DiscussionVM");
            builder.EntitySet<OrganisationVM>("OrganisationVM");
            builder.EntitySet<SectionVM>("SectionVM");
            builder.EntitySet<ApplicationUser>("Identity");
            builder.EntitySet<IdentityUserClaim>("IdentityUserClaims");
            builder.EntitySet<Postcode>("Postcodes");

            builder.EntitySet<DiscussionIM>("DiscussionIM");
            builder.EntitySet<SectionIM>("SectionIM");
            builder.EntitySet<SectionCommentIM>("SectionCommentIM");
            builder.EntitySet<SectionEditIM>("SectionEditIM");
            builder.EntitySet<SectionExplanationIM>("SectionExplanationIM");
            builder.EntitySet<SectionEvidenceIM>("SectionEvidenceIM");
            builder.EntitySet<SectionVoteIM>("SectionVoteIM");

            builder.Function("CanUserAddToOrg").Returns<bool>().Parameter<int>("orgId");
            builder.Function("CanUserAddToOrgCat").Returns<bool>().Parameter<int>("orgCatId");
            builder.Function("UserLocationOrgs").ReturnsCollectionFromEntitySet<OrganisationVM>("OrganisationVM").Parameter<string>("userId");
            builder.Function("UserWorkLocationOrgs").ReturnsCollectionFromEntitySet<OrganisationVM>("OrganisationVM").Parameter<string>("userId");
            
            builder.EntityType<ApplicationUser>().Ignore<DateTime?>(u => u.LockoutEndDateUtc);
            builder.EntityType<ApplicationUser>().Ignore<String>(u => u.Email);
            builder.EntityType<ApplicationUser>().Ignore<Boolean>(u => u.EmailConfirmed);
            builder.EntityType<ApplicationUser>().Ignore<String>(u => u.PasswordHash);
            builder.EntityType<ApplicationUser>().Ignore<String>(u => u.SecurityStamp);
            builder.EntityType<ApplicationUser>().Ignore<String>(u => u.PhoneNumber);
            builder.EntityType<ApplicationUser>().Ignore<Boolean>(u => u.PhoneNumberConfirmed);
            builder.EntityType<ApplicationUser>().Ignore<Boolean>(u => u.TwoFactorEnabled);
            builder.EntityType<ApplicationUser>().Ignore<Boolean>(u => u.LockoutEnabled);
            builder.EntityType<ApplicationUser>().Ignore<Int32>(u => u.AccessFailedCount);
            builder.EntityType<ApplicationUser>().Ignore<ICollection<IdentityUserLogin>>(u => u.Logins);
            builder.EntityType<ApplicationUser>().Ignore<Postcode>(u => u.Postcode);
            builder.EntityType<ApplicationUser>().Ignore<Postcode>(u => u.WorkPostcode);

            config.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
    }
}
