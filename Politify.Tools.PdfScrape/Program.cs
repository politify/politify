﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IFilter;

namespace Politify.Tools.PdfScrape
{
    class Program
    {
        static void Main(string[] args)
        {
            // remember to rename the output exe to "filtdump.exe" to get access to the Adobe PDF IFilter 
            // installed with Acrobat Reader

            // read more: http://www.squarepdf.net/parsing-pdf-files-using-ifilter-c-net

            string path = @"E:\politify\Politify.Tools.PdfScrape\AppData\ukpga_19500037_en.pdf";
            Console.WriteLine("Parsing " + path);
            
            var pages = DefaultParser.ExtractPages(path);

            pages.ForEach(Console.WriteLine);
        }

        public static string ExtractTextFromPdf(string path)
        {
            return DefaultParser.Extract(path);
        }
    }
}
